
/* tree.cc */

#include "tree.h"

/* ---------------------------------------------------------------------- */

Tree::Tree (QWidget * parent) :
   QTreeWidget (parent)
{
}

/* ---------------------------------------------------------------------- */

TreeNode::TreeNode (QTreeWidget * parent, QString text) :
   QTreeWidgetItem (parent),
   src_file (0),
   src_line (0),
   src_col (0),
   src_obj (NULL),
   src_gra (NULL)
{
   if (text != "")
      setText (0, text);
}

TreeNode::TreeNode (QTreeWidgetItem * parent, QString text) :
   QTreeWidgetItem (parent),
   src_file (0),
   src_line (0),
   src_col (0),
   src_obj (NULL),
   src_gra (NULL)
{
   if (text != "")
      setText (0, text);
}
