
/* view.cc */

#include "view.h"

#ifdef AREA
   #include "area.h"
#endif

#ifdef BUILDER
   #include "builder.h"
#endif

#ifdef CKIT
   #include "ckit_parser.h"
   #include "ckit_product.h"
#endif

#ifdef CLANG
   #include "clang-view.h"
   // #include "clang-print.h"
   #include "compiler.h"
   #include "fibonacci.h"
#endif

#ifdef DESIGN
   #include "design.h"
#endif

#ifdef HTML
   #include "html.h"
#endif

#ifdef UI
   #include "ui-view.h"
#endif

#ifdef TRACE
   #include "trace.h"
#endif

#include <QApplication>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>

#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QHeaderView>
#include <QStatusBar>
#include <QObject>

#ifdef CHART
   #include "chart.h"
#endif

#ifdef VISUAL
   #include "visualization.h"
#endif

#ifdef DB
   #include "db.h"
#endif

#ifdef JS
   #include "js.h"
#endif

#ifdef SOCKET
   #include "socket.h"
#endif

#ifdef DBUS
   #include "dbus.h"
#endif

#ifdef SVG_VIEW
   #include "svg.h"
#endif

/* ---------------------------------------------------------------------- */

void Window::addMenuItem (QMenu * menu, QString title, QObject * obj, const char * slot, const char * shortcut)
{
   QAction * act = new QAction (title, menu);
   if (shortcut != NULL)
      act->setShortcut (QKeySequence (shortcut));
   QObject::connect (act, SIGNAL(triggered ()), obj, slot);
   menu->addAction (act);
}

void Window::addView (QString title, QWidget * widget)
{
    tabs->addTab (widget, title);
}

/* ---------------------------------------------------------------------- */

Window::Window (QWidget * parent) :
   QMainWindow (parent)
{
   fileMenu = menuBar()->addMenu ("&File");

   addMenuItem (fileMenu, "&Open ...", this, SLOT(openFile ()), "Ctrl+O");

   #ifdef CKIT
      addMenuItem (fileMenu, "&Run C--", this, SLOT(runCmm ()), "Ctrl+R");
   #endif

   #ifdef UI
      addMenuItem (fileMenu, "&Load UI", this, SLOT(openUI ()), "Ctrl+U");
   #endif

   addMenuItem (fileMenu, "&Quit", this, SLOT(quit ()), "Ctrl+Q");

   viewMenu = menuBar()->addMenu ("&View");

   #ifdef CLANG
      addMenuItem (viewMenu, "&CLang", this, SLOT(showCLang ()));
   #endif

   #ifdef DESIGN
      addMenuItem (viewMenu, "&Designer", this, SLOT(showDesigner ()));
   #endif

   tree = new QTreeWidget (this);
   tree->header()->hide ();

   tabs = new QTabWidget (this);

   prop = new PropertyTable (this);

   empty_edit = new QTextEdit (this);
   tabs->addTab (empty_edit, "");

   info = new QTextEdit (this);

   hsplitter = new QSplitter (this);
   hsplitter->addWidget (tree);
   hsplitter->addWidget (tabs);
   hsplitter->addWidget (prop);


   vsplitter = new QSplitter (this);
   vsplitter->setOrientation (Qt::Vertical);
   vsplitter->addWidget (hsplitter);
   vsplitter->addWidget (info);

   setCentralWidget (vsplitter);

   hsplitter->setStretchFactor (0, 1);
   hsplitter->setStretchFactor (1, 4);

   vsplitter->setStretchFactor (0, 4);
   vsplitter->setStretchFactor (1, 1);

   #ifdef AREA
      AreaWidget * area = new AreaWidget (this);
      QGraphicsScene * scene = area->getScene ();
      tabs->addTab (area, "Area");
   #endif

   #ifdef BUILDER
      tabs->addTab (new BuilderWidget (this), "Builder");
   #endif

   #ifdef HTML
      HtmlView * w = new HtmlView (this, statusBar());
      addView ("Html", w);
      w->load (QUrl ("http://doc.qt.io/qt-4.8/index.html"));
   #endif

   #ifdef JS
      JsEdit * e = new JsEdit (this, tree, info);
      addView ("Java Script", e);
      // addMenuItem (viewMenu, "Execute JS", e, SLOT (executeJS ()));
   #endif

   #ifdef DBUS
      new Receiver (this, info);
   #endif

   #ifdef SOCKET
      new SocketReceiver (this, 12345, info);
      #ifdef HTML
         HtmlView * w2 = new HtmlView (this, statusBar());
         addView ("Socket", w2);
         w2->load (QUrl ("http://localhost:12345"));
      #endif
   #endif

   #ifdef CHART
      #ifdef AREA
         addView ("Chart", chartExample (scene));
         addView ("Pie Chart", pieChartExample (scene));
      #endif
   #endif

   #ifdef VISUAL
      addView  ("Bar 3D", dataVisualizationExample ());
   #endif

   #ifdef DB
      #ifdef AREA
         addView ("Database", new DbView (this, scene, tree, info));
      #endif
  #endif

   #ifdef SVG_VIEW
      addView ("SVG", new SvgView (this));
   #endif

   // loadFile (":/data/abc.xml");

   setMinimumSize (640, 480);
   statusBar()->showMessage (QString ("Qt ") + qVersion());
}

Window::~Window()
{
}

void Window::removeEmptyEditor ()
{
    int inx = tabs->indexOf (empty_edit);
    if (inx >= 0)
       tabs->removeTab (inx);
}
/* ---------------------------------------------------------------------- */

void Window::readFile (Edit * edit, QString fileName)
{
   QFile f (fileName);
   if (f.open (QFile::ReadOnly))
   {
      QByteArray code = f.readAll ();
      edit->setPlainText (code);
   }
   else
   {
      QMessageBox::warning (NULL, "Open File Error", "Cannot read file: " + fileName);
   }
}


Edit * Window::openEditor (QString fileName)
{
   Edit * edit = NULL;
   QFileInfo fi (fileName);
   fileName = fi.absoluteFilePath();
   if (sourceEditors.contains (fileName))
   {
      edit = sourceEditors [fileName];
   }
   else
   {
      edit = new Edit (this);
      edit->setToolTip (fileName);
      tabs->addTab (edit, fi.fileName ());
      sourceEditors [fileName] = edit;
      readFile (edit, fileName);
   }
   return edit;
}

void Window::openFile ()
{
   QString fileName = QFileDialog::getOpenFileName (this, "Open file");
   if (fileName != "")
      openEditor (fileName);
}

Edit * Window::getEditor ()
{
   Edit * edit = dynamic_cast < Edit * > (tabs->currentWidget ());
   return edit;
}

QString Window::getEditorFileName (Edit * edit)
{
   QString result = "";
   if (edit != NULL)
      result = edit->toolTip ();
   return result;
}

void selectLine (Edit * edit, int line, int column = 0)
{
   if (edit != NULL && line > 0)
   {
       QTextCursor cursor = edit->textCursor ();

       cursor.movePosition (QTextCursor::Start);
       cursor.movePosition (QTextCursor::QTextCursor::Down, QTextCursor::MoveAnchor, line-1);

       if (column > 0)
       {
          cursor.movePosition (QTextCursor::QTextCursor::Right, QTextCursor::MoveAnchor, column-1);

          // select to the end of line
          cursor.movePosition (QTextCursor::QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
       }
       else
       {
          // select line
          cursor.movePosition (QTextCursor::QTextCursor::Down, QTextCursor::KeepAnchor, 1);
       }

       edit->setTextCursor (cursor);
       edit->ensureCursorVisible ();
   }
}

/* ---------------------------------------------------------------------- */

#ifdef CKIT

void display_declarations (QTreeWidgetItem * target, CmmStatSect * decl_list);

inline QString str (string s) { return QString::fromStdString (s); }
inline QString num (int n) { return QString::number (n); }

string get_name (CmmName * name)
{
    string result = "";
    if (name->kind == simpleName)
    {
        CmmSimpleName * simple = name->conv_CmmSimpleName ();
        result = simple->id;
    }
    else
    {
        result = "???";
    }
    return result;
}

string get_declarator_name (CmmDeclarator * decl)
{
    string result = "";
    if (decl->kind == basicDeclarator)
    {
        CmmBasicDeclarator * basic = decl->conv_CmmBasicDeclarator ();
        result = get_name (basic->qual_name);
    }
    else
    {
        result = "???";
    }
    return result;
}

void display_simple_item (QTreeWidgetItem * target, CmmSimpleDecl * simple, CmmSimpleItem * item)
{
   string name = get_declarator_name (item->decl);
   TreeNode * node = new TreeNode (target, str (name));
}

void display_declaration (QTreeWidgetItem * target, CmmStat * decl)
{
   // new TreeNode (target, "declaration");
   if (CmmNamespaceDecl * ns = decl->conv_CmmNamespaceDecl ())
   {
      TreeNode * node = new TreeNode (target, str (ns->simp_name->id));
      if (ns->body != NULL)
         display_declarations (node, ns->body);
   }
   else if (CmmClassDecl * cls = decl->conv_CmmClassDecl ())
   {
      TreeNode * node = new TreeNode (target, str (cls->simp_name->id));
      if (cls->members != NULL)
         display_declarations (node, cls->members);
   }
   else if (CmmEnumDecl * type = decl->conv_CmmEnumDecl ())
   {
      TreeNode * node = new TreeNode (target, str (type->simp_name->id));
      if (type->enum_items != NULL)
         for (CmmEnumItem * item : type->enum_items->items)
         {
             new TreeNode (node, str (item->simp_name->id));
         }
   }
   else if (CmmSimpleDecl * simple = decl->conv_CmmSimpleDecl ())
   {
      for (CmmSimpleItem * item : simple->items)
          display_simple_item (target, simple, item);
   }

}

void display_declarations (QTreeWidgetItem * target, CmmStatSect * decl_list)
{
   // new TreeNode (target, "declarations");
   for (CmmStat * decl : decl_list->items)
   {
      display_declaration (target, decl);
   }
}

void Window::runCmm ()
{
   Edit * edit = getEditor ();
   if (edit != NULL)
   {
      string fileName = getEditorFileName (edit).toStdString ();
      // info->append ("fileName " + str (fileName));

      try
      {
         Parser parser;
         parser.open (fileName);

         // parser.NextToken ();
         // parser.parse_declaration ();
         CmmStatSect * decl_list = parser.parse_declaration_list ();

         /*
         while (! parser.isEof ())
         {
            info->append ("token " + str (parser.tokenText) + ", " + str (parser.tokenToString (parser.token)));
            parser.nextToken ();
         }
         */

         parser.close ();

         display_declarations (tree->invisibleRootItem (), decl_list);
         tree->expandAll ();
      }
      catch (LexerException & e)
      {
          info->append (str (e.filename) + ":" + num (e.line) + ":" + num (e.column) + ": error: " + str (e.msg));
          selectLine (edit, e.line);
      }
   }
}

#else
void Window::runCmm ()
{
}
#endif

/* ---------------------------------------------------------------------- */

void Window::showCLang ()
{
   #ifdef CLANG
      scanSource (tree, "../../../ext/llvm-dump/test-simple.cc");

      compileFile ("../../../ext/llvm-dump/test-simple.cc");

      // fibonacci (8);
   #endif
}

/* ---------------------------------------------------------------------- */

void Window::showDesigner ()
{
   #ifdef DESIGN
      Designer * w = new Designer ();
      w->show ();
      w->design->open ("../../qdesigner/example.ui");
   #endif
}

/* ---------------------------------------------------------------------- */

void Window::loadUI (QString fileName)
{
#ifdef UI
    QFileInfo fi (fileName);
    fileName = fi.absoluteFilePath ();

    QWidget * form = load_form (fileName);
    addView ("UI " + fi.fileName (), form);

    // display_form (tree, form);

    openEditor (fileName);
#endif
}

void Window::openUI ()
{
#ifdef UI
   QString fileName = QFileDialog::getOpenFileName (this, "Open UI file", QString (), "UI files (*.ui)");
   if (fileName != "")
      loadUI (fileName);
#endif
}

void Window::executeJS ()
{
#ifdef JS
#endif
}

void Window::quit ()
{
   close ();
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * argv [])
{
    #ifdef TRACE
    set_error_handlers ();
    #endif

    #ifdef CLANG
    // global_envp = envp;
    #endif

    QApplication a (argc, argv);
    Window w;
    w.show ();
    w.openEditor ("../qview/qcode/input/example.t");
    w.removeEmptyEditor ();
    w.runCmm ();

    #ifdef UI
    w.loadUI ("../../qcode/example.ui");
    #endif

    return a.exec ();
}
