
/* code-table.cc */

#include "code-table.h"

#include <QDialog>
#include <QDataWidgetMapper>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include <QLineEdit>
#include <QCheckBox>
#include <QSpinBox>
#include <QComboBox>
#include <QPushButton>

/* ---------------------------------------------------------------------- */

const int name_col = 0;
const int mode_col = 1;

const int col_count = 2;

/* ---------------------------------------------------------------------- */

DataTable::DataTable (Scope * root, QWidget * parent) :
   QTableView (parent),
   collection (root),
   model (NULL)
{
   model = new TableModel (collection, this);
   setModel (model);

   modeList << "" << "namespace" << "class" << "variable" << "function";

   ComboBoxDelegate * delegate = new ComboBoxDelegate (modeList, this);
   setItemDelegateForColumn (mode_col, delegate);

   connect (this, SIGNAL (doubleClicked (const QModelIndex &)), this, SLOT(onClick (const QModelIndex &)));
}

void DataTable::onClick (const QModelIndex & index)
{
   int line = index.row ();
   Item * item = collection->item_list [line];

   QDialog * dlg = new QDialog (this);
   dlg->setWindowTitle ("Edit Record");

   QVBoxLayout * layout = new QVBoxLayout (dlg);
   dlg->setLayout (layout);

   QDataWidgetMapper * mapper = new QDataWidgetMapper (this);
   mapper->setModel (model);
   mapper->setSubmitPolicy (QDataWidgetMapper::AutoSubmit);

   QLineEdit * name_edit = new QLineEdit (dlg);
   mapper->addMapping (name_edit, name_col);
   layout->addWidget (name_edit);

   QComboBox * mode_edit = new QComboBox (dlg);
   for (int i = 0; i < modeList.count (); i++)
      mode_edit->addItem (modeList [i]);
   mapper->addMapping (mode_edit, mode_col, "currentIndex");
   layout->addWidget (mode_edit);

   /*
   QCheckBox * flag_edit = new QCheckBox (dlg);
   mapper->addMapping (flag_edit, flag_col);
   layout->addWidget (flag_edit);

   QSpinBox * number_edit = new QSpinBox (dlg);
   mapper->addMapping (number_edit, number_col);
   layout->addWidget (number_edit);

   QDoubleSpinBox * real_edit = new QDoubleSpinBox (dlg);
   mapper->addMapping (real_edit, real_col);
   layout->addWidget (real_edit);
   */

   QDialogButtonBox * buttonBox = new QDialogButtonBox (QDialogButtonBox::Apply | QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
   connect (buttonBox, SIGNAL (accepted ()), dlg, SLOT (accept ()));
   connect (buttonBox, SIGNAL (rejected ()), dlg, SLOT (reject ()));

   QPushButton * firstButton = buttonBox->addButton ("First", QDialogButtonBox::ActionRole);
   QPushButton * prevButton  = buttonBox->addButton ("Prev", QDialogButtonBox::ActionRole);
   QPushButton * nextButton  = buttonBox->addButton ("Next", QDialogButtonBox::ActionRole);
   QPushButton * lastButton  = buttonBox->addButton ("Last", QDialogButtonBox::ActionRole);

   connect (firstButton, SIGNAL (clicked ()), mapper, SLOT (toFirst ()));
   connect (prevButton, SIGNAL (clicked ()), mapper, SLOT (toPrevious ()));
   connect (nextButton, SIGNAL (clicked ()), mapper, SLOT (toNext ()));
   connect (lastButton, SIGNAL (clicked ()), mapper, SLOT (toLast ()));

   QPushButton * applyButton = buttonBox->button (QDialogButtonBox::Apply);
   connect (applyButton, SIGNAL (clicked ()), mapper, SLOT (submit ()));

   layout->addWidget (buttonBox);
   mapper->setCurrentIndex (line);
   dlg->exec ();
}

/* ---------------------------------------------------------------------- */

TableModel::TableModel (Scope * root, QObject * parent) :
   QAbstractTableModel (parent),
   collection (root)
{
}

/* ---------------------------------------------------------------------- */

int TableModel::rowCount (const QModelIndex & parent) const
{
    return collection->item_list.count ();
}

int TableModel::columnCount (const QModelIndex & parent) const
{
   return col_count;
}

QVariant TableModel::headerData (int section, Qt::Orientation orientation, int role) const
{
   QVariant result;

   if (role == Qt::DisplayRole)
   {
      if (orientation == Qt::Horizontal)
      {
          if (section == name_col)
             result = "name";
          else if (section == mode_col)
             result = "mode";
      }
   }

   return result;
}

/* ---------------------------------------------------------------------- */

QVariant TableModel::data (const QModelIndex & index, int role) const
{
   QVariant result;

   int line = index.row();
   int col = index.column();
   Item * item = collection->item_list [line];

   if (role == Qt::DisplayRole || role == Qt::EditRole )
   {

      if (col == name_col)
         result = item->name;
      else if (col == mode_col)
         result = 3; // !?
   }
   else if (role == Qt::CheckStateRole)
   {
   }

   return result;
}

bool TableModel::setData (const QModelIndex & index, const QVariant & value, int role)
{
    bool result = false;
    if (role == Qt::EditRole)
    {
        int line = index.row();
        int col = index.column();
        Item * item = collection->item_list [line];

        if (col == name_col)
           item->name = value.toString();

         result = true;
    }
    return result;
}

Qt::ItemFlags TableModel::flags(const QModelIndex & /*index*/) const
{
    return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
}

/* ---------------------------------------------------------------------- */

ComboBoxDelegate::ComboBoxDelegate (QStringList p_item_list, QObject * parent) :
   QStyledItemDelegate (parent)
{
   item_list = p_item_list;
}

QWidget *ComboBoxDelegate::createEditor(QWidget * parent,
                                        const QStyleOptionViewItem & /* option */,
                                        const QModelIndex & /* index */) const
{
    QComboBox * editor = new QComboBox (parent);
    editor->setFrame (false);
    editor->addItems (item_list);
    return editor;
}

void ComboBoxDelegate::setEditorData (QWidget * param_editor,
                                      const QModelIndex & index) const
{
    QComboBox * editor = static_cast <QComboBox *> (param_editor);
    int value = index.model()->data(index, Qt::EditRole).toInt();
    editor->setCurrentIndex (value);
}

void ComboBoxDelegate::setModelData (QWidget * param_editor,
                                     QAbstractItemModel * model,
                                     const QModelIndex & index) const
{
    QComboBox * editor = static_cast <QComboBox *> (param_editor);
    int value = editor->currentIndex ();
    model->setData (index, value, Qt::EditRole);
}

void ComboBoxDelegate::updateEditorGeometry (QWidget *editor,
                                             const QStyleOptionViewItem & option,
                                             const QModelIndex &/* index */) const
{
    editor->setGeometry (option.rect);
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

