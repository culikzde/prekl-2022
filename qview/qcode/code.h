#ifndef CODE_H
#define CODE_H

#include <QString>
#include <QList>
#include <QMap>

/* ---------------------------------------------------------------------- */

class Item
{
    public:
       QString name;
       int src_file;
       int src_line;
       int src_col;
       Item * above;

       Item () : name (""), src_file (0), src_line (0), src_col (0), above (NULL) { }
       virtual int something () { return 0; }
};

/* ---------------------------------------------------------------------- */

class Scope : public Item
{
    public:
       QList <Item *> item_list;
       QMap <QString, Item *> item_dict;
};

/* ---------------------------------------------------------------------- */

class Namespace : public Scope
{
};

class Class : public Scope
{
};

class Variable : public Item
{
    public:
       Item * type;
       Variable () : type (NULL) { }
};

class Function : public Variable
{
    public:
       QList <Item *> parameters;
};

/* ---------------------------------------------------------------------- */

#endif // CODE_H
