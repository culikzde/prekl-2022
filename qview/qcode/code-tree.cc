
/* code-tree.cc */

#include "code-tree.h"

#include <QMap>

/* ---------------------------------------------------------------------- */

int fileNameCount = 0;
QList <QString> fileNameList;
QMap <QString, int> fileNameDict;

int fileNameToIndex (QString fileName)
{
   int result = 0;
   if (fileNameDict.contains (fileName))
       result = fileNameDict [fileName];
   else
   {
       fileNameCount ++;
       result = fileNameCount;
       fileNameList.append (fileName);
       fileNameDict [fileName] = result;
   }
}

QString indexToFileName (int inx)
{
   if (inx >= 1 && inx <= fileNameCount)
      return fileNameList [inx-1];
   else
      return "";
}


/* ---------------------------------------------------------------------- */

Tree::Tree (QWidget * parent) :
   QTreeWidget (parent)
{
}

/* ---------------------------------------------------------------------- */

TreeNode::TreeNode (QTreeWidget * parent, QString text) :
   QTreeWidgetItem (parent),
   src_file (0),
   src_line (0),
   src_col (0),
   src_obj (NULL),
   src_gra (NULL)
{
   if (text != "")
      setText (0, text);
}

TreeNode::TreeNode (QTreeWidgetItem * parent, QString text) :
   src_file (0),
   QTreeWidgetItem (parent),
   src_line (0),
   src_col (0),
   src_obj (NULL),
   src_gra (NULL)
{
   if (text != "")
      setText (0, text);
}


