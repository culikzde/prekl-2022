
/* code-table.h */

#ifndef CODE_TABLE_H
#define CODE_TABLE_H

#include "code.h"
#include <QTableView>
#include <QAbstractItemModel>
#include <QStyledItemDelegate>

class TableModel;

/* ---------------------------------------------------------------------- */

class DataTable : public QTableView
{
   Q_OBJECT
   public:
      DataTable (Scope * root, QWidget * parent = NULL);
   private:
      Scope * collection;
      TableModel * model;
      QStringList modeList;
   private slots:
      void onClick (const QModelIndex & index);
};

/* ---------------------------------------------------------------------- */

class TableModel : public QAbstractTableModel
{
   public:
      TableModel (Scope * root, QObject * parent = NULL);

      int rowCount (const QModelIndex & parent = QModelIndex ()) const override;
      int columnCount (const QModelIndex & parent = QModelIndex ()) const override;
      QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

      QVariant data (const QModelIndex & index, int role) const override;
      bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;
      Qt::ItemFlags flags (const QModelIndex & index) const override;

      // Qt::DropActions supportedDropActions () const override;
      // QStringList mimeTypes() const override;
      // bool dropMimeData (const QMimeData * data, Qt::DropAction action,
      //                   int row, int column, const QModelIndex & parent) override;

   private:
      Scope * collection;
};

/* ---------------------------------------------------------------------- */

class ComboBoxDelegate : public QStyledItemDelegate
{
   public:
      ComboBoxDelegate (QStringList p_item_list, QObject *parent = nullptr);

      QWidget *createEditor (QWidget * parent,
                             const QStyleOptionViewItem & option,
                             const QModelIndex & index) const override;

      void setEditorData (QWidget * editor,
                          const QModelIndex & index) const override;

      void setModelData (QWidget * editor,
                         QAbstractItemModel * model,
                         const QModelIndex & index) const override;

      void updateEditorGeometry (QWidget *editor,
                                 const QStyleOptionViewItem & option,
                                 const QModelIndex &index) const override;
   private:
      QStringList item_list;
};

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // CODE_TABLE_H
