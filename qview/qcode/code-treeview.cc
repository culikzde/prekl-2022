
/* code-treeview.cc */

#include "code-treeview.h"

#include <QPushButton>
#include <QMimeData>

/* ---------------------------------------------------------------------- */

static QString itemText (Item * elem)
{
    QString result = elem->name;
    return result;
}

/* ---------------------------------------------------------------------- */

TreeModel::TreeModel (Scope * root, QObject * parent)
   : QAbstractItemModel (parent), rootItem (root)
{
}

TreeModel::~TreeModel ()
{
}

int TreeModel::columnCount (const QModelIndex & parent) const
{
   return 1;
}

QVariant TreeModel::data (const QModelIndex & index, int role) const
{
   if (! index.isValid ())
      return QVariant ();

   if (role != Qt::DisplayRole)
      return QVariant ();

   Item * item = static_cast <Item *> (index.internalPointer ());

   return itemText (item);
}

QVariant TreeModel::headerData (int section, Qt::Orientation orientation, int role) const
{
   if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
      return "area title";

   return QVariant ();
}

QModelIndex TreeModel::index (int row, int column, const QModelIndex & parent) const
{
   if (! hasIndex (row, column, parent))
      return QModelIndex ();

   Item * parentItem;

   if (! parent.isValid ())
      parentItem = rootItem;
   else
      parentItem = static_cast <Item *> (parent.internalPointer ());

   Item * childItem = NULL;
   Scope * scope = dynamic_cast <Scope *> (parentItem);
   if (scope != NULL)
   {
      if (row >= 0 && row < scope->item_list.count ())
         childItem = scope->item_list [row];
   }

   if (childItem != NULL)
      return createIndex (row, column, childItem);
   else
      return QModelIndex ();
}

QModelIndex TreeModel::parent (const QModelIndex & index) const
{
   if (!index.isValid ())
      return QModelIndex ();

   Item * childItem = static_cast <Item *> (index.internalPointer ());
   Item * parentItem = childItem->above;

   if (parentItem == NULL || parentItem == rootItem)
      return QModelIndex ();

   int pos = 0;
   Scope * scope = dynamic_cast <Scope *> (parentItem);
   if (scope != NULL)
   {
       pos = scope->item_list.indexOf (childItem);
   }

   return createIndex (pos, 0, parentItem);
}

int TreeModel::rowCount (const QModelIndex & parent) const
{
   Item * parentItem;
   if (parent.column () > 0)
      return 0;

   if (! parent.isValid ())
      parentItem = rootItem;
   else
      parentItem = static_cast <Item*> (parent.internalPointer ());

   int cnt = 0;
   Scope * scope = dynamic_cast <Scope *> (parentItem);
   if (scope != NULL)
   {
       cnt = scope->item_list.count ();
   }

   return cnt;
}

Qt::ItemFlags TreeModel::flags (const QModelIndex & index) const
{
   if (! index.isValid ())
      // return 0;
      return Qt::ItemIsDropEnabled;

   return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled ;
}

Qt::DropActions TreeModel::supportedDropActions () const
{
     return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}

QStringList TreeModel::mimeTypes() const
{
   QStringList types;
   types << "application/x-color";
   return types;
}

bool TreeModel::dropMimeData (const QMimeData * data, Qt::DropAction action,
                              int row, int column, const QModelIndex & parent)
{
   bool ok = false;
   if (data->hasColor ())
   {
      QColor color = data->colorData().value<QColor>();
      // QColor color = qVariantValue<QColor> (data->colorData());

      if (parent.isValid ())
      {
          Item * item = static_cast <Item*> (parent.internalPointer ());
          // item->setBrush (color);
          ok = true;
      }
   }
   return ok;
}

/* ---------------------------------------------------------------------- */

TreeView::TreeView (QWidget* parent) :
   QTreeView (parent)
{
   // setSelectionMode (SingleSelection);
   // setDragEnabled (true);
   setAcceptDrops (true);
   setDropIndicatorShown (true);

   // setDragDropMode (DragDropMode (DragDrop | InternalMove));
   // setDragDropMode (InternalMove);
}

void TreeView::display (Scope * root)
{
   TreeModel * model = new TreeModel (root);
   setModel (model);
}

void TreeView::currentChanged (const QModelIndex & current, const QModelIndex & previous)
{
   Item * elem = NULL;
   if (current.isValid())
      elem = static_cast <Item *> (current.internalPointer ());
   emit itemSelected (elem);
}

/* ---------------------------------------------------------------------- */
