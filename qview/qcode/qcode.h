#ifndef QCODE_H
#define QCODE_H

#include <QMainWindow>

#include <QTreeWidget>
#include <QTabWidget>
#include <QTextEdit>
#include <QSplitter>

#include <QMap>

#ifdef KATE
   #include <KTextEditor/Document>
   #include <KTextEditor/Editor>
   #include <KTextEditor/View>
   // http://api.kde.org/frameworks/ktexteditor/html
#endif

#ifdef CMM
   #include "cmm_parser.hpp"
#endif

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window (QWidget * parent = nullptr);
    ~ Window ();

private :
    QMenu * fileMenu;

    QTreeWidget * tree;

    QTabWidget * tabs;
    QTextEdit * empty_edit;

    QTextEdit * info;

    QSplitter * vsplitter;
    QSplitter * hsplitter;

    QMap < QString, QTextEdit * > sourceEditors;

private:
    void readFile (QTextEdit * edit, QString fileName);

    QTextEdit * getEditor ();
    QString getEditorFileName (QTextEdit * edit);

public:
    QTextEdit * openEditor (QString fileName);
    void loadUI (QString fileName);
    void loadKate (QString fileName);
    void removeEmptyEditor ();

public slots:
    void openFile ();
    void openUI ();
    void openKate ();
    void runCmm ();
    void runClang ();
    void quit ();
};

/* ---------------------------------------------------------------------- */

#endif // QCODE_H
