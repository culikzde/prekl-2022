
/* lexer.cc */

#include "lexer.h"

#ifdef ENABLE_TRACE
   #include "trace.h"
#endif

#include <map>
#include <cassert>
#include <cstring>

#include <fstream>
#include <sstream>

using std::map;
using std::ifstream;
using std::ofstream;
using std::istringstream;
// using std::memcpy;
// using std::noskipws;
// using std::hex;
// using std::endl;
// using std::cerr;
// using std::streamsize;

/* ---------------------------------------------------------------------- */

const int NO_SYMBOL = 0;

typedef Lexer TEnvInput;
typedef Output TTextOutput;

inline void reset (ostringstream & storage)
{
   storage.str ("");
   storage.clear ();
}

/* ---------------------------------------------------------------------- */

const char cr = '\r';
const char lf = '\n';
// const char tab = '\t';
const char quote1 = '\'';
const char quote2 = '\"';
const char backslash = '\\';

/* ---------------------------------------------------------------------- */

inline bool isLetter (char ch)
{
   return (ch >= 'A' && ch <= 'Z') ||
          (ch >= 'a' && ch <= 'z') ||
          (ch == '_');
}

inline bool isUpperCaseLetter (char ch)
{
   return (ch >= 'A' && ch <= 'Z') || (ch == '_');
}

inline bool isDigit (char ch)
{
   return ch >= '0' && ch <= '9';
}

inline bool isOctal (char ch)
{
   return ch >= '0' && ch <= '7';
}

inline bool isHexDigit (char ch)
{
   return (ch >= '0' && ch <= '9') ||
          (ch >= 'a' && ch <= 'f') ||
          (ch >= 'A' && ch <= 'F');
}

inline bool isLetterOrDigit (int ch)
{
   return isLetter (ch) || isDigit (ch);
}

inline char UpCase (char ch)
{
   if (ch >= 'a' && ch <= 'z')
      return ch - 'a' + 'A';
   else
      return ch;
}

inline char LoCase (char ch)
{
   if (ch >= 'A' && ch <= 'Z')
      return ch - 'A' + 'a';
   else
      return ch;
}

string upperCase (string s)
{
   int len = s.length ();
   for (int i = 0; i<len; i ++)
   {
      char c = s[i];
      if (c >= 'a' && c <= 'z')
      {
         c = c - 'a' + 'A';
         s[i] = c;
      }
   }
   return s;
}

string lowerCase (string s)
{
   int len = s.length ();
   for (int i = 0; i<len; i ++)
   {
      char c = s[i];
      if (c >= 'A' && c <= 'Z')
      {
         c = c - 'A' + 'a';
         s[i] = c;
      }
   }
   return s;
}

/* ---------------------------------------------------------------------- */

bool isValidIdent (const string value)
{
   bool result = false;
   int len = value.length ();

   if (len != 0)
   {
      result = isLetter (value [0]);

      for (int i = 1; i<len && result; i++)
         if (! isLetterOrDigit (value [i]))
            result = false;
   }

   return result;
}

/* ---------------------------------------------------------------------- */

inline bool is_WhiteSpace (int n)
{
   return n == ' ' ||
          n == '\n' ||
          n == '\r' ||
          n == '\x09';
}

string trim (string s)
{
   int len = s.length ();
   while (len > 0 && is_WhiteSpace (s[len-1]))
      len --;

   int ofs = 1;
   while (ofs <= len && is_WhiteSpace (s[ofs-1]))
      ofs ++;

   return s.substr (ofs-1, 1+len-ofs);
}

/* ---------------------------------------------------------------------- */

inline string Head (const string s, int i)
{
   // first i characters
   return s.substr (0, i);
}

/* ---------------------------------------------------------------------- */

string addSlash (string s)
{
   int len = s.length ();
   if (len > 0 && s[len-1] != '/')
      return s + '/';
   else
      return s;
}

bool hasPrefix (const string value, char prefix)
{
   int len = value.length ();
   return len > 1 &&  value [0] == prefix;
}

string removeLastSlash (string s)
{
   int len = s.length ();
   if (len > 1 && s[len-1] == '/')
      return s.substr (0, len-1);
   else
      return s;
}

string addSubdir (const string dir_name, const string file_name)
{
   if (dir_name == "")
      return file_name;
   else if (file_name == "")
      return dir_name;
   else if (hasPrefix (file_name, '/'))
      return removeLastSlash (dir_name) + file_name;
   else
      return addSlash (dir_name) + file_name;
}

string removePath (string s)
{
   string::size_type inx = s.rfind ('/');

   if (inx != string::npos) // if found
      s = s.substr (inx+1);

   return s;
}

string extractPath (string s)
{
   string::size_type inx = s.rfind ('/');

   if (inx != string::npos) // if found
      s = s.substr (0, inx);
   else
      s = "";

   return s;
}

/* ---------------------------------------------------------------------- */

template <typename T>
inline string NumToStr (T num)
{
   ostringstream stream;
   stream << num;
   return stream.str ();
}
template <typename T>
inline void StrToNum (string txt, T & result, bool & ok)
// txt ... input value
// ok == true ... conversion succeded
// result ... converted value, assigned only when ok == true
{
   T n;
   istringstream stream (txt);
   stream >> std::noskipws;

   if (txt.length () > 2 && txt[0] == '0' && (txt[1] == 'x' || txt[1] == 'X'))
   {
      char c1;
      char c2;
      stream >> c1 >> c2 >> std::hex; // skip two characters and set hex
   }

   stream >> n;
   ok = (stream.eof () && ! stream.fail ());

   if (ok)
      result = n;

   // ok == false, nothing is assigned to result
}

template <typename T>
inline bool StrToNum (string txt, T & result)
{
   bool ok;
   StrToNum (txt, result, ok);
   return ok;
}

/* ---------------------------------------------------------------------- */

inline string IntToStr (int value)
// convert int to string
{
   return NumToStr (value);
}

inline void StrToInt (string txt, int & result, bool & ok)
// convert string to int
// txt ... input value
// ok == true ... conversion succeded
// result ... converted value, assigned only when ok == true
{
   StrToNum (txt, result, ok);
}

inline bool StrToInt (string txt, int & result)
// returns ok from previous function
{
   bool ok;
   StrToInt (txt, result, ok);
   return ok;
}

/* ---------------------------------------------------------------------- */

const char hex_digit [16+1] = "0123456789abcdef";

inline string charToStr (char c) { return string (1, c); } // convert one character to string

string quoteChrContent (char value)
{
   string result = "";

   unsigned char ch = value;

   if (ch < ' ' || ch == 128 || ch >= 255)
   {
      result = '\\';
      switch (ch)
      {
         case '\a': result = result + 'a'; break;
         case '\b': result = result + 'b'; break;
         case '\f': result = result + 'f'; break;
         case '\n': result = result + 'n'; break;
         case '\r': result = result + 'r'; break;
         case '\t': result = result + 't'; break;
         case '\v': result = result + 'v'; break;

         default:
            result = result + 'x' + hex_digit [ch >> 4] + hex_digit [ch & 15];
            break;
      }
   }
   else
   {
      if (ch == '\\' || ch == '"')
         result = charToStr ('\\') + value;
      else
         result = value;
   }

   return result;
}

string quoteStrContent (const string value)
{
   int len = value.length ();

   bool simple = true;
   for (int i = 0; i < len && simple; i++)
   {
      unsigned char ch = value [i];
      if (ch < ' ' || ch == '\\' || ch == '"' || ch == 128 || ch >= 255)
         simple = false;
   }

   if (simple)
   {
      return value;
   }
   else
   {
      string result = "";

      for (int i = 0; i < len; i++)
         result += quoteChrContent (value [i]);

      return result;
   }
}

string quoteStr (const string value, char quote = quote2)
{
   return quote + quoteStrContent (value) + quote;
}

string quoteChr (char value, char quote = quote1)
{
   return quote + quoteStrContent (charToStr (value)) + quote;
}

/* ---------------------------------------------------------------------- */

void show_message (MessageLevel level,
                   string msg, string file_name, int line, int column)
{
   switch (level)
   {
      case DebugLevel:
         msg = "debug: " + msg;
         break;

      case InfoLevel:
         msg = "info: " + msg;
         break;

      case WarningLevel:
         msg = "warning: " + msg;
         break;

      case ErrorLevel:
         msg = "error: " + msg;
         break;
   }

   // show position

   string txt = "";

   if (file_name != "")
   {
      txt = file_name + ":";
      if (line != 0) // !?
         txt = txt + IntToStr (line) + ":";
   }
   else
   {
      if (line != 0)
         txt = "line " + IntToStr (line) + ":";
   }

   if (column != 0)
      txt = txt + IntToStr (column) + ":";
      // emacs - column number, without any space, with colon

   if (txt != "")
      txt = txt + " ";

   txt = txt + msg;

   std::cerr << txt << std::endl;

   if (level == ErrorLevel)
   {
      #ifdef ENABLE_TRACE
         trace (); // show stack trace (before throwing exception)
      #endif

      throw LexerException (msg, file_name, line, column);
   }
}

/******************************* FILE NAMES *******************************/

typedef map <string, int> StringMap;
typedef map <int, string> IntMap;

// global variables
static StringMap FileNameDict;
static IntMap FileNumberDict;
static int last_index = 0;

void clearFileNames ()
{
   FileNameDict.clear ();
   FileNumberDict.clear ();
}

int storeFileName (string file_name)
{
   int index = 0;

   if (FileNameDict.find (file_name) != FileNameDict.end ())
   {
      index = FileNameDict [file_name];
   }
   else
   {
      last_index ++;
      index = last_index;
      FileNameDict [file_name] = index;
      FileNumberDict [index] = file_name;
   }

   return index;
}

string recallFileName (int index)
{
   if (FileNumberDict.find (index) != FileNumberDict.end ())
   {
      return FileNumberDict [index];
   }
   else
   {
      return "";
   }

   // return empty string if file index is not dictionay
   // important for __FILE__ macro expansion
}

int recallFileIndex (string file_name)
{
   if (FileNameDict.find (file_name) != FileNameDict.end ())
   {
      return FileNameDict [file_name];
   }
   else
   {
      return 0;
   }
}

/********************************* QUEUE **********************************/

/* TInputItem */

TInputItem::TInputItem () :
   symbol (NO_SYMBOL),
   kind (startToken),
   token_text (""),
   // token_sym (' '),
   // token_val (""),

   file_num (0),
   line_num (0),
   col_num (0),

   next (NULL)
{ }

void TInputItem::clear ()
{
   file_num = 0;
   line_num = 0;
   col_num = 0;

   symbol = 0;
   kind = startToken;
   token_text = "";

   // do not change 'next' field
}

/* TInputQueue */

void TInputQueue::insertLast (TInputItem * item)
{
   // assert (this != NULL);
   assert (item != NULL);
   assert (item->next == NULL);

   item->next = NULL;

   if (last != NULL)
      last->next = item;
   else
      first = item;

   last = item;
}

TInputItem * TInputQueue::removeFirst ()
{
   TInputItem * p = first;
   assert (p != NULL);

   first = p->next;

   if (first == NULL)
      last = NULL;

   p->next = NULL; // important
   return p;
}

void TInputQueue::insertLastFree (TInputItem * item)
{
   // assert (this != NULL);
   assert (item != NULL);
   assert (item->next == NULL);

   item->next = NULL;

   if (last_free != NULL)
      last_free->next = item;
   else
      first_free = item;

   last_free = item;
}

TInputItem * TInputQueue::removeFirstFree ()
{
   TInputItem * p = first_free;
   assert (p != NULL);

   first_free = p->next;

   if (first_free == NULL)
      last_free = NULL;

   p->next = NULL; // important
   return p;
}

void TInputQueue::removeAll ()
{
   TInputItem * p = first;
   while (p != NULL)
   {
      TInputItem * t = p;
      p = p->next;
      delete t;
   }

   first = NULL;
   last = NULL;

   p = first_free;
   while (p != NULL)
   {
      TInputItem * t = p;
      p = p->next;
      delete t;
   }

   first_free = NULL;
   last_free = NULL;
}

TInputQueue::~TInputQueue ()
{
   removeAll ();
}

TInputQueue::TInputQueue () :
   first (NULL),
   last (NULL),
   first_free (NULL),
   last_free (NULL)
{ }

/******************************* CHAR INPUT *******************************/

TCharInput::TCharInput ():
   inpStream (NULL),
   bufPos (0),
   bufLen (0),
   // ignore buf
   releaseStream (false),

   fileInx (0),
   linePos (0),
   colPos (0),
   charPos (0),

   tokenFileInx (0),
   tokenLinePos (0),
   tokenColPos (0),
   tokenCharPos (0),

   inpCh (0),
   prevCh (0),
   prevEol (false),
   inpEof (false),

   positionInitialized (false)
{
}

TCharInput::~TCharInput ()
{
   if (releaseStream && inpStream != NULL)
   {
      delete inpStream;
      inpStream = NULL;
   }
}

void TCharInput::Init ()
{
   bufPos = 0;
   bufLen = 0;
   // do not change already initialized inpStream, fileName and releaseStream

   inpCh = 0;
   prevCh = 0;
   prevEol = false;
   inpEof = false;

   if (! positionInitialized)
   {
      linePos = 1;
      colPos = 0;
      charPos = 0;

      tokenLinePos = 1;
      tokenColPos = 0;
      tokenCharPos = 0;
   }
}

string TCharInput::getFileName ()
{
   return recallFileName (fileInx);
}

void TCharInput::setFileName (const string file_name)
{
   fileInx = storeFileName (file_name);
}

/* ---------------------------------------------------------------------- */

void TCharInput::openStream (istream & stream)
{
   inpStream = & stream;
   assert (inpStream != NULL);
   Init ();
}

void TCharInput::openString (string text)
{
   istringstream * tmp = new istringstream (text);
   releaseStream = true;
   openStream (*tmp);
}

void TCharInput::open (string name)
{
   setFileName (name);

   ifstream * tmp = new ifstream;
   tmp->open (name.c_str ());

   if (!tmp->good ())
   {
      error ("Cannot open " + name);
      delete tmp;
   }
   else
   {
      releaseStream = true;
      openStream (*tmp);
   }
}

/* ---------------------------------------------------------------------- */

void TCharInput::readBuf ()
{
   assert (inpStream != NULL);
   if (! inpStream->good ()) // important for string streams
   {
       bufLen = 0;
   }
   else
   {
      inpStream->read (static_cast <char *> (static_cast <void *> (& buf)), bufSize);
      bufLen = inpStream->gcount ();
   }
   bufPos = 0;
}

void TCharInput::readData (void * adr, size_t size)
{
   size_t step;
   assert (bufPos >= 0);
   assert (bufPos <= bufLen);

   assert (bufLen >= 0);
   assert (bufLen <= bufSize);

   while (size > 0)
   {
      if (bufPos >= bufLen)
      {
         readBuf ();
         if (bufLen == 0)
            error ("End of file");
      }

      step = bufLen - bufPos;
      if (step > size)
         step = size;

      assert (step > 0);
      assert (step <= size);

      assert (bufPos >= 0);
      assert (bufPos + step <= bufLen);

      assert (bufLen >= 0);
      assert (bufLen <= bufSize);

      std::memcpy (adr, & buf[bufPos], step);
      bufPos += step;
      size -= step;

      adr = (void *) ((char *) (adr) + step);

      assert (size >= 0);
   }
}

void TCharInput::getCh ()
{
   // assert (bufPos >= 0);
   // assert (bufPos <= bufLen);

   // assert (bufLen >= 0);
   // assert (bufLen <= bufSize);

   if (bufPos >= bufLen)
   {
      if (inpEof)
      {
          error ("End of file");
      }
      else
      {
         readBuf ();
         inpEof = (bufLen == 0);
      }
   }

   if (inpEof)
   {
      inpCh = 0;
   }
   else
   {
      // assert (bufPos >= 0);
      // assert (bufPos < bufLen);

      // assert (bufLen >= 0);
      // assert (bufLen <= bufSize);

      inpCh = (char) (buf[bufPos]);
      bufPos ++;
   }

   // assert (bufPos >= 0);
   // assert (bufPos <= bufLen);

   // assert (bufLen >= 0);
   // assert (bufLen <= bufSize);

   if (prevEol)
   {
      linePos ++;
      colPos = 0;
      prevEol = false;
   }

   if (inpCh == cr)
   {
      prevEol = true;
      colPos ++;
   }
   else if (inpCh == lf)
   {
      if (prevCh != cr)
      {
         prevEol = true;
         colPos ++;
     }
   }
   else
   {
      colPos ++;
   }

   charPos ++;
   prevCh = inpCh;
}

/* ---------------------------------------------------------------------- */

void TCharInput::message (MessageLevel level, const string msg)
{
   show_message (level, msg, getFileName (), linePos, colPos);
}

void TCharInput::debug (const string msg)
{
   Message (InfoLevel, msg);
}

void TCharInput::info (const string msg)
{
   Message (InfoLevel, msg);
}

void TCharInput::warning (const string msg)
{
   Message (WarningLevel, msg);
}

void TCharInput::error (const string msg)
{
   Message (ErrorLevel, msg);
}

/****************************** MIXED INPUT *******************************/

TMixedInput::TMixedInput ():
   inp (NULL),

   inpCh (0),
   inpStarted (false),
   inpEof (false),

   tokenKind (startToken),

   tokenText (""),
   tokenVal (""),
   tokenSym (0),

   anyWhiteSpace (false)
{
}

TMixedInput::~TMixedInput ()
{
   if (inp != NULL)
   {
      delete inp;
      inp = NULL;
   }
}

/* ---------------------------------------------------------------------- */

void TMixedInput::Init ()
{
   // inpCh = 0;
   // inpEof = false;

   assert (inp != NULL);
   inp->Init ();

   inpCh = inp->inpCh;
   inpStarted = false;
   inpEof = inp->inpEof;

   // do not read first character, already called by TTextInput::Init ()

   TokenKind = startToken;
   tokenTxt = "";
   tokenVal = "";
   tokenSym = 0;
   anyWhiteSpace = false;
}

/* ---------------------------------------------------------------------- */

void TMixedInput::openStream (istream & stream)
{
   if (inp == NULL)
      inp = new TCharInput;

   inp->openStream (stream);
   Init (); // after open, important
}

void TMixedInput::openString (string text)
{
   if (inp == NULL)
      inp = new TCharInput;

   inp->openString (text);
   Init (); // after open, important
}

void TMixedInput::open (string name)
{
   if (inp == NULL)
      inp = new TCharInput;

   inp->open (name);
   Init (); // after open, important
}

/* ---------------------------------------------------------------------- */

void TMixedInput::getCh ()
{
   assert (inp != NULL);
   inp->getCh ();

   inpCh = inp->inpCh;
   inpEof = inp->inpEof;
   inpStarted = true;

   // info ("character: " + quoteStr (charToStr (inpCh)));
}

/* ---------------------------------------------------------------------- */

string TMixedInput::getFileName ()
{
   assert (inp != NULL);
   return inp->getFileName ();
}

void TMixedInput::setFileName (const string p_filename)
{
   assert (inp != NULL);
   inp->setFileName (p_filename);
}

void TMixedInput::setTokenFileInx (int p_file_inx)
{
   assert (inp != NULL);
   inp->tokenFileInx = p_file_inx;
}

void TMixedInput::setTokenLine (int p_line)
{
   assert (inp != NULL);
   inp->tokenLinePos = p_line;
}

void TMixedInput::setTokenCol (int p_col)
{
   assert (inp != NULL);
   inp->tokenColPos = p_col;
}

/* ---------------------------------------------------------------------- */

void TMixedInput::message (MessageLevel level, const string msg)
{
   // string file_name = "";
   // int line = 0;
   // int column = 0;

   if (inp != NULL)
   {
      // file_name = recallFileName (inp->tokenFileInx);
      // line = inp->tokenLinePos;
      // column = inp->tokenColPos;
      // inp->Message (level, msg, file_name, line, column);
      inp->Message (level, msg);
   }
}

/* ---------------------------------------------------------------------- */

void TMixedInput::debug (const string msg)
{
   message (DebugLevel, msg);
}

void TMixedInput::info (const string msg)
{
   message (InfoLevel, msg);
}

void TMixedInput::warning (const string msg)
{
   message (WarningLevel, msg);
}

void TMixedInput::error (const string msg)
{
   message (ErrorLevel, msg);
}

/**************************** LOOK AHEAD INPUT ****************************/

TRewindInput::TRewindInput ():

   mark_level (0),
   actual (NO_SYMBOL),

   present (NULL),
   queue (),

   comments (false)
{
}

TRewindInput::~TRewindInput ()
{
}

/* ---------------------------------------------------------------------- */

void TRewindInput::Init ()
{
   TMixedInput::Init ();

   queue.removeAll (); // !?

   mark_level = 0;

   actual = NO_SYMBOL;
   present = NULL;
}

/* ---------------------------------------------------------------------- */

void TRewindInput::stop (string msg)
{
   Error (msg);
}

void TRewindInput::stop ()
{
   if (actual == NO_SYMBOL)
      stop ("Unexpected end of source text");
   else
      stop ("Unexpected token " + conv (actual));
}

/* ---------------------------------------------------------------------- */

void TRewindInput::pick ()
{
   assert (present != NULL);

   actual = present->symbol;
   TokenKind = present->kind;

   tokenTxt = present->token_text;
   // tokenVal = present->token_val;
   // tokenSym = present->token_sym;

   tokenVal = ""; // !? clean
   tokenSym = ' '; // !? clean

   setTokenFileInx (present->file_num);
   setTokenLine (present->line_num);
   setTokenCol (present->col_num);

   // info ("pick " + conv (actual) + " " + tokenTxt);
}

/* ---------------------------------------------------------------------- */

void TRewindInput::remember ()
{
   assert (present != NULL);

   present->symbol = actual;
   present->kind = TokenKind;

   present->token_text = tokenTxt;
   // present->token_val = tokenVal;
   // present->token_sym = tokenSym;

   tokenVal = ""; // !? clean
   tokenSym = ' '; // !? clean

   present->file_num = getTokenFileInx ();
   present->line_num = getTokenLine ();
   present->col_num = getTokenCol ();

   // info ("remember " + conv (actual) + " " + tokenTxt);
}

/* ---------------------------------------------------------------------- */

void TRewindInput::readToken ()
{
   TInputItem * start = NULL;
   bool done = false;

   while (! done)
   {
      getToken ();

      if (TokenKind != eolToken)
      {
         done = true;
      }

      if (done || comments)
      {
         // add new item into queue
         if (queue.first_free != NULL)
         {
            present = queue.removeFirstFree ();
            present->clear ();
         }
         else
         {
            present = new TInputItem;
         }
         queue.insertLast (present);
      }

      actual = NO_SYMBOL; // !?

      // translate TokenKind to TInputItem
      if (done)
         translate ();

      // store token
      if (done || comments)
         remember ();

      if (start == NULL)
         start = present;
   }
}

/* ---------------------------------------------------------------------- */

void TRewindInput::next ()
// read next token
{
   // info ("nxt");

   if (present != NULL)
   {
      {
         // move to actual item
         present = present->next;
      }
   }

   if (present == NULL)
   {
      // read next token
      readToken ();
   }
   else
   {
      // use item from queue
      pick ();
   }

   // info ("next " + conv (actual) + " " + tokenTxt);
}

/* ---------------------------------------------------------------------- */

TInputItem * TRewindInput::mark ()
// remember current position
{
   mark_level ++;
   return present;
}

void TRewindInput::rewind (TInputItem * pos)
// return to previous position
{
   mark_level --;
   present = pos;
   pick ();
}

void TRewindInput::skipRewind ()
{
   mark_level --;
   // !?
}

/* ---------------------------------------------------------------------- */

void TRewindInput::match (TInputSymbol s)
// compare token and read next token
{
   if (actual != s)
   {
      // stop (quoteStr (conv (s)) + " expected");
      stop (quoteStr (conv (s)) + " expected and found " + tokenTxt);
   }

   if (s != NO_SYMBOL)
      next ();
}

TInputSymbol TRewindInput::LA (int i)
{
   TInputSymbol result;

   if (i == 1)
   {
      result = actual;
   }
   else
   {
      // !? not efficient
      TInputItem * inx = Mark ();
      for (int k = 1; k <= i-1; k++)
         next ();
      result = actual;
      Rewind (inx);
   }

   // info ("LA (" + IntToStr (i) + ")  = " + conv (result));
   return result;
}

/******************************* TEXT INPUT *******************************/

TTextInput::TTextInput ():
   text_storage (),
   value_storage ()
{
}

TTextInput::~TTextInput ()
{
}

/* ---------------------------------------------------------------------- */

void TTextInput::Init ()
{
   // original method
   TRewindInput::Init ();

   // not necessary
   reset (text_storage);
   reset (value_storage);

   // read first character
   nextCh ();
}

/* ---------------------------------------------------------------------- */

string ShowChr (char c)
{
   if (c >= ' ' && c <= 127)
      return charToStr (c);
   if (c == lf)
      return quoteStrContent (charToStr (c)) + charToStr (c);
   else
      return quoteStrContent (charToStr (c));
}

void TTextInput::nextCh ()
{
   // NO value_storage.put (inpCh); // token characters
   text_storage.put (inpCh); // characters from original source
   getCh ();
}

void TTextInput::textCh ()
// characters in string literals
{
   // NO value_storage.put (inpCh);
   text_storage.put (inpCh);

   getCh ();
}

void TTextInput::spaceCh ()
// characters in comments and white spaces
{
   // NO value_storage.put (inpCh);
   // NO text_storage.put (inpCh);

   getCh ();
}

/* ---------------------------------------------------------------------- */

void TTextInput::getIdent (bool to_lower_case, bool to_upper_case)
{
   while (isLetterOrDigit (inpCh))
      nextCh ();

   tokenTxt = text_storage.str ();
   tokenVal = "";
   tokenSym = 0;
   TokenKind = identToken;

   if (to_lower_case)
     tokenTxt = lowerCase (tokenTxt);

   if (to_upper_case)
     tokenTxt = upperCase (tokenTxt);
}

void TTextInput::getNumber (bool pascal_number)
{
   bool hex = false;
   bool any_digit = false;
   bool floating_point = false;

   if (inpCh == '0' && ! pascal_number)
   {
      nextCh (); // first digit
      any_digit = true;

      if (inpCh == 'x' || inpCh == 'X')
      {
         nextCh (); // store 'x'
         hex = true;
         while (isHexDigit (inpCh))
            nextCh ();
      }
   }
   else if (inpCh == '$' && pascal_number)
   {
      nextCh (); // store '$'
      hex = true;

      while (isHexDigit (inpCh))
      nextCh ();
   }

   if (! hex)
   {
      while (isDigit (inpCh))
      {
         any_digit = true;
         nextCh ();
      }

      if (inpCh == '.')
      {
         nextCh (); // decimal point
         floating_point = true;

         if (pascal_number)
         {
            if (!any_digit && isDigit (inpCh))
               Error ("Missing digit before decimal point");
            if (any_digit && ! isDigit (inpCh))
               Error ("Digit after decimal point expected");
         }

         while (isDigit (inpCh))
         {
            any_digit = true;
            nextCh ();
         }
      }

      if (any_digit && (inpCh == 'E' || inpCh == 'e'))
      {
         nextCh ();
         floating_point = true;

         if (inpCh == '+' || inpCh == '-')
            nextCh ();

         if (! isDigit (inpCh))
            error ("Digit expected");

         while (isDigit (inpCh))
            nextCh ();
      }
   }

   if (any_digit && ! pascal_number)
   {
      if (floating_point)
         while (inpCh == 'f' || inpCh == 'F' || inpCh == 'l' || inpCh == 'L')
            nextCh ();
      else
         while (inpCh == 'u' || inpCh == 'U' || inpCh == 'l' || inpCh == 'L')
            nextCh ();
   }

   if (any_digit)
   {
      tokenText = text_storage.str ();
      tokenVal = "";
      tokenSym = 0;

      tokenKind = numberToken;
   }
   else
   {
      if (pascal_number && inpCh == '.')
         storeLongSeparator ("..");
      else
         storeSeparator ('.');
   }
}

char TTextInput::character ()
// read one C-style character
{
  if (inpCh != backslash)             // simple character
  {
     char last = inpCh;
     textCh ();
     return last;
  }
  else                                // escape sequence
  {
     textCh ();                       // skip backslash

     if (isOctal (inpCh))             // octal
     {
        int n = 0;
        int cnt = 1;
        while (isOctal (inpCh) && cnt <= 3)
        {
           n = n*8 + inpCh-'0';
           cnt++;
           textCh ();
        }
        return char (n);
     }

     else if (inpCh == 'x' || inpCh == 'X')     // hex
     {
        textCh ();
        int n = 0;
        while (isHexDigit (inpCh))
        {
           int d;
           if (inpCh >= 'A' && inpCh <= 'F')
              d = inpCh - 'A' + 10;
           else if (inpCh >= 'a' && inpCh <= 'f')
              d = inpCh - 'a' + 10;
           else
              d = inpCh - '0';
           n = n*16 + d;
           textCh ();
        }
        return char (n);
     }
     else
     {
        char last = inpCh;
        textCh ();
        switch (last)                 // other
        {
           case 'a': return '\a';
           case 'b': return '\b';
           case 'f': return '\f';
           case 'n': return '\n';
           case 'r': return '\r';
           case 't': return '\t';
           case 'v': return '\v';

           case quote1:    return last;
           case quote2:    return last;
           case backslash: return last;
           case '?':       return last;

           default:        return last;
        }
     }
  }
}

// read one Pascal-style character
char TTextInput::pascalCharacter (char pascal_quote)
{
  if (inpCh == pascal_quote)
     textCh ();                       // skip backslash

  char last = inpCh;
  textCh ();
  return last;
}

void TTextInput::getChar (bool pascal_string)
{
   textCh (); // skip quote

   // if (inpCh == quote1)
   //    error ("Empty character constant");

   while (inpCh != quote1)
   {
      if (inpEof || inpCh == cr || inpCh == lf)
         error ("character constant exceeds line");

      char c;

      if (pascal_string)
         c = pascalCharacter (quote1);
      else
         c = character ();

      value_storage.put (c);
   }

   textCh (); // skip quote

   tokenTxt = text_storage.str (); // with quotes and escape sequences
   tokenVal = value_storage.str (); // value without quotes and escape sequences
   tokenSym = 0;
   TokenKind = charToken;

   if (pascal_string)
      TokenKind = stringToken;
}

void TTextInput::getString (bool pascal_string)
{
   textCh (); // skip quote

   while (inpCh != quote2)
   {
      if (inpEof || inpCh == cr || inpCh == lf)
         error ("String exceeds line");

      char c;

      if (pascal_string)
         c = pascalCharacter (quote2); // accept double quoted strings
      else
         c = character ();

      value_storage.put (c);
   }

   textCh (); // skip quote

   tokenText = text_storage.str (); // string with quotes and escape sequences
   tokenVal = value_storage.str (); // value without quotes and escape sequences
   tokenSym = 0;
   tokenKind = stringToken;
}

/* ---------------------------------------------------------------------- */

void TTextInput::storeSeparator (char ch)
{
   text_storage.put (ch);

   tokenTxt = ch;
   tokenVal = "";
   tokenSym = ch; // important
   TokenKind = separatorToken;
 }

void TTextInput::storeLongSeparator (string s)
{
   text_storage << s;

   tokenTxt = s;
   tokenVal = "";
   tokenSym = 0; // !?
   TokenKind = separatorToken;
 }

void TTextInput::getSeparator ()
{
   char c = inpCh;
   nextCh ();
   storeSeparator (c);
}

/* ---------------------------------------------------------------------- */

void TTextInput::getSpace ()
{
   while (! inpEof && inpCh <= ' ' && inpCh != cr && inpCh != lf) // WHITE SPACE
      spaceCh ();
}

bool TTextInput::getBackslash ()
{
   assert (inpCh == backslash);
   spaceCh ();
   bool backslash = false;

   if (! inpEof && (inpCh == cr || inpCh == lf))
   {
      spaceCh ();
   }
   else
   {
      storeSeparator (backslash);
      backslash = true;
   }

   return backslash;
}

void TTextInput::getEol ()
{
   if (! inpEof && inpCh == cr)
      spaceCh (); // skip cr

   if (! inpEof && inpCh == lf)
      spaceCh (); // skip lf

   tokenTxt = text_storage.str ();
   tokenVal = "";
   tokenSym = 0;
   TokenKind = eolToken;
}

/* ---------------------------------------------------------------------- */

void TTextInput::skipLine ()
{
   while (! inpEof && inpCh != cr && inpCh != lf)
      spaceCh ();
}

bool TTextInput::getComment ()
{
   bool slash = false;

   assert (inpCh == '/');
   spaceCh (); // skip slash

   if (! inpEof && inpCh == '/') // SINGLE LINE COMMENT
   {
      skipLine ();
   }

   else if (! inpEof && inpCh == '*')  // COMMENT
   {
      spaceCh (); // skip asterisk

      char prev = ' ';
      while (! inpEof && ! (prev == '*' && inpCh == '/'))
      {
         prev = inpCh;
         spaceCh ();
      }

      if (! inpEof)
         spaceCh (); // skip slash
   }

   else slash = true; // SLASH

   if (slash)
   {
      storeSeparator ('/');
   }

   return slash;
}

bool TTextInput::getPascalComment ()
{
   bool slash = false;
   bool parenthesis = false;

   if (inpCh == '/')
   {
      spaceCh (); // skip slash

      if (! inpEof && inpCh == '/') // SINGLE LINE COMMENT
         skipLine ();
      else
         slash = true;
   }
   else if (inpCh == '{')
   {
      spaceCh (); // skip braces

      while (! inpEof && inpCh != '}')
         spaceCh ();

      if (! inpEof)
      spaceCh (); // skip braces
   }
   else if (inpCh == '(')
   {
      spaceCh (); // skip parenthesis

      if (! inpEof && inpCh == '*') // COMMENT
      {
         spaceCh (); // skip asterisk

         char prev = ' ';
         while (! inpEof && ! (prev == '*' && inpCh == ')'))
         {
            prev = inpCh;
            spaceCh ();
         }

         if (! inpEof)
            spaceCh (); // skip parenthesis
      }
      else parenthesis = true; // PARENTHESIS
   }

   if (slash)
      storeSeparator ('/');
   else if (parenthesis)
      storeSeparator ('(');

   return slash || parenthesis;
}

/* ---------------------------------------------------------------------- */

void TTextInput::getToken ()
{
   again:

   // store token position
   assert (inp != NULL);
   inp->tokenFileInx = inp->fileInx;
   inp->tokenLinePos = inp->linePos;
   inp->tokenColPos  = inp->colPos;
   inp->tokenCharPos = inp->charPos;

   reset (text_storage);
   reset (value_storage);

   if (inpEof)                                 // END OF FILE
   {
      tokenTxt = "";
      tokenVal = "";
      tokenSym = 0;
      TokenKind = eofToken;
   }

   else if (inpCh == cr || inpCh == lf)        // END OF LINE
   {
      getEol ();
      // goto again;
   }

   else if (inpCh <= ' ')                      // WHITE SPACE
   {
      getSpace ();
      goto again;
   }

   else if (inpCh == backslash)                // BACKSLASH
   {
      bool backslash = getBackslash ();
      if (! backslash) goto again;
   }

   else if (inpCh == '/')                      // COMMENT (or slash)
   {
      bool slash = getComment ();
      if (! slash) goto again;
   }

   else if (isLetter (inpCh))                  // IDENTIFIER
      getIdent (false, false);

   else if (isDigit (inpCh))                   // NUMBER
      getNumber (false);

   else if (inpCh == '.')                      // NUMBER or PERIOD
      getNumber (false);

   else if (inpCh == quote1)                   // CHARACTER
      getChar (false);

   else if (inpCh == quote2)                   // STRING
      getString (false);

   else                                        // SEPARATOR
      getSeparator ();
}

/* ---------------------------------------------------------------------- */

bool TTextInput::NextSeparator (char par)
{
   while (! inpEof && inpCh <= ' ' && inpCh != cr && inpCh != lf) // WHITE SPACE
      spaceCh ();
   return inpCh == par;
}

/* ---------------------------------------------------------------------- */

bool TTextInput::isIdent (string par)
{
   return (TokenKind == identToken) && (tokenTxt == par);
}

bool TTextInput::isSeparator (char par)
{
   return (TokenKind == separatorToken) && (tokenSym == par);
}

/* ---------------------------------------------------------------------- */

void TTextInput::checkIdent (string par)
{
   if (TokenKind != identToken || tokenTxt != par)
      // Error (par + " expected");
      Error (par + " expected and found " + quoteStr (tokenTxt));
   getToken ();
}

void TTextInput::checkSeparator (char par)
{
   if (TokenKind != separatorToken || tokenSym != par)
   {
      // Error (charToStr (par) + " expected");
      Error (charToStr (par) + " expected and found " + quoteStr (tokenTxt));
   }
   getToken ();
}

void TTextInput::checkEof ()
{
   if (TokenKind != eofToken)
      Error ("End of file expected");
}

/* ---------------------------------------------------------------------- */

string TTextInput::readIdent ()
{
   string result;
   if (TokenKind != identToken)
      Error ("Identifier expected");
   result = tokenTxt;
   getToken ();
   return result;
}

string TTextInput::readNumber ()
{
   string result;
   if (TokenKind != numberToken)
      Error ("Number expected");
   result = tokenTxt;
   getToken ();
   return result;
}

char TTextInput::readChar ()
{
   char result;
   if (TokenKind != charToken)
      Error ("character constant expected");
   result = tokenSym;
   getToken ();
   return result;
}

string TTextInput::readString ()
{
   string result;
   if (TokenKind != stringToken)
      Error ("String expected");
   result = tokenVal;
   getToken ();
   return result;
}

/* ---------------------------------------------------------------------- */

bool TTextInput::readBool ()
{
   bool result = false;

   if (isIdent ("true"))
   {
      getToken ();
      result = true;
   }
   else if (isIdent ("false"))
   {
      getToken ();
      result = false;
   }
   else
   {
      Error ("Boolean value expected");
   }

   return result;
}

string TTextInput::readSignedNumber ()
{
   bool minus;
   string result;

   minus = false;
   if (TokenKind == separatorToken && tokenSym == '-')
   {
      minus = true;
      getToken ();
   }

   result = readNumber ();

   if (minus)
      result = '-' + result;
   return result;
}

string TTextInput::readValue ()
{
   string result;

   if (isSeparator ('-') || TokenKind == numberToken)
   {
      result = readSignedNumber ();
   }
   else if (TokenKind == identToken)
   {
      result = readIdent ();
      while (isSeparator ('.'))
      {
          getToken ();
          string id = readIdent ();
          result = result + "." + id;
      }
   }
   else if  (TokenKind == stringToken || TokenKind == charToken )
   {
      result = tokenVal;
      getToken ();
   }
   else
   {
      Error ("Identifier, number, character or string expected");
   }

   return result;
}

/* ---------------------------------------------------------------------- */

int TTextInput::readInt ()
{
   int result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      Error ("Integer expected");
   return result;
}

long TTextInput::readLong ()
{
   long result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      Error ("Long integer expected");
   return result;
}

unsigned int TTextInput::readUInt ()
{
   unsigned int result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      Error ("Unsigned integer expected");
   return result;
}

unsigned long TTextInput::readULong ()
{
   unsigned long result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      Error ("Unsinged long integer expected");
   return result;
}

float TTextInput::readFloat ()
{
   float result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      Error ("Float expected");
   return result;
}

double TTextInput::readDouble ()
{
   double result = 0;
   string txt = readSignedNumber ();
   bool ok;
   StrToNum (txt, result, ok);
   if (!ok)
      Error ("Double expected");
   return result;
}

/****************************** TOKEN TYPES *******************************/

string TEnvInput::TokenKindToStr (TTokenKind kind)
{
   static string table [] =
   {
      "startToken",

      "identToken",
      "numberToken",
      "charToken",
      "stringToken",
      "separatorToken",

      "eolToken",
      "stopToken",
      "finalToken",
      "eofToken"
   };

   const int lim = sizeof (table) / sizeof (table [0]);
   assert (eofToken == lim-1);

   string result;

   if (kind >= 0 && kind < lim)
      result = table [kind];
   else
      result = "TokenKind (" + IntToStr (kind) + ")";

   return result;
}

/************************** INCLUDE DIRECTORIES ***************************/

TInclDir::TInclDir () :
   name (),
   kind (normal_incl),
   next (NULL)
{
}

TInclDirectories::TInclDirectories () :
   first_dir (NULL),
   last_dir (NULL)
{
}

TInclDirectories::~TInclDirectories ()
{
   TInclDir * item = first_dir;
   while (item != NULL)
   {
      TInclDir * tmp = item->next;
      delete item;
      item = tmp;
   }
   first_dir = NULL;
   last_dir = NULL;
}

/* ---------------------------------------------------------------------- */

void TInclDirectories::InsInclDir (string name, TInclKind kind)
{
   assert (this != NULL);

   TInclDir * item = new TInclDir ();
   item->name = name;
   item->kind = kind;

   item->next = NULL;

   if (first_dir == NULL)
   {
      assert (last_dir == NULL);
      first_dir = item;
      last_dir = item;
   }
   else
   {
      assert (last_dir != NULL);
      last_dir->next = item;
      last_dir = item;
   }
}

void TInclDirectories::AddInclDir (string name, TInclKind kind)
{
   TInclDir * item = first_dir;
   while (item != NULL && item->name != name)
      item = item->next;

   if (item == NULL)
      InsInclDir (name, kind);
}

/* ---------------------------------------------------------------------- */

inline bool FileExists (string file_name)
{
   ifstream f;

   f.open (file_name.c_str ());
   bool found = f.good ();
   f.close ();

   return found;
}

inline bool ItemFound (TInclDir * item, string name, bool system_file)
{
   bool result = false;

   bool allowed;

   if (system_file)
      allowed = (item->kind != user_incl);
   else
      allowed = (item->kind != system_incl);

   if (allowed)
   {
      string file_name = addSlash (item->name) + name;

      // debug ("try " + file_name);
      if (FileExists (file_name))
      {
         // debug ("found " + file_name);
         result = true;
      }
   }

   return result;
}

/* ---------------------------------------------------------------------- */

string TInclDirectories::FindInclFile (string name,
                                       bool system_file,
                                       string include_next)
{
   assert (this != NULL);
   string result = ""; // when not found

   if (! hasPrefix (name, '/'))
   {
      bool found = false;
      TInclDir * item = first_dir;

      while (item != NULL && ! found)
      {
         bool allowed;

         if (system_file)
            allowed = (item->kind != user_incl);
         else
            allowed = (item->kind != system_incl);

         if (allowed)
         {
            string file_name = addSlash (item->name) + name;

            if (FileExists (file_name))
            {
               if (include_next != "")
               {
                  name = include_next; // search again (possibly another file name)
                  include_next = "";
               }
               else
               {
                  found = true;
                  result = file_name;
               }
            }
         }
         item = item->next;
      }
   }

   return result;
}

string TInclDirectories::LookupInclFile (string name,
                                         string dir,
                                         bool system_file,
                                         string include_next)
{
   string result = "";

   string local_name = addSubdir (dir, name);
   // debug ("LookupInclFile " + local_name + ", next: " + include_next);

   if (! system_file && FileExists (local_name))
   {
      if (include_next != "")
      {
         name = include_next; // search again (possibly another file name)
         include_next = "";
         result = FindInclFile (name, system_file, include_next);
      }
      else
      {
         result = local_name;
      }
   }
   else
   {
      result = FindInclFile (name, system_file, include_next);
   }

   // debug ("end of LookupInclFile " + result);
   return result;
}

/************************** CONDITIONAL SYMBOLS ***************************/

TCondParam::TCondParam () :
   name (),
   multi (false),
   next (NULL)
{
}

/* ---------------------------------------------------------------------- */

TCondSymbol::TCondSymbol () :
   name (),
   value (""),
   with_params (false),
   local_param (false),
   orig_value (""),
   first_param (NULL),
   last_param (NULL),
   prev (NULL),
   next (NULL),
   above (NULL),
   fileInx (0),
   linePos (0),
   colPos (0),
   charPos (0)
{
}

void TCondSymbol::DeleteParameters ()
{
   TCondParam * item = first_param;
   while (item != NULL)
   {
      TCondParam * tmp = item->next;
      delete item;
      item = tmp;
   }
   first_param = NULL;
   last_param = NULL;
}

TCondSymbol::~TCondSymbol ()
{
   DeleteParameters ();

   if (above != NULL)
   {
      if (prev != NULL)
         prev->next = next;
      else
         above->first_symbol = next;

      if (next != NULL)
         next->prev = prev;
      else
         above->last_symbol = prev;

      above = NULL;
      prev = NULL;
      next = NULL;
   }
}

/* ---------------------------------------------------------------------- */

TCondSymbols::TCondSymbols () :
   first_symbol (NULL),
   last_symbol (NULL)
{
}

TCondSymbols::~TCondSymbols ()
{
   TCondSymbol * item = first_symbol;
   while (item != NULL)
   {
      TCondSymbol * tmp = item->next;
      delete item;
      item = tmp;
   }

   first_symbol = NULL;
   last_symbol = NULL;
}

/* ---------------------------------------------------------------------- */

void TCondSymbol::InsParam (string name, bool multi)
{
   assert (this != NULL);

   TCondParam * item = new TCondParam;
   item->name = name;
   item->multi = multi;
   item->next = NULL;

   if (first_param == NULL)
   {
      assert (last_param == NULL);
      first_param = item;
      last_param = item;
   }
   else
   {
      assert (last_param != NULL);
      last_param->next = item;
      last_param = item;
   }
}

void TCondSymbols::InsSymbol (TCondSymbol * item)
{
   assert (this != NULL);
   assert (item != NULL);

   item->prev = last_symbol;
   item->next = NULL;
   item->above = this;

   if (first_symbol == NULL)
   {
      assert (last_symbol == NULL);
      first_symbol = item;
      last_symbol = item;
   }
   else
   {
      assert (last_symbol != NULL);
      last_symbol->next = item;
      last_symbol = item;
   }
}

/* ---------------------------------------------------------------------- */

TCondSymbol * TCondSymbols::SearchSymbol (string name)
{
   assert (this != NULL);
   TCondSymbol * item = first_symbol;
   while (item != NULL && item->name != name)
      item = item->next;
   return item;
}

string TCondSymbols::RecallSymbol (string name)
{
   TCondSymbol * item = SearchSymbol (name);

   if (item == NULL)
      return "";
   else
      return item->value;
}

/* ---------------------------------------------------------------------- */

bool TCondSymbols::IsDefinedSymbol (string name)
{
   TCondSymbol * item = SearchSymbol (name);
   return item != NULL;
}

TCondSymbol * TCondSymbols::DefineNewSymbol (string name, string value)
{
   TCondSymbol * item = SearchSymbol (name);

   if (item == NULL)
   {
      item = new TCondSymbol;
      item->name = name;
      item->value = value;
      InsSymbol (item);
   }
   else
   {
      item->DeleteParameters ();
      item->value = value;
   }

   return item;
}

void TCondSymbols::DefineSymbolWithParams (string name, string params, string value)
{
   TCondSymbol * item = DefineNewSymbol (name, value);

   if (params != "")
   {
      item->with_params = true; // important

      int inx = 0;
      int len = params.length ();

      while (inx < len)
      {
         int start = inx;

         while (inx < len && params [inx] != ',')
            inx ++;

         string s = params.substr (start, inx-start);
         s = trim (s);
         // debug ("Define Item " + name + " with parameter " + s);
         item->InsParam (s);

         if (inx < len && params [inx] == ',')
            inx ++;
      }
   }
}

void TCondSymbols::DefineSymbol (string name, string value)
{
   DefineNewSymbol (name, value);
}

void TCondSymbols::UndefineSymbol (string name)
{
   TCondSymbol * item = SearchSymbol (name);
   if (item != NULL)
      delete item;
}

/*************************** CONDITIONAL LEVELS ***************************/

TCondLevel::TCondLevel () :
   value (false),
   done (false),
   prev (NULL),
   next (NULL)
{
}

TCondLevels::TCondLevels () :
   first_level (NULL),
   last_level (NULL),
   current_level (NULL),
   level (0)
{
}

TCondLevels::~TCondLevels()
{
   TCondLevel * item = first_level;
   while (item != NULL)
   {
      TCondLevel * tmp = item->next;
      delete item;
      item = tmp;
   }
   first_level = NULL;
   last_level = NULL;
   current_level = NULL;
   level = 0;
}

/* ---------------------------------------------------------------------- */

void TCondLevels::InsCondLevel (TCondLevel * item)
{
   assert (this != NULL);
   assert (item != NULL);

   item->prev = last_level;

   if (first_level == NULL)
   {
      assert (last_level == NULL);
      first_level = item;
      last_level = item;
   }
   else
   {
      assert (last_level != NULL);
      last_level->next = item;
      last_level = item;
   }
}

/* ---------------------------------------------------------------------- */

const int cond_level_max = 128; // only for loop detection
const int sub_level_max = 32; // only for loop detection

void TCondLevels::AddCondLevel (bool value)
{
   // look for unused item
   if (current_level == NULL)
      current_level = first_level;
   else
      current_level = current_level->next;

   if (current_level == NULL)
   {
      // allocate new item
      current_level = new TCondLevel;
      InsCondLevel (current_level);
   }

   current_level->value = value;
   current_level->done = false;

   level ++;
}

void TCondLevels::RemoveCondLevel ()
{
   assert (current_level != NULL);
   current_level = current_level->prev; // old item may be reused later
   level --;
}

void TCondLevels::RemoveAllLevels ()
{
   current_level = NULL;
   level = 0;
   // other items may be reused later
}

bool TCondLevels::Calculate ()
{
   bool result = true;

   if (current_level != NULL)
   {
      TCondLevel * item = first_level;
      while (item != current_level && result)
      {
         assert (item != NULL);
         if (! item->value)
            result = false;
         item = item->next;
      }

      if (result && ! item->value)
         result = false;
   }

   return result;
}

/******************************** OPTIONS *********************************/

TEnvOptions::TEnvOptions ():

   LowerCaseIdentifiers (false),
   UpperCaseIdentifiers (false),

   PascalNumbers (false),
   PascalStrings (false),

   PascalComments (false),

   HashDirectives (true), /* <-- */

   IgnoreIncludes (false),
   TolerateIncludes (false),

   IgnoreWarningDirectives (false),
   IgnoreErrorDirectives (false),

   IgnoreMacroExpansion (false)
{ }

/******************************* SUB INPUT ********************************/

TSubInput::TSubInput ():
   prev (NULL),
   next (NULL),
   title (),
   orig_include_name (),
   is_include_file (false),
   add_new_line (false),
   add_final_token (false),
   save_real_input (NULL)
{
}

TSubInput::~TSubInput ()
{
}

/* ---------------------------------------------------------------------- */

TStrSubInput::TStrSubInput ():
   inx (0),
   value ()
{
}

TStrSubInput::~TStrSubInput ()
{
}

void TStrSubInput::readBuf ()
{
   bufPos = 0;
   bufLen = 0;

   int len = value.length ();
   if (inx < len)
   {
      bufLen = len - inx;

      if (bufLen > bufSize)
         bufLen = bufSize;

      std::memcpy (buf, value.c_str () + inx, bufLen);

      inx += bufLen;
   }
}

/******************************* ENV INPUT ********************************/

Lexer::Lexer ():

   first_input (NULL),
   real_input (NULL),
   input (NULL),

   sub_level (0),
   line_cont (0),
   inside_directive (false),

   CondTable (),
   CondValue (true),

   compound_separators (false),

   symbols (),
   incl_directories (),

   // variables (),
   opt ()
{
   OpenSubInput (new TSubInput); // !? delete
   real_input = input;
   input->save_real_input = real_input;
}

TEnvInput::~TEnvInput ()
{
   // delete input streams
   while (input != NULL)
      CleanSubInput ();

  Finish ();
}

/* ---------------------------------------------------------------------- */

void TEnvInput::CheckEnd ()
{
   if (input != NULL && input->prev != NULL)
      Error ("Remaining nested input");

   if (! inpEof)
      Error ("Remaining text");
}

/* ---------------------------------------------------------------------- */

void TEnvInput::Init ()
{
   // original method
   TTextInput::Init ();

   CondTable.RemoveAllLevels ();
   CondValue = true;

   // do not read first character, already done by TTextInput::Init ()
}

void TEnvInput::Finish ()
{
}

/* ---------------------------------------------------------------------- */

void TEnvInput::SetStartToken ()
{
   TokenKind = startToken;
   tokenTxt = "";
   tokenVal = "";
   tokenSym = 0;
}

void TEnvInput::SetStringToken (const string txt)
{
   TokenKind = stringToken;
   tokenTxt = txt;
   tokenVal = "";
   tokenSym = 0;
}

/* ---------------------------------------------------------------------- */

void TEnvInput::GetSimpleToken () // @@
{
   again:

   assert (TokenKind != eofToken);
   assert (input != NULL);

   while (input->inpEof &&
          ! input->add_new_line &&
          ! input->add_final_token &&
          input != first_input)
   {
      // close include file or macro
      CloseSubInput ();

      inpCh = input->inpCh;
      inpEof = input->inpEof;
   }

   // store token position
   input->tokenFileInx = input->fileInx;
   input->tokenLinePos = input->linePos;
   input->tokenColPos  = input->colPos;
   input->tokenCharPos = input->charPos;

   reset (text_storage);
   reset (value_storage);

   if (inpEof)                                     // END OF FILE
   {
      tokenTxt = "";
      tokenVal = "";
      tokenSym = 0;

      if (input != NULL && input->add_new_line)
      {
         TokenKind = eolToken;
         tokenTxt = charToStr (lf);
         input->add_new_line = false;
      }
      else if (input != NULL && input->add_final_token)
      {
         TokenKind = finalToken;
         input->add_final_token = false;
      }
      else
      {
         if (TokenKind == stopToken || TokenKind == eofToken)
            Error ("Unexpected end of source");

         if (input != NULL && input->prev != NULL)
            TokenKind = stopToken;
         else
            TokenKind = eofToken;
      }
   }

   else
   {
      if (inpCh == cr || inpCh == lf)              // END OF LINE
      {
         getEol ();
         // NO goto again;
      }
      else if (inpCh <= ' ')                       // WHITE SPACE
      {
         getSpace ();
         goto again;
      }

      else if (inpCh == backslash && opt.HashDirectives)   // BACKSLASH
      {
         bool backslash = getBackslash ();
         if (! backslash) goto again;
      }

      else if (inpCh == '/')                          // COMMENT (or slash)
      {
         bool slash;
         if (opt.PascalComments)
            slash = getPascalComment ();
         else
            slash = getComment ();
         if (!slash) goto again;
      }

      else if (inpCh == '{' && opt.PascalComments)    // COMMENT
      {
         getPascalComment ();
         goto again;
      }

      else if (inpCh == '(' && opt.PascalComments)    // COMMENT (or parenthesis)
      {
         bool parenthesis = getPascalComment ();
         if (! parenthesis) goto again;
      }

      if (isLetter (inpCh))                           // IDENTIFIER
         getIdent (opt.LowerCaseIdentifiers,
                   opt.UpperCaseIdentifiers);

      else if (isDigit (inpCh))                       // NUMBER
         getNumber (opt.PascalNumbers);

      else if (inpCh == '.')                          // NUMBER or PERIOD
         getNumber (opt.PascalNumbers);

      else if (inpCh == quote1)                       // CHARACTER
         getChar (opt.PascalStrings);

      else if (inpCh == quote2 )                      // STRING
         getString (opt.PascalStrings);

      else                                            // SEPARATOR
         getSeparator ();
   }
}

/* ---------------------------------------------------------------------- */

void TEnvInput::OpenSubInput (TSubInput * item) // @@
{
   if (first_input == NULL)
   {
      assert (input == NULL);
      first_input = item;
      item->prev = NULL;
   }
   else
   {
      item->prev = input;
      input->next = item;
   }

   item->next = NULL;
   input = item;

   inp = input;
   sub_level ++;
   if (sub_level > sub_level_max)
      Error ("Too many nested includes or macros");

   input->Init ();
   inpCh = input->inpCh;
   inpEof = input->inpEof; // important

   if (input->is_include_file)
   {
      real_input = input;
      OpeningInclude ();
   }

   input->save_real_input = real_input;
}

void TEnvInput::CleanSubInput ()
{
   assert (input != NULL);

   // remove input stream from list of include files
   TCharInput * tmp = input;
   input = input->prev;

   if (input == NULL)
      first_input = NULL;
   else
      input->next = NULL;

   delete tmp;

   inp = input;
   sub_level --;

   // important
   if (input == NULL)
   {
      inpCh = 0;
      inpEof = true;
      real_input = NULL;
   }
   else
   {
      inpCh = input->inpCh;
      inpEof = input->inpEof;
      real_input = input->save_real_input;
   }
}

void TEnvInput::CloseSubInput ()
{
   assert (input != NULL);

   if (input->is_include_file)
   {
      ClosingInclude ();
   }

   CleanSubInput ();
}

/* ---------------------------------------------------------------------- */

void TEnvInput::OpenInclude (string file_name, string orig_name)
{
   TSubInput * item = new TSubInput ();
   item->open (file_name);

   item->is_include_file = true;
   item->orig_include_name = orig_name;
   item->add_new_line = true; // finish last line

   OpenSubInput (item);

   nextCh (); // read first character

   // !? !! SetStartToken (); // formal first token
}

void TEnvInput::Include (string file_name)
{
   OpenInclude (file_name, removePath (file_name));
}

void TEnvInput::OpenText (const string text, bool add_stop)
{
   TStrSubInput * item = new TStrSubInput (); // !? delete
   item->is_include_file = false;
   item->add_final_token = add_stop;
   item->value = text;

   if (input != NULL)
   {
      // copy location
      item->positionInitialized = true;

      item->fileInx = input->fileInx;
      item->linePos = input->linePos;
      item->colPos  = input->colPos;
      item->charPos = input->charPos;

      item->tokenFileInx = input->fileInx;
      item->tokenLinePos = input->linePos;
      item->tokenColPos  = input->colPos;
      item->tokenCharPos = input->charPos;
   }

   OpenSubInput (item);
   getCh (); // skip current character (is stored as extra_character)

   // !? !! SetStartToken (); // formal first token
}

void TEnvInput::OpenMacro (TSubInput * item, bool add_stop)
{
   assert (input != NULL);
   assert (real_input != NULL);

   item->is_include_file = false;
   item->add_final_token = add_stop;

   OpenSubInput (item);

   getCh (); // skip current character (is stored as extra_character)

   // !? !! SetStartToken (); // formal first token
}

/* ---------------------------------------------------------------------- */

TCondSymbol * TEnvInput::LocalSearchSymbol (const string name)
{
   TCondSymbol * symbol = NULL;

   if (input != NULL)
      symbol = input->local_symbols.SearchSymbol (name);

   if (symbol == NULL)
   {
      symbol = symbols.SearchSymbol (name);
   }

   return symbol;
}

TCondSymbol * TEnvInput::MacroSearchSymbol (TStrSubInput * macro_input, const string name)
{
   TCondSymbol * symbol =
      macro_input->local_symbols.SearchSymbol (name);

   if (symbol == NULL)
      symbol = LocalSearchSymbol (name);

   return symbol;
}

string TEnvInput::ReadParameter (bool multi) // @@
{
   ostringstream orig_stream; // original value for current parametr
   int level = 0; // level of nested parenthesis
   bool inside = false; // true => after some non-empty token
   bool tail = false; // true => add one space before next tokem

   // inpCh is first character of current parameter
   // TokenKind, tokenTxt is old token

   while (level > 0 || ! ( ( ! multi && inpCh == ',' ) || inpCh == ')') )
   {
      // now read first parameter token
      GetSimpleToken ();

      if (isSeparator ('(')) level ++;
      if (isSeparator (')')) level --;

      if (TokenKind == startToken ||
          TokenKind == stopToken ||
          TokenKind == finalToken ||
          TokenKind == eofToken)
      {
         if (inside) // skip leading spaces
            tail = true; // only one space
      }
      else
      {
         if (tail)
            orig_stream.put (' '); // previous space

         orig_stream << tokenTxt; // collect parameter value

         tail = false;
         inside = true;
      }
   }

   return orig_stream.str ();
}

string TEnvInput::ExpandParameter (const string orig_value) // @@
{
   OpenText (orig_value, true);

   ResolveSymbol (); // skip macro name or right parenthesis (after opening sub-input)

   ostringstream expanded_stream;

   while (TokenKind != startToken &&
          TokenKind != stopToken &&
          TokenKind != finalToken &&
          TokenKind != eofToken)
   {
      expanded_stream << tokenTxt;

      ResolveSymbol ();
   }

   CloseSubInput ();

   if (TokenKind == finalToken)
   {
      GetSimpleToken (); // skip finalToken
   }
   else
   {
      Error ("finalToken expected - internal bug");
   }

   return expanded_stream.str ();
}

void TEnvInput::ExpandParameters (TStrSubInput * macro_input, TCondSymbol * symbol) // @@
{
   GetSimpleToken (); // skip macro name, move to left parenthesis
   assert (isSeparator ('('));
   // NO GetSimpleToken (); // keep left parenthesis

   TCondParam * param = symbol->first_param;
   bool finish = false;

   if (param == NULL)
      GetSimpleToken (); // skip left parenthesis, important

   while (param != NULL && ! finish)
   {
      // inpCh is first character of current parameter
      // TokenKind, tokenTxt is old token

      // expand one parameter
      string orig_value = ReadParameter (param->multi);
      string expanded_value = ExpandParameter (orig_value);

      // store parameter value
      TCondSymbol * sym = macro_input->local_symbols.DefineNewSymbol (param->name, expanded_value);
      sym->local_param = true;
      sym->orig_value = orig_value;

      param = param->next;

      // check comma (parameter separator)
      if (param != NULL)
      {
         if (param->multi && isSeparator (')'))
         {
            // special case multi parameter, but without value
            TCondSymbol * sym = macro_input->local_symbols.DefineNewSymbol (param->name, "");
            sym->local_param = true;
            finish = true;  // end of loop
         }

         if (! finish && ! isSeparator (','))
         {
            Error ("',' expected (in macro expansion of " + symbol->name + "." + param->name + ")");
         }
         // NO GetSimpleToken (); // keep comma as current token
      }
   }

   if (! isSeparator (')'))
   {
      Error ("')' expected (in macro expansion of " + symbol->name + ")");
   }
   // NO GetSimpleToken (); // keep right parenthesis token on input
}

string TEnvInput::CompoundItem (TStrSubInput * macro_input, const string name)
{
   string result;

   TCondSymbol * symbol = macro_input->local_symbols.SearchSymbol (name);

   if (symbol == NULL)
      result = name;
   else if (symbol->local_param)
      result = symbol->orig_value;
   else
      result = symbol->value;

   return result;
}

void TEnvInput::StringSymbol (TStrSubInput * macro_input) // @@
{
   GetSimpleToken (); // skip hash mark

   if (TokenKind != identToken)
      Error ("Identifier expected after hash mark");

   string name = tokenTxt;
   TCondSymbol * symbol = MacroSearchSymbol (macro_input, name);

   if (symbol == NULL)
   {
      Error ("Undefined symbol after hash mark: " + name);
   }
   else
   {
      string t = "";

      if (symbol->local_param)
         t = quoteStr (symbol->orig_value); // use non-expanded parameter value
      else
         t = quoteStr (symbol->value);

      OpenText (t, false);
      GetSimpleToken (); // skip original token
   }
}

void TEnvInput::CompoundSymbol (TStrSubInput * macro_input) // @@
{
   GetSimpleToken ();

   // spaces around hash mark(s) are removed during macro definitions

   if (opt.HashDirectives && isSeparator ('#'))
   {
      StringSymbol (macro_input);
   }
   else
   {
      // inpCh ... next character after current token

      if (CondValue &&
          TokenKind == identToken &&
          opt.HashDirectives &&
          inpCh == '#')
      {
         /* collect values from concatenated symbols */
         string compound_value = CompoundItem (macro_input, tokenTxt);

         while (inpCh == '#')
         {
            GetSimpleToken (); // read first hash mark

            if (inpCh != '#')
            {
               // Error ("Two hash marks expected: " + quoteStr (charToStr (inpCh)));
               StringSymbol (macro_input);
            }
            else
            {
                GetSimpleToken ();  // read second hash mark

                GetSimpleToken (); // read next token (identifier)

                // NO if (TokenKind != identToken)
                //    Error ("Identifier expected after hash marks: "+ quoteStr (tokenTxt));
            }

            compound_value += CompoundItem (macro_input, tokenTxt);
         }

         /* lookup compound symbol */
         TCondSymbol * symbol = LocalSearchSymbol (compound_value);

         if (symbol != NULL)
         {
            compound_value = symbol->value;
         }

         /* use collected value as new input */
         OpenText (compound_value, false);

         GetSimpleToken (); // skip previous identifier
                            // do it after OpenText, otherwise one token after compound name is lost
      }
   }
}

void TEnvInput::ExpandAgain (TCondSymbol * symbol, TStrSubInput * macro_input) // @@
{
   ostringstream expanded_stream;

   OpenText (symbol->value, true);

   CompoundSymbol (macro_input); // skip right parenthesis (after OpenMacro)

   while (TokenKind != startToken &&
          TokenKind != stopToken &&
          TokenKind != finalToken &&
          TokenKind != eofToken)
   {
      TCondSymbol * sym = NULL;

      // search in local symbols
      if (TokenKind == identToken)
         sym = macro_input->local_symbols.SearchSymbol (tokenTxt);

      if (sym != NULL)
         expanded_stream << sym->value;
      else
         expanded_stream << tokenTxt;

      CompoundSymbol (macro_input);
   }

   CloseSubInput ();

   assert (TokenKind == finalToken);
   // NO GetSimpleToken (); // keep finalToken

   macro_input->value = expanded_stream.str (); // store pre-expanded text
}

void TEnvInput::ExpandSymbol (TCondSymbol * symbol) // @@
{
   TStrSubInput * macro_input = new TStrSubInput (); // !? delete
   macro_input->title = symbol->name;
   macro_input->value = symbol->value; // value for "not function like" macros

   // copy location
   macro_input->positionInitialized = true;

   macro_input->fileInx = symbol->fileInx;
   macro_input->linePos = symbol->linePos;
   macro_input->colPos  = symbol->colPos;
   macro_input->charPos = symbol->charPos;

   macro_input->tokenFileInx = symbol->fileInx;
   macro_input->tokenLinePos = symbol->linePos;
   macro_input->tokenColPos  = symbol->colPos;
   macro_input->tokenCharPos = symbol->charPos;

   // without parameters, leave macro name as current token
   // with parameters, leave right parenthesis as current token
   // skip this token after sub-input is opened

   bool only_macro_name = false;

   // parameters
   if (symbol->with_params)
   {
      // skip spaces
      while (! inpEof && inpCh <= ' ' && inpCh != cr && inpCh != lf)
         nextCh ();

      if (inpCh != '(')
         // spaces after symbol name are ignored
         only_macro_name = true;
      else
         ExpandParameters (macro_input, symbol);
   }

   if (only_macro_name)
   {
      delete macro_input;
      macro_input = NULL;
   }
   else
   {
      // expand parameters and compund names in macro
      ExpandAgain (symbol, macro_input);

      // expand macro
      OpenMacro (macro_input, false);

      ResolveSymbol (); // skip right parenthesis (after OpenMacro)
                        // use ResolveSymbol to expand following token
   }
}

void TEnvInput::ResolveSymbol () // @@
{
   // read macro identifier with parameters ( or read one another token )

   GetSimpleToken ();

   if (TokenKind == identToken && CondValue)
   {
      // search symbol definition
      string symbol_name = tokenTxt;
      TCondSymbol * symbol = NULL;
      if (! opt.IgnoreMacroExpansion)
      {
         if (symbol_name == "__FILE__")
            SetStringToken (quoteStr (recallFileName (real_input->tokenFileInx)));
            // first_input - original source file_name
            // input - possible macro in another file
         else if (symbol_name == "__LINE__")
            SetStringToken (IntToStr (real_input->tokenLinePos));
         else
            symbol = LocalSearchSymbol (symbol_name);
      }

      // check recursion
      if (symbol != NULL)
      {
         TSubInput * temp = input;
         while (temp != NULL)
         {
            if (symbol_name == temp->title)
               symbol = NULL; // no recursion allowed
            temp = temp->prev;
         }
      }

      // read parameters and expand macro
      if (symbol != NULL)
         ExpandSymbol (symbol);
   }
}

/* ---------------------------------------------------------------------- */

void TEnvInput::CheckEndOfLine ()
{
   if (TokenKind != eolToken &&
       TokenKind != stopToken &&
       TokenKind != eofToken)
   {
      Error ("End of line (after directive) expected"
             ", tokenTxt = " + tokenTxt +
             ", TokenKind = " + TokenKindToStr (TokenKind));
   }
}

/********************************** DEFINE ********************************/

bool TEnvInput::ThreeDots ()
{
   bool result;
   if (compound_separators)
   {
       result = isSeparator() && tokenTxt == "...";
       if (result)
          GetSimpleToken ();
   }
   else
   {
      result = isSeparator ('.');

      if (result)
      {
         GetSimpleToken ();

         if (! isSeparator ('.'))
            Error ("'...' expected");

         GetSimpleToken ();

         if (! isSeparator ('.'))
            Error ("'...' expected");

         GetSimpleToken ();
      }
   }
   return result;
}

void TEnvInput::DefineParameter (TCondSymbol * symbol)
{
   string name = "";
   bool multi = false;

   if (ThreeDots ())
   {
       name = "__VA_ARGS__";
       multi = true;
   }
   else
   {
      if (TokenKind != identToken)
         Error ("Parameter identifier expected");

      name = tokenTxt;
      GetSimpleToken (); // skip parameter name

      if (ThreeDots ())
      {
         multi = true;
      }
   }

   // debug ("DefineParameter " + name);
   symbol->InsParam (name, multi);
}

string TEnvInput::ParametersToText (TCondSymbol * symbol)
{
   string txt = "";

   if (symbol->with_params)
   {
      txt += "(";
      TCondParam * param = symbol->first_param;
      while (param != NULL)
      {
         if (param->multi)
            txt += "...";
         else
            txt += param->name;

         param = param->next;

         if (param != NULL)
            txt += ",";
      }
      txt += ")";
   }

   return txt;
}

string TEnvInput::DefineToText (TCondSymbol * symbol)
{
   string txt = symbol->name +ParametersToText(symbol) + " ";
   // txt += + "--> ";
   txt += symbol->value;
   return txt;
}

void TEnvInput::DefineDirective ()
{
   GetSimpleToken (); // skip define

   if (TokenKind != identToken)
      Error ("Identifier expected");

   TCondSymbol * symbol = new TCondSymbol ();
   symbol->name = tokenTxt;

   // debug ("DefineDirective " + tokenTxt);

   symbol->with_params = (inpCh == '('); // parenthesis without space
   GetSimpleToken (); // skip symbol name

   if (symbol->with_params)
   {
      GetSimpleToken (); // skip '('

      if (! isSeparator (')'))
      {
         DefineParameter (symbol);
         while (isSeparator (','))
         {
            if (symbol->last_param->multi)
               Error ("'...' must be last parameter");
            GetSimpleToken ();
            DefineParameter (symbol);
         }
      }

      if (! isSeparator (')'))
         Error ("')' expected (in macro definition))");

      GetSimpleToken ();
   }

   assert (input != NULL);
   symbol->fileInx = input->fileInx;
   symbol->linePos = input->linePos;
   symbol->colPos  = input->colPos;
   symbol->charPos = input->charPos;

   // collect value, trim white spaces
   bool space = false;
   bool ignore_space = false;
   string value = "";

   while (TokenKind != eolToken &&
          TokenKind != stopToken &&
          TokenKind != finalToken &&
          TokenKind != eofToken)
   {
      if (tokenTxt == "#") // !?
         ignore_space = true; // ignore space before ##

      if (space && ! ignore_space)
         value += " ";

      value += tokenTxt;

      space = false;
      ignore_space = (tokenTxt == "#" || tokenTxt == "##"); // ignore space after # or ##

      // skip trailing space
      GetSimpleToken ();
   }

   symbol->value = value;

   if (CondValue) // is it valid source section
   {
      assert (input != NULL);
      symbols.UndefineSymbol (symbol->name);
      symbols.InsSymbol (symbol);
   }
   else
   {
      delete symbol; // !?
   }
}

void TEnvInput::UndefDirective ()
{
   GetSimpleToken (); // skip undef

   if (TokenKind != identToken)
      Error ("Identifier expected");

   if (CondValue) // is it valid source section
   {
      symbols.UndefineSymbol (tokenTxt);
   }

   GetSimpleToken (); // skip symbol name
   CheckEndOfLine ();
}

/****************************** EXPRESSIONS *******************************/

void TEnvInput::GetExprToken () // @@
{
   again:

   ResolveSymbol ();

   if (TokenKind == startToken) goto again;
   if (TokenKind == stopToken) goto again;
}

/* ---------------------------------------------------------------------- */

bool TEnvInput::TwoSeparators (char c1, char c2)
{
    bool result;
    if (compound_separators)
    {
       result = isSeparator () &&
                tokenTxt.length () == 2 &&
                tokenTxt [0] == c1 &&
                tokenTxt [1] == c2;
    }
    else
    {
       result = isSeparator (c1) && (inpCh == c2);
    }
    return result;
}

void TEnvInput::SkipTwoSeparators ()
{
   GetExprToken ();
   if (! compound_separators)
      GetExprToken ();
}

bool TEnvInput::CondSymbolDefined ()
{
   if (TokenKind != identToken || tokenTxt == "defined")
      Error ("Identifier of conditional symbol expected");

   // search identifier
   TCondSymbol * symbol = LocalSearchSymbol (tokenTxt);
   bool result = (symbol != NULL);

   GetExprToken (); // skip identifier

   return result;
}

int TEnvInput::CondIdent (bool cont)
{
   if (TokenKind != identToken || tokenTxt == "defined")
      Error ("Identifier of conditional symbol expected");

   // search identifier
   TCondSymbol * symbol = LocalSearchSymbol (tokenTxt);

   // !? !! BUG
   if (symbol != NULL && isValidIdent (symbol->value))
      symbol = LocalSearchSymbol (symbol->value);

   int result = 0;

   if (symbol == NULL)
   {
      if (cont)
         Error ("Unknown conditional symbol: " + tokenTxt);
         // cont == false => symbol need not exist
   }
   else
   {
      bool ok;
      StrToInt (symbol->value, result, ok);
      if (! ok && cont)
         Error ("Numeric conditional symbol expected: " + tokenTxt + " (" + symbol->value + ")");
   }

   GetExprToken (); // skip identifier

   if (! cont)
   {
      // macro parameters of possible undefined macro identifier
      if (isSeparator ('('))
      {
         GetExprToken (); // skip left parenthesis

         int level = 0;
         while ( level > 0 || ! isSeparator (')') )
         {
            if (isSeparator ('(')) level ++;
            if (isSeparator (')')) level --;

            GetExprToken (); // next token
         }

         if (! isSeparator (')'))
            Error ("')' expected");

         GetExprToken (); // skip right parenthesis
      }
   }

   return result;
}

/* ---------------------------------------------------------------------- */

int TEnvInput::CondSimpleExpr (bool cont)
{
   int result = 0;

   if (TokenKind == numberToken)
   {
      string txt = tokenTxt;
      int len = txt.length ();
      int inx = len;

      while (inx > 1 &&
             (txt [inx-1] == 'l' ||
              txt [inx-1] == 'L' ||
              txt [inx-1] == 'u' ||
              txt [inx-1] == 'U' ))
      {
         inx--;
      }

      if (inx < len)
         txt = Head (txt, inx);

      bool ok = StrToInt (txt, result);
      if (! ok)
      {
         unsigned long temp;
         ok = StrToNum (txt, temp);
         if (ok)
            result = temp;
         else
            Error ("Invalid number: " + txt);
      }
      GetExprToken (); // skip number
   }
   else if (TokenKind == identToken)
   {
      if (tokenTxt == "defined")
      {
         GetSimpleToken (); // skip defined, no macro expansion

         if (isSeparator ('('))
         {
            GetSimpleToken (); // skip parenthesis, no macro expansion

            result = CondSymbolDefined ();
            if (! isSeparator (')'))
               Error ("')' expected (in defined)");

            GetExprToken (); // skip parenthesis, expand symbol
         }
         else
         {
            result = CondSymbolDefined ();
         }
      }
      else
      {
          result = CondIdent (cont);
      }
   }
   else if (isSeparator ('!'))
   {
      GetExprToken (); // skip exclamation mark
      result = ! CondSimpleExpr (cont);
   }
   else if (isSeparator ('~'))
   {
      GetExprToken (); // skip exclamation mark
      result = ~ CondSimpleExpr (cont);
   }
   else if (isSeparator ('('))
   {
      GetExprToken (); // skip left parenthesis
      result = CondExpr (cont);
      if (! isSeparator (')'))
         Error ("')' expected (in subexpression): " + tokenTxt);
      GetExprToken (); // skip right parenthesis
   }
   else Error ("Simple conditional expression expected: " + tokenTxt);
                // ", tokenTxt = " + tokenTxt +
                // ", TokenKind = " + TokenKindToStr (TokenKind));

   return result;
}

int TEnvInput::CondFactor (bool cont)
{
   int result = CondSimpleExpr (cont);

   while (isSeparator ('*') || isSeparator ('/') || isSeparator ('%'))
   {
      char mark = tokenSym;
      GetExprToken ();

      int temp = CondSimpleExpr (cont);

      // !?
      if (mark == '*')
      {
         result = result * temp;
      }
      else
      {
         if (temp == 0)
            Error ("Zero divide");

         if (mark == '/')
            result = result / temp;
         else
            result = result % temp;
      }
   }

   return result;
}

int TEnvInput::CondTerm (bool cont)
{
   int result = CondFactor (cont);

   while (isSeparator ('+') || isSeparator ('-'))
   {
      char mark = tokenSym;
      GetExprToken ();

      int temp = CondFactor (cont);

      if (mark == '+')
         result = result + temp;
      else
         result = result - temp;
   }

   return result;
}

int TEnvInput::CondShift (bool cont)
{
   int result = CondTerm (cont);

   while (TwoSeparators ('<', '<') || TwoSeparators ('>', '>'))
   {
      char left = TwoSeparators ('<', '<');
      SkipTwoSeparators ();

      int temp = CondTerm (cont);

      if (left)
         result = result << temp;
      else
         result = result >> temp;
   }

   return result;
}

int TEnvInput::CondRel (bool cont)
{
   int result = CondShift (cont);

   while (isSeparator ('<') || isSeparator ('>'))
   {
      char mark = tokenSym;
      GetExprToken (); // skip first

      char mark2 = ' ';
      if (isSeparator ('='))
      {
         mark2 = tokenSym;
         GetExprToken (); // skip second
      }

      int temp = CondShift (cont);

      if (mark == '<')
      {
         if (mark2 == '=')
            result = (result <= temp);
         else
            result = (result < temp);
      }
      else
      {
         if (mark2 == '=')
            result = (result >= temp);
         else
            result = (result > temp);
      }
   }

   return result;
}

int TEnvInput::CondEq (bool cont)
{
   int result = CondRel (cont);

   while (TwoSeparators ('=', '=') || TwoSeparators ('!', '=') )
   {
      bool eq = TwoSeparators ('=', '=');
      SkipTwoSeparators ();

      int temp = CondRel (cont);

      if (eq)
         result = (result == temp);
      else
         result = (result != temp);
   }

   return result;
}

int TEnvInput::CondAnd (bool cont)
{
   int result = CondEq (cont);

   while (isSeparator ('&') && ! TwoSeparators ('&', '&'))
   {
      GetExprToken (); // skip &

      int temp = CondEq (cont);
      result = result & temp;
   }

   return result;
}

int TEnvInput::CondXor (bool cont)
{
   int result = CondAnd (cont);

   while (isSeparator ('^'))
   {
      GetExprToken (); // skip ^

      int temp = CondAnd (cont);
      result = result ^ temp;
   }

   return result;
}

int TEnvInput::CondOr (bool cont)
{
   int result = CondXor (cont);

   while (isSeparator ('|') && ! TwoSeparators ('|', '|'))
   {
      GetExprToken (); // skip |

      int temp = CondXor (cont);
      result = result | temp;
   }

   return result;
}

int TEnvInput::CondLogAnd (bool cont)
{
   int result = CondOr (cont);

   while (TwoSeparators ('&', '&'))
   {
      SkipTwoSeparators ();

      int temp = CondOr (cont && result);
      result = result && temp;
   }

   return result;
}

int TEnvInput::CondLogOr (bool cont)
{
   int result = CondLogAnd (cont);

   while (TwoSeparators ('|', '|'))
   {
      SkipTwoSeparators ();

      int temp = CondLogAnd (cont && ! result);
      result = result || temp;
   }

   return result;
}

int TEnvInput::CondExpr (bool cont)
{
   int result = CondLogOr (cont);

   if (isSeparator ('?'))
   {
      // Debug ("QUESTION mark");
      GetExprToken (); // skip

      // GetSimpleToken();
      int branch1 = CondLogOr (cont && result);

      // DO NOT USE checkSeparator (which uses getToken)
      if (! isSeparator (':'))
         Error ("Colon expected");

      // Debug ("COLON");
      GetExprToken (); // skip

      int branch2 = CondLogOr (cont && ! result);

      result = result ? branch1 : branch2;
   }

   return result;
}

/************************* CONDITIONAL DIRECTIVES *************************/

void TEnvInput::CondDirective (bool inv, bool only_symbol)
{
   bool value;

   if (only_symbol)
   {
      GetSimpleToken (); // skip if, no macro expansion
      value = CondSymbolDefined ();
   }
   else
   {
      GetExprToken (); // skip if and expand next symbol
      value = CondExpr (CondValue); // be tolerant to unknown symbols and macros, when CondValue == false
   }

   if (inv)
      value = ! value;

   if (CondTable.level+1 > cond_level_max)
      Error ("Too many nested conditional directives");

   CondTable.AddCondLevel (value);

   CheckEndOfLine ();
}

/* ---------------------------------------------------------------------- */

void TEnvInput::IfDirective ()
{
   CondDirective (false, false); // CondDirective (inversion, only_symbol)
}

void TEnvInput::IfdefDirective ()
{
   CondDirective (false, true);
}

void TEnvInput::IfndefDirective ()
{
   CondDirective (true, true);
}

void TEnvInput::ElifDirective ()
{
   if (CondTable.current_level == NULL)
      Error ("elif directive without if directive");

   if (CondTable.current_level->value)
      CondTable.current_level->done = true;

   if (CondTable.current_level->done)
   {
      // only read condition and skip following conditional section
      GetExprToken (); // skip elif and expand next symbol
      CondExpr (false);
      CondTable.current_level->value = false;
   }
   else
   {
      CondValue = true; // importat, turn on conditional compilation (before reading first macro symbol)
      GetExprToken (); // skip elif and expand next symbol
      CondTable.current_level->value = CondExpr ();
   }

   CheckEndOfLine ();
}

void TEnvInput::ElseDirective ()
{
   GetSimpleToken (); // skip else

   if (CondTable.current_level == NULL)
      Error ("else directive without if directive");

   if (CondTable.current_level->value)
      CondTable.current_level->done = true;

   CondTable.current_level->value = ! CondTable.current_level->done;

   CheckEndOfLine ();
}

void TEnvInput::EndifDirective ()
{
   GetSimpleToken (); // skip endif

   if (CondTable.current_level == NULL)
      Error ("endif directive without if directive");
   else
      CondTable.RemoveCondLevel ();

   CheckEndOfLine ();
}

/******************************** INCLUDE *********************************/

void TEnvInput::IncludeDirective (bool include_next) // @@
{
   if (! CondValue)
   {
      GetSimpleToken (); // skip include
   }

   if (CondValue) // valid source section
   {
      // skip spaces
      while (! inpEof && inpCh <= ' ' && inpCh != cr && inpCh != lf)
         nextCh ();

      bool simple = (inpCh == quote2 || inpCh == '<');

      // simple == false => expand macro names
      // #define _MULTIARCH_HEADER(arch,name) <arch-name>
      // #include _MULTIARCH_HEADER(i386,name)

      // simple == true => do not expand macro names within < >
      // #define linux 1
      // #include <linux/limits.h>

      if (simple)
         GetSimpleToken ();
      else
         GetExprToken (); // skip include and expand next symbol

      // check only if enabled input, important for <file name>

      string symbolic_name = "";
      string exact_name = "";
      string name = "";
      bool   system_dir = false;

      if (TokenKind == stringToken)
      {
         symbolic_name = tokenTxt;
         exact_name = tokenTxt;
         name = tokenVal;
      }
      else if (isSeparator ('<'))
      {
         if (simple)
            GetSimpleToken ();
         else
            GetExprToken (); // skip '<'

         exact_name = "<";

         while (TokenKind != eolToken &&
                TokenKind != stopToken &&
                TokenKind != finalToken &&
                TokenKind != eofToken &&
                ! isSeparator ('>'))
         {
            name = name + tokenTxt;
            exact_name = exact_name + tokenTxt;
            if (simple)
               GetSimpleToken ();
            else
               GetExprToken (); // store character also into input_storage
         }

         exact_name = exact_name + ">";

         if (! isSeparator ('>'))
            Error ("'>' expected");

         symbolic_name = '<' + name + '>';
         system_dir = true;
      }
      else
      {
         Error ("File name expected: " + tokenTxt);
      }

      GetSimpleToken ();
      CheckEndOfLine (); // check end of line, but do not move to another line
      inside_directive = false; // stop reporting end of line

      string orig_name = name;

         if (! opt.IgnoreIncludes)
         {
            assert (input != NULL);
            assert (real_input != NULL);
            string dir = extractPath (real_input->getFileName ());

            if (! include_next)
            {
               // Debug ("Searching " + name);
               name = incl_directories.LookupInclFile (name, dir, system_dir);
            }
            else
            {
               string this_file_name = input->orig_include_name;
               string next_file_name = name;
               name = incl_directories.LookupInclFile (this_file_name, dir, system_dir, next_file_name);
            }

            if (name == "" && ! opt.TolerateIncludes)
               Error ("Cannot open include file: " + symbolic_name);
         }
         else
         {
            name = "";
         }
   }
}

/**************************** OTHER DIRECTIVES ****************************/

void TEnvInput::PragmaDirective ()
{
   // string name1 = tokenTxt;
   GetSimpleToken (); // skip pragma

   string name2 = tokenTxt;
   // no GetInnerToken (); // skip name

   if (! CustomPragma (name2))
   {
      // read remaining text
      string txt;
      while (TokenKind != eolToken &&
             TokenKind != stopToken &&
             TokenKind != finalToken &&
             TokenKind != eofToken)
      {
         txt = txt + tokenTxt;
         GetSimpleToken ();
      }
   }
}

/* ---------------------------------------------------------------------- */

void TEnvInput::WarningDirective ()
{
   GetSimpleToken (); // skip name

   string txt;

   while (TokenKind != eolToken &&
          TokenKind != stopToken &&
          TokenKind != finalToken &&
          TokenKind != eofToken)
   {
      txt = txt + tokenTxt;
      GetSimpleToken ();
   }

   if (CondValue && ! opt.IgnoreWarningDirectives)
      Warning (txt);
}

void TEnvInput::ErrorDirective ()
{
   GetSimpleToken (); // skip name

   string txt;

   while (TokenKind != eolToken && TokenKind != stopToken && TokenKind != eofToken)
   {
      txt = txt + tokenTxt;
      GetSimpleToken ();
   }

   if (CondValue && ! opt.IgnoreErrorDirectives)
      Error (txt);
}

/* ---------------------------------------------------------------------- */

void TEnvInput::ExtraCh ()
{
   tokenTxt += inpCh; // !?
   tokenSym = 0; // multi character

   nextCh ();
}

/******************************* GET TOKEN ********************************/

void TEnvInput::getToken ()
{
   again:

   ResolveSymbol (); // read one token

   string name = "";

   if ((isSeparator ('#') && opt.HashDirectives && ! TwoSeparators ('#', '#')))
   {
      assert (input != NULL);
      int SaveFileInx = input->tokenFileInx;
      int SaveLinePos = input->tokenLinePos;
      int SaveColPos  = input->tokenColPos;
      int SaveCharPos = input->tokenCharPos;

      line_cont = 0;
      inside_directive = true; // report end of line

      GetSimpleToken (); // skip hash mark ( and comments )

      if (TokenKind != eolToken)
      {
         if (TokenKind != identToken)
            Error ("Directive name expected");

         name = tokenTxt;

         if (name == "define")
            DefineDirective ();

         else if (name == "undef")
            UndefDirective ();

         else if (name == "if")
            IfDirective ();

         else if (name == "ifdef")
            IfdefDirective ();

         else if (name == "ifndef")
            IfndefDirective ();

         else if (name == "elif")
            ElifDirective ();

         else if (name == "else")
            ElseDirective ();

         else if (name == "endif")
            EndifDirective ();

         else if (name == "include")
            IncludeDirective ();

         else if (name == "include_next")
            IncludeDirective (true);

         else if (name == "pragma")
            PragmaDirective ();

         else if (name == "warning")
            WarningDirective ();

         else if (name == "error")
            ErrorDirective ();

         else if (CustomDirective (name))
            /* nothing */ ;

         else Error ("Unknown directive: " + name);
      }
      inside_directive = false; // stop reporting end of line

      assert (input != NULL);
      input->tokenFileInx = SaveFileInx;
      input->tokenLinePos = SaveLinePos;
      input->tokenColPos  = SaveColPos;
      input->tokenCharPos = SaveCharPos;

      // calculate value
      CondValue = CondTable.Calculate ();
   }

   // evaluate condition
   if (! CondValue)
   {
      if (TokenKind == eofToken)
         Error ("Unterminated conditional directive");
      goto again;
   }

   if (TokenKind == eolToken) goto again;
   if (TokenKind == startToken) goto again;
   if (TokenKind == stopToken)  goto again;
}

/****************************** TEXT OUTPUT *******************************/

Output::Output ():
   text_storage (),
   fileName (),
   showMsg (true),
   startLine (false),
   skipEmptyLine (false),
   addEmptyLine (false),
   ignoreEol (false),
   indentation (0),
   linePos (0),
   colPos (0)
{
}

Output::~Output ()
{
   // NO store ();
}

void TTextOutput::Init ()
{
   reset (text_storage); // important for repeated open

   indentation = 0;
   startLine = true;
   skipEmptyLine = false;
   addEmptyLine = false;
   ignoreEol = false;

   linePos = 1;
   colPos = 0;
}

void TTextOutput::open (string name)
{
   setFileName (name);
   Init ();
}

/* ---------------------------------------------------------------------- */

void TTextOutput::message (MessageLevel level, const string msg)
{
   show_message (level, msg, fileName, linePos, colPos);
}

/* ---------------------------------------------------------------------- */

void TTextOutput::debug (const string msg)
{
   message (InfoLevel, msg);
}

void TTextOutput::info (const string msg)
{
   message (InfoLevel, msg);
}

void TTextOutput::warning (const string msg)
{
   message (WarningLevel, msg);
}

void TTextOutput::error (const string msg)
{
   message (ErrorLevel, msg);
}

/* ---------------------------------------------------------------------- */

void TTextOutput::setIndent (int i)
{
   indentation = i;
}

void TTextOutput::indent ()
{
   indentation += 3;
}

void TTextOutput::unindent ()
{
   indentation -= 3;
}

/* ---------------------------------------------------------------------- */

void TTextOutput::putPlainChr (char c)
{
   text_storage.put (c);

   if (c == lf)
   {
      linePos ++;
      colPos = 0;
   }
   else
   {
      colPos ++;
   }
}

void TTextOutput::putPlainSpaces (int n)
{
   text_storage << string (n, ' ');
   colPos += n;
}

void TTextOutput::putPlainStr (const string s)
{
   text_storage << s;
   colPos += s.length (); // no line feed checking
}

void TTextOutput::putPlainData (void * adr, size_t len)
{
   text_storage.write ((const char *) adr, len);
   // no line and column numbers
}

void TTextOutput::putPlainEol ()
{
   // putPlainChr (cr);
   putPlainChr (lf);
}

/* ---------------------------------------------------------------------- */

void TTextOutput::openLine ()
{
   if (addEmptyLine && ! skipEmptyLine)
      putPlainEol ();

   addEmptyLine = false;
   skipEmptyLine = false;

   putPlainSpaces (indentation);
   startLine = false;
}

void TTextOutput::closeLine ()
{
   startLine = true;
}

/* ---------------------------------------------------------------------- */

void TTextOutput::put (const string s)
{
   if (s != "")
   {
      if (startLine)
         openLine ();

      putPlainStr (s);
   }
}

void TTextOutput::putChr (char c)
{
   if (startLine)
      openLine ();

   putPlainChr (c);
}

void TTextOutput::putSpaces (int n)
{
   if (n != 0)
   {
      if (startLine)
         openLine ();

      putPlainSpaces (n);
  }
}

void TTextOutput::putEol ()
{
   if (ignoreEol)
   {
      putPlainChr (' ');
   }
   else
   {
      putPlainEol ();
      closeLine ();
   }
}

void TTextOutput::putCondEol ()
{
   // finish not empty line
   if (! startLine)
      putEol ();
}

void TTextOutput::putLn (const string s)
{
   put (s);
   putEol ();
}

/* ---------------------------------------------------------------------- */

void TTextOutput::emptyLine ()
{
   if (! startLine)
      putEol ();

   addEmptyLine = true;
}

void TTextOutput::noEmptyLine ()
{
   skipEmptyLine = true;
}

/* ---------------------------------------------------------------------- */

void TTextOutput::putBool (bool value)
{
   if (value)
      put ("true");
   else
      put ("false");
}

void TTextOutput::putChar (char value)
{
    put (quoteChr (value, quote1));
}

void TTextOutput::putString (string value)
{
    put (quoteStr (value, quote2));
}

void TTextOutput::putInt (int value)
{
    put (NumToStr (value));
}

void TTextOutput::putLong (long value)
{
    put (NumToStr (value));
}

void TTextOutput::putUInt (unsigned int value)
{
    put (NumToStr (value));
}

void TTextOutput::putULong (unsigned long value)
{
    put (NumToStr (value));
}

void TTextOutput::putFloat (float value)
{
    put (NumToStr (value));
}

void TTextOutput::putDouble (double value)
{
    put (NumToStr (value));
}

/* ---------------------------------------------------------------------- */

#if 0
void Output::send (string s)
{
   // add optional space and print string
   char c = ' ';
   if (s.length () >= 1) c = s[0];

   if (c == '.' || c == ',' || c == ';' || c == ')' || c == ']' ) space = false;

   if (space && skip_spaces) putChr (' ');
   put (s);

   space = true;
   if (c == '(' || c == '[' || c == '.' || c == '#') space = false;
}

/* ---------------------------------------------------------------------- */

void Output::style_space ()
{
   space = true;
}

void Output::style_no_space ()
{
   space = false;
}

void Output::style_new_line ()
{
   // finish line and start new one
   if ((! comments || generated_area) && ! skip_newlines)
   {
      send_cond_eol (); // quoted stencils - use send, not print
   }
}

void Output::style_empty_line ()
{
   // add one empty line
   send_cond_eol (); // quoted stencils - use send, not print
   add_empty_line = true;
}

void Output::style_no_empty_line ()
{
   // wait until end of line and then add empty line
   if (! skip_newlines)
      ignore_empty_line = true;
}

/* ---------------------------------------------------------------------- */

void Output::style_indent ()
{
   // indent recomendation
   style_new_line ();
   indent ();
}

void Output::style_unindent ()
{
   // unindent recomendation
   unindent ();
   style_new_line ();
}
#endif

/* ---------------------------------------------------------------------- */

void Output::readFrom (const string file_name)
{
   ifstream f (file_name.c_str ());

   if (f.good ())
   {
      bool done = false;
      while (! done)
      {
         char data [4096];
         f.read (data, sizeof (data));
         std::streamsize count = f.gcount ();

         if (count > 0)
            putPlainData (data, count);
         else
            done = true;
      }
   }
}

/* ---------------------------------------------------------------------- */

string Output::toString ()
{
   return text_storage.str ();
}

/* ---------------------------------------------------------------------- */

void Output::writeText ()
{
   ofstream f (fileName);
   if (! f.good ())
   {
      Error ("Cannot open file for writing: " + fileName);
   }
   else
   {
      string text = text_storage.str ();
      f << text;

      f.close ();
      if (! f.good ())
         Error ("Error writing file: " + fileName); // simple error form inf.h
   }
}

/* ---------------------------------------------------------------------- */

bool Output::compare ()
{
   bool equal = false;

   ifstream f (fileName);
   if (f.good ())
   {
      ostringstream buffer;
      buffer << f.rdbuf();
      string file_text = buffer.str ();

      string local_text = text_storage.str ();
      equal = file_text == local_text;
      f.close ();
   }

   return equal;
}

/* ---------------------------------------------------------------------- */

void Output::store ()
{
   bool equal = compare ();

   if (! equal)
   {
      writeText ();
      info ("writing " + fileName);
   }
   else
   {
      if (showMsg)
         info ("not modified " + fileName);
   }
}

/* ---------------------------------------------------------------------- */
