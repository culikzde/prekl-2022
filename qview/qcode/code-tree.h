#ifndef CODE_TREE_H
#define CODE_TREE_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <QGraphicsItem>

#include <string>
using std::string;

/* ---------------------------------------------------------------------- */

class Tree : public QTreeWidget
{
    Q_OBJECT

public:
    Tree (QWidget * parent = nullptr);
};

/* ---------------------------------------------------------------------- */

class TreeNode : public QTreeWidgetItem
{
public:
    int src_file;
    int src_line;
    int src_col;

    QObject * src_obj;
    QGraphicsItem * src_gra;

public:
    TreeNode (QTreeWidget * parent = nullptr, QString text = "");
    TreeNode (QTreeWidgetItem * parent = nullptr, QString text = "");
};

/* ---------------------------------------------------------------------- */

int fileNameToIndex (QString fileName);
QString indexToFileName (int inx);

/* ---------------------------------------------------------------------- */

#endif // CODE_TREE_H
