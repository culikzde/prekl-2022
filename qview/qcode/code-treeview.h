
/* code-treeview.h */

#ifndef __CODE_TREEVIEW_H__
#define __CODE_TREEVIEW_H__

/* ---------------------------------------------------------------------- */

#include "code.h"
#include <QTreeView>

/* ---------------------------------------------------------------------- */

class TreeModel : public QAbstractItemModel
{
   // Q_OBJECT

   public:
      TreeModel (Scope * root, QObject *parent = 0);
      ~TreeModel ();

      QVariant data (const QModelIndex & index, int role) const;
      Qt::ItemFlags flags (const QModelIndex & index) const;
      QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
      QModelIndex index (int row, int column, const QModelIndex & parent = QModelIndex ()) const;
      QModelIndex parent (const QModelIndex & index) const;
      int rowCount (const QModelIndex & parent = QModelIndex ()) const;
      int columnCount (const QModelIndex & parent = QModelIndex ()) const;

      Qt::DropActions supportedDropActions () const;
      QStringList mimeTypes() const;
      bool dropMimeData (const QMimeData * data, Qt::DropAction action,
                         int row, int column, const QModelIndex & parent);

   private:
      Scope * rootItem;
};

class TreeView : public QTreeView
{
   Q_OBJECT

   public:
       explicit TreeView (QWidget *parent = 0);
       // virtual ~ TreeView ();

   public:
       void display (Scope * root);

   protected:
      virtual void currentChanged (const QModelIndex & current, const QModelIndex & previous);

   signals:
      void itemSelected (Item * elem);
};

/* ---------------------------------------------------------------------- */

#endif // __CODE_TREEVIEW_H__
