
#ifndef _LEXER_H_
#define _LEXER_H_

#include <string>
#include <iostream>
#include <sstream>
using std::string;
using std::istream;
using std::ostringstream;

/* ---------------------------------------------------------------------- */

typedef unsigned char byte;

/* ---------------------------------------------------------------------- */

int storeFileName (string file_name);

string recallFileName (int index);
int    recallFileIndex (string file_name);

void clearFileNames ();

/* ---------------------------------------------------------------------- */

enum MessageLevel
{
   DebugLevel,    // debugging
   InfoLevel,     // information ( program start, stop, ... ) (not included in message summary)
   WarningLevel,  // warning about possible mistake
   ErrorLevel     // error
};

/* ---------------------------------------------------------------------- */

enum TokenKind
{
   startToken,     // formal first token

   identToken,     // identifier
   numberToken,    // number
   charToken,      // character in single quotes
   stringToken,    // string in double quotes
   separatorToken, // single character separator

   eolToken,       // end of line in directives
   stopToken,      // formal last token
   finalToken,     // end of macro expansion
   eofToken        // end of file
};

/* ---------------------------------------------------------------------- */

typedef int TInputSymbol;

class TInputItem
{
   public:
      TInputSymbol symbol;
      TTokenKind kind;
      string token_text;

   public:
      int file_num;
      int line_num;
      int col_num;

   public:
      TInputItem * next;

      void clear ();

      TInputItem ();
      friend class TInputQueue;
};

class TInputQueue
{
   public:
      TInputItem * first;
      TInputItem * last;

      TInputItem * first_free;
      TInputItem * last_free;

   public:
      void insertLast (TInputItem * item);
      TInputItem * removeFirst ();

      void insertLastFree (TInputItem * item);
      TInputItem * removeFirstFree ();

      void removeAll ();

   public:
      TInputQueue ();
      virtual ~ TInputQueue ();
      friend class TInputItem;
};

/* ---------------------------------------------------------------------- */

class LexerException : public std::exception
{
   public:
      string msg;
      string filename;
      int line;
      int column;

      LexerException
      (
         const string p_msg,
         const string p_filename = "",
         int p_line = 0,
         int p_column = 0
      )
      throw ()
      :
         msg (p_msg),
         filename (p_filename),
         line (p_line),
         column (p_column)
      {
         /* nothing */
      }

      virtual ~ LexerException () throw () { }

      string getMessage ()  { return msg; }
      string getFilename () { return filename; }
      int    getLine ()     { return line; }
      int    getColumn ()   { return column; }

      virtual const char * what () const throw ()
      {
         string txt = msg;
         return txt.c_str ();
      }
};

/* ---------------------------------------------------------------------- */

class TCharInput
{
   protected:
      static const size_t bufSize = 4 * 1024;

      istream * inpStream;

      size_t bufPos;
      size_t bufLen;
      byte buf [bufSize];

      bool releaseStream;

   protected:
      int fileInx; // file number
      int linePos; // line counter
      int colPos;  // column counter
      int charPos; // character counter

      int tokenFileInx;
      int tokenLinePos;
      int tokenColPos;
      int tokenCharPos;

      char inpCh; // current input character
      char prevCh; // previous character (for end of line detection)
      bool prevEol; // end of line detected (but linePos will be incremented with next character)
      bool inpEof; // end of source text

      bool positionInitialized;

   protected:
      virtual void Init ();

      virtual void readBuf ();
      void readData (void * adr, size_t size);

      void getCh ();
      void Message (MessageLevel level, const string msg);

   public:
      void open (string file_name);
      void openStream (istream & stream);
      void openString (string text);

      string getFileName ();
      void setFileName (const string file_name);

   public:
      void debug (const string msg);
      void info (const string msg);
      void warning (const string msg);
      void error (const string msg);

   public:
      TCharInput ();
      virtual ~ TCharInput ();

      friend class TTextInput;
      friend class TMixedInput;
      friend class Lexer;
      friend class Output;
};

/* ---------------------------------------------------------------------- */

class TMixedInput
{
   protected:
      TCharInput * inp;

      char inpCh;  // current inpput character
      bool inpStarted; // true => inpCh contains valid character
      bool inpEof; // end of source text

   protected:
      virtual void Init ();
      virtual void Finish () { }
      void getCh ();

      void setTokenFileInx (int p_file_inx);
      void setTokenLine (int p_line);
      void setTokenCol (int p_col);

      virtual void message (MessageLevel level, const string msg);

   public:
      void open (string name);
      void openStream (istream & stream);
      void openString (string text);

      void close () { Finish (); }

      string getFileName ();
      void setFileName (const string file_name);

      int getTokenFileInx () { return (inp == NULL) ? 0 : inp->tokenFileInx; }
      int getTokenLine () { return (inp == NULL) ? 0 : inp->tokenLinePos; }
      int getTokenCol() { return (inp == NULL) ? 0 : inp->tokenColPos; }

      void debug (const string msg);
      void info (const string msg);
      void warning (const string msg);
      void error (const string msg);

   public:
      TokenKind tokenKind;

      string tokenText; // original token
      string tokenVal; // value of string
      char   tokenSym; // value of separator

      bool   anyWhiteSpace; // true => space or comment encountered (sticky bit)

   public:
      virtual void getToken () { }

   public:
      TMixedInput ();
      virtual ~ TMixedInput ();
};

/* ---------------------------------------------------------------------- */

class TRewindInput : public TMixedInput
{
   public:
      int mark_level;
      TInputSymbol actual;

      TInputItem * present;
      TInputQueue queue;

      bool comments;

   public:
      virtual string conv (TInputSymbol sym) { return "???"; }
      virtual void translate () { }

   public:
      inline bool isSymbol (TInputSymbol s) // test current symbol
      {
         return actual == s;
      }

      inline bool testSymbols (const unsigned char table []) // test current symbol with table
      {
         int pos = actual / 8;
         int bit = actual % 8;
         return table [pos] & (1 << bit);
      }

   public:
      TInputSymbol LA (int i);
      void match (TInputSymbol s);
      void next ();

   private:
      void pick (); // recall from queue
      void remember (); // store data to queue
   public:
      void readToken ();

   public:
      TInputItem * mark ();
      void rewind (TInputItem * pos);
      void skipRewind ();

      void stop (string msg);
      void stop ();

   protected:
      virtual void Init ();

   public:
      TRewindInput ();
      virtual ~ TRewindInput ();
};

/* ---------------------------------------------------------------------- */

class TTextInput : public TRewindInput
{
   protected:
      ostringstream text_storage;
      ostringstream value_storage;

   protected:
      virtual void Init ();

      void nextCh ();
      void textCh ();
      void spaceCh ();

      char character ();
      char pascalCharacter (char pascal_quote);

      void getIdent (bool to_lower_case, bool to_upper_case);
      void getNumber (bool pascal_number);
      void getChar (bool pascal_string);
      void getString (bool pascal_string);

      void storeSeparator (char ch);
      void storeLongSeparator (string s);
      virtual void getSeparator ();

      void skipLine ();
      bool getComment ();
      bool getPascalComment ();

      void getSpace ();
      void getEol ();
      bool getBackslash ();

   public:
      virtual void getToken ();

      bool isIdent () { return tokenKind == identToken; }
      bool isNumber () { return tokenKind == numberToken; }
      bool isChar () { return tokenKind == charToken; }
      bool isString () { return tokenKind == stringToken; }
      bool isSeparator () { return tokenKind == separatorToken; }
      bool isEof () { return tokenKind == eofToken; }

      bool isIdent (const string par);
      bool isSeparator (char par);

      void checkIdent (const string par);
      void checkSeparator (char par);
      void checkEof ();

      bool nextSeparator (char par);

      string readIdent ();
      char   readChar ();
      string readNumber ();
      string readString ();

      bool   readBool ();

      string readSignedNumber (); // including minus sign
      string readValue (); // identifier, number, character or string

      int    readInt ();
      long   readLong ();

      unsigned int  readUInt ();
      unsigned long readULong ();

      float  readFloat ();
      double readDouble ();

      string readRestOfLine ();

      TTextInput ();
      virtual ~ TTextInput ();
};

/* ---------------------------------------------------------------------- */

enum TInclKind
{
   normal_incl, // used by include <file_name> and include "file_name"
   system_incl, // only for include <file_name>
   user_incl    // only for include "file_name"
};

struct TInclDir
{
   string name;
   TInclKind kind;
   TInclDir * next;

   TInclDir ();
};

struct TInclDirectories
{
   TInclDir * first_dir;
   TInclDir * last_dir;

   void InsInclDir (string name, TInclKind kind = normal_incl);
   void AddInclDir (string name, TInclKind kind = normal_incl);

   string FindInclFile (string name,
                        bool system_file = false,
                        string include_next = "");

   string LookupInclFile (string name,
                          string dir = "",
                          bool system_file = false,
                          string include_next = "");

   TInclDirectories ();
   ~ TInclDirectories ();
};

/* ---------------------------------------------------------------------- */

struct TCondSymbols;

struct TCondParam
{
   string name;
   bool multi; // true => ... parameter
   TCondParam * next;

   TCondParam ();
};

struct TCondSymbol
{
   string name;
   string value;

   bool with_params; // function like macro, defined with (possible empty) parameter list

   bool local_param; // used as local macro parameter
   string orig_value; // non-expanded parameter value

   TCondParam * first_param;
   TCondParam * last_param;

   TCondSymbol * prev;
   TCondSymbol * next;
   TCondSymbols * above;

   int fileInx; // location of symbol defininiton
   int linePos;
   int colPos;
   int charPos;

   void InsParam (string name, bool multi = false);
   void DeleteParameters ();

   TCondSymbol ();
   ~ TCondSymbol ();
};

struct TCondSymbols
{
   TCondSymbol * first_symbol;
   TCondSymbol * last_symbol;

   void InsSymbol (TCondSymbol * item);

   TCondSymbol * SearchSymbol (string name);
   string RecallSymbol (string name);

   TCondSymbol * DefineNewSymbol (string name, string value);

   bool IsDefinedSymbol (string name);
   void DefineSymbolWithParams (string name, string params, string value);
   void DefineSymbol (string name, string value);
   void UndefineSymbol (string name);

   TCondSymbols ();
   ~ TCondSymbols ();
};

/* ---------------------------------------------------------------------- */

struct TCondLevel
{
   bool value; // true => valid branch
   bool done; // true => some valid branch already evaluated

   TCondLevel * prev;
   TCondLevel * next;

   TCondLevel ();
};

struct TCondLevels
{
   TCondLevel * first_level;
   TCondLevel * last_level;

   TCondLevel * current_level;

   int level;

   void InsCondLevel (TCondLevel * item);

   void AddCondLevel (bool value);
   void RemoveCondLevel ();
   void RemoveAllLevels ();
   bool Calculate ();

   TCondLevels ();
   ~ TCondLevels ();
};

/* ---------------------------------------------------------------------- */

struct TEnvOptions
{
   bool LowerCaseIdentifiers; // true => conver identifiers to lower case
   bool UpperCaseIdentifiers; // true => conver identifiers to upper case

   bool PascalNumbers; // true => require digit before and after decimal point
   bool PascalStrings; // true => strings with single quote

   bool PascalComments; // true => (* ... *) and { ... } comments

   bool HashDirectives;   // true => directives starts with hash mark (normal situation)

   bool IgnoreIncludes; // true => do not read include files, write original directive to output
   bool TolerateIncludes; // true => if include file is not found, write original directive to output

   bool IgnoreWarningDirectives; // true => warning directives are ignored
   bool IgnoreErrorDirectives; // true => error and warning directives are ignored

   bool IgnoreMacroExpansion; // true => do not expand macros

   TEnvOptions ();
};

/* ---------------------------------------------------------------------- */

class TSubInput : public TCharInput
{
   public:
      TCondSymbols local_symbols; // macro parameters

      TSubInput * prev;
      TSubInput * next;

      string title; // macro name, check for recursion
      string orig_include_name; // original include file name, without additional directory

      bool is_include_file;

      bool add_new_line;
      bool add_final_token;

      TSubInput * save_real_input; // file input (not macro input)

      virtual string GetMsg () { return "included from this position"; }
      virtual string GetInfo () { return getFileName (); }

      TSubInput ();
      virtual ~ TSubInput ();
};

class TStrSubInput : public TSubInput
{
   private:
      int inx; // position in 'value' variable

   public:
      string value; // read source text from this variable

      virtual void readBuf ();

      virtual string GetMsg () { return "called from this position"; }
      virtual string GetInfo () { return value; }

      TStrSubInput ();
      virtual ~ TStrSubInput ();
};

/* ---------------------------------------------------------------------- */

class Lexer : public TTextInput
{
   private:
      TSubInput * first_input; // original file or string input (not include or macro)
      TSubInput * real_input; // original file, string or include input (not macro input)
      TSubInput * input; // current character stream

      int sub_level;

      int line_cont; // count directive lines, only for opt.KeepEmptySections

      bool inside_directive; // true => report line end

   protected:
      TCondLevels CondTable; // nested conditional branches
      bool CondValue; // calculated result

   public:
      bool compound_separators; // multi character separators

   public:
      TCondSymbols symbols;
      TInclDirectories incl_directories;

      TEnvOptions opt;

   public:
      bool GetCondValue () { return CondValue; }

   private:
      void GetSimpleToken ();

      void OpenSubInput (TSubInput * item);
      void CloseSubInput ();
      void CleanSubInput ();

      void OpenInclude (string file_name, string orig_name);
      void OpenMacro (TSubInput * item, bool add_stop = false);
      void OpenText (const string text, bool add_stop = false);

      void ExtraCh (); // used by multi-character separators

      TCondSymbol * LocalSearchSymbol (const string name);
      TCondSymbol * MacroSearchSymbol (TStrSubInput * macro_input, const string name);

      string ReadParameter (bool multi);
      string ExpandParameter (const string orig_value);
      void   ExpandParameters (TStrSubInput * macro_input, TCondSymbol * symbol);

      string CompoundItem (TStrSubInput * macro_input, const string name);
      void   StringSymbol (TStrSubInput * macro_input);
      void   CompoundSymbol (TStrSubInput * macro_input);

      void ExpandAgain (TCondSymbol * symbol, TStrSubInput * macro_input);
      void ExpandSymbol (TCondSymbol * symbol);
      void ResolveSymbol ();

      void SkipSpaces ();
      void CheckEndOfLine ();

      bool ThreeDots ();
      void DefineParameter (TCondSymbol * symbol);

      bool TwoSeparators (char c1, char c2);
      void SkipTwoSeparators ();
      bool CondSymbolDefined ();

      int CondIdent (bool cont);
      int CondSimpleExpr (bool cont);
      int CondFactor (bool cont);
      int CondTerm (bool cont);
      int CondShift (bool cont);
      int CondRel (bool cont);
      int CondEq (bool cont);
      int CondAnd (bool cont);
      int CondXor (bool cont);
      int CondOr (bool cont);
      int CondLogAnd (bool cont);
      int CondLogOr (bool cont);
      int CondExpr (bool cont = true);

   protected:
      void GetExprToken ();

      void DefineDirective ();
      void UndefDirective ();
      void CondDirective (bool inv, bool only_symbol);
      void IfDirective ();
      void IfdefDirective ();
      void IfndefDirective ();
      void ElifDirective ();
      void ElseDirective ();
      void EndifDirective ();
      void IncludeDirective (bool include_next = false);
      void PragmaDirective ();
      void WarningDirective ();
      void ErrorDirective ();

   protected:
      virtual void Init ();
      virtual void Finish ();

      void SetStartToken ();
      void SetStringToken (const string txt);

      virtual bool CustomDirective (const string name) { return false; }
      virtual bool CustomPragma (const string name) { return false; }

      virtual void OpeningInclude () { }
      virtual void ClosingInclude () { }

   public:
      void Include (string file_name);
      static string TokenKindToStr (TTokenKind kind);

      string ParametersToText (TCondSymbol * symbol);
      string DefineToText (TCondSymbol * symbol);

   public:
      virtual void getToken ();
      void NextToken () { getToken (); }

      void CheckEnd ();

      Lexer ();
      virtual ~ Lexer ();
};

/* ---------------------------------------------------------------------- */

class Output
{
   protected:
      ostringstream text_storage;

   private:
      string fileName;
      bool showMsg;

   protected:
      bool startLine;
      bool skipEmptyLine;
      bool addEmptyLine;
      bool ignoreEol;

   private:
      int indentation;

   public:
      int linePos;
      int colPos;

   protected:
      virtual void Init ();
      virtual void Finish () { }

   public:
      void open (string name);
      void close () { Finish (); }

      string getFileName () { return fileName; }
      void setFileName (const string p_filename) { fileName = p_filename; }

      bool compare ();
      void store ();
      void writeText ();

      string toString ();

   protected:
      virtual void message (MessageLevel level, const string msg);

   public:
      void debug (const string msg);
      void info (const string msg);
      void warning (const string msg);
      void error (const string msg);

   public:
      int  getIndent () { return indentation; }
      void setIndent (int i);

      void indent ();
      void unindent ();

      void incIndent () { indent (); }
      void decIndent () { unindent (); }

   protected:
      void putPlainChr (char c);
      void putPlainSpaces (int n);
      void putPlainStr (const string s);
      void putPlainData (void * adr, size_t len);
      void putPlainEol ();

      virtual void openLine ();
      virtual void closeLine ();

   public:
      void put (const string s);
      void putChr (char c);
      void putSpaces (int n);
      void putEol ();
      void putCondEol ();
      void putLn (const string s);

      void emptyLine ();
      void noEmptyLine ();

      void putBool (bool value);

      void putChar (char value);
      void putString (string value);

      void putInt (int value);
      void putLong (long value);

      void putUInt (unsigned int value);
      void putULong (unsigned long value);

      void putFloat (float value);
      void putDouble (double value);

      void readFrom (const string file_name);

   public:
      void send (string s);

   public:
      void style_space ();
      void style_no_space ();
      void style_new_line ();
      void style_empty_line ();
      void style_no_empty_line ();
      void style_indent ();
      void style_unindent ();

   public:
      Output ();
      virtual ~ Output ();
};

/* ---------------------------------------------------------------------- */

#endif /* _LEXER_H__ */
