namespace Example
{
    // paper ("lime");
    enum Abc { a, b, c, d, e };

    enum Color { red, blue, green, yellow, orange, cornflowerblue };

    class string; 
    class Entry
    {
        // paper ("yellow");
        bool boolean;
        int number; 
        Color color;
        string text;
        Color z;
        Something t;
        Something u;

        void show (string txt, int size)
        {
           /*
           string t = "";
           int n = 1;
           for (int i = 1; i <= 10; i++)
               n = n + i;
           warning ("Show function warning");
           error ("Show function error");
           tooltip ("Show function");
           add (1, 2);
           ink ("red");
           paper ("cornflowerblue");
           text = "show";
           unknown1 = unknown2 + 2;
           unknown2 = 3;
           */
        }

        void hide () { /* text = "hide"; show (); */ }

        /*
        QPushButton button
        {
           // string text = "Abc";
           // int n = 1;
        }

        QLineEdit edit
        {
           text = "Some text";
        }
        */
    };
}
