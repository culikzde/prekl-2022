
/* prop.cc */

#include "prop.h"

#include <QHeaderView>
#include <QAbstractItemModel>
#include <QItemDelegate>
#include <QTreeWidgetItem>

#include <QPainter>
#include <QApplication>

#include <QPalette>
#include <QBitmap>

#include <QColorDialog>
#include <QFontDialog>

#include <QMetaProperty>

#include <cassert>

#include <iostream>
#include <string>
using namespace std;

/* ---------------------------------------------------------------------- */

PropItem * ItemDelegate::getNode (const QModelIndex & index) const
{
   PropItem * node = NULL;

   if (index.isValid ())
   {
      node = static_cast < PropItem * > (index.internalPointer ());
      assert (node != NULL);
   }

   return node;
}

QWidget * ItemDelegate::createEditor (QWidget * parent,
                                      const QStyleOptionViewItem & option,
                                      const QModelIndex & index) const
{
   QWidget * editor = NULL;

   PropItem * node = getNode (index);
   if (node != NULL && index.column () == 1)
   {
      editor = node->createEditor (parent);
   }

   return editor;
}

void ItemDelegate::setEditorData (QWidget * param_editor,
                                  const QModelIndex & index) const
{
   ItemEditor * editor = dynamic_cast < ItemEditor * > (param_editor);
   assert (editor != NULL);

   PropItem * node = getNode (index);
   if (node != NULL)
   {
      if (editor->line_edit != NULL)
      {
         editor->line_edit->setText (node->value.toString ());
      }
      if (editor->spin_box != NULL)
      {
         editor->spin_box->setValue (node->value.toInt ());
      }
      if (editor->real_box != NULL)
      {
         editor->real_box->setValue (node->value.toDouble ());
      }
   }
}

void ItemDelegate::setModelData (QWidget * param_editor,
                                 QAbstractItemModel * model,
                                 const QModelIndex & index) const
{
   ItemEditor * editor = dynamic_cast < ItemEditor * > (param_editor);
   assert (editor != NULL);

   PropItem * node = getNode (index);
   if (node != NULL)
   {
      if (editor->line_edit != NULL)
      {
         QString txt = editor->line_edit->text ();
         node->value = txt;
      }
      if (editor->spin_box != NULL)
      {
         int n = editor->spin_box->value ();
         node->value = n;
      }
      if (editor->real_box != NULL)
      {
         double x = editor->real_box->value ();
         node->value = x;
      }
   }
}

/* ---------------------------------------------------------------------- */

void ItemDelegate::paint (QPainter * painter,
                          const QStyleOptionViewItem & opt,
                          const QModelIndex & index) const
{
   QStyleOptionViewItem option = opt;

   #if 0
   if (index.column() == 0)
      option.rect.setX (0);

   QColor c;
   if ((index.row () & 1) == 0)
      c = evenColor;
   else
      c = oddColor;
   QBrush br = QBrush (c);

   painter->save();
   painter->setBrushOrigin (option.rect.x(), option.rect.y());
   painter->fillRect (option.rect, br);
   painter->restore();
   #endif

   QItemDelegate::paint (painter, option, index);

   const QColor color = static_cast<QRgb> (QApplication::style()->styleHint (QStyle::SH_Table_GridLineColor, &option));
   const QPen oldPen = painter->pen();
   painter->setPen (QPen (color));

   painter->drawLine (option.rect.right(), option.rect.y(), option.rect.right(), option.rect.bottom());
   painter->drawLine (0 /* option.rect.x() */, option.rect.bottom(), option.rect.right(), option.rect.bottom());

   painter->setPen (oldPen);
}

// see ColorDelegate::paint in qt-4.8.0/tools/designer/src/components/propertyeditor/paletteeditor.cpp

/*
void ItemDelegate::updateEditorGeometry (QWidget * param_editor,
                                         const QStyleOptionViewItem & option,
                                         const QModelIndex &index) const
{
    QItemDelegate::updateEditorGeometry (param_editor, option, index);
    param_editor->setGeometry (param_editor->geometry().adjusted(0, 0, -1, -1));
}

QSize ItemDelegate::sizeHint (const QStyleOptionViewItem & option,
                              const QModelIndex & index) const
{
    return QItemDelegate::sizeHint (option, index) + QSize (4, 4);
}
*/

/* ---------------------------------------------------------------------- */

void ItemEditor::spin_box_value_changed (int n)
{
    cout << "spin box : value changed : " << n << endl;
    if (item != NULL)
       item->spinModified (n);
}

void ItemEditor::real_box_value_changed (double x)
{
    cout << "double spin box : value changed : " << x << endl;
    if (item != NULL)
       item->realModified (x);
}

void ItemEditor::line_edit_text_changed (QString s)
{
    cout << "line edit : text changed : " << s.toStdString () << endl;
}

void ItemEditor::line_edit_return_pressed ()
{
    cout << "line edit : return pressed" << endl;
    QString s = line_edit->text ();
    // if (item != NULL && item->table != NULL)
    //   item->table->storeVariant (item->name, s);
    if (item != NULL)
       item->editModified (s);
}

void ItemEditor::combo_box_activated (int inx)
{
    cout << "combo box : activated : " << inx << endl;
    if (item != NULL)
       item->listClick (inx);
}

void ItemEditor::button_clicked ()
{
    cout << "button : clicked" << endl;
    if (item != NULL)
    {
       if (item->fontEnabled)
       {
           bool ok;
           QFontDialog::getFont (&ok);
       }
       else if (item->colorEnabled)
       {
           QColorDialog::getColor ();
       }
       else
       {
          item->dialogExecute ();
       }
    }
}

/* ---------------------------------------------------------------------- */

QWidget * PropItem::createEditor (QWidget * parent)
{
   ItemEditor * editor = new ItemEditor (parent);
   editor->item = this;

   QHBoxLayout * layout = new QHBoxLayout (editor);
   layout->setMargin (0);
   layout->setSpacing (0);
   editor->setLayout (layout);

   if (this->spinEnabled)
   {
      editor->spin_box = new QSpinBox (editor);
      layout->addWidget (editor->spin_box);

      // spin box ... item changed
      QObject::connect (editor->spin_box, SIGNAL (valueChanged (int)),
                        editor, SLOT (spin_box_value_changed (int)));
   }
   else if (this->realEnabled)
   {
      editor->real_box = new QDoubleSpinBox (editor);
      layout->addWidget (editor->real_box);

      // double spin box ... item changed
      QObject::connect (editor->real_box, SIGNAL (valueChanged (int)),
                        editor, SLOT (real_box_value_changed (int)));
   }
   else if (this->listEnabled)
   {
      editor->combo_box = new QComboBox (editor);
      layout->addWidget (editor->combo_box);

      int cnt = this->list.count ();
      for (int inx = 0; inx < cnt; inx++)
         editor->combo_box->addItem (this->list [inx]);

      QPalette p = editor->combo_box->palette ();
      p.setColor (QPalette::Button, p.color (QPalette::Base));
      editor->combo_box->setPalette (p);

      // combo box ... item changed
      QObject::connect (editor->combo_box, SIGNAL (activated (int)),
                        editor, SLOT (combo_box_activated (int)));
   }
   else
   {
      editor->line_edit = new QLineEdit (editor);
      layout->addWidget (editor->line_edit);

      // line_edit ... text changed
      QObject::connect (editor->line_edit, SIGNAL (textChanged (const QString &)),
                        editor, SLOT (line_edit_text_changed (QString)));

      // line_edit ... return preesed
      QObject::connect (editor->line_edit, SIGNAL (returnPressed ()),
                        editor, SLOT (line_edit_return_pressed ()));
   }

   if (this->dialogEnabled || this->fontEnabled || this->colorEnabled)
   {
      editor->button = new QPushButton (editor);
      editor->button->setText ("...");
      editor->button->setMaximumWidth (32);
      layout->addWidget (editor->button);

      QObject::connect (editor->button, SIGNAL (clicked ()),
                        editor, SLOT (button_clicked ()));
   }

   return editor;
}

/* ---------------------------------------------------------------------- */

void PropItem::addNumber (QString name, int n)
{
   QTreeWidgetItem * node = new QTreeWidgetItem;
   node->setText (0, name);
   node->setText (1, QString::number (n));
   this->addChild (node);
}


/* ---------------------------------------------------------------------- */

const QPalette::ColorGroup groups [ ] =
{
   QPalette::Active,
   QPalette::Inactive,
   QPalette::Disabled
};

const QString group_names [ ] =
{
   "Active",
   "Inactive",
   "Disabled"
};

const QPalette::ColorRole roles [ ] =
{
    QPalette::WindowText,
    QPalette::Button,
    QPalette::Light,
    QPalette::Midlight,
    QPalette::Dark,
    QPalette::Mid,
    QPalette::Text,
    QPalette::BrightText,
    QPalette::ButtonText,
    QPalette::Base,
    QPalette::Window,
    QPalette::Shadow,
    QPalette::Highlight,
    QPalette::HighlightedText,
    QPalette::Link,
    QPalette::LinkVisited,
    QPalette::AlternateBase,
    QPalette::NoRole,
    QPalette::ToolTipBase,
    QPalette::ToolTipText,
    // QPalette::Background,
    // QPalette::Foreground,
};

const QString role_names [ ] =
{
    "WindowText",
    "Button",
    "Light",
    "Midlight",
    "Dark",
    "Mid",
    "Text",
    "BrightText",
    "ButtonText",
    "Base",
    "Window",
    "Shadow",
    "Highlight",
    "HighlightedText",
    "Link",
    "LinkVisited",
    "AlternateBase",
    "NoRole",
    "ToolTipBase",
    "ToolTipText",
 };


void PropItem::addPaletteItem (QString name, QPalette::ColorGroup group, QPalette::ColorRole role, QPalette & p)
{
   QTreeWidgetItem * node = new QTreeWidgetItem;
   node->setText (0, name);
   QColor c = p.color (group, role);
   QBrush b = p.brush (group, role);
   node->setText (1, c.name ());
   node->setData (1, Qt::DecorationRole, c);
   this->addChild (node);
}

void PropItem::addPaletteItems (QPalette & p)
{
   assert (sizeof (roles) / sizeof (roles [0]) == 20);
   assert (sizeof (role_names) / sizeof (role_names [0]) == 20);

   assert (sizeof (roles) / sizeof (roles [0]) == sizeof (role_names) / sizeof (role_names [0]));
   assert (sizeof (groups) / sizeof (groups [0]) == sizeof (group_names) / sizeof (group_names [0]));

   int group_index = 0;
   for (QPalette::ColorGroup group : groups)
   {
      int role_index = 0;
      for (QPalette::ColorRole role : roles)
      {
         QString name = group_names [group_index] + " " + role_names [role_index];
         addPaletteItem (name, group, role, p);
         role_index ++;
      }
      group_index ++;
   }
}

/* ---------------------------------------------------------------------- */

void PropItem::setupItem ()
{
   // this->setText (0, name);
   this->setText (0, name + " : " + value.typeName());
   this->setToolTip (0, value.typeName());

   QVariant v = value;
   QVariant::Type t = v.type();

   QString s;

   if (t == QVariant::Bool)
   {
      if (v.toBool ())
         this->setData (1, Qt::CheckStateRole, Qt::Checked);
      else
         this->setData (1, Qt::CheckStateRole, Qt::Unchecked);

      this->listEnabled = true;
      this->list = QStringList () << "false" << "true";
   }
   else if (t == QVariant::Int || t == QVariant::UInt || t == QVariant::LongLong || t == QVariant::ULongLong  )
   {
      this->spinEnabled = true;
   }
   else if (t == QVariant::Double )
   {
      this->realEnabled = true;
   }
   else if (t == QVariant::Rect)
   {
       QRect r = v.toRect ();
       s = QString::number (r.x ()) + ", " +
           QString::number (r.y ()) +  ", " +
           QString::number (r.width ()) + " x " +
           QString::number (r.height ());
       this->setText (1, s);
       addNumber ("x", r.x ());
       addNumber ("y", r.y ());
       addNumber ("width", r.width ());
       addNumber ("height", r.height ());
       // displayNumber (this, "x", getRectX, setRectX);
       // displayNumber (this, "y", getRectY, setRectY);
       // displayNumber (this, "width", getRectWidth, setRectWidth);
       // displayNumber (this, "height", getRectHeight, setRectHeight);
   }
   else if (t == QVariant::Point)
   {
       QPoint r = v.toPoint ();
       s = QString::number (r.x ()) + ", " + QString::number (r.y ());
       this->setText (1, s);
       addNumber ("x", r.x ());
       addNumber ("y", r.y ());
   }
   else if (t == QVariant::Size)
   {
       QSize r = v.toSize ();
       s = QString::number (r.width ()) + " x " + QString::number (r.height ());
       this->setText (1, s);
       addNumber ("width", r.width ());
       addNumber ("height", r.height ());
   }
   else if (t == QVariant::RectF)
   {
       QRectF r = v.toRectF ();
       s = QString::number (r.x ()) + ", " +
           QString::number (r.y ()) +  ", " +
           QString::number (r.width ()) + " x " +
           QString::number (r.height ());
       this->setText (1, s);
       addReal ("x", r.x ());
       addReal ("y", r.y ());
       addReal ("width", r.width ());
       addReal ("height", r.height ());
   }
   else if (t == QVariant::PointF)
   {
       QPointF r = v.toPointF ();
       s = QString::number (r.x ()) + ", " + QString::number (r.y ());
       this->setText (1, s);
       addReal ("x", r.x ());
       addReal ("y", r.y ());
   }
   else if (t == QVariant::Palette)
   {
       s = v.toString();
       this->setText (1, s);
       QPalette p = v.value <QPalette> ();
       addPaletteItems (p);
   }
   else if (t == QVariant::StringList)
   {
       this->setText (1, v.toString());
       QStringList list = v.toStringList();
       int inx = 0;
       for (QString val : list)
       {
          addString (QString::number (inx), val);
          inx ++;
       }
   }
   else if (t == QVariant::List)
   {
       this->setText (1, v.toString());
       QList <QVariant> list = v.toList();
       int inx = 0;
       for (QVariant val : list)
       {
          addVariant (QString::number (inx), val);
          inx ++;
       }
   }
   else if (t == QVariant::Map)
   {
       this->setText (1, v.toString());
       QMap <QString, QVariant> dict = v.toMap ();
       for (QString key : dict.keys ())
          addVariant (key, dict [key]);
   }
   else if (t == QVariant::Hash)
   {
       this->setText (1, v.toString());
       QHash <QString, QVariant> dict = v.toHash ();
       for (QString key : dict.keys ())
          addVariant (key, dict [key]);
   }
   else
   {
       s = v.toString();
       this->setText (1, s);
   }

   if (t == QVariant::Color)
      this->setData (1, Qt::DecorationRole, v.value <QColor>());
   if (t == QVariant::Pen)
      this->setData (1, Qt::DecorationRole, v.value <QPen>());
   if (t == QVariant::Brush)
      this->setData (1, Qt::DecorationRole, v.value <QBrush>());

   if (t == QVariant::Bitmap)
      this->setData (1, Qt::DecorationRole, v.value <QBitmap>());
   if (t == QVariant::Pixmap)
      this->setData (1, Qt::DecorationRole, v.value <QPixmap>());
   if (t == QVariant::Image)
      this->setData (1, Qt::DecorationRole, v.value <QImage>());
   if (t == QVariant::Icon)
      this->setData (1, Qt::DecorationRole, v.value <QIcon>());
   if (t == QVariant::Cursor)
      this->setData (1, Qt::DecorationRole, v.value <QCursor>());

   if (t == QVariant::Font)
   {
      this->fontEnabled = true;
   }
   else if (t == QVariant::Color ||
            t == QVariant::Pen ||
            t == QVariant::Brush )
   {
      this->colorEnabled = true;
   }
   else if (
            t == QVariant::Bitmap ||
            t == QVariant::Pixmap ||
            t == QVariant::Image ||
            t == QVariant::Icon ||
            t == QVariant::Cursor )
   {
      this->dialogEnabled = true;
   }
}

/* ---------------------------------------------------------------------- */

Prop::Prop (QWidget * parent) :
   QTreeWidget (parent)
{
   setColumnCount (2);
   headerItem ()->setText (0, "Name");
   headerItem ()->setText (1, "Value");
   header ()->setStretchLastSection (true);

   setAlternatingRowColors (true);
   // setEditTriggers (QAbstractItemView::CurrentChanged);
   setEditTriggers (QAbstractItemView::DoubleClicked);

   ItemDelegate * delegate = new ItemDelegate;
   // delegate->evenColor = parent->palette().color (QPalette::Normal, QPalette::Base);
   // delegate->oddColor = parent->palette().color (QPalette::Normal, QPalette::AlternateBase);
   setItemDelegate (delegate);
}

/* ---------------------------------------------------------------------- */

typedef int  ( * get_int_func ) (QVariant);
typedef void ( * set_int_func ) (QVariant & , int);

void displayNumber (PropItem * target,
                    QString name,
                    get_int_func get_func,
                    set_int_func set_func)
{
   int value =  get_func (target->value);

   PropItem * node = new PropItem ();
   node->setText (0, name);
   node->setText (1, QString::number (value));
   target->addChild (node);
}

int getRectX (QVariant v) { return v.toRect().x(); }
int getRectY (QVariant v) { return v.toRect().y(); }
int getRectWidth (QVariant v) { return v.toRect().width(); }
int getRectHeight (QVariant v) { return v.toRect().height(); }

void setRectX (QVariant & v, int x) { v.toRect().setX(x); }
void setRectY (QVariant & v, int y) { v.toRect().setY(y); }
void setRectWidth (QVariant & v, int w) { v.toRect().setWidth(w); }
void setRectHeight (QVariant & v, int h) { v.toRect().setHeight(h); }

/* ---------------------------------------------------------------------- */

void Prop::displayVariant (QString name, QVariant value)
{
   PropItem * node = new PropItem ();
   node->name = name;
   node->value = value;
   node->table = this;
   node->setupItem ();
   addTopLevelItem (node);
}

/* ---------------------------------------------------------------------- */

void Prop::storeVariant (QString name, QVariant value)
{
#if 0
   if (elem != NULL)
   {
      elem->setProperty (name, value);
      cout << "set property " << name.toStdString () << " to " << value.toString ().toStdString () << endl;
   }
#endif
}

/* ---------------------------------------------------------------------- */

/*
void Prop::display_qobject (QObject * obj)
{
   clear ();

   if (obj != NULL)
   {
      const QMetaObject * cls = obj->metaObject();
      int cnt = cls->propertyCount();
      for (int inx = 0; inx < cnt ; inx ++)
      {
          QMetaProperty p = cls->property (inx);

          ReflectionItem * item = new ReflectionItem (this);
          item->obj = obj;
          item->prop = p;

          QVariant v = p.read (obj);
          QVariant::Type t = v.type();

          QString s;

          if (p.isEnumType())
          {
              QMetaEnum e = p.enumerator ();
              s = e.valueToKey (v.toInt ());
          }
          else if (p.isFlagType())
          {
              QMetaEnum e = p.enumerator ();
              s = e.valueToKeys (v.toInt ());
          }
          else if (t == QVariant::Rect)
          {
              QRect r = v.toRect ();
              s = QString::number (r.x ()) + ", " + QString::number (r.y ()) +  ", " +
                  QString::number (r.width ()) + " x " + QString::number (r.height ());
         }
          else if (t == QVariant::Point)
          {
              QPoint r = v.toPoint ();
              s = QString::number (r.x ()) + ", " + QString::number (r.y ());
          }
          else if (t == QVariant::Size)
          {
              QSize r = v.toSize ();
              s = QString::number (r.width ()) + " x " + QString::number (r.height ());
          }
          else
          {
              s = v.toString();
          }

          item->setText (0, QString ("") + p.name () + " : " + p.typeName ());
          item->setText (1, s);

          if (t == QVariant::Color)
             item->setData (1, Qt::DecorationRole, v.value <QColor>());
          if (t == QVariant::Icon)
             item->setData (1, Qt::DecorationRole, v.value <QIcon>());
          if (t == QVariant::Pixmap)
             item->setData (1, Qt::DecorationRole, v.value <QPixmap>());

          #if 0
          if (t == QVariant::Brush)
             item->setData (1, Qt::DecorationRole, v.value <QPixmap>());
          if (t == QVariant::Cursor)
             item->setData (1, Qt::DecorationRole, v.value <QPixmap>());
          if (t == QVariant::Bitmap)
             item->setData (1, Qt::DecorationRole, v.value <QPixmap>());
          if (t == QVariant::Image)
             item->setData (1, Qt::DecorationRole, v.value <QPixmap>());
          #endif

          // item->setData(1, Qt::CheckStateRole, Qt::Checked);

          this->addTopLevelItem (item);

          if (QString (p.name()) == "palette")
          {
              ColorItem * item = new ColorItem (this);
              item->obj = obj;
              item->role = QPalette::Foreground; // !?
              item->setText (0, "Foreground");
              item->setData (1, Qt::DecorationRole, item->getValue().value <QColor>());
              this->addTopLevelItem (item);

              item = new ColorItem (this);
              item->obj = obj;
              item->role = QPalette::Background;
              item->setText (0, "Background");
              item->setData (1, Qt::DecorationRole, item->getValue().value <QColor>());
              this->addTopLevelItem (item);
          }
      }
   }
}
*/

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

