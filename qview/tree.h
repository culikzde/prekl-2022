#ifndef TREE_H
#define TREE_H

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QGraphicsItem>

/* ---------------------------------------------------------------------- */

class Tree : public QTreeWidget
{
    Q_OBJECT

public:
    Tree (QWidget * parent = nullptr);
};

/* ---------------------------------------------------------------------- */

class TreeNode : public QTreeWidgetItem
{
public:
    int src_file;
    int src_line;
    int src_col;

    QObject * src_obj;
    QGraphicsItem * src_gra;

public:
    TreeNode (QTreeWidget * parent = nullptr, QString text = "");
    TreeNode (QTreeWidgetItem * parent = nullptr, QString text = "");
};

/* ---------------------------------------------------------------------- */

#endif // TREE_H
