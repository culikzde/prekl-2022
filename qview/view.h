#ifndef VIEW_H
#define VIEW_H

#include <QMainWindow>

#include <QTabWidget>
#include <QSplitter>

#include "edit.h"
#include "tree.h"
// #include "prop.h"
#include "property.h"

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window (QWidget * parent = nullptr);
    ~ Window ();

private :
    QMenu * fileMenu;
    QMenu * viewMenu;

    QTreeWidget * tree;
    PropertyTable * prop;

    QTabWidget * tabs;
    QTextEdit * empty_edit;

    QTextEdit * info;

    QSplitter * vsplitter;
    QSplitter * hsplitter;

    QMap < QString, Edit * > sourceEditors;

private:
    void readFile (Edit * edit, QString fileName);

    Edit * getEditor ();
    QString getEditorFileName (Edit * edit);

    void addMenuItem (QMenu * menu, QString title, QObject * obj, const char * slot, const char * shortcut = NULL);
    void addView (QString title, QWidget * widget);

public:
    Edit * openEditor (QString fileName);
    void loadUI (QString fileName);
    void removeEmptyEditor ();

public slots:
    void openFile ();
    void executeJS ();
    void openUI ();
    void runCmm ();
    void quit ();

    void showCLang ();
    void showDesigner ();
};

/* ---------------------------------------------------------------------- */

#endif // VIEW_H
