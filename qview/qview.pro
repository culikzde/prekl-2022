QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qview.bin

CONFIG += warn_off

if (0) {
   CONFIG += TRACE
   CONFIG += DEMANGLE
   CONFIG += LIBERTY_DEMANGLE
   CONFIG += BFD
}

if (0) {
   CONFIG += AREA
}

if (0) {
   CONFIG += BUILDER
}

if (true) {
   CONFIG += CODE
}

if (0) {
   CONFIG += CLANG
}

if (0) {
   CONFIG += CKIT
}

if (0) {
   CONFIG += DESIGN
}

if (0) {
   CONFIG += UI
}

if (0) {
   CONFIG += WEBKIT
}

if (0) {
   CONFIG += WEBENGINE
}

if (0) {
   CONFIG += CHART
   CONFIG += VISUAL
   CONFIG += DB
   CONFIG += JS
   CONFIG += DBUS
   CONFIG += SVG
   CONFIG += SOCKET
}

# --------------------------------------------------------------------------

TRACE {
   DEFINES += TRACE
   HEADERS += trace.h
   SOURCES += trace.cc

   DEMANGLE {
      DEFINES += DEMANGLE
   }

   LIBERTY_DEMANGLE {
      # LIBERTY_DEMANGLE ... alternative demangle
      DEFINES += LIBERTY_DEMANGLE
      LIBS    += -liberty
   }

   BFD {
      # BFD ... line numbers
      DEFINES += BFD
      LIBS    += -lbfd
      # dnf install binutils-devel
   }

   # QMAKE_CXXFLAGS += -g # necessary for line numbers
   # QMAKE_LFLAGS += -rdynamic # necessary for glibc backtrace function to show function names
}

# --------------------------------------------------------------------------

# EDIT_SRC = ../../src/qview
# INCLUDEPATH += $$EDIT_SRC

# HEADERS += $$EDIT_SRC/edit.h
# SOURCES += $$EDIT_SRC/edit.cc

HEADERS += edit.h
SOURCES += edit.cc

HEADERS += tree.h
SOURCES += tree.cc

# HEADERS += prop.h
# SOURCES += prop.cc

HEADERS += property.h
SOURCES += property.cc

HEADERS += view.h
SOURCES += view.cc

# --------------------------------------------------------------------------

CODE {
   DEFINES += CODE
   CONFIG += INCL_QCODE

   HEADERS += qcode/lexer0.h
   SOURCES += qcode/lexer0.cc

   HEADERS += qcode/code-treeview.h
   SOURCES += qcode/code-treeview.cc

   HEADERS += qcode/code-table.h
   SOURCES += qcode/code-table.cc
}

# --------------------------------------------------------------------------

CKIT {
   DEFINES += CKIT
   INCLUDEPATH += ckit

   # ! CODE {
   #    INCLUDEPATH += ../qcode
   #    HEADERS += ../qcode/lexer0.h
   #    SOURCES += ../qcode/lexer0.cc
   # }

   HEADERS += ckit/ckit_parser.h
   SOURCES += ckit/ckit_parser.cc

   # HEADERS += ../ckit/ckit_propduct.h
   # SOURCES += ../ckit/ckit_product.cc
}

# --------------------------------------------------------------------------

CLANG {
   DEFINES += CLANG
   INCLUDEPATH += ../qllvm

   HEADERS += ../qllvm/clang-view.h
   SPECIAL_SOURCES += ../qllvm/clang-view.cc

   HEADERS += ../qllvm/clang-print.h
   SPECIAL_SOURCES += ../qllvm/clang-print.cc

   HEADERS += ../qllvm/compiler.h
   SPECIAL_SOURCES += ../qllvm/compiler.cc

   HEADERS += ../qllvm/fibonacci.h
   SPECIAL_SOURCES += ../qllvm/fibonacci.cc

   # LLVM >= 10
   SPECIAL_CXXFLAGS += $$system(llvm-config --cflags) $$system(pkg-config Qt5Widgets --cflags)
   QMAKE_LFLAGS += $$system(llvm-config --ldflags)
   QMAKE_LIBS += $$system(llvm-config --libs)
   QMAKE_LIBS += -lclang -lclang-cpp

   special.name = special
   special.input = SPECIAL_SOURCES
   special.dependency_type = TYPE_CC
   special.variable_out = OBJECTS
   special.output = ${QMAKE_VAR_OBJECTS_DIR}${QMAKE_FILE_IN_BASE}$${first(QMAKE_EXT_OBJ)}
   special.commands = $${QMAKE_CXX} $(SPECIAL_CXXFLAGS) $(INCPATH) -c ${QMAKE_FILE_IN} -o ${QMAKE_FILE_OUT}
   QMAKE_EXTRA_COMPILERS += special
   # $(SPECIAL_CXXFLAGS) instead of $(CXXFLAGS) ... clang is in conflict with QT += UI
   # see http://stackoverflow.com/questions/27683777/how-to-specify-compiler-flag-to-a-single-source-file-with-qmake
}

# --------------------------------------------------------------------------

DESIGN {
   DEFINES += DESIGN
   INCLUDEPATH += ../qdesigner

   equals(QT_MAJOR_VERSION, 4): CONFIG += link_pkgconfig
   equals(QT_MAJOR_VERSION, 4): PKGCONFIG += QtDesignerComponents

   greaterThan(QT_MAJOR_VERSION, 4): QT += designer
   greaterThan(QT_MAJOR_VERSION, 4): LIBS += -lQt5DesignerComponents

   HEADERS += ../qdesigner/design.h
   SOURCES += ../qdesigner/design.cc
}

# --------------------------------------------------------------------------

UI {
   DEFINES += UI
   CONFIG += INCL_QCODE
   QT += designer

   HEADERS += ../qcode/ui-view.h
   SOURCES += ../qcode/ui-view.cc
   # dnf install qt5-qttools-devel
}

# --------------------------------------------------------------------------

AREA {
   DEFINES += AREA
   CONFIG += INCL_QDEMO

   HEADERS += ../qdemo/area.h
   SOURCES += ../qdemo/area.cc

   HEADERS += ../qdemo/colorbutton.h
   SOURCES += ../qdemo/colorbutton.cc

   HEADERS += ../qdemo/toolbutton.h
   SOURCES += ../qdemo/toolbutton.cc

}

BUILDER {
   DEFINES += BUILDER
   CONFIG += INCL_QDEMO

   HEADERS += ../qdemo/builder.h
   SOURCES += ../qdemo/builder.cc
}

WEBKIT {
    DEFINES += HTML WEBKIT
    CONFIG += INCL_QDEMO
    QT += webkit
    greaterThan(QT_MAJOR_VERSION, 4): QT += webkitwidgets

    HEADERS += ../qdemo/html.h
    SOURCES += ../qdemo/html.cc
    # dnf install qt5-qtwebkit-devel
}

WEBENGINE {
    DEFINES += HTML
    CONFIG += INCL_QDEMO
    QT += webengine webenginewidgets

    HEADERS += ../qdemo/html.h
    SOURCES += ../qdemo/html.cc
    # dnf install qt5-qtwebengine-devel
}

# --------------------------------------------------------------------------

CHART {
    # left braces on the same line as condition
    DEFINES += CHART
    CONFIG += INCL_QDEMO
    QT += charts

    SOURCES += ../qdemo/chart.cc
    HEADERS += ../qdemo/chart.h
    # dnf install qt5-qtcharts-devel
}

VISUAL {
    DEFINES += VISUAL
    CONFIG += INCL_QDEMO
    QT += datavisualization

    HEADERS += ../qdemo/visualization.h
    SOURCES += ../qdemo/visualization.cc
    # dnf install qt5-qtdatavis3d-devel
}

DB {
    DEFINES += DB
    CONFIG += INCL_QDEMO
    QT += sql

    HEADERS += ../qdemo/db.h
    SOURCES += ../qdemo/db.cc
}

JS {
    DEFINES += JS
    CONFIG += INCL_QDEMO
    QT += script scripttools

    HEADERS += ../qdemo/js.h
    SOURCES += ../qdemo/js.cc
    # dnf install qt5-qtscript-devel
}

DBUS {
    DEFINES += DBUS
    CONFIG += INCL_QDEMO
    QT += dbus

    HEADERS += ../qdemo/dbus.h
    SOURCES += ../qdemo/dbus.cc
}

SOCKET {
    DEFINES += SOCKET
    CONFIG += INCL_QDEMO

    HEADERS += ../qdemo/socket.h
    SOURCES += ../qdemo/socket.cc
}

SVG {
    DEFINES += SVG_VIEW # do not use SVG symbol (used in qpaintengine)
    CONFIG += INCL_QDEMO
    QT += svg network

    HEADERS += ../qdemo/svg.h
    SOURCES += ../qdemo/svg.cc
    # dnf install qt5-qtsvg-devel
}

# --------------------------------------------------------------------------

INCL_QCODE {
   INCLUDEPATH += ../qcode
}

INCL_QDEMO {
   INCLUDEPATH += ../qdemo
}

# --------------------------------------------------------------------------
