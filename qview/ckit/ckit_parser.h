
#ifndef CKIT_PARSER_H
#define CKIT_PARSER_H

#include "lexer0.h"
#include <string>
#include <vector>
using std::string;
using std::vector;

class CmmBasic;
class CmmExpr;
class CmmName;
class CmmBinaryExpr;
class CmmStat;
class CmmDecl;
class CmmDeclarator;
class CmmPtrSpecifier;
class CmmContSpecifier;
class CmmTextStat;
class CmmEolStat;
class CmmIndentStat;
class CmmUnindentStat;
class CmmEmptyLineStat;
class CmmCppOnlyStat;
class CmmPythonOnlyStat;
class CmmSimpleName;
class CmmGlobalName;
class CmmTemplateName;
class CmmContName;
class CmmDestructorName;
class CmmCompoundName;
class CmmIntValue;
class CmmRealValue;
class CmmCharValue;
class CmmStringValue;
class CmmStringCont;
class CmmThisExpr;
class CmmSubexprExpr;
class CmmModernCastExpr;
class CmmTypeIdExpr;
class CmmIndexExpr;
class CmmCallExpr;
class CmmFieldExpr;
class CmmPtrFieldExpr;
class CmmPostIncExpr;
class CmmPostDecExpr;
class CmmExprList;
class CmmIncExpr;
class CmmDecExpr;
class CmmDerefExpr;
class CmmAddrExpr;
class CmmPlusExpr;
class CmmMinusExpr;
class CmmBitNotExpr;
class CmmLogNotExpr;
class CmmSizeofExp;
class CmmNewExpr;
class CmmNewTypeId;
class CmmNewArrayLimit;
class CmmDeleteExpr;
class CmmTypeCastExpr;
class CmmEmptyStat;
class CmmCompoundStat;
class CmmIfStat;
class CmmWhileStat;
class CmmDoStat;
class CmmForStat;
class CmmSwitchStat;
class CmmCaseStat;
class CmmDefaultStat;
class CmmBreakStat;
class CmmContinueStat;
class CmmReturnStat;
class CmmGotoStat;
class CmmSimpleStat;
class CmmBlockStat;
class CmmDeclSpec;
class CmmConstSpecifier;
class CmmVolatileSpecifier;
class CmmTypeSpec;
class CmmTypeName;
class CmmPtrSpecifierSect;
class CmmPointerSpecifier;
class CmmReferenceSpecifier;
class CmmCvSpecifier;
class CmmContSpecifierSect;
class CmmArraySpecifier;
class CmmFunctionSpecifier;
class CmmStatSect;
class CmmSimpleDecl;
class CmmSimpleItem;
class CmmDotsParam;
class CmmBasicDeclarator;
class CmmEmptyDeclarator;
class CmmNestedDeclarator;
class CmmTypeId;
class CmmInitializer;
class CmmInitItem;
class CmmInitSimple;
class CmmInitList;
class CmmAttr;
class CmmAttrGroup;
class CmmAttrSect;
class CmmNamespaceDecl;
class CmmUsingDecl;
class CmmExternDecl;
class CmmTypedefDecl;
class CmmFriendDecl;
class CmmEnumDecl;
class CmmEnumSect;
class CmmEnumItem;
class CmmClassDecl;
class CmmMemberVisibility;
class CmmBaseSect;
class CmmBaseItem;
class CmmCtorInitializer;
class CmmMemberInitializer;
class CmmConvSpec;
class CmmSpecialFuction;
class CmmTemplateDecl;
class CmmTemplateParamSect;
class CmmTemplateParam;
class CmmTemplateTypeParam;
class CmmTemplateNormalParam;
class CmmTemplateArgSect;
class CmmTemplateArg;
class CmmTryStat;
class CmmHandlerSect;
class CmmHandlerItem;
class CmmThrowExpr;
class CmmExceptionSect;
class CmmTypeDecl;
class CmmExceptionItem;
class CmmPtrMemExpr;
class CmmMulExpr;
class CmmAddExpr;
class CmmShiftExpr;
class CmmRelExpr;
class CmmEqExpr;
class CmmAndExpr;
class CmmXorExpr;
class CmmOrExpr;
class CmmAndAndExpr;
class CmmOrOrExpr;
class CmmAssignExpr;
class CmmColonExpr;
class CmmCommaExpr;

enum CmmSymbol
{
   eos,
   identifier,
   number,
   real_number,
   character_literal,
   string_literal,
   separator,
   end_of_line,
   LOG_NOT,
   UNEQUAL,
   MOD,
   MOD_ASSIGN,
   BIT_AND,
   LOG_AND,
   BIT_AND_ASSIGN,
   LPAREN,
   RPAREN,
   STAR,
   MUL_ASSIGN,
   PLUS,
   INC,
   PLUS_ASSIGN,
   COMMA,
   MINUS,
   DEC,
   MINUS_ASSIGN,
   ARROW,
   ARROW_STAR,
   DOT,
   DOT_STAR,
   DOTS,
   SLASH,
   DIV_ASSIGN,
   COLON,
   SCOPE,
   SEMICOLON,
   LESS,
   SHL,
   SHL_ASSIGN,
   LESS_OR_EQUAL,
   ASSIGN,
   EQUAL,
   GREATER,
   GREATER_OR_EQUAL,
   SHR,
   SHR_ASSIGN,
   QUESTION,
   LBRACKET,
   literal_1,
   RBRACKET,
   literal_2,
   BIT_XOR,
   BIT_XOR_ASSIGN,
   LBRACE,
   BIT_OR,
   BIT_OR_ASSIGN,
   LOG_OR,
   RBRACE,
   BIT_NOT,
   keyword_bool,
   keyword_break,
   keyword_case,
   keyword_catch,
   keyword_char,
   keyword_class,
   keyword_const,
   keyword_const_cast,
   keyword_continue,
   keyword_default,
   keyword_delete,
   keyword_do,
   keyword_double,
   keyword_dynamic_cast,
   keyword_else,
   keyword_enum,
   keyword_explicit,
   keyword_extern,
   keyword_float,
   keyword_for,
   keyword_friend,
   keyword_goto,
   keyword_if,
   keyword_inline,
   keyword_int,
   keyword_long,
   keyword_mutable,
   keyword_namespace,
   keyword_new,
   keyword_operator,
   keyword_private,
   keyword_protected,
   keyword_public,
   keyword_register,
   keyword_reinterpret_cast,
   keyword_return,
   keyword_short,
   keyword_signed,
   keyword_sizeof,
   keyword_static,
   keyword_static_cast,
   keyword_struct,
   keyword_switch,
   keyword_template,
   keyword_this,
   keyword_throw,
   keyword_try,
   keyword_type_cast,
   keyword_typedef,
   keyword_typeid,
   keyword_typename,
   keyword_union,
   keyword_unsigned,
   keyword_using,
   keyword_virtual,
   keyword_void,
   keyword_volatile,
   keyword_wchar_t,
   keyword_while
};

class Parser : public Lexer
{
   public:
      Parser ();

      CmmSimpleName * parse_simple_name ();
      CmmName * parse_global_variant ();
      CmmGlobalName * parse_global_name ();
      CmmName * parse_base_variant ();
      CmmTemplateName * parse_template_name (CmmName * store);
      CmmName * parse_base_name ();
      CmmContName * parse_cont_name ();
      CmmDestructorName * parse_destructor_name ();
      CmmName * parse_cont_variant ();
      CmmName * parse_compound_variant ();
      CmmCompoundName * parse_compound_name (CmmName * store);
      CmmName * parse_qualified_name ();
      CmmExpr * parse_primary_expr ();
      CmmName * parse_ident_expr ();
      CmmIntValue * parse_int_value ();
      CmmRealValue * parse_real_value ();
      CmmCharValue * parse_char_value ();
      CmmStringValue * parse_string_value ();
      CmmStringCont * parse_string_value_cont ();
      CmmThisExpr * parse_this_expr ();
      CmmSubexprExpr * parse_subexpr_expr ();
      CmmExpr * parse_postfix_start ();
      CmmModernCastExpr * parse_modern_cast_expr ();
      CmmTypeIdExpr * parse_typeid_expr ();
      CmmExpr * parse_postfix_expr ();
      CmmIndexExpr * parse_index_expr (CmmExpr * store);
      CmmCallExpr * parse_call_expr (CmmExpr * store);
      CmmFieldExpr * parse_field_expr (CmmExpr * store);
      CmmPtrFieldExpr * parse_ptr_field_expr (CmmExpr * store);
      CmmPostIncExpr * parse_post_inc_expr (CmmExpr * store);
      CmmPostDecExpr * parse_post_dec_expr (CmmExpr * store);
      CmmExprList * parse_expr_list ();
      CmmExpr * parse_unary_expr ();
      CmmIncExpr * parse_inc_expr ();
      CmmDecExpr * parse_dec_expr ();
      CmmDerefExpr * parse_deref_expr ();
      CmmAddrExpr * parse_addr_expr ();
      CmmPlusExpr * parse_plus_expr ();
      CmmMinusExpr * parse_minus_expr ();
      CmmBitNotExpr * parse_bit_not_expr ();
      CmmLogNotExpr * parse_log_not_expr ();
      CmmSizeofExp * parse_sizeof_expr ();
      CmmNewExpr * parse_allocation_expr ();
      CmmNewTypeId * parse_new_type_id ();
      CmmNewArrayLimit * parse_allocation_array_limit ();
      CmmDeleteExpr * parse_deallocation_expr ();
      CmmExpr * parse_cast_expr ();
      CmmTypeCastExpr * parse_cast_formula ();
      CmmExpr * parse_pm_expr ();
      CmmExpr * parse_multiplicative_expr ();
      CmmExpr * parse_additive_expr ();
      CmmExpr * parse_shift_expr ();
      CmmExpr * parse_relational_expr ();
      CmmExpr * parse_equality_expr ();
      CmmExpr * parse_and_expr ();
      CmmExpr * parse_exclusive_or_expr ();
      CmmExpr * parse_inclusive_or_expr ();
      CmmExpr * parse_logical_and_expr ();
      CmmExpr * parse_logical_or_expr ();
      CmmExpr * parse_assignment_expr ();
      CmmExpr * parse_colon_expr ();
      CmmExpr * parse_comma_expr ();
      CmmExpr * parse_expr ();
      CmmExpr * parse_const_expr ();
      CmmStat * parse_stat ();
      CmmStat * parse_nested_stat ();
      CmmEmptyStat * parse_empty_stat ();
      CmmCompoundStat * parse_compound_stat ();
      CmmIfStat * parse_if_stat ();
      CmmWhileStat * parse_while_stat ();
      CmmDoStat * parse_do_stat ();
      CmmForStat * parse_for_stat ();
      CmmSwitchStat * parse_switch_stat ();
      CmmCaseStat * parse_case_stat ();
      CmmDefaultStat * parse_default_stat ();
      CmmBreakStat * parse_break_stat ();
      CmmContinueStat * parse_continue_stat ();
      CmmReturnStat * parse_return_stat ();
      CmmGotoStat * parse_goto_stat ();
      CmmStat * parse_flexible_stat ();
      CmmSimpleStat * parse_simple_stat (CmmExpr * store);
      CmmBlockStat * parse_block_stat (CmmExpr * store);
      CmmDeclSpec * parse_decl_specifiers ();
      CmmConstSpecifier * parse_const_specifier ();
      CmmVolatileSpecifier * parse_volatile_specifier ();
      CmmTypeSpec * parse_type_specifier ();
      CmmTypeName * parse_type_name ();
      CmmExpr * parse_type_specifiers ();
      CmmPtrSpecifierSect * parse_ptr_specifier_list ();
      CmmPtrSpecifier * parse_ptr_specifier ();
      CmmPointerSpecifier * parse_pointer_specifier ();
      CmmReferenceSpecifier * parse_reference_specifier ();
      CmmCvSpecifier * parse_cv_specifier_list ();
      CmmContSpecifierSect * parse_cont_specifier_list ();
      CmmContSpecifier * parse_cont_specifier ();
      CmmArraySpecifier * parse_array_specifier ();
      CmmFunctionSpecifier * parse_function_specifier ();
      CmmStatSect * parse_parameter_declaration_list ();
      CmmStat * parse_parameter_declaration ();
      CmmSimpleStat * parse_value_parameter (CmmExpr * store);
      CmmSimpleDecl * parse_plain_parameter (CmmExpr * store);
      CmmSimpleItem * parse_parameter_item ();
      CmmDotsParam * parse_dots_parameter ();
      CmmDeclarator * parse_declarator ();
      CmmBasicDeclarator * parse_basic_declarator (CmmPtrSpecifierSect * store);
      CmmEmptyDeclarator * parse_empty_declarator (CmmPtrSpecifierSect * store);
      CmmNestedDeclarator * parse_nested_declarator (CmmPtrSpecifierSect * store);
      CmmDeclarator * parse_typedef_declarator ();
      CmmTypeId * parse_type_id ();
      CmmInitializer * parse_initializer ();
      CmmInitItem * parse_initializer_item ();
      CmmInitSimple * parse_simple_initializer ();
      CmmInitList * parse_initializer_list ();
      CmmAttr * parse_attr_item ();
      CmmAttrGroup * parse_attr_group ();
      CmmAttrSect * parse_attr_list ();
      CmmSimpleDecl * parse_simple_declaration ();
      CmmSimpleDecl * parse_continue_simple_declaration (CmmExpr * store);
      CmmSimpleDecl * parse_modify_simple_declaration (CmmSimpleDecl * result);
      CmmSimpleItem * parse_simple_item ();
      CmmStat * parse_condition ();
      CmmSimpleDecl * parse_condition_declaration (CmmExpr * store);
      CmmSimpleStat * parse_condition_value (CmmExpr * store);
      CmmStatSect * parse_declaration_list ();
      CmmStat * parse_declaration ();
      CmmNamespaceDecl * parse_namespace_declaration ();
      CmmUsingDecl * parse_using_declaration ();
      CmmExternDecl * parse_extern_declaration ();
      CmmTypedefDecl * parse_typedef_declaration ();
      CmmFriendDecl * parse_friend_declaration ();
      CmmEnumDecl * parse_enum_declaration ();
      CmmEnumSect * parse_enum_list ();
      CmmEnumItem * parse_enumerator ();
      CmmClassDecl * parse_class_declaration ();
      CmmMemberVisibility * parse_member_visibility ();
      CmmStat * parse_member_item ();
      CmmStatSect * parse_member_list ();
      CmmBaseSect * parse_base_specifier_list ();
      CmmBaseItem * parse_base_specifier ();
      CmmCtorInitializer * parse_ctor_initializer ();
      CmmMemberInitializer * parse_member_initializer ();
      CmmConvSpec * parse_conversion_specifiers ();
      CmmSpecialFuction * parse_special_function ();
      CmmTemplateDecl * parse_template_declaration ();
      CmmTemplateParamSect * parse_template_param_list ();
      CmmTemplateParam * parse_template_param ();
      CmmTemplateTypeParam * parse_template_type_param ();
      CmmTemplateNormalParam * parse_template_normal_param ();
      CmmTemplateArgSect * parse_template_arg_list ();
      CmmTemplateArg * parse_template_arg ();
      CmmTryStat * parse_try_stat ();
      CmmHandlerSect * parse_handler_list ();
      CmmHandlerItem * parse_handler ();
      CmmThrowExpr * parse_throw_expr ();
      CmmExceptionSect * parse_exception_specification ();
      CmmExceptionItem * parse_exception_specification_item ();

      CmmSymbol token;

      void lookupKeyword ();
      void processSeparator ();
      string tokenToString (CmmSymbol value);

      inline bool isSymbol (CmmSymbol sym)
      {
         return token == sym;
      }

      inline void checkSymbol (CmmSymbol sym)
      {
         if (token != sym)
            stop (tokenToString (sym) + " expected");
         nextToken ();
      }

      inline bool testSymbols (const unsigned char table [])
      {
         int pos = token / 8;
         int bit = token % 8;
         return table [pos] & (1 << bit);
      }

      void stop (string msg)
      {
         error (msg);
      }

      void stop ()
      {
         stop ("Unexpected " + tokenToString (token));
      }

      void translate ();
      void storeLocation (CmmBasic * item);
      virtual void on_begin_flexible_expr () { }
      virtual void on_end_flexible_expr () { }
      virtual void on_binary_expr (CmmBasic * item) { }
      virtual bool is_binary_expression (CmmBasic * item) { return false; }
      virtual bool is_postfix_expression (CmmBasic * item) { return false; }
      virtual bool is_multiplicative_expression (CmmBasic * item) { return false; }
      virtual bool is_and_expression (CmmBasic * item) { return false; }
      virtual void on_ident_expr (CmmBasic * item) { }
      virtual void on_int_value (CmmBasic * item) { }
      virtual void on_real_value (CmmBasic * item) { }
      virtual void on_char_value (CmmBasic * item) { }
      virtual void on_string_value (CmmBasic * item) { }
      virtual void on_this_expr (CmmBasic * item) { }
      virtual void on_subexpr_expr (CmmBasic * item) { }
      virtual void on_field_expr (CmmBasic * item) { }
      virtual void on_ptr_field_expr (CmmBasic * item) { }
      virtual void on_call_expr (CmmBasic * item) { }
      virtual void on_type_specifier (CmmBasic * item) { }
      virtual void on_type_name (CmmBasic * item) { }
      virtual void on_cv_specifier (CmmBasic * item) { }
      virtual void on_bit_not_expr (CmmBasic * item) { }
      virtual void on_name_expr (CmmBasic * item) { }
      virtual void on_other_expr () { }
      virtual void on_template_args (CmmTemplateName * item) { }
      virtual void on_base_name (CmmName * item) { }
      virtual bool is_template (CmmBasic * item) { return false; }
      virtual void on_cont_name (CmmContName * item) { }
      virtual bool is_cont_template (CmmBasic * item) { return false; }
      virtual void on_begin_compound_name (CmmCompoundName * item) { }
      virtual void on_compound_name (CmmCompoundName * item) { }
      virtual bool is_mul (CmmBasic * item) { return false; }
      virtual bool is_add (CmmBasic * item) { return false; }
      virtual bool is_expression (CmmExpr * item) { return false; }
      virtual void on_simple_statement (CmmSimpleStat * item) { }
      virtual void on_block_statement (CmmBlockStat * item) { }
      virtual bool is_value_parameter (CmmBasic * item) { return false; }
      virtual void on_simple_item (CmmSimpleDecl * item) { }
      virtual bool is_constructor (CmmBasic * item) { return false; }
      virtual void on_open_function (CmmSimpleDecl * item) { }
      virtual void on_close_function (CmmSimpleDecl * item) { }
      virtual void on_simple_declaration (CmmSimpleDecl * item) { }
      virtual void on_start_namespace (CmmNamespaceDecl * item) { }
      virtual void on_namespace (CmmNamespaceDecl * item) { }
      virtual void on_open_namespace (CmmNamespaceDecl * item) { }
      virtual void on_close_namespace (CmmNamespaceDecl * item) { }
      virtual void on_stop_namespace (CmmNamespaceDecl * item) { }
      virtual void on_typedef (CmmTypedefDecl * item) { }
      virtual void on_start_enum (CmmEnumDecl * item) { }
      virtual void on_enum (CmmEnumDecl * item) { }
      virtual void on_open_enum (CmmEnumDecl * item) { }
      virtual void on_close_enum (CmmEnumDecl * item) { }
      virtual void on_stop_enum (CmmEnumDecl * item) { }
      virtual void on_enum_item (CmmEnumItem * item) { }
      virtual void on_start_class (CmmClassDecl * item) { }
      virtual void on_class (CmmClassDecl * item) { }
      virtual void on_open_class (CmmClassDecl * item) { }
      virtual void on_close_class (CmmClassDecl * item) { }
      virtual void on_stop_class (CmmClassDecl * item) { }
      virtual void on_template_declaration (CmmTemplateDecl * item) { }
};

enum CmmExprKind
{
   simpleName,
   globalName,
   templateName,
   contName,
   destructorName,
   compoundName,
   intValue,
   realValue,
   charValue,
   stringValue,
   thisExp,
   subexprExp,
   modernCastExp,
   typeIdExp,
   indexExp,
   callExp,
   fieldExp,
   ptrFieldExp,
   postIncExp,
   postDecExp,
   incExp,
   decExp,
   derefExp,
   addrExp,
   plusExp,
   minusExp,
   bitNotExp,
   logNotExp,
   sizeofExp,
   newExp,
   deleteExp,
   typecastExp,
   constSpec,
   volatileSpec,
   typeSpec,
   typeName,
   specialName,
   throwExp,
   dotMemberExp,
   arrowMemberExp,
   mulExp,
   divExp,
   modExp,
   customMulExp,
   addExp,
   subExp,
   customAddExp,
   shlExp,
   shrExp,
   ltExp,
   gtExp,
   leExp,
   geExp,
   eqExp,
   neExp,
   bitAndExp,
   bitXorExp,
   bitOrExp,
   logAndExp,
   logOrExp,
   assignExp,
   addAssignExp,
   subAssignExp,
   mulAssignExp,
   divAssignExp,
   modAssignExp,
   shlAssignExp,
   shrAssignExp,
   andAssignExp,
   xorAssignExp,
   orAssignExp,
   condExp,
   colonExp,
   commaExp
};

enum CmmStatMode
{
   textStat,
   eolStat,
   indentStat,
   unindentStat,
   emptyLineStat,
   cppOnlyStat,
   pythonOnlyStat,
   emptyStat,
   compoundStat,
   ifStat,
   whileStat,
   doStat,
   forStat,
   switchStat,
   caseStat,
   defaultStat,
   breakStat,
   continueStat,
   returnStat,
   gotoStat,
   simpleStat,
   blockStat,
   simpleDecl,
   namespaceDecl,
   usingDecl,
   externDecl,
   typedefDecl,
   friendDecl,
   enumDecl,
   classDecl,
   visibilityDecl,
   templateDecl,
   tryStat
};

enum CmmDeclaratorKind
{
   basicDeclarator,
   emptyDeclarator,
   nestedDeclarator
};

enum CmmPtrKind
{
   pointerSpec,
   referenceSpec
};

enum CmmContKind
{
   arraySpec,
   functionSpec
};

enum CmmModernCastExprCast_kind
{
   DynamicCast,
   StaticCast,
   ConstCast,
   ReinterpreterCast
};

enum CmmClassDeclStyle
{
   ClassStyle,
   StructStyle,
   UnionStyle
};

enum CmmMemberVisibilityAccess
{
   PrivateAccess,
   ProtectedAccess,
   PublicAccess
};

enum CmmBaseItemAccess
{
   PrivateBase,
   ProtectedBase,
   PublicBase,
   UnknownBase
};

enum CmmTemplateTypeParamKind
{
   templateTypeParam
};

enum CmmTemplateNormalParamKind
{
   templateCommonParam
};

class CmmBasic
{
   public:
      int src_file;
      int src_line;
      int src_column;

   public:
      CmmBasic () :
         src_file (0),
         src_line (0),
         src_column (0)
      {
      }

      virtual CmmBasic * conv_CmmBasic () { return this; }
};

class CmmExpr : public CmmBasic
{
   public:
      CmmExprKind kind;
      bool type_flag;
      bool remember_enable_declaration;
      CmmDecl * item_decl;
      string alt_assign;
      string alt_connect;
      CmmDecl * alt_connect_dcl;
      string alt_connect_signal;

   public:
      CmmExpr () :
         kind (),
         type_flag (false),
         remember_enable_declaration (false),
         item_decl (),
         alt_assign (""),
         alt_connect (""),
         alt_connect_dcl (),
         alt_connect_signal ("")
      {
      }

      virtual CmmName * conv_CmmName () { return NULL; }
      virtual CmmBinaryExpr * conv_CmmBinaryExpr () { return NULL; }
      virtual CmmSimpleName * conv_CmmSimpleName () { return NULL; }
      virtual CmmGlobalName * conv_CmmGlobalName () { return NULL; }
      virtual CmmTemplateName * conv_CmmTemplateName () { return NULL; }
      virtual CmmContName * conv_CmmContName () { return NULL; }
      virtual CmmDestructorName * conv_CmmDestructorName () { return NULL; }
      virtual CmmCompoundName * conv_CmmCompoundName () { return NULL; }
      virtual CmmIntValue * conv_CmmIntValue () { return NULL; }
      virtual CmmRealValue * conv_CmmRealValue () { return NULL; }
      virtual CmmCharValue * conv_CmmCharValue () { return NULL; }
      virtual CmmStringValue * conv_CmmStringValue () { return NULL; }
      virtual CmmThisExpr * conv_CmmThisExpr () { return NULL; }
      virtual CmmSubexprExpr * conv_CmmSubexprExpr () { return NULL; }
      virtual CmmModernCastExpr * conv_CmmModernCastExpr () { return NULL; }
      virtual CmmTypeIdExpr * conv_CmmTypeIdExpr () { return NULL; }
      virtual CmmIndexExpr * conv_CmmIndexExpr () { return NULL; }
      virtual CmmCallExpr * conv_CmmCallExpr () { return NULL; }
      virtual CmmFieldExpr * conv_CmmFieldExpr () { return NULL; }
      virtual CmmPtrFieldExpr * conv_CmmPtrFieldExpr () { return NULL; }
      virtual CmmPostIncExpr * conv_CmmPostIncExpr () { return NULL; }
      virtual CmmPostDecExpr * conv_CmmPostDecExpr () { return NULL; }
      virtual CmmIncExpr * conv_CmmIncExpr () { return NULL; }
      virtual CmmDecExpr * conv_CmmDecExpr () { return NULL; }
      virtual CmmDerefExpr * conv_CmmDerefExpr () { return NULL; }
      virtual CmmAddrExpr * conv_CmmAddrExpr () { return NULL; }
      virtual CmmPlusExpr * conv_CmmPlusExpr () { return NULL; }
      virtual CmmMinusExpr * conv_CmmMinusExpr () { return NULL; }
      virtual CmmBitNotExpr * conv_CmmBitNotExpr () { return NULL; }
      virtual CmmLogNotExpr * conv_CmmLogNotExpr () { return NULL; }
      virtual CmmSizeofExp * conv_CmmSizeofExp () { return NULL; }
      virtual CmmNewExpr * conv_CmmNewExpr () { return NULL; }
      virtual CmmDeleteExpr * conv_CmmDeleteExpr () { return NULL; }
      virtual CmmTypeCastExpr * conv_CmmTypeCastExpr () { return NULL; }
      virtual CmmConstSpecifier * conv_CmmConstSpecifier () { return NULL; }
      virtual CmmVolatileSpecifier * conv_CmmVolatileSpecifier () { return NULL; }
      virtual CmmTypeSpec * conv_CmmTypeSpec () { return NULL; }
      virtual CmmTypeName * conv_CmmTypeName () { return NULL; }
      virtual CmmSpecialFuction * conv_CmmSpecialFuction () { return NULL; }
      virtual CmmThrowExpr * conv_CmmThrowExpr () { return NULL; }
      virtual CmmPtrMemExpr * conv_CmmPtrMemExpr () { return NULL; }
      virtual CmmMulExpr * conv_CmmMulExpr () { return NULL; }
      virtual CmmAddExpr * conv_CmmAddExpr () { return NULL; }
      virtual CmmShiftExpr * conv_CmmShiftExpr () { return NULL; }
      virtual CmmRelExpr * conv_CmmRelExpr () { return NULL; }
      virtual CmmEqExpr * conv_CmmEqExpr () { return NULL; }
      virtual CmmAndExpr * conv_CmmAndExpr () { return NULL; }
      virtual CmmXorExpr * conv_CmmXorExpr () { return NULL; }
      virtual CmmOrExpr * conv_CmmOrExpr () { return NULL; }
      virtual CmmAndAndExpr * conv_CmmAndAndExpr () { return NULL; }
      virtual CmmOrOrExpr * conv_CmmOrOrExpr () { return NULL; }
      virtual CmmAssignExpr * conv_CmmAssignExpr () { return NULL; }
      virtual CmmColonExpr * conv_CmmColonExpr () { return NULL; }
      virtual CmmCommaExpr * conv_CmmCommaExpr () { return NULL; }
      virtual CmmExpr * conv_CmmExpr () { return this; }
};

class CmmName : public CmmExpr
{
   public:

   public:
      CmmName () {
      }

      virtual CmmName * conv_CmmName () { return this; }
};

class CmmBinaryExpr : public CmmExpr
{
   public:

   public:
      CmmBinaryExpr () {
      }

      virtual CmmBinaryExpr * conv_CmmBinaryExpr () { return this; }
};

class CmmStat : public CmmBasic
{
   public:
      CmmStatMode mode;

   public:
      CmmStat () :
         mode ()
      {
      }

      virtual CmmDecl * conv_CmmDecl () { return NULL; }
      virtual CmmTextStat * conv_CmmTextStat () { return NULL; }
      virtual CmmEolStat * conv_CmmEolStat () { return NULL; }
      virtual CmmIndentStat * conv_CmmIndentStat () { return NULL; }
      virtual CmmUnindentStat * conv_CmmUnindentStat () { return NULL; }
      virtual CmmEmptyLineStat * conv_CmmEmptyLineStat () { return NULL; }
      virtual CmmCppOnlyStat * conv_CmmCppOnlyStat () { return NULL; }
      virtual CmmPythonOnlyStat * conv_CmmPythonOnlyStat () { return NULL; }
      virtual CmmEmptyStat * conv_CmmEmptyStat () { return NULL; }
      virtual CmmCompoundStat * conv_CmmCompoundStat () { return NULL; }
      virtual CmmIfStat * conv_CmmIfStat () { return NULL; }
      virtual CmmWhileStat * conv_CmmWhileStat () { return NULL; }
      virtual CmmDoStat * conv_CmmDoStat () { return NULL; }
      virtual CmmForStat * conv_CmmForStat () { return NULL; }
      virtual CmmSwitchStat * conv_CmmSwitchStat () { return NULL; }
      virtual CmmCaseStat * conv_CmmCaseStat () { return NULL; }
      virtual CmmDefaultStat * conv_CmmDefaultStat () { return NULL; }
      virtual CmmBreakStat * conv_CmmBreakStat () { return NULL; }
      virtual CmmContinueStat * conv_CmmContinueStat () { return NULL; }
      virtual CmmReturnStat * conv_CmmReturnStat () { return NULL; }
      virtual CmmGotoStat * conv_CmmGotoStat () { return NULL; }
      virtual CmmSimpleStat * conv_CmmSimpleStat () { return NULL; }
      virtual CmmBlockStat * conv_CmmBlockStat () { return NULL; }
      virtual CmmSimpleDecl * conv_CmmSimpleDecl () { return NULL; }
      virtual CmmDotsParam * conv_CmmDotsParam () { return NULL; }
      virtual CmmNamespaceDecl * conv_CmmNamespaceDecl () { return NULL; }
      virtual CmmUsingDecl * conv_CmmUsingDecl () { return NULL; }
      virtual CmmExternDecl * conv_CmmExternDecl () { return NULL; }
      virtual CmmTypedefDecl * conv_CmmTypedefDecl () { return NULL; }
      virtual CmmFriendDecl * conv_CmmFriendDecl () { return NULL; }
      virtual CmmEnumDecl * conv_CmmEnumDecl () { return NULL; }
      virtual CmmClassDecl * conv_CmmClassDecl () { return NULL; }
      virtual CmmMemberVisibility * conv_CmmMemberVisibility () { return NULL; }
      virtual CmmTemplateDecl * conv_CmmTemplateDecl () { return NULL; }
      virtual CmmTryStat * conv_CmmTryStat () { return NULL; }
      virtual CmmStat * conv_CmmStat () { return this; }
};

class CmmDecl : public CmmStat
{
   public:

   public:
      CmmDecl () {
      }

      virtual CmmDecl * conv_CmmDecl () { return this; }
};

class CmmDeclarator : public CmmBasic
{
   public:
      CmmDeclaratorKind kind;

   public:
      CmmDeclarator () :
         kind ()
      {
      }

      virtual CmmBasicDeclarator * conv_CmmBasicDeclarator () { return NULL; }
      virtual CmmEmptyDeclarator * conv_CmmEmptyDeclarator () { return NULL; }
      virtual CmmNestedDeclarator * conv_CmmNestedDeclarator () { return NULL; }
      virtual CmmDeclarator * conv_CmmDeclarator () { return this; }
};

class CmmPtrSpecifier : public CmmBasic
{
   public:
      CmmPtrKind kind;

   public:
      CmmPtrSpecifier () :
         kind ()
      {
      }

      virtual CmmPointerSpecifier * conv_CmmPointerSpecifier () { return NULL; }
      virtual CmmReferenceSpecifier * conv_CmmReferenceSpecifier () { return NULL; }
      virtual CmmPtrSpecifier * conv_CmmPtrSpecifier () { return this; }
};

class CmmContSpecifier : public CmmBasic
{
   public:
      CmmContKind kind;

   public:
      CmmContSpecifier () :
         kind ()
      {
      }

      virtual CmmArraySpecifier * conv_CmmArraySpecifier () { return NULL; }
      virtual CmmFunctionSpecifier * conv_CmmFunctionSpecifier () { return NULL; }
      virtual CmmContSpecifier * conv_CmmContSpecifier () { return this; }
};

class CmmTextStat : public CmmStat
{
   public:
      string text;

   public:
      CmmTextStat () :
         text ("")
      {
         mode = textStat;
      }

      virtual CmmTextStat * conv_CmmTextStat () { return this; }
};

class CmmEolStat : public CmmStat
{
   public:

   public:
      CmmEolStat () {
         mode = eolStat;
      }

      virtual CmmEolStat * conv_CmmEolStat () { return this; }
};

class CmmIndentStat : public CmmStat
{
   public:

   public:
      CmmIndentStat () {
         mode = indentStat;
      }

      virtual CmmIndentStat * conv_CmmIndentStat () { return this; }
};

class CmmUnindentStat : public CmmStat
{
   public:

   public:
      CmmUnindentStat () {
         mode = unindentStat;
      }

      virtual CmmUnindentStat * conv_CmmUnindentStat () { return this; }
};

class CmmEmptyLineStat : public CmmStat
{
   public:

   public:
      CmmEmptyLineStat () {
         mode = emptyLineStat;
      }

      virtual CmmEmptyLineStat * conv_CmmEmptyLineStat () { return this; }
};

class CmmCppOnlyStat : public CmmStat
{
   public:
      CmmStat * inner_stat;

   public:
      CmmCppOnlyStat () :
         inner_stat ()
      {
         mode = cppOnlyStat;
      }

      virtual CmmCppOnlyStat * conv_CmmCppOnlyStat () { return this; }
};

class CmmPythonOnlyStat : public CmmStat
{
   public:
      CmmStat * inner_stat;

   public:
      CmmPythonOnlyStat () :
         inner_stat ()
      {
         mode = pythonOnlyStat;
      }

      virtual CmmPythonOnlyStat * conv_CmmPythonOnlyStat () { return this; }
};

class CmmSimpleName : public CmmName
{
   public:
      string id;

   public:
      CmmSimpleName () :
         id ("")
      {
         kind = simpleName;
      }

      virtual CmmSimpleName * conv_CmmSimpleName () { return this; }
};

class CmmGlobalName : public CmmName
{
   public:
      CmmName * inner_name;

   public:
      CmmGlobalName () :
         inner_name ()
      {
         kind = globalName;
      }

      virtual CmmGlobalName * conv_CmmGlobalName () { return this; }
};

class CmmTemplateName : public CmmName
{
   public:
      CmmName * left;
      CmmTemplateArgSect * template_args;

   public:
      CmmTemplateName () :
         left (),
         template_args ()
      {
         kind = templateName;
      }

      virtual CmmTemplateName * conv_CmmTemplateName () { return this; }
};

class CmmContName : public CmmName
{
   public:
      string id;

   public:
      CmmContName () :
         id ("")
      {
         kind = contName;
      }

      virtual CmmContName * conv_CmmContName () { return this; }
};

class CmmDestructorName : public CmmName
{
   public:
      CmmSimpleName * inner_name;

   public:
      CmmDestructorName () :
         inner_name ()
      {
         kind = destructorName;
      }

      virtual CmmDestructorName * conv_CmmDestructorName () { return this; }
};

class CmmCompoundName : public CmmName
{
   public:
      CmmName * left;
      CmmName * right;

   public:
      CmmCompoundName () :
         left (),
         right ()
      {
         kind = compoundName;
      }

      virtual CmmCompoundName * conv_CmmCompoundName () { return this; }
};

class CmmIntValue : public CmmExpr
{
   public:
      string value;

   public:
      CmmIntValue () :
         value ("")
      {
         kind = intValue;
      }

      virtual CmmIntValue * conv_CmmIntValue () { return this; }
};

class CmmRealValue : public CmmExpr
{
   public:
      string value;

   public:
      CmmRealValue () :
         value ("")
      {
         kind = realValue;
      }

      virtual CmmRealValue * conv_CmmRealValue () { return this; }
};

class CmmCharValue : public CmmExpr
{
   public:
      string value;

   public:
      CmmCharValue () :
         value ("")
      {
         kind = charValue;
      }

      virtual CmmCharValue * conv_CmmCharValue () { return this; }
};

class CmmStringValue : public CmmExpr
{
   public:
      string value;
      vector < CmmStringCont * > items;

   public:
      CmmStringValue () :
         value (""),
         items ()
      {
         kind = stringValue;
      }

      virtual CmmStringValue * conv_CmmStringValue () { return this; }
};

class CmmStringCont : public CmmBasic
{
   public:
      string value;

   public:
      CmmStringCont () :
         value ("")
      {
      }

      virtual CmmStringCont * conv_CmmStringCont () { return this; }
};

class CmmThisExpr : public CmmExpr
{
   public:

   public:
      CmmThisExpr () {
         kind = thisExp;
      }

      virtual CmmThisExpr * conv_CmmThisExpr () { return this; }
};

class CmmSubexprExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmSubexprExpr () :
         param ()
      {
         kind = subexprExp;
      }

      virtual CmmSubexprExpr * conv_CmmSubexprExpr () { return this; }
};

class CmmModernCastExpr : public CmmExpr
{
   public:
      CmmModernCastExprCast_kind cast_kind;
      CmmTypeId * type;
      CmmExpr * param;

   public:
      CmmModernCastExpr () :
         cast_kind (),
         type (),
         param ()
      {
         kind = modernCastExp;
      }

      virtual CmmModernCastExpr * conv_CmmModernCastExpr () { return this; }
};

class CmmTypeIdExpr : public CmmExpr
{
   public:
      CmmExpr * value;

   public:
      CmmTypeIdExpr () :
         value ()
      {
         kind = typeIdExp;
      }

      virtual CmmTypeIdExpr * conv_CmmTypeIdExpr () { return this; }
};

class CmmIndexExpr : public CmmExpr
{
   public:
      CmmExpr * left;
      CmmExpr * param;

   public:
      CmmIndexExpr () :
         left (),
         param ()
      {
         kind = indexExp;
      }

      virtual CmmIndexExpr * conv_CmmIndexExpr () { return this; }
};

class CmmCallExpr : public CmmExpr
{
   public:
      CmmExpr * left;
      CmmExprList * param_list;

   public:
      CmmCallExpr () :
         left (),
         param_list ()
      {
         kind = callExp;
      }

      virtual CmmCallExpr * conv_CmmCallExpr () { return this; }
};

class CmmFieldExpr : public CmmExpr
{
   public:
      CmmExpr * left;
      CmmSimpleName * simp_name;

   public:
      CmmFieldExpr () :
         left (),
         simp_name ()
      {
         kind = fieldExp;
      }

      virtual CmmFieldExpr * conv_CmmFieldExpr () { return this; }
};

class CmmPtrFieldExpr : public CmmExpr
{
   public:
      CmmExpr * left;
      CmmSimpleName * simp_name;

   public:
      CmmPtrFieldExpr () :
         left (),
         simp_name ()
      {
         kind = ptrFieldExp;
      }

      virtual CmmPtrFieldExpr * conv_CmmPtrFieldExpr () { return this; }
};

class CmmPostIncExpr : public CmmExpr
{
   public:
      CmmExpr * left;

   public:
      CmmPostIncExpr () :
         left ()
      {
         kind = postIncExp;
      }

      virtual CmmPostIncExpr * conv_CmmPostIncExpr () { return this; }
};

class CmmPostDecExpr : public CmmExpr
{
   public:
      CmmExpr * left;

   public:
      CmmPostDecExpr () :
         left ()
      {
         kind = postDecExp;
      }

      virtual CmmPostDecExpr * conv_CmmPostDecExpr () { return this; }
};

class CmmExprList : public CmmBasic
{
   public:
      vector < CmmExpr * > items;

   public:
      CmmExprList () :
         items ()
      {
      }

      virtual CmmExprList * conv_CmmExprList () { return this; }
};

class CmmIncExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmIncExpr () :
         param ()
      {
         kind = incExp;
      }

      virtual CmmIncExpr * conv_CmmIncExpr () { return this; }
};

class CmmDecExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmDecExpr () :
         param ()
      {
         kind = decExp;
      }

      virtual CmmDecExpr * conv_CmmDecExpr () { return this; }
};

class CmmDerefExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmDerefExpr () :
         param ()
      {
         kind = derefExp;
      }

      virtual CmmDerefExpr * conv_CmmDerefExpr () { return this; }
};

class CmmAddrExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmAddrExpr () :
         param ()
      {
         kind = addrExp;
      }

      virtual CmmAddrExpr * conv_CmmAddrExpr () { return this; }
};

class CmmPlusExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmPlusExpr () :
         param ()
      {
         kind = plusExp;
      }

      virtual CmmPlusExpr * conv_CmmPlusExpr () { return this; }
};

class CmmMinusExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmMinusExpr () :
         param ()
      {
         kind = minusExp;
      }

      virtual CmmMinusExpr * conv_CmmMinusExpr () { return this; }
};

class CmmBitNotExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmBitNotExpr () :
         param ()
      {
         kind = bitNotExp;
      }

      virtual CmmBitNotExpr * conv_CmmBitNotExpr () { return this; }
};

class CmmLogNotExpr : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmLogNotExpr () :
         param ()
      {
         kind = logNotExp;
      }

      virtual CmmLogNotExpr * conv_CmmLogNotExpr () { return this; }
};

class CmmSizeofExp : public CmmExpr
{
   public:
      CmmExpr * value;

   public:
      CmmSizeofExp () :
         value ()
      {
         kind = sizeofExp;
      }

      virtual CmmSizeofExp * conv_CmmSizeofExp () { return this; }
};

class CmmNewExpr : public CmmExpr
{
   public:
      CmmNewTypeId * type1;
      CmmTypeId * type2;
      CmmExprList * init_list;

   public:
      CmmNewExpr () :
         type1 (),
         type2 (),
         init_list ()
      {
         kind = newExp;
      }

      virtual CmmNewExpr * conv_CmmNewExpr () { return this; }
};

class CmmNewTypeId : public CmmBasic
{
   public:
      CmmExpr * type_spec;
      CmmPtrSpecifierSect * ptr;
      vector < CmmNewArrayLimit * > items;

   public:
      CmmNewTypeId () :
         type_spec (),
         ptr (),
         items ()
      {
      }

      virtual CmmNewTypeId * conv_CmmNewTypeId () { return this; }
};

class CmmNewArrayLimit : public CmmBasic
{
   public:
      CmmExpr * value;

   public:
      CmmNewArrayLimit () :
         value ()
      {
      }

      virtual CmmNewArrayLimit * conv_CmmNewArrayLimit () { return this; }
};

class CmmDeleteExpr : public CmmExpr
{
   public:
      bool a_array;
      CmmExpr * param;

   public:
      CmmDeleteExpr () :
         a_array (false),
         param ()
      {
         kind = deleteExp;
      }

      virtual CmmDeleteExpr * conv_CmmDeleteExpr () { return this; }
};

class CmmTypeCastExpr : public CmmExpr
{
   public:
      CmmTypeId * type;
      CmmExpr * param;

   public:
      CmmTypeCastExpr () :
         type (),
         param ()
      {
         kind = typecastExp;
      }

      virtual CmmTypeCastExpr * conv_CmmTypeCastExpr () { return this; }
};

class CmmEmptyStat : public CmmStat
{
   public:

   public:
      CmmEmptyStat () {
         mode = emptyStat;
      }

      virtual CmmEmptyStat * conv_CmmEmptyStat () { return this; }
};

class CmmCompoundStat : public CmmStat
{
   public:
      vector < CmmStat * > items;

   public:
      CmmCompoundStat () :
         items ()
      {
         mode = compoundStat;
      }

      virtual CmmCompoundStat * conv_CmmCompoundStat () { return this; }
};

class CmmIfStat : public CmmStat
{
   public:
      CmmExpr * cond;
      CmmStat * then_stat;
      CmmStat * else_stat;

   public:
      CmmIfStat () :
         cond (),
         then_stat (),
         else_stat ()
      {
         mode = ifStat;
      }

      virtual CmmIfStat * conv_CmmIfStat () { return this; }
};

class CmmWhileStat : public CmmStat
{
   public:
      CmmExpr * cond;
      CmmStat * body;

   public:
      CmmWhileStat () :
         cond (),
         body ()
      {
         mode = whileStat;
      }

      virtual CmmWhileStat * conv_CmmWhileStat () { return this; }
};

class CmmDoStat : public CmmStat
{
   public:
      CmmStat * body;
      CmmExpr * cond_expr;

   public:
      CmmDoStat () :
         body (),
         cond_expr ()
      {
         mode = doStat;
      }

      virtual CmmDoStat * conv_CmmDoStat () { return this; }
};

class CmmForStat : public CmmStat
{
   public:
      CmmStat * from_expr;
      CmmExpr * iter_expr;
      CmmExpr * cond_expr;
      CmmExpr * step_expr;
      CmmStat * body;

   public:
      CmmForStat () :
         from_expr (),
         iter_expr (),
         cond_expr (),
         step_expr (),
         body ()
      {
         mode = forStat;
      }

      virtual CmmForStat * conv_CmmForStat () { return this; }
};

class CmmSwitchStat : public CmmStat
{
   public:
      CmmExpr * cond;
      CmmStat * body;

   public:
      CmmSwitchStat () :
         cond (),
         body ()
      {
         mode = switchStat;
      }

      virtual CmmSwitchStat * conv_CmmSwitchStat () { return this; }
};

class CmmCaseStat : public CmmStat
{
   public:
      CmmExpr * case_expr;
      CmmStat * body;

   public:
      CmmCaseStat () :
         case_expr (),
         body ()
      {
         mode = caseStat;
      }

      virtual CmmCaseStat * conv_CmmCaseStat () { return this; }
};

class CmmDefaultStat : public CmmStat
{
   public:
      CmmStat * body;

   public:
      CmmDefaultStat () :
         body ()
      {
         mode = defaultStat;
      }

      virtual CmmDefaultStat * conv_CmmDefaultStat () { return this; }
};

class CmmBreakStat : public CmmStat
{
   public:

   public:
      CmmBreakStat () {
         mode = breakStat;
      }

      virtual CmmBreakStat * conv_CmmBreakStat () { return this; }
};

class CmmContinueStat : public CmmStat
{
   public:

   public:
      CmmContinueStat () {
         mode = continueStat;
      }

      virtual CmmContinueStat * conv_CmmContinueStat () { return this; }
};

class CmmReturnStat : public CmmStat
{
   public:
      CmmExpr * return_expr;

   public:
      CmmReturnStat () :
         return_expr ()
      {
         mode = returnStat;
      }

      virtual CmmReturnStat * conv_CmmReturnStat () { return this; }
};

class CmmGotoStat : public CmmStat
{
   public:
      string goto_lab;

   public:
      CmmGotoStat () :
         goto_lab ("")
      {
         mode = gotoStat;
      }

      virtual CmmGotoStat * conv_CmmGotoStat () { return this; }
};

class CmmSimpleStat : public CmmStat
{
   public:
      CmmExpr * inner_expr;

   public:
      CmmSimpleStat () :
         inner_expr ()
      {
         mode = simpleStat;
      }

      virtual CmmSimpleStat * conv_CmmSimpleStat () { return this; }
};

class CmmBlockStat : public CmmStat
{
   public:
      CmmExpr * inner_expr;
      CmmCompoundStat * body;

   public:
      CmmBlockStat () :
         inner_expr (),
         body ()
      {
         mode = blockStat;
      }

      virtual CmmBlockStat * conv_CmmBlockStat () { return this; }
};

class CmmDeclSpec : public CmmBasic
{
   public:
      bool a_inline;
      bool a_virtual;
      bool a_explicit;
      bool a_mutable;
      bool a_static;
      bool a_register;

   public:
      CmmDeclSpec () :
         a_inline (false),
         a_virtual (false),
         a_explicit (false),
         a_mutable (false),
         a_static (false),
         a_register (false)
      {
      }

      virtual CmmDeclSpec * conv_CmmDeclSpec () { return this; }
};

class CmmConstSpecifier : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmConstSpecifier () :
         param ()
      {
         kind = constSpec;
      }

      virtual CmmConstSpecifier * conv_CmmConstSpecifier () { return this; }
};

class CmmVolatileSpecifier : public CmmExpr
{
   public:
      CmmExpr * param;

   public:
      CmmVolatileSpecifier () :
         param ()
      {
         kind = volatileSpec;
      }

      virtual CmmVolatileSpecifier * conv_CmmVolatileSpecifier () { return this; }
};

class CmmTypeSpec : public CmmExpr
{
   public:
      bool a_signed;
      bool a_unsigned;
      bool a_short;
      bool a_long;
      bool a_long_long;
      bool a_long_double;
      bool a_bool;
      bool a_char;
      bool a_wchar;
      bool a_int;
      bool a_float;
      bool a_double;
      bool a_void;

   public:
      CmmTypeSpec () :
         a_signed (false),
         a_unsigned (false),
         a_short (false),
         a_long (false),
         a_long_long (false),
         a_long_double (false),
         a_bool (false),
         a_char (false),
         a_wchar (false),
         a_int (false),
         a_float (false),
         a_double (false),
         a_void (false)
      {
         kind = typeSpec;
      }

      virtual CmmTypeSpec * conv_CmmTypeSpec () { return this; }
};

class CmmTypeName : public CmmExpr
{
   public:
      CmmName * qual_name;

   public:
      CmmTypeName () :
         qual_name ()
      {
         kind = typeName;
      }

      virtual CmmTypeName * conv_CmmTypeName () { return this; }
};

class CmmPtrSpecifierSect : public CmmBasic
{
   public:
      vector < CmmPtrSpecifier * > items;

   public:
      CmmPtrSpecifierSect () :
         items ()
      {
      }

      virtual CmmPtrSpecifierSect * conv_CmmPtrSpecifierSect () { return this; }
};

class CmmPointerSpecifier : public CmmPtrSpecifier
{
   public:
      CmmCvSpecifier * cv_spec;

   public:
      CmmPointerSpecifier () :
         cv_spec ()
      {
         kind = pointerSpec;
      }

      virtual CmmPointerSpecifier * conv_CmmPointerSpecifier () { return this; }
};

class CmmReferenceSpecifier : public CmmPtrSpecifier
{
   public:
      CmmCvSpecifier * cv_spec;

   public:
      CmmReferenceSpecifier () :
         cv_spec ()
      {
         kind = referenceSpec;
      }

      virtual CmmReferenceSpecifier * conv_CmmReferenceSpecifier () { return this; }
};

class CmmCvSpecifier : public CmmBasic
{
   public:
      bool cv_const;
      bool cv_volatile;

   public:
      CmmCvSpecifier () :
         cv_const (false),
         cv_volatile (false)
      {
      }

      virtual CmmCvSpecifier * conv_CmmCvSpecifier () { return this; }
};

class CmmContSpecifierSect : public CmmBasic
{
   public:
      vector < CmmContSpecifier * > items;

   public:
      CmmContSpecifierSect () :
         items ()
      {
      }

      virtual CmmContSpecifierSect * conv_CmmContSpecifierSect () { return this; }
};

class CmmArraySpecifier : public CmmContSpecifier
{
   public:
      CmmExpr * lim;

   public:
      CmmArraySpecifier () :
         lim ()
      {
         kind = arraySpec;
      }

      virtual CmmArraySpecifier * conv_CmmArraySpecifier () { return this; }
};

class CmmFunctionSpecifier : public CmmContSpecifier
{
   public:
      CmmStatSect * parameters;
      bool a_const;
      CmmExceptionSect * exception_spec;

   public:
      CmmFunctionSpecifier () :
         parameters (),
         a_const (false),
         exception_spec ()
      {
         kind = functionSpec;
      }

      virtual CmmFunctionSpecifier * conv_CmmFunctionSpecifier () { return this; }
};

class CmmStatSect : public CmmBasic
{
   public:
      vector < CmmStat * > items;

   public:
      CmmStatSect () :
         items ()
      {
      }

      virtual CmmStatSect * conv_CmmStatSect () { return this; }
};

class CmmSimpleDecl : public CmmDecl
{
   public:
      CmmExpr * type_spec;
      vector < CmmSimpleItem * > items;
      CmmDeclSpec * decl_spec;
      bool a_destructor;
      CmmCtorInitializer * ctor_init;
      CmmCompoundStat * body;

   public:
      CmmSimpleDecl () :
         type_spec (),
         items (),
         decl_spec (),
         a_destructor (false),
         ctor_init (),
         body ()
      {
         mode = simpleDecl;
      }

      virtual CmmSimpleDecl * conv_CmmSimpleDecl () { return this; }
};

class CmmSimpleItem : public CmmBasic
{
   public:
      CmmDeclarator * decl;
      CmmInitializer * init;
      CmmAttrSect * attr;
      CmmExpr * width;

   public:
      CmmSimpleItem () :
         decl (),
         init (),
         attr (),
         width ()
      {
      }

      virtual CmmSimpleItem * conv_CmmSimpleItem () { return this; }
};

class CmmDotsParam : public CmmStat
{
   public:

   public:
      CmmDotsParam () {
      }

      virtual CmmDotsParam * conv_CmmDotsParam () { return this; }
};

class CmmBasicDeclarator : public CmmDeclarator
{
   public:
      CmmPtrSpecifierSect * ptr_spec;
      CmmName * qual_name;
      CmmContSpecifierSect * cont_spec;

   public:
      CmmBasicDeclarator () :
         ptr_spec (),
         qual_name (),
         cont_spec ()
      {
         kind = basicDeclarator;
      }

      virtual CmmBasicDeclarator * conv_CmmBasicDeclarator () { return this; }
};

class CmmEmptyDeclarator : public CmmDeclarator
{
   public:
      CmmPtrSpecifierSect * ptr_spec;
      CmmContSpecifierSect * cont_spec;

   public:
      CmmEmptyDeclarator () :
         ptr_spec (),
         cont_spec ()
      {
         kind = emptyDeclarator;
      }

      virtual CmmEmptyDeclarator * conv_CmmEmptyDeclarator () { return this; }
};

class CmmNestedDeclarator : public CmmDeclarator
{
   public:
      CmmPtrSpecifierSect * ptr_spec;
      CmmDeclarator * inner_declarator;
      CmmContSpecifierSect * cont_spec;

   public:
      CmmNestedDeclarator () :
         ptr_spec (),
         inner_declarator (),
         cont_spec ()
      {
         kind = nestedDeclarator;
      }

      virtual CmmNestedDeclarator * conv_CmmNestedDeclarator () { return this; }
};

class CmmTypeId : public CmmBasic
{
   public:
      CmmExpr * type_spec;
      CmmDeclarator * decl;

   public:
      CmmTypeId () :
         type_spec (),
         decl ()
      {
      }

      virtual CmmTypeId * conv_CmmTypeId () { return this; }
};

class CmmInitializer : public CmmBasic
{
   public:
      CmmInitItem * value;

   public:
      CmmInitializer () :
         value ()
      {
      }

      virtual CmmInitializer * conv_CmmInitializer () { return this; }
};

class CmmInitItem : public CmmBasic
{
   public:

   public:
      CmmInitItem () {
      }

      virtual CmmInitSimple * conv_CmmInitSimple () { return NULL; }
      virtual CmmInitList * conv_CmmInitList () { return NULL; }
      virtual CmmInitItem * conv_CmmInitItem () { return this; }
};

class CmmInitSimple : public CmmInitItem
{
   public:
      CmmExpr * inner_expr;

   public:
      CmmInitSimple () :
         inner_expr ()
      {
      }

      virtual CmmInitSimple * conv_CmmInitSimple () { return this; }
};

class CmmInitList : public CmmInitItem
{
   public:
      vector < CmmInitItem * > items;

   public:
      CmmInitList () :
         items ()
      {
      }

      virtual CmmInitList * conv_CmmInitList () { return this; }
};

class CmmAttr : public CmmBasic
{
   public:
      CmmExpr * attr_expr;

   public:
      CmmAttr () :
         attr_expr ()
      {
      }

      virtual CmmAttr * conv_CmmAttr () { return this; }
};

class CmmAttrGroup : public CmmBasic
{
   public:
      vector < CmmAttr * > items;

   public:
      CmmAttrGroup () :
         items ()
      {
      }

      virtual CmmAttrGroup * conv_CmmAttrGroup () { return this; }
};

class CmmAttrSect : public CmmBasic
{
   public:
      vector < CmmAttrGroup * > items;

   public:
      CmmAttrSect () :
         items ()
      {
      }

      virtual CmmAttrSect * conv_CmmAttrSect () { return this; }
};

class CmmNamespaceDecl : public CmmDecl
{
   public:
      CmmSimpleName * simp_name;
      CmmAttrSect * attr;
      CmmStatSect * body;

   public:
      CmmNamespaceDecl () :
         simp_name (),
         attr (),
         body ()
      {
         mode = namespaceDecl;
      }

      virtual CmmNamespaceDecl * conv_CmmNamespaceDecl () { return this; }
};

class CmmUsingDecl : public CmmDecl
{
   public:
      bool a_namespace;
      CmmName * qual_name;

   public:
      CmmUsingDecl () :
         a_namespace (false),
         qual_name ()
      {
         mode = usingDecl;
      }

      virtual CmmUsingDecl * conv_CmmUsingDecl () { return this; }
};

class CmmExternDecl : public CmmDecl
{
   public:
      string language;
      CmmStatSect * decl_list;
      CmmStat * inner_declaration;

   public:
      CmmExternDecl () :
         language (""),
         decl_list (),
         inner_declaration ()
      {
         mode = externDecl;
      }

      virtual CmmExternDecl * conv_CmmExternDecl () { return this; }
};

class CmmTypedefDecl : public CmmDecl
{
   public:
      CmmExpr * type_spec;
      CmmDeclarator * decl;

   public:
      CmmTypedefDecl () :
         type_spec (),
         decl ()
      {
         mode = typedefDecl;
      }

      virtual CmmTypedefDecl * conv_CmmTypedefDecl () { return this; }
};

class CmmFriendDecl : public CmmDecl
{
   public:
      CmmStat * inner_declaration;

   public:
      CmmFriendDecl () :
         inner_declaration ()
      {
         mode = friendDecl;
      }

      virtual CmmFriendDecl * conv_CmmFriendDecl () { return this; }
};

class CmmEnumDecl : public CmmDecl
{
   public:
      CmmSimpleName * simp_name;
      CmmEnumSect * enum_items;

   public:
      CmmEnumDecl () :
         simp_name (),
         enum_items ()
      {
         mode = enumDecl;
      }

      virtual CmmEnumDecl * conv_CmmEnumDecl () { return this; }
};

class CmmEnumSect : public CmmBasic
{
   public:
      vector < CmmEnumItem * > items;

   public:
      CmmEnumSect () :
         items ()
      {
      }

      virtual CmmEnumSect * conv_CmmEnumSect () { return this; }
};

class CmmEnumItem : public CmmBasic
{
   public:
      CmmSimpleName * simp_name;
      CmmExpr * init_value;

   public:
      CmmEnumItem () :
         simp_name (),
         init_value ()
      {
      }

      virtual CmmEnumItem * conv_CmmEnumItem () { return this; }
};

class CmmClassDecl : public CmmDecl
{
   public:
      CmmClassDeclStyle style;
      CmmSimpleName * simp_name;
      CmmAttrSect * attr;
      CmmBaseSect * base_list;
      CmmStatSect * members;

   public:
      CmmClassDecl () :
         style (),
         simp_name (),
         attr (),
         base_list (),
         members ()
      {
         mode = classDecl;
      }

      virtual CmmClassDecl * conv_CmmClassDecl () { return this; }
};

class CmmMemberVisibility : public CmmDecl
{
   public:
      CmmMemberVisibilityAccess access;

   public:
      CmmMemberVisibility () :
         access ()
      {
         mode = visibilityDecl;
      }

      virtual CmmMemberVisibility * conv_CmmMemberVisibility () { return this; }
};

class CmmBaseSect : public CmmBasic
{
   public:
      vector < CmmBaseItem * > items;

   public:
      CmmBaseSect () :
         items ()
      {
      }

      virtual CmmBaseSect * conv_CmmBaseSect () { return this; }
};

class CmmBaseItem : public CmmBasic
{
   public:
      bool a_virtual;
      CmmBaseItemAccess access;
      CmmName * from_cls;

   public:
      CmmBaseItem () :
         a_virtual (false),
         access (),
         from_cls ()
      {
      }

      virtual CmmBaseItem * conv_CmmBaseItem () { return this; }
};

class CmmCtorInitializer : public CmmBasic
{
   public:
      vector < CmmMemberInitializer * > items;

   public:
      CmmCtorInitializer () :
         items ()
      {
      }

      virtual CmmCtorInitializer * conv_CmmCtorInitializer () { return this; }
};

class CmmMemberInitializer : public CmmBasic
{
   public:
      CmmSimpleName * simp_name;
      CmmExprList * params;

   public:
      CmmMemberInitializer () :
         simp_name (),
         params ()
      {
      }

      virtual CmmMemberInitializer * conv_CmmMemberInitializer () { return this; }
};

class CmmConvSpec : public CmmBasic
{
   public:
      CmmExpr * type_spec;
      CmmPtrSpecifierSect * ptr_spec;

   public:
      CmmConvSpec () :
         type_spec (),
         ptr_spec ()
      {
      }

      virtual CmmConvSpec * conv_CmmConvSpec () { return this; }
};

class CmmSpecialFuction : public CmmName
{
   public:
      CmmConvSpec * conv;
      bool spec_new;
      bool spec_new_array;
      bool spec_delete;
      bool spec_delete_array;
      bool spec_add;
      bool spec_sub;
      bool spec_mul;
      bool spec_div;
      bool spec_mod;
      bool spec_xor;
      bool spec_and;
      bool spec_or;
      bool spec_not;
      bool spec_log_not;
      bool spec_assign;
      bool spec_lt;
      bool spec_gt;
      bool spec_add_assign;
      bool spec_sub_assign;
      bool spec_mul_assign;
      bool spec_div_assign;
      bool spec_mod_assign;
      bool spec_xor_assign;
      bool spec_and_assign;
      bool spec_or_assign;
      bool spec_shl;
      bool spec_shr;
      bool spec_shl_assign;
      bool spec_shr_assign;
      bool spec_eq;
      bool spec_ne;
      bool spec_le;
      bool spec_ge;
      bool spec_log_and;
      bool spec_log_or;
      bool spec_inc;
      bool spec_dec;
      bool spec_comma;
      bool spec_member_deref;
      bool spec_deref;
      bool spec_call;
      bool spec_index;

   public:
      CmmSpecialFuction () :
         conv (),
         spec_new (false),
         spec_new_array (false),
         spec_delete (false),
         spec_delete_array (false),
         spec_add (false),
         spec_sub (false),
         spec_mul (false),
         spec_div (false),
         spec_mod (false),
         spec_xor (false),
         spec_and (false),
         spec_or (false),
         spec_not (false),
         spec_log_not (false),
         spec_assign (false),
         spec_lt (false),
         spec_gt (false),
         spec_add_assign (false),
         spec_sub_assign (false),
         spec_mul_assign (false),
         spec_div_assign (false),
         spec_mod_assign (false),
         spec_xor_assign (false),
         spec_and_assign (false),
         spec_or_assign (false),
         spec_shl (false),
         spec_shr (false),
         spec_shl_assign (false),
         spec_shr_assign (false),
         spec_eq (false),
         spec_ne (false),
         spec_le (false),
         spec_ge (false),
         spec_log_and (false),
         spec_log_or (false),
         spec_inc (false),
         spec_dec (false),
         spec_comma (false),
         spec_member_deref (false),
         spec_deref (false),
         spec_call (false),
         spec_index (false)
      {
         kind = specialName;
      }

      virtual CmmSpecialFuction * conv_CmmSpecialFuction () { return this; }
};

class CmmTemplateDecl : public CmmDecl
{
   public:
      CmmTemplateParamSect * params;
      CmmStat * inner_declaration;

   public:
      CmmTemplateDecl () :
         params (),
         inner_declaration ()
      {
         mode = templateDecl;
      }

      virtual CmmTemplateDecl * conv_CmmTemplateDecl () { return this; }
};

class CmmTemplateParamSect : public CmmBasic
{
   public:
      vector < CmmTemplateParam * > items;

   public:
      CmmTemplateParamSect () :
         items ()
      {
      }

      virtual CmmTemplateParamSect * conv_CmmTemplateParamSect () { return this; }
};

class CmmTemplateParam : public CmmBasic
{
   public:

   public:
      CmmTemplateParam () {
      }

      virtual CmmTemplateTypeParam * conv_CmmTemplateTypeParam () { return NULL; }
      virtual CmmTemplateNormalParam * conv_CmmTemplateNormalParam () { return NULL; }
      virtual CmmTemplateParam * conv_CmmTemplateParam () { return this; }
};

class CmmTemplateTypeParam : public CmmTemplateParam
{
   public:
      CmmTemplateTypeParamKind kind;
      CmmTemplateParamSect * params;
      bool a_class;
      bool a_typename;
      CmmSimpleName * simp_name;

   public:
      CmmTemplateTypeParam () :
         params (),
         a_class (false),
         a_typename (false),
         simp_name ()
      {
         kind = templateTypeParam;
      }

      virtual CmmTemplateTypeParam * conv_CmmTemplateTypeParam () { return this; }
};

class CmmTemplateNormalParam : public CmmTemplateParam
{
   public:
      CmmTemplateNormalParamKind kind;
      CmmExpr * type_spec;
      CmmDeclarator * decl;
      CmmExpr * init;

   public:
      CmmTemplateNormalParam () :
         type_spec (),
         decl (),
         init ()
      {
         kind = templateCommonParam;
      }

      virtual CmmTemplateNormalParam * conv_CmmTemplateNormalParam () { return this; }
};

class CmmTemplateArgSect : public CmmBasic
{
   public:
      vector < CmmTemplateArg * > items;

   public:
      CmmTemplateArgSect () :
         items ()
      {
      }

      virtual CmmTemplateArgSect * conv_CmmTemplateArgSect () { return this; }
};

class CmmTemplateArg : public CmmBasic
{
   public:
      CmmStat * value;

   public:
      CmmTemplateArg () :
         value ()
      {
      }

      virtual CmmTemplateArg * conv_CmmTemplateArg () { return this; }
};

class CmmTryStat : public CmmStat
{
   public:
      CmmCompoundStat * body;
      CmmHandlerSect * handlers;

   public:
      CmmTryStat () :
         body (),
         handlers ()
      {
         mode = tryStat;
      }

      virtual CmmTryStat * conv_CmmTryStat () { return this; }
};

class CmmHandlerSect : public CmmBasic
{
   public:
      vector < CmmHandlerItem * > items;

   public:
      CmmHandlerSect () :
         items ()
      {
      }

      virtual CmmHandlerSect * conv_CmmHandlerSect () { return this; }
};

class CmmHandlerItem : public CmmBasic
{
   public:
      CmmExpr * type_spec;
      CmmDeclarator * decl;
      bool dots;
      CmmCompoundStat * body;

   public:
      CmmHandlerItem () :
         type_spec (),
         decl (),
         dots (false),
         body ()
      {
      }

      virtual CmmHandlerItem * conv_CmmHandlerItem () { return this; }
};

class CmmThrowExpr : public CmmExpr
{
   public:
      CmmExpr * inner_expr;

   public:
      CmmThrowExpr () :
         inner_expr ()
      {
         kind = throwExp;
      }

      virtual CmmThrowExpr * conv_CmmThrowExpr () { return this; }
};

class CmmExceptionSect : public CmmBasic
{
   public:
      vector < CmmExceptionItem * > items;

   public:
      CmmExceptionSect () :
         items ()
      {
      }

      virtual CmmExceptionSect * conv_CmmExceptionSect () { return this; }
};

class CmmTypeDecl : public CmmBasic
{
   public:

   public:
      CmmTypeDecl () {
      }

      virtual CmmExceptionItem * conv_CmmExceptionItem () { return NULL; }
      virtual CmmTypeDecl * conv_CmmTypeDecl () { return this; }
};

class CmmExceptionItem : public CmmTypeDecl
{
   public:
      CmmTypeId * type;

   public:
      CmmExceptionItem () :
         type ()
      {
      }

      virtual CmmExceptionItem * conv_CmmExceptionItem () { return this; }
};

class CmmPtrMemExpr : public CmmExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmPtrMemExpr () :
         left (),
         right ()
      {
      }

      virtual CmmPtrMemExpr * conv_CmmPtrMemExpr () { return this; }
};

class CmmMulExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      string custom_mul;
      CmmExpr * right;

   public:
      CmmMulExpr () :
         left (),
         custom_mul (""),
         right ()
      {
      }

      virtual CmmMulExpr * conv_CmmMulExpr () { return this; }
};

class CmmAddExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      string custom_add;
      CmmExpr * right;

   public:
      CmmAddExpr () :
         left (),
         custom_add (""),
         right ()
      {
      }

      virtual CmmAddExpr * conv_CmmAddExpr () { return this; }
};

class CmmShiftExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmShiftExpr () :
         left (),
         right ()
      {
      }

      virtual CmmShiftExpr * conv_CmmShiftExpr () { return this; }
};

class CmmRelExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmRelExpr () :
         left (),
         right ()
      {
      }

      virtual CmmRelExpr * conv_CmmRelExpr () { return this; }
};

class CmmEqExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmEqExpr () :
         left (),
         right ()
      {
      }

      virtual CmmEqExpr * conv_CmmEqExpr () { return this; }
};

class CmmAndExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmAndExpr () :
         left (),
         right ()
      {
         kind = bitAndExp;
      }

      virtual CmmAndExpr * conv_CmmAndExpr () { return this; }
};

class CmmXorExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmXorExpr () :
         left (),
         right ()
      {
         kind = bitXorExp;
      }

      virtual CmmXorExpr * conv_CmmXorExpr () { return this; }
};

class CmmOrExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmOrExpr () :
         left (),
         right ()
      {
      }

      virtual CmmOrExpr * conv_CmmOrExpr () { return this; }
};

class CmmAndAndExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmAndAndExpr () :
         left (),
         right ()
      {
         kind = logAndExp;
      }

      virtual CmmAndAndExpr * conv_CmmAndAndExpr () { return this; }
};

class CmmOrOrExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmOrOrExpr () :
         left (),
         right ()
      {
         kind = logOrExp;
      }

      virtual CmmOrOrExpr * conv_CmmOrOrExpr () { return this; }
};

class CmmAssignExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * middle;
      CmmExpr * right;

   public:
      CmmAssignExpr () :
         left (),
         middle (),
         right ()
      {
      }

      virtual CmmAssignExpr * conv_CmmAssignExpr () { return this; }
};

class CmmColonExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmColonExpr () :
         left (),
         right ()
      {
         kind = colonExp;
      }

      virtual CmmColonExpr * conv_CmmColonExpr () { return this; }
};

class CmmCommaExpr : public CmmBinaryExpr
{
   public:
      CmmExpr * left;
      CmmExpr * right;

   public:
      CmmCommaExpr () :
         left (),
         right ()
      {
         kind = commaExp;
      }

      virtual CmmCommaExpr * conv_CmmCommaExpr () { return this; }
};

#endif // CKIT_PARSER_H
