
#include "ckit_parser.h"

const unsigned char set_0 [15] = {62, 128, 0, 0, 4, 0, 0, 0, 0, 0, 0, 1, 128, 0, 0};
// set_0 :  identifier number real_number character_literal string_literal ( :: operator this

const unsigned char set_1 [15] = {0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 0, 32, 8, 0, 0};
// set_1 :  const_cast dynamic_cast reinterpret_cast static_cast

const unsigned char set_2 [15] = {0, 0, 0, 0, 0, 0, 0, 136, 128, 32, 24, 128, 1, 128, 20};
// set_2 :  bool char double float int long short signed unsigned void wchar_t

const unsigned char set_3 [15] = {62, 145, 154, 1, 4, 0, 0, 140, 164, 33, 152, 161, 139, 181, 20};
// set_3 :  identifier number real_number character_literal string_literal ! & ( * + ++ - -- :: ~ bool char const_cast delete double dynamic_cast float int long new operator reinterpret_cast short signed sizeof static_cast this throw type_cast typeid typename unsigned void wchar_t

const unsigned char set_4 [15] = {62, 128, 0, 0, 4, 0, 0, 136, 132, 33, 24, 161, 137, 176, 20};
// set_4 :  identifier number real_number character_literal string_literal ( :: bool char const_cast double dynamic_cast float int long operator reinterpret_cast short signed static_cast this typeid typename unsigned void wchar_t

const unsigned char set_5 [15] = {2, 0, 0, 0, 4, 0, 0, 136, 130, 32, 24, 129, 1, 160, 28};
// set_5 :  identifier :: bool char const double float int long operator short signed typename unsigned void volatile wchar_t

const unsigned char set_6 [15] = {62, 145, 154, 1, 4, 0, 0, 140, 164, 33, 152, 161, 139, 177, 20};
// set_6 :  identifier number real_number character_literal string_literal ! & ( * + ++ - -- :: ~ bool char const_cast delete double dynamic_cast float int long new operator reinterpret_cast short signed sizeof static_cast this throw typeid typename unsigned void wchar_t

const unsigned char set_7 [15] = {62, 145, 154, 1, 12, 0, 32, 188, 252, 97, 155, 225, 171, 183, 52};
// set_7 :  identifier number real_number character_literal string_literal ! & ( * + ++ - -- :: ; { ~ bool break case char const_cast continue default delete do double dynamic_cast float for goto if int long new operator reinterpret_cast return short signed sizeof static_cast switch this throw try type_cast typeid typename unsigned void wchar_t while

const unsigned char set_8 [15] = {2, 144, 66, 0, 14, 129, 33, 0, 0, 0, 0, 1, 0, 0, 0};
// set_8 :  identifier & ( * , : :: ; = [ [[ { operator

const unsigned char set_9 [15] = {62, 145, 154, 65, 4, 0, 0, 140, 164, 33, 152, 161, 139, 181, 20};
// set_9 :  identifier number real_number character_literal string_literal ! & ( * + ++ - -- ... :: ~ bool char const_cast delete double dynamic_cast float int long new operator reinterpret_cast short signed sizeof static_cast this throw type_cast typeid typename unsigned void wchar_t

const unsigned char set_10 [15] = {2, 144, 2, 0, 4, 129, 1, 0, 0, 0, 0, 1, 0, 0, 0};
// set_10 :  identifier & ( * :: = [ [[ operator

const unsigned char set_11 [15] = {62, 145, 154, 1, 4, 0, 32, 140, 164, 33, 152, 161, 139, 181, 20};
// set_11 :  identifier number real_number character_literal string_literal ! & ( * + ++ - -- :: { ~ bool char const_cast delete double dynamic_cast float int long new operator reinterpret_cast short signed sizeof static_cast this throw type_cast typeid typename unsigned void wchar_t

const unsigned char set_12 [15] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 36, 16, 4, 0, 2};
// set_12 :  explicit inline mutable register static virtual

const unsigned char set_13 [15] = {2, 144, 66, 0, 6, 129, 1, 0, 0, 0, 0, 1, 0, 0, 0};
// set_13 :  identifier & ( * , : :: = [ [[ operator

const unsigned char set_14 [15] = {62, 145, 154, 1, 12, 0, 32, 140, 165, 245, 218, 161, 219, 253, 53};
// set_14 :  identifier number real_number character_literal string_literal ! & ( * + ++ - -- :: ; { ~ bool char class const_cast delete double dynamic_cast enum extern float for friend if int long namespace new operator reinterpret_cast short signed sizeof static_cast struct template this throw type_cast typedef typeid typename union unsigned using void wchar_t while

const unsigned char set_15 [15] = {62, 145, 154, 1, 12, 0, 32, 140, 165, 229, 154, 175, 219, 253, 53};
// set_15 :  identifier number real_number character_literal string_literal ! & ( * + ++ - -- :: ; { ~ bool char class const_cast delete double dynamic_cast enum float for friend if int long new operator private protected public reinterpret_cast short signed sizeof static_cast struct template this throw type_cast typedef typeid typename union unsigned using void wchar_t while

const unsigned char set_16 [15] = {2, 0, 0, 0, 4, 0, 0, 136, 131, 32, 24, 129, 65, 160, 28};
// set_16 :  identifier :: bool char class const double float int long operator short signed template typename unsigned void volatile wchar_t

CmmSimpleName * Parser::parse_simple_name ()
{
   CmmSimpleName * result = new CmmSimpleName;
   result->kind = simpleName;
   storeLocation (result);
   result->id = readIdent ();
   return result;
}

CmmName * Parser::parse_global_variant ()
{
   CmmName * result = NULL;
   if (isSymbol (identifier))
   {
      result = parse_simple_name ();
   }
   else if (isSymbol (keyword_operator))
   {
      result = parse_special_function ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmGlobalName * Parser::parse_global_name ()
{
   CmmGlobalName * result = new CmmGlobalName;
   result->kind = globalName;
   storeLocation (result);
   checkSymbol (SCOPE);
   result->inner_name = parse_global_variant ();
   return result;
}

CmmName * Parser::parse_base_variant ()
{
   CmmName * result = NULL;
   if (isSymbol (SCOPE))
   {
      result = parse_global_name ();
   }
   else if (isSymbol (identifier))
   {
      result = parse_simple_name ();
   }
   else if (isSymbol (keyword_operator))
   {
      result = parse_special_function ();
   }
   else
   {
      stop ("Unexpected token");
   }
   on_name_expr (result);
   return result;
}

CmmTemplateName * Parser::parse_template_name (CmmName * store)
{
   CmmTemplateName * result = new CmmTemplateName;
   result->kind = templateName;
   storeLocation (result);
   result->left = store;
   result->template_args = parse_template_arg_list ();
   on_template_args (result);
   return result;
}

CmmName * Parser::parse_base_name ()
{
   CmmName * result = NULL;
   result = parse_base_variant ();
   on_base_name (result);
   if (isSymbol (LESS) && is_template (result))
   {
      result = parse_template_name (result);
   }
   return result;
}

CmmContName * Parser::parse_cont_name ()
{
   CmmContName * result = new CmmContName;
   result->kind = contName;
   storeLocation (result);
   result->id = readIdent ();
   on_cont_name (result);
   return result;
}

CmmDestructorName * Parser::parse_destructor_name ()
{
   CmmDestructorName * result = new CmmDestructorName;
   result->kind = destructorName;
   storeLocation (result);
   checkSymbol (BIT_NOT);
   result->inner_name = parse_simple_name ();
   return result;
}

CmmName * Parser::parse_cont_variant ()
{
   CmmName * result = NULL;
   if (isSymbol (identifier))
   {
      result = parse_cont_name ();
   }
   else if (isSymbol (keyword_operator))
   {
      result = parse_special_function ();
   }
   else if (isSymbol (BIT_NOT))
   {
      result = parse_destructor_name ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmName * Parser::parse_compound_variant ()
{
   CmmName * result = NULL;
   result = parse_cont_variant ();
   if (isSymbol (LESS) && is_cont_template (result))
   {
      result = parse_template_name (result);
   }
   return result;
}

CmmCompoundName * Parser::parse_compound_name (CmmName * store)
{
   CmmCompoundName * result = new CmmCompoundName;
   result->kind = compoundName;
   storeLocation (result);
   result->left = store;
   on_begin_compound_name (result);
   checkSymbol (SCOPE);
   result->right = parse_compound_variant ();
   on_compound_name (result);
   return result;
}

CmmName * Parser::parse_qualified_name ()
{
   CmmName * result = NULL;
   result = parse_base_name ();
   while (isSymbol (SCOPE))
   {
      result = parse_compound_name (result);
   }
   return result;
}

CmmExpr * Parser::parse_primary_expr ()
{
   CmmExpr * result = NULL;
   if (isSymbol (identifier) || isSymbol (SCOPE) || isSymbol (keyword_operator))
   {
      result = parse_ident_expr ();
   }
   else if (isSymbol (number))
   {
      result = parse_int_value ();
   }
   else if (isSymbol (real_number))
   {
      result = parse_real_value ();
   }
   else if (isSymbol (character_literal))
   {
      result = parse_char_value ();
   }
   else if (isSymbol (string_literal))
   {
      result = parse_string_value ();
   }
   else if (isSymbol (keyword_this))
   {
      result = parse_this_expr ();
   }
   else if (isSymbol (LPAREN))
   {
      result = parse_subexpr_expr ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmName * Parser::parse_ident_expr ()
{
   CmmName * result = NULL;
   result = parse_qualified_name ();
   on_ident_expr (result);
   return result;
}

CmmIntValue * Parser::parse_int_value ()
{
   CmmIntValue * result = new CmmIntValue;
   result->kind = intValue;
   storeLocation (result);
   on_other_expr ();
   result->value = readNumber ();
   on_int_value (result);
   return result;
}

CmmRealValue * Parser::parse_real_value ()
{
   CmmRealValue * result = new CmmRealValue;
   result->kind = realValue;
   storeLocation (result);
   on_other_expr ();
   result->value = readNumber ();
   on_real_value (result);
   return result;
}

CmmCharValue * Parser::parse_char_value ()
{
   CmmCharValue * result = new CmmCharValue;
   result->kind = charValue;
   storeLocation (result);
   on_other_expr ();
   result->value = readChar ();
   on_char_value (result);
   return result;
}

CmmStringValue * Parser::parse_string_value ()
{
   CmmStringValue * result = new CmmStringValue;
   result->kind = stringValue;
   storeLocation (result);
   on_other_expr ();
   result->value = readString ();
   while (isSymbol (string_literal))
   {
      result->items.push_back (parse_string_value_cont ());
   }
   on_string_value (result);
   return result;
}

CmmStringCont * Parser::parse_string_value_cont ()
{
   CmmStringCont * result = new CmmStringCont;
   storeLocation (result);
   result->value = readString ();
   return result;
}

CmmThisExpr * Parser::parse_this_expr ()
{
   CmmThisExpr * result = new CmmThisExpr;
   result->kind = thisExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_this);
   on_this_expr (result);
   return result;
}

CmmSubexprExpr * Parser::parse_subexpr_expr ()
{
   CmmSubexprExpr * result = new CmmSubexprExpr;
   result->kind = subexprExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (LPAREN);
   result->param = parse_expr ();
   checkSymbol (RPAREN);
   on_subexpr_expr (result);
   return result;
}

CmmExpr * Parser::parse_postfix_start ()
{
   CmmExpr * result = NULL;
   if (testSymbols (set_0))
   {
      result = parse_primary_expr ();
   }
   else if (testSymbols (set_1))
   {
      result = parse_modern_cast_expr ();
   }
   else if (isSymbol (keyword_typeid))
   {
      result = parse_typeid_expr ();
   }
   else if (isSymbol (keyword_typename))
   {
      result = parse_type_name ();
   }
   else if (testSymbols (set_2))
   {
      result = parse_type_specifier ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmModernCastExpr * Parser::parse_modern_cast_expr ()
{
   CmmModernCastExpr * result = new CmmModernCastExpr;
   result->kind = modernCastExp;
   storeLocation (result);
   on_other_expr ();
   if (isSymbol (keyword_dynamic_cast))
   {
      checkSymbol (keyword_dynamic_cast);
      result->cast_kind = DynamicCast;
   }
   else if (isSymbol (keyword_static_cast))
   {
      checkSymbol (keyword_static_cast);
      result->cast_kind = StaticCast;
   }
   else if (isSymbol (keyword_const_cast))
   {
      checkSymbol (keyword_const_cast);
      result->cast_kind = ConstCast;
   }
   else if (isSymbol (keyword_reinterpret_cast))
   {
      checkSymbol (keyword_reinterpret_cast);
      result->cast_kind = ReinterpreterCast;
   }
   else
   {
      stop ("Unexpected token");
   }
   checkSymbol (LESS);
   result->type = parse_type_id ();
   checkSymbol (GREATER);
   checkSymbol (LPAREN);
   result->param = parse_expr ();
   checkSymbol (RPAREN);
   return result;
}

CmmTypeIdExpr * Parser::parse_typeid_expr ()
{
   CmmTypeIdExpr * result = new CmmTypeIdExpr;
   result->kind = typeIdExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_typeid);
   checkSymbol (LPAREN);
   result->value = parse_expr ();
   checkSymbol (RPAREN);
   return result;
}

CmmExpr * Parser::parse_postfix_expr ()
{
   CmmExpr * result = NULL;
   result = parse_postfix_start ();
   if (is_postfix_expression (result))
   {
      while (isSymbol (LBRACKET) || isSymbol (LPAREN) || isSymbol (DOT) || isSymbol (ARROW) || isSymbol (INC) || isSymbol (DEC))
      {
         if (isSymbol (LBRACKET))
         {
            result = parse_index_expr (result);
         }
         else if (isSymbol (LPAREN))
         {
            result = parse_call_expr (result);
         }
         else if (isSymbol (DOT))
         {
            result = parse_field_expr (result);
         }
         else if (isSymbol (ARROW))
         {
            result = parse_ptr_field_expr (result);
         }
         else if (isSymbol (INC))
         {
            result = parse_post_inc_expr (result);
         }
         else if (isSymbol (DEC))
         {
            result = parse_post_dec_expr (result);
         }
         else
         {
            stop ("Unexpected token");
         }
      }
   }
   return result;
}

CmmIndexExpr * Parser::parse_index_expr (CmmExpr * store)
{
   CmmIndexExpr * result = new CmmIndexExpr;
   result->kind = indexExp;
   storeLocation (result);
   result->left = store;
   checkSymbol (LBRACKET);
   result->param = parse_assignment_expr ();
   checkSymbol (RBRACKET);
   return result;
}

CmmCallExpr * Parser::parse_call_expr (CmmExpr * store)
{
   CmmCallExpr * result = new CmmCallExpr;
   result->kind = callExp;
   storeLocation (result);
   result->left = store;
   checkSymbol (LPAREN);
   result->param_list = parse_expr_list ();
   checkSymbol (RPAREN);
   on_call_expr (result);
   return result;
}

CmmFieldExpr * Parser::parse_field_expr (CmmExpr * store)
{
   CmmFieldExpr * result = new CmmFieldExpr;
   result->kind = fieldExp;
   storeLocation (result);
   result->left = store;
   checkSymbol (DOT);
   result->simp_name = parse_simple_name ();
   on_field_expr (result);
   return result;
}

CmmPtrFieldExpr * Parser::parse_ptr_field_expr (CmmExpr * store)
{
   CmmPtrFieldExpr * result = new CmmPtrFieldExpr;
   result->kind = ptrFieldExp;
   storeLocation (result);
   result->left = store;
   checkSymbol (ARROW);
   result->simp_name = parse_simple_name ();
   on_ptr_field_expr (result);
   return result;
}

CmmPostIncExpr * Parser::parse_post_inc_expr (CmmExpr * store)
{
   CmmPostIncExpr * result = new CmmPostIncExpr;
   result->kind = postIncExp;
   storeLocation (result);
   result->left = store;
   checkSymbol (INC);
   return result;
}

CmmPostDecExpr * Parser::parse_post_dec_expr (CmmExpr * store)
{
   CmmPostDecExpr * result = new CmmPostDecExpr;
   result->kind = postDecExp;
   storeLocation (result);
   result->left = store;
   checkSymbol (DEC);
   return result;
}

CmmExprList * Parser::parse_expr_list ()
{
   CmmExprList * result = new CmmExprList;
   storeLocation (result);
   if (testSymbols (set_3))
   {
      result->items.push_back (parse_assignment_expr ());
      while (isSymbol (COMMA))
      {
         checkSymbol (COMMA);
         result->items.push_back (parse_assignment_expr ());
      }
   }
   return result;
}

CmmExpr * Parser::parse_unary_expr ()
{
   CmmExpr * result = NULL;
   if (testSymbols (set_4))
   {
      result = parse_postfix_expr ();
   }
   else if (isSymbol (INC))
   {
      result = parse_inc_expr ();
   }
   else if (isSymbol (DEC))
   {
      result = parse_dec_expr ();
   }
   else if (isSymbol (STAR))
   {
      result = parse_deref_expr ();
   }
   else if (isSymbol (BIT_AND))
   {
      result = parse_addr_expr ();
   }
   else if (isSymbol (PLUS))
   {
      result = parse_plus_expr ();
   }
   else if (isSymbol (MINUS))
   {
      result = parse_minus_expr ();
   }
   else if (isSymbol (BIT_NOT))
   {
      result = parse_bit_not_expr ();
   }
   else if (isSymbol (LOG_NOT))
   {
      result = parse_log_not_expr ();
   }
   else if (isSymbol (keyword_sizeof))
   {
      result = parse_sizeof_expr ();
   }
   else if (isSymbol (keyword_new))
   {
      result = parse_allocation_expr ();
   }
   else if (isSymbol (keyword_delete))
   {
      result = parse_deallocation_expr ();
   }
   else if (isSymbol (keyword_throw))
   {
      result = parse_throw_expr ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmIncExpr * Parser::parse_inc_expr ()
{
   CmmIncExpr * result = new CmmIncExpr;
   result->kind = incExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (INC);
   result->param = parse_cast_expr ();
   return result;
}

CmmDecExpr * Parser::parse_dec_expr ()
{
   CmmDecExpr * result = new CmmDecExpr;
   result->kind = decExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (DEC);
   result->param = parse_cast_expr ();
   return result;
}

CmmDerefExpr * Parser::parse_deref_expr ()
{
   CmmDerefExpr * result = new CmmDerefExpr;
   result->kind = derefExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (STAR);
   result->param = parse_cast_expr ();
   return result;
}

CmmAddrExpr * Parser::parse_addr_expr ()
{
   CmmAddrExpr * result = new CmmAddrExpr;
   result->kind = addrExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (BIT_AND);
   result->param = parse_cast_expr ();
   return result;
}

CmmPlusExpr * Parser::parse_plus_expr ()
{
   CmmPlusExpr * result = new CmmPlusExpr;
   result->kind = plusExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (PLUS);
   result->param = parse_cast_expr ();
   return result;
}

CmmMinusExpr * Parser::parse_minus_expr ()
{
   CmmMinusExpr * result = new CmmMinusExpr;
   result->kind = minusExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (MINUS);
   result->param = parse_cast_expr ();
   return result;
}

CmmBitNotExpr * Parser::parse_bit_not_expr ()
{
   CmmBitNotExpr * result = new CmmBitNotExpr;
   result->kind = bitNotExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (BIT_NOT);
   result->param = parse_cast_expr ();
   on_bit_not_expr (result);
   return result;
}

CmmLogNotExpr * Parser::parse_log_not_expr ()
{
   CmmLogNotExpr * result = new CmmLogNotExpr;
   result->kind = logNotExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (LOG_NOT);
   result->param = parse_cast_expr ();
   return result;
}

CmmSizeofExp * Parser::parse_sizeof_expr ()
{
   CmmSizeofExp * result = new CmmSizeofExp;
   result->kind = sizeofExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_sizeof);
   result->value = parse_unary_expr ();
   return result;
}

CmmNewExpr * Parser::parse_allocation_expr ()
{
   CmmNewExpr * result = new CmmNewExpr;
   result->kind = newExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_new);
   if (testSymbols (set_5))
   {
      result->type1 = parse_new_type_id ();
   }
   else if (isSymbol (LPAREN))
   {
      checkSymbol (LPAREN);
      result->type2 = parse_type_id ();
      checkSymbol (RPAREN);
   }
   else
   {
      stop ("Unexpected token");
   }
   if (isSymbol (LPAREN))
   {
      checkSymbol (LPAREN);
      result->init_list = parse_expr_list ();
      checkSymbol (RPAREN);
   }
   return result;
}

CmmNewTypeId * Parser::parse_new_type_id ()
{
   CmmNewTypeId * result = new CmmNewTypeId;
   storeLocation (result);
   result->type_spec = parse_type_specifiers ();
   result->ptr = parse_ptr_specifier_list ();
   while (isSymbol (LBRACKET))
   {
      result->items.push_back (parse_allocation_array_limit ());
   }
   return result;
}

CmmNewArrayLimit * Parser::parse_allocation_array_limit ()
{
   CmmNewArrayLimit * result = new CmmNewArrayLimit;
   storeLocation (result);
   checkSymbol (LBRACKET);
   result->value = parse_expr ();
   checkSymbol (RBRACKET);
   return result;
}

CmmDeleteExpr * Parser::parse_deallocation_expr ()
{
   CmmDeleteExpr * result = new CmmDeleteExpr;
   result->kind = deleteExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_delete);
   if (isSymbol (LBRACKET))
   {
      checkSymbol (LBRACKET);
      checkSymbol (RBRACKET);
      result->a_array = true;
   }
   result->param = parse_cast_expr ();
   return result;
}

CmmExpr * Parser::parse_cast_expr ()
{
   CmmExpr * result = NULL;
   if (isSymbol (keyword_type_cast))
   {
      result = parse_cast_formula ();
   }
   else if (testSymbols (set_6))
   {
      result = parse_unary_expr ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmTypeCastExpr * Parser::parse_cast_formula ()
{
   CmmTypeCastExpr * result = new CmmTypeCastExpr;
   result->kind = typecastExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_type_cast);
   checkSymbol (LESS);
   result->type = parse_type_id ();
   checkSymbol (GREATER);
   checkSymbol (LPAREN);
   result->param = parse_cast_expr ();
   checkSymbol (RPAREN);
   return result;
}

CmmExpr * Parser::parse_pm_expr ()
{
   CmmExpr * result = NULL;
   result = parse_cast_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (DOT_STAR) || isSymbol (ARROW_STAR))
      {
         CmmExpr * store = result;
         CmmPtrMemExpr * result1 = new CmmPtrMemExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         if (isSymbol (DOT_STAR))
         {
            checkSymbol (DOT_STAR);
            result1->kind = dotMemberExp;
         }
         else if (isSymbol (ARROW_STAR))
         {
            checkSymbol (ARROW_STAR);
            result1->kind = arrowMemberExp;
         }
         else
         {
            stop ("Unexpected token");
         }
         result1->right = parse_cast_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_multiplicative_expr ()
{
   CmmExpr * result = NULL;
   result = parse_pm_expr ();
   if (is_multiplicative_expression (result))
   {
      while (isSymbol (STAR) || isSymbol (SLASH) || isSymbol (MOD) || isSymbol (identifier) && is_mul (result))
      {
         CmmExpr * store = result;
         CmmMulExpr * result1 = new CmmMulExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         if (isSymbol (STAR))
         {
            checkSymbol (STAR);
            result1->kind = mulExp;
         }
         else if (isSymbol (SLASH))
         {
            checkSymbol (SLASH);
            result1->kind = divExp;
         }
         else if (isSymbol (MOD))
         {
            checkSymbol (MOD);
            result1->kind = modExp;
         }
         else if (isSymbol (identifier) && is_mul (result1))
         {
            result1->kind = customMulExp;
            result1->custom_mul = readIdent ();
         }
         else
         {
            stop ("Unexpected token");
         }
         result1->right = parse_pm_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_additive_expr ()
{
   CmmExpr * result = NULL;
   result = parse_multiplicative_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (PLUS) || isSymbol (MINUS) || isSymbol (identifier) && is_add (result))
      {
         CmmExpr * store = result;
         CmmAddExpr * result1 = new CmmAddExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         if (isSymbol (PLUS))
         {
            checkSymbol (PLUS);
            result1->kind = addExp;
         }
         else if (isSymbol (MINUS))
         {
            checkSymbol (MINUS);
            result1->kind = subExp;
         }
         else if (isSymbol (identifier) && is_add (result1))
         {
            result1->kind = customAddExp;
            result1->custom_add = readIdent ();
         }
         else
         {
            stop ("Unexpected token");
         }
         result1->right = parse_multiplicative_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_shift_expr ()
{
   CmmExpr * result = NULL;
   result = parse_additive_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (SHL) || isSymbol (SHR))
      {
         CmmExpr * store = result;
         CmmShiftExpr * result1 = new CmmShiftExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         if (isSymbol (SHL))
         {
            checkSymbol (SHL);
            result1->kind = shlExp;
         }
         else if (isSymbol (SHR))
         {
            checkSymbol (SHR);
            result1->kind = shrExp;
         }
         else
         {
            stop ("Unexpected token");
         }
         result1->right = parse_additive_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_relational_expr ()
{
   CmmExpr * result = NULL;
   result = parse_shift_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (LESS) || isSymbol (GREATER) || isSymbol (LESS_OR_EQUAL) || isSymbol (GREATER_OR_EQUAL))
      {
         CmmExpr * store = result;
         CmmRelExpr * result1 = new CmmRelExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         if (isSymbol (LESS))
         {
            checkSymbol (LESS);
            result1->kind = ltExp;
         }
         else if (isSymbol (GREATER))
         {
            checkSymbol (GREATER);
            result1->kind = gtExp;
         }
         else if (isSymbol (LESS_OR_EQUAL))
         {
            checkSymbol (LESS_OR_EQUAL);
            result1->kind = leExp;
         }
         else if (isSymbol (GREATER_OR_EQUAL))
         {
            checkSymbol (GREATER_OR_EQUAL);
            result1->kind = geExp;
         }
         else
         {
            stop ("Unexpected token");
         }
         result1->right = parse_shift_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_equality_expr ()
{
   CmmExpr * result = NULL;
   result = parse_relational_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (EQUAL) || isSymbol (UNEQUAL))
      {
         CmmExpr * store = result;
         CmmEqExpr * result1 = new CmmEqExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         if (isSymbol (EQUAL))
         {
            checkSymbol (EQUAL);
            result1->kind = eqExp;
         }
         else if (isSymbol (UNEQUAL))
         {
            checkSymbol (UNEQUAL);
            result1->kind = neExp;
         }
         else
         {
            stop ("Unexpected token");
         }
         result1->right = parse_relational_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_and_expr ()
{
   CmmExpr * result = NULL;
   result = parse_equality_expr ();
   if (is_and_expression (result))
   {
      while (isSymbol (BIT_AND))
      {
         CmmExpr * store = result;
         CmmAndExpr * result1 = new CmmAndExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         checkSymbol (BIT_AND);
         result1->kind = bitAndExp;
         result1->right = parse_equality_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_exclusive_or_expr ()
{
   CmmExpr * result = NULL;
   result = parse_and_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (BIT_XOR))
      {
         CmmExpr * store = result;
         CmmXorExpr * result1 = new CmmXorExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         checkSymbol (BIT_XOR);
         result1->kind = bitXorExp;
         result1->right = parse_and_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_inclusive_or_expr ()
{
   CmmExpr * result = NULL;
   result = parse_exclusive_or_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (BIT_OR))
      {
         CmmExpr * store = result;
         CmmOrExpr * result1 = new CmmOrExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         checkSymbol (BIT_OR);
         result1->kind = bitOrExp;
         result1->right = parse_exclusive_or_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_logical_and_expr ()
{
   CmmExpr * result = NULL;
   result = parse_inclusive_or_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (LOG_AND))
      {
         CmmExpr * store = result;
         CmmAndAndExpr * result1 = new CmmAndAndExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         checkSymbol (LOG_AND);
         result1->kind = logAndExp;
         result1->right = parse_inclusive_or_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_logical_or_expr ()
{
   CmmExpr * result = NULL;
   result = parse_logical_and_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (LOG_OR))
      {
         CmmExpr * store = result;
         CmmOrOrExpr * result1 = new CmmOrOrExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         checkSymbol (LOG_OR);
         result1->kind = logOrExp;
         result1->right = parse_logical_and_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_assignment_expr ()
{
   CmmExpr * result = NULL;
   result = parse_logical_or_expr ();
   if (is_binary_expression (result))
   {
      if (isSymbol (ASSIGN) || isSymbol (PLUS_ASSIGN) || isSymbol (MINUS_ASSIGN) || isSymbol (MUL_ASSIGN) || isSymbol (DIV_ASSIGN) || isSymbol (MOD_ASSIGN) || isSymbol (SHL_ASSIGN) || isSymbol (SHR_ASSIGN) || isSymbol (BIT_AND_ASSIGN) || isSymbol (BIT_XOR_ASSIGN) || isSymbol (BIT_OR_ASSIGN) || isSymbol (QUESTION))
      {
         CmmExpr * store = result;
         CmmAssignExpr * result1 = new CmmAssignExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         if (isSymbol (ASSIGN))
         {
            checkSymbol (ASSIGN);
            result1->kind = assignExp;
         }
         else if (isSymbol (PLUS_ASSIGN))
         {
            checkSymbol (PLUS_ASSIGN);
            result1->kind = addAssignExp;
         }
         else if (isSymbol (MINUS_ASSIGN))
         {
            checkSymbol (MINUS_ASSIGN);
            result1->kind = subAssignExp;
         }
         else if (isSymbol (MUL_ASSIGN))
         {
            checkSymbol (MUL_ASSIGN);
            result1->kind = mulAssignExp;
         }
         else if (isSymbol (DIV_ASSIGN))
         {
            checkSymbol (DIV_ASSIGN);
            result1->kind = divAssignExp;
         }
         else if (isSymbol (MOD_ASSIGN))
         {
            checkSymbol (MOD_ASSIGN);
            result1->kind = modAssignExp;
         }
         else if (isSymbol (SHL_ASSIGN))
         {
            checkSymbol (SHL_ASSIGN);
            result1->kind = shlAssignExp;
         }
         else if (isSymbol (SHR_ASSIGN))
         {
            checkSymbol (SHR_ASSIGN);
            result1->kind = shrAssignExp;
         }
         else if (isSymbol (BIT_AND_ASSIGN))
         {
            checkSymbol (BIT_AND_ASSIGN);
            result1->kind = andAssignExp;
         }
         else if (isSymbol (BIT_XOR_ASSIGN))
         {
            checkSymbol (BIT_XOR_ASSIGN);
            result1->kind = xorAssignExp;
         }
         else if (isSymbol (BIT_OR_ASSIGN))
         {
            checkSymbol (BIT_OR_ASSIGN);
            result1->kind = orAssignExp;
         }
         else if (isSymbol (QUESTION))
         {
            checkSymbol (QUESTION);
            result1->kind = condExp;
            result1->middle = parse_assignment_expr ();
            checkSymbol (COLON);
         }
         else
         {
            stop ("Unexpected token");
         }
         result1->right = parse_logical_or_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_colon_expr ()
{
   CmmExpr * result = NULL;
   result = parse_assignment_expr ();
   if (is_binary_expression (result))
   {
      if (isSymbol (COLON))
      {
         CmmExpr * store = result;
         CmmColonExpr * result1 = new CmmColonExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         checkSymbol (COLON);
         result1->kind = colonExp;
         result1->right = parse_assignment_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_comma_expr ()
{
   CmmExpr * result = NULL;
   result = parse_colon_expr ();
   if (is_binary_expression (result))
   {
      while (isSymbol (COMMA))
      {
         CmmExpr * store = result;
         CmmCommaExpr * result1 = new CmmCommaExpr;
         storeLocation (result1);
         result1->left = store;
         result = result1;
         checkSymbol (COMMA);
         result1->kind = commaExp;
         result1->right = parse_colon_expr ();
         on_binary_expr (result);
      }
   }
   return result;
}

CmmExpr * Parser::parse_expr ()
{
   CmmExpr * result = NULL;
   result = parse_comma_expr ();
   return result;
}

CmmExpr * Parser::parse_const_expr ()
{
   CmmExpr * result = NULL;
   result = parse_assignment_expr ();
   return result;
}

CmmStat * Parser::parse_stat ()
{
   CmmStat * result = NULL;
   if (testSymbols (set_3))
   {
      result = parse_flexible_stat ();
   }
   else if (isSymbol (SEMICOLON))
   {
      result = parse_empty_stat ();
   }
   else if (isSymbol (LBRACE))
   {
      result = parse_compound_stat ();
   }
   else if (isSymbol (keyword_if))
   {
      result = parse_if_stat ();
   }
   else if (isSymbol (keyword_while))
   {
      result = parse_while_stat ();
   }
   else if (isSymbol (keyword_do))
   {
      result = parse_do_stat ();
   }
   else if (isSymbol (keyword_for))
   {
      result = parse_for_stat ();
   }
   else if (isSymbol (keyword_switch))
   {
      result = parse_switch_stat ();
   }
   else if (isSymbol (keyword_case))
   {
      result = parse_case_stat ();
   }
   else if (isSymbol (keyword_default))
   {
      result = parse_default_stat ();
   }
   else if (isSymbol (keyword_break))
   {
      result = parse_break_stat ();
   }
   else if (isSymbol (keyword_continue))
   {
      result = parse_continue_stat ();
   }
   else if (isSymbol (keyword_return))
   {
      result = parse_return_stat ();
   }
   else if (isSymbol (keyword_goto))
   {
      result = parse_goto_stat ();
   }
   else if (isSymbol (keyword_try))
   {
      result = parse_try_stat ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmStat * Parser::parse_nested_stat ()
{
   CmmStat * result = NULL;
   result = parse_stat ();
   return result;
}

CmmEmptyStat * Parser::parse_empty_stat ()
{
   CmmEmptyStat * result = new CmmEmptyStat;
   result->mode = emptyStat;
   storeLocation (result);
   checkSymbol (SEMICOLON);
   return result;
}

CmmCompoundStat * Parser::parse_compound_stat ()
{
   CmmCompoundStat * result = new CmmCompoundStat;
   result->mode = compoundStat;
   storeLocation (result);
   checkSymbol (LBRACE);
   while (testSymbols (set_7))
   {
      result->items.push_back (parse_stat ());
   }
   checkSymbol (RBRACE);
   return result;
}

CmmIfStat * Parser::parse_if_stat ()
{
   CmmIfStat * result = new CmmIfStat;
   result->mode = ifStat;
   storeLocation (result);
   checkSymbol (keyword_if);
   checkSymbol (LPAREN);
   result->cond = parse_expr ();
   checkSymbol (RPAREN);
   result->then_stat = parse_nested_stat ();
   if (isSymbol (keyword_else))
   {
      checkSymbol (keyword_else);
      result->else_stat = parse_nested_stat ();
   }
   return result;
}

CmmWhileStat * Parser::parse_while_stat ()
{
   CmmWhileStat * result = new CmmWhileStat;
   result->mode = whileStat;
   storeLocation (result);
   checkSymbol (keyword_while);
   checkSymbol (LPAREN);
   result->cond = parse_expr ();
   checkSymbol (RPAREN);
   result->body = parse_nested_stat ();
   return result;
}

CmmDoStat * Parser::parse_do_stat ()
{
   CmmDoStat * result = new CmmDoStat;
   result->mode = doStat;
   storeLocation (result);
   checkSymbol (keyword_do);
   result->body = parse_nested_stat ();
   checkSymbol (keyword_while);
   checkSymbol (LPAREN);
   result->cond_expr = parse_expr ();
   checkSymbol (RPAREN);
   checkSymbol (SEMICOLON);
   return result;
}

CmmForStat * Parser::parse_for_stat ()
{
   CmmForStat * result = new CmmForStat;
   result->mode = forStat;
   storeLocation (result);
   checkSymbol (keyword_for);
   checkSymbol (LPAREN);
   if (testSymbols (set_3))
   {
      result->from_expr = parse_condition ();
   }
   if (isSymbol (COLON))
   {
      checkSymbol (COLON);
      result->iter_expr = parse_expr ();
   }
   else if (isSymbol (SEMICOLON))
   {
      checkSymbol (SEMICOLON);
      if (testSymbols (set_3))
      {
         result->cond_expr = parse_expr ();
      }
      checkSymbol (SEMICOLON);
      if (testSymbols (set_3))
      {
         result->step_expr = parse_expr ();
      }
   }
   else
   {
      stop ("Unexpected token");
   }
   checkSymbol (RPAREN);
   result->body = parse_nested_stat ();
   return result;
}

CmmSwitchStat * Parser::parse_switch_stat ()
{
   CmmSwitchStat * result = new CmmSwitchStat;
   result->mode = switchStat;
   storeLocation (result);
   checkSymbol (keyword_switch);
   checkSymbol (LPAREN);
   result->cond = parse_expr ();
   checkSymbol (RPAREN);
   result->body = parse_nested_stat ();
   return result;
}

CmmCaseStat * Parser::parse_case_stat ()
{
   CmmCaseStat * result = new CmmCaseStat;
   result->mode = caseStat;
   storeLocation (result);
   checkSymbol (keyword_case);
   result->case_expr = parse_expr ();
   checkSymbol (COLON);
   result->body = parse_nested_stat ();
   return result;
}

CmmDefaultStat * Parser::parse_default_stat ()
{
   CmmDefaultStat * result = new CmmDefaultStat;
   result->mode = defaultStat;
   storeLocation (result);
   checkSymbol (keyword_default);
   checkSymbol (COLON);
   result->body = parse_nested_stat ();
   return result;
}

CmmBreakStat * Parser::parse_break_stat ()
{
   CmmBreakStat * result = new CmmBreakStat;
   result->mode = breakStat;
   storeLocation (result);
   checkSymbol (keyword_break);
   checkSymbol (SEMICOLON);
   return result;
}

CmmContinueStat * Parser::parse_continue_stat ()
{
   CmmContinueStat * result = new CmmContinueStat;
   result->mode = continueStat;
   storeLocation (result);
   checkSymbol (keyword_continue);
   checkSymbol (SEMICOLON);
   return result;
}

CmmReturnStat * Parser::parse_return_stat ()
{
   CmmReturnStat * result = new CmmReturnStat;
   result->mode = returnStat;
   storeLocation (result);
   checkSymbol (keyword_return);
   if (testSymbols (set_3))
   {
      result->return_expr = parse_expr ();
   }
   checkSymbol (SEMICOLON);
   return result;
}

CmmGotoStat * Parser::parse_goto_stat ()
{
   CmmGotoStat * result = new CmmGotoStat;
   result->mode = gotoStat;
   storeLocation (result);
   checkSymbol (keyword_goto);
   result->goto_lab = readIdent ();
   checkSymbol (SEMICOLON);
   return result;
}

CmmStat * Parser::parse_flexible_stat ()
{
   CmmStat * result = NULL;
   on_begin_flexible_expr ();
   CmmExpr * result0 = parse_expr ();
   on_end_flexible_expr ();
   if (isSymbol (SEMICOLON) && is_expression (result0))
   {
      result = parse_simple_stat (result0);
   }
   else if (isSymbol (LBRACE) && is_expression (result0))
   {
      result = parse_block_stat (result0);
   }
   else if (testSymbols (set_8))
   {
      result = parse_continue_simple_declaration (result0);
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmSimpleStat * Parser::parse_simple_stat (CmmExpr * store)
{
   CmmSimpleStat * result = new CmmSimpleStat;
   result->mode = simpleStat;
   storeLocation (result);
   result->inner_expr = store;
   checkSymbol (SEMICOLON);
   on_simple_statement (result);
   return result;
}

CmmBlockStat * Parser::parse_block_stat (CmmExpr * store)
{
   CmmBlockStat * result = new CmmBlockStat;
   result->mode = blockStat;
   storeLocation (result);
   result->inner_expr = store;
   result->body = parse_compound_stat ();
   on_block_statement (result);
   return result;
}

CmmDeclSpec * Parser::parse_decl_specifiers ()
{
   CmmDeclSpec * result = new CmmDeclSpec;
   storeLocation (result);
   while (isSymbol (keyword_inline) || isSymbol (keyword_virtual) || isSymbol (keyword_explicit) || isSymbol (keyword_mutable) || isSymbol (keyword_static) || isSymbol (keyword_register))
   {
      if (isSymbol (keyword_inline))
      {
         checkSymbol (keyword_inline);
         result->a_inline = true;
      }
      else if (isSymbol (keyword_virtual))
      {
         checkSymbol (keyword_virtual);
         result->a_virtual = true;
      }
      else if (isSymbol (keyword_explicit))
      {
         checkSymbol (keyword_explicit);
         result->a_explicit = true;
      }
      else if (isSymbol (keyword_mutable))
      {
         checkSymbol (keyword_mutable);
         result->a_mutable = true;
      }
      else if (isSymbol (keyword_static))
      {
         checkSymbol (keyword_static);
         result->a_static = true;
      }
      else if (isSymbol (keyword_register))
      {
         checkSymbol (keyword_register);
         result->a_register = true;
      }
      else
      {
         stop ("Unexpected token");
      }
   }
   return result;
}

CmmConstSpecifier * Parser::parse_const_specifier ()
{
   CmmConstSpecifier * result = new CmmConstSpecifier;
   result->kind = constSpec;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_const);
   result->param = parse_type_specifiers ();
   on_cv_specifier (result);
   return result;
}

CmmVolatileSpecifier * Parser::parse_volatile_specifier ()
{
   CmmVolatileSpecifier * result = new CmmVolatileSpecifier;
   result->kind = volatileSpec;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_volatile);
   result->param = parse_type_specifiers ();
   on_cv_specifier (result);
   return result;
}

CmmTypeSpec * Parser::parse_type_specifier ()
{
   CmmTypeSpec * result = new CmmTypeSpec;
   result->kind = typeSpec;
   storeLocation (result);
   on_other_expr ();
   if (isSymbol (keyword_signed) || isSymbol (keyword_unsigned))
   {
      if (isSymbol (keyword_signed))
      {
         checkSymbol (keyword_signed);
         result->a_signed = true;
      }
      else if (isSymbol (keyword_unsigned))
      {
         checkSymbol (keyword_unsigned);
         result->a_unsigned = true;
      }
      else
      {
         stop ("Unexpected token");
      }
   }
   if (isSymbol (keyword_short))
   {
      checkSymbol (keyword_short);
      result->a_short = true;
   }
   else if (isSymbol (keyword_long))
   {
      checkSymbol (keyword_long);
      result->a_long = true;
      if (isSymbol (keyword_long) || isSymbol (keyword_double))
      {
         if (isSymbol (keyword_long))
         {
            checkSymbol (keyword_long);
            result->a_long_long = true;
         }
         else if (isSymbol (keyword_double))
         {
            checkSymbol (keyword_double);
            result->a_long_double = true;
         }
         else
         {
            stop ("Unexpected token");
         }
      }
   }
   else if (isSymbol (keyword_bool))
   {
      checkSymbol (keyword_bool);
      result->a_bool = true;
   }
   else if (isSymbol (keyword_char))
   {
      checkSymbol (keyword_char);
      result->a_char = true;
   }
   else if (isSymbol (keyword_wchar_t))
   {
      checkSymbol (keyword_wchar_t);
      result->a_wchar = true;
   }
   else if (isSymbol (keyword_int))
   {
      checkSymbol (keyword_int);
      result->a_int = true;
   }
   else if (isSymbol (keyword_float))
   {
      checkSymbol (keyword_float);
      result->a_float = true;
   }
   else if (isSymbol (keyword_double))
   {
      checkSymbol (keyword_double);
      result->a_double = true;
   }
   else if (isSymbol (keyword_void))
   {
      checkSymbol (keyword_void);
      result->a_void = true;
   }
   else
   {
      stop ("Unexpected token");
   }
   on_type_specifier (result);
   return result;
}

CmmTypeName * Parser::parse_type_name ()
{
   CmmTypeName * result = new CmmTypeName;
   result->kind = typeName;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_typename);
   result->qual_name = parse_qualified_name ();
   on_type_name (result);
   return result;
}

CmmExpr * Parser::parse_type_specifiers ()
{
   CmmExpr * result = NULL;
   if (isSymbol (identifier) || isSymbol (SCOPE) || isSymbol (keyword_operator))
   {
      result = parse_ident_expr ();
   }
   else if (isSymbol (keyword_typename))
   {
      result = parse_type_name ();
   }
   else if (testSymbols (set_2))
   {
      result = parse_type_specifier ();
   }
   else if (isSymbol (keyword_const))
   {
      result = parse_const_specifier ();
   }
   else if (isSymbol (keyword_volatile))
   {
      result = parse_volatile_specifier ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmPtrSpecifierSect * Parser::parse_ptr_specifier_list ()
{
   CmmPtrSpecifierSect * result = new CmmPtrSpecifierSect;
   storeLocation (result);
   while (isSymbol (BIT_AND) || isSymbol (STAR))
   {
      result->items.push_back (parse_ptr_specifier ());
   }
   return result;
}

CmmPtrSpecifier * Parser::parse_ptr_specifier ()
{
   CmmPtrSpecifier * result = NULL;
   if (isSymbol (STAR))
   {
      result = parse_pointer_specifier ();
   }
   else if (isSymbol (BIT_AND))
   {
      result = parse_reference_specifier ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmPointerSpecifier * Parser::parse_pointer_specifier ()
{
   CmmPointerSpecifier * result = new CmmPointerSpecifier;
   result->kind = pointerSpec;
   storeLocation (result);
   checkSymbol (STAR);
   result->cv_spec = parse_cv_specifier_list ();
   return result;
}

CmmReferenceSpecifier * Parser::parse_reference_specifier ()
{
   CmmReferenceSpecifier * result = new CmmReferenceSpecifier;
   result->kind = referenceSpec;
   storeLocation (result);
   checkSymbol (BIT_AND);
   result->cv_spec = parse_cv_specifier_list ();
   return result;
}

CmmCvSpecifier * Parser::parse_cv_specifier_list ()
{
   CmmCvSpecifier * result = new CmmCvSpecifier;
   storeLocation (result);
   while (isSymbol (keyword_const) || isSymbol (keyword_volatile))
   {
      if (isSymbol (keyword_const))
      {
         checkSymbol (keyword_const);
         result->cv_const = true;
      }
      else if (isSymbol (keyword_volatile))
      {
         checkSymbol (keyword_volatile);
         result->cv_volatile = true;
      }
      else
      {
         stop ("Unexpected token");
      }
   }
   return result;
}

CmmContSpecifierSect * Parser::parse_cont_specifier_list ()
{
   CmmContSpecifierSect * result = new CmmContSpecifierSect;
   storeLocation (result);
   while (isSymbol (LPAREN) || isSymbol (LBRACKET))
   {
      result->items.push_back (parse_cont_specifier ());
   }
   return result;
}

CmmContSpecifier * Parser::parse_cont_specifier ()
{
   CmmContSpecifier * result = NULL;
   if (isSymbol (LBRACKET))
   {
      result = parse_array_specifier ();
   }
   else if (isSymbol (LPAREN))
   {
      result = parse_function_specifier ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmArraySpecifier * Parser::parse_array_specifier ()
{
   CmmArraySpecifier * result = new CmmArraySpecifier;
   result->kind = arraySpec;
   storeLocation (result);
   checkSymbol (LBRACKET);
   if (testSymbols (set_3))
   {
      result->lim = parse_expr ();
   }
   checkSymbol (RBRACKET);
   return result;
}

CmmFunctionSpecifier * Parser::parse_function_specifier ()
{
   CmmFunctionSpecifier * result = new CmmFunctionSpecifier;
   result->kind = functionSpec;
   storeLocation (result);
   checkSymbol (LPAREN);
   result->parameters = parse_parameter_declaration_list ();
   checkSymbol (RPAREN);
   if (isSymbol (keyword_const))
   {
      checkSymbol (keyword_const);
      result->a_const = true;
   }
   if (isSymbol (keyword_throw))
   {
      result->exception_spec = parse_exception_specification ();
   }
   return result;
}

CmmStatSect * Parser::parse_parameter_declaration_list ()
{
   CmmStatSect * result = new CmmStatSect;
   storeLocation (result);
   if (testSymbols (set_9))
   {
      result->items.push_back (parse_parameter_declaration ());
      while (isSymbol (COMMA))
      {
         checkSymbol (COMMA);
         result->items.push_back (parse_parameter_declaration ());
      }
   }
   return result;
}

CmmStat * Parser::parse_parameter_declaration ()
{
   CmmStat * result = NULL;
   if (testSymbols (set_3))
   {
      on_begin_flexible_expr ();
      CmmExpr * result0 = parse_assignment_expr ();
      on_end_flexible_expr ();
      if (is_value_parameter (result0))
      {
         result = parse_value_parameter (result0);
      }
      else if (testSymbols (set_10))
      {
         result = parse_plain_parameter (result0);
      }
      else
      {
         stop ("Unexpected token");
      }
   }
   else if (isSymbol (DOTS))
   {
      on_begin_flexible_expr ();
      result = parse_dots_parameter ();
      on_end_flexible_expr ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmSimpleStat * Parser::parse_value_parameter (CmmExpr * store)
{
   CmmSimpleStat * result = new CmmSimpleStat;
   result->mode = simpleStat;
   storeLocation (result);
   result->inner_expr = store;
   return result;
}

CmmSimpleDecl * Parser::parse_plain_parameter (CmmExpr * store)
{
   CmmSimpleDecl * result = new CmmSimpleDecl;
   result->mode = simpleDecl;
   storeLocation (result);
   result->type_spec = store;
   result->items.push_back (parse_parameter_item ());
   return result;
}

CmmSimpleItem * Parser::parse_parameter_item ()
{
   CmmSimpleItem * result = new CmmSimpleItem;
   storeLocation (result);
   result->declarator = parse_declarator ();
   if (isSymbol (ASSIGN))
   {
      result->init = parse_initializer ();
   }
   result->attr = parse_attr_list ();
   return result;
}

CmmDotsParam * Parser::parse_dots_parameter ()
{
   CmmDotsParam * result = new CmmDotsParam;
   storeLocation (result);
   checkSymbol (DOTS);
   return result;
}

CmmDeclarator * Parser::parse_declarator ()
{
   CmmDeclarator * result = NULL;
   CmmPtrSpecifierSect * result0 = parse_ptr_specifier_list ();
   if (isSymbol (identifier) || isSymbol (SCOPE) || isSymbol (keyword_operator))
   {
      result = parse_basic_declarator (result0);
   }
   else if (isSymbol (LPAREN) || isSymbol (LBRACKET))
   {
      result = parse_empty_declarator (result0);
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmBasicDeclarator * Parser::parse_basic_declarator (CmmPtrSpecifierSect * store)
{
   CmmBasicDeclarator * result = new CmmBasicDeclarator;
   result->kind = basicDeclarator;
   storeLocation (result);
   result->ptr_spec = store;
   result->qual_name = parse_qualified_name ();
   result->cont_spec = parse_cont_specifier_list ();
   return result;
}

CmmEmptyDeclarator * Parser::parse_empty_declarator (CmmPtrSpecifierSect * store)
{
   CmmEmptyDeclarator * result = new CmmEmptyDeclarator;
   result->kind = emptyDeclarator;
   storeLocation (result);
   result->ptr_spec = store;
   result->cont_spec = parse_cont_specifier_list ();
   return result;
}

CmmNestedDeclarator * Parser::parse_nested_declarator (CmmPtrSpecifierSect * store)
{
   CmmNestedDeclarator * result = new CmmNestedDeclarator;
   result->kind = nestedDeclarator;
   storeLocation (result);
   result->ptr_spec = store;
   checkSymbol (LPAREN);
   result->inner_declarator = parse_declarator ();
   checkSymbol (RPAREN);
   result->cont_spec = parse_cont_specifier_list ();
   return result;
}

CmmDeclarator * Parser::parse_typedef_declarator ()
{
   CmmDeclarator * result = NULL;
   CmmPtrSpecifierSect * result0 = parse_ptr_specifier_list ();
   if (isSymbol (identifier) || isSymbol (SCOPE) || isSymbol (keyword_operator))
   {
      result = parse_basic_declarator (result0);
   }
   else if (isSymbol (LPAREN))
   {
      result = parse_nested_declarator (result0);
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmTypeId * Parser::parse_type_id ()
{
   CmmTypeId * result = new CmmTypeId;
   storeLocation (result);
   result->type_spec = parse_type_specifiers ();
   result->declarator = parse_declarator ();
   return result;
}

CmmInitializer * Parser::parse_initializer ()
{
   CmmInitializer * result = new CmmInitializer;
   storeLocation (result);
   checkSymbol (ASSIGN);
   result->value = parse_initializer_item ();
   return result;
}

CmmInitItem * Parser::parse_initializer_item ()
{
   CmmInitItem * result = NULL;
   if (testSymbols (set_3))
   {
      result = parse_simple_initializer ();
   }
   else if (isSymbol (LBRACE))
   {
      result = parse_initializer_list ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmInitSimple * Parser::parse_simple_initializer ()
{
   CmmInitSimple * result = new CmmInitSimple;
   storeLocation (result);
   result->inner_expr = parse_assignment_expr ();
   return result;
}

CmmInitList * Parser::parse_initializer_list ()
{
   CmmInitList * result = new CmmInitList;
   storeLocation (result);
   checkSymbol (LBRACE);
   if (testSymbols (set_11))
   {
      result->items.push_back (parse_initializer_item ());
      while (isSymbol (COMMA))
      {
         checkSymbol (COMMA);
         result->items.push_back (parse_initializer_item ());
      }
   }
   checkSymbol (RBRACE);
   return result;
}

CmmAttr * Parser::parse_attr_item ()
{
   CmmAttr * result = new CmmAttr;
   storeLocation (result);
   result->attr_expr = parse_assignment_expr ();
   return result;
}

CmmAttrGroup * Parser::parse_attr_group ()
{
   CmmAttrGroup * result = new CmmAttrGroup;
   storeLocation (result);
   checkSymbol (literal_1);
   result->items.push_back (parse_attr_item ());
   while (isSymbol (COMMA))
   {
      checkSymbol (COMMA);
      result->items.push_back (parse_attr_item ());
   }
   checkSymbol (literal_2);
   return result;
}

CmmAttrSect * Parser::parse_attr_list ()
{
   CmmAttrSect * result = new CmmAttrSect;
   storeLocation (result);
   while (isSymbol (literal_1))
   {
      result->items.push_back (parse_attr_group ());
   }
   return result;
}

CmmSimpleDecl * Parser::parse_simple_declaration ()
{
   CmmSimpleDecl * result = new CmmSimpleDecl;
   result->mode = simpleDecl;
   storeLocation (result);
   if (testSymbols (set_12))
   {
      result->decl_spec = parse_decl_specifiers ();
   }
   if (isSymbol (BIT_NOT))
   {
      checkSymbol (BIT_NOT);
      result->a_destructor = true;
   }
   result->type_spec = parse_type_specifiers ();
   parse_modify_simple_declaration (result);
   return result;
}

CmmSimpleDecl * Parser::parse_continue_simple_declaration (CmmExpr * store)
{
   CmmSimpleDecl * result = new CmmSimpleDecl;
   result->mode = simpleDecl;
   storeLocation (result);
   result->type_spec = store;
   parse_modify_simple_declaration (result);
   return result;
}

CmmSimpleDecl * Parser::parse_modify_simple_declaration (CmmSimpleDecl * result)
{
   result->items.push_back (parse_simple_item ());
   on_simple_item (result);
   while (isSymbol (COMMA))
   {
      checkSymbol (COMMA);
      result->items.push_back (parse_simple_item ());
      on_simple_item (result);
   }
   if (isSymbol (COLON) || isSymbol (LBRACE))
   {
      if (isSymbol (COLON) && is_constructor (result))
      {
         checkSymbol (COLON);
         result->ctor_init = parse_ctor_initializer ();
      }
      on_open_function (result);
      result->body = parse_compound_stat ();
      on_close_function (result);
   }
   else if (isSymbol (SEMICOLON))
   {
      checkSymbol (SEMICOLON);
   }
   else
   {
      stop ("Unexpected token");
   }
   on_simple_declaration (result);
   return result;
}

CmmSimpleItem * Parser::parse_simple_item ()
{
   CmmSimpleItem * result = new CmmSimpleItem;
   storeLocation (result);
   result->declarator = parse_declarator ();
   if (isSymbol (COLON))
   {
      checkSymbol (COLON);
      result->width = parse_const_expr ();
   }
   if (isSymbol (ASSIGN))
   {
      result->init = parse_initializer ();
   }
   result->attr = parse_attr_list ();
   return result;
}

CmmStat * Parser::parse_condition ()
{
   CmmStat * result = NULL;
   on_begin_flexible_expr ();
   CmmExpr * result0 = parse_expr ();
   on_end_flexible_expr ();
   if (is_expression (result0))
   {
      result = parse_condition_value (result0);
   }
   else if (testSymbols (set_13))
   {
      result = parse_condition_declaration (result0);
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmSimpleDecl * Parser::parse_condition_declaration (CmmExpr * store)
{
   CmmSimpleDecl * result = new CmmSimpleDecl;
   result->mode = simpleDecl;
   storeLocation (result);
   result->type_spec = store;
   result->items.push_back (parse_simple_item ());
   on_simple_item (result);
   while (isSymbol (COMMA))
   {
      checkSymbol (COMMA);
      result->items.push_back (parse_simple_item ());
      on_simple_item (result);
   }
   return result;
}

CmmSimpleStat * Parser::parse_condition_value (CmmExpr * store)
{
   CmmSimpleStat * result = new CmmSimpleStat;
   result->mode = simpleStat;
   storeLocation (result);
   result->inner_expr = store;
   return result;
}

CmmStatSect * Parser::parse_declaration_list ()
{
   CmmStatSect * result = new CmmStatSect;
   storeLocation (result);
   while (testSymbols (set_14))
   {
      result->items.push_back (parse_declaration ());
   }
   return result;
}

CmmStat * Parser::parse_declaration ()
{
   CmmStat * result = NULL;
   if (testSymbols (set_3))
   {
      result = parse_flexible_stat ();
   }
   else if (isSymbol (SEMICOLON))
   {
      result = parse_empty_stat ();
   }
   else if (isSymbol (LBRACE))
   {
      result = parse_compound_stat ();
   }
   else if (isSymbol (keyword_if))
   {
      result = parse_if_stat ();
   }
   else if (isSymbol (keyword_while))
   {
      result = parse_while_stat ();
   }
   else if (isSymbol (keyword_for))
   {
      result = parse_for_stat ();
   }
   else if (isSymbol (keyword_class) || isSymbol (keyword_struct) || isSymbol (keyword_union))
   {
      result = parse_class_declaration ();
   }
   else if (isSymbol (keyword_enum))
   {
      result = parse_enum_declaration ();
   }
   else if (isSymbol (keyword_typedef))
   {
      result = parse_typedef_declaration ();
   }
   else if (isSymbol (keyword_friend))
   {
      result = parse_friend_declaration ();
   }
   else if (isSymbol (keyword_namespace))
   {
      result = parse_namespace_declaration ();
   }
   else if (isSymbol (keyword_extern))
   {
      result = parse_extern_declaration ();
   }
   else if (isSymbol (keyword_using))
   {
      result = parse_using_declaration ();
   }
   else if (isSymbol (keyword_template))
   {
      result = parse_template_declaration ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmNamespaceDecl * Parser::parse_namespace_declaration ()
{
   CmmNamespaceDecl * result = new CmmNamespaceDecl;
   result->mode = namespaceDecl;
   storeLocation (result);
   on_start_namespace (result);
   checkSymbol (keyword_namespace);
   result->simp_name = parse_simple_name ();
   result->attr = parse_attr_list ();
   on_namespace (result);
   checkSymbol (LBRACE);
   on_open_namespace (result);
   result->body = parse_declaration_list ();
   on_close_namespace (result);
   checkSymbol (RBRACE);
   on_stop_namespace (result);
   return result;
}

CmmUsingDecl * Parser::parse_using_declaration ()
{
   CmmUsingDecl * result = new CmmUsingDecl;
   result->mode = usingDecl;
   storeLocation (result);
   checkSymbol (keyword_using);
   if (isSymbol (keyword_namespace))
   {
      checkSymbol (keyword_namespace);
      result->a_namespace = true;
   }
   result->qual_name = parse_qualified_name ();
   checkSymbol (SEMICOLON);
   return result;
}

CmmExternDecl * Parser::parse_extern_declaration ()
{
   CmmExternDecl * result = new CmmExternDecl;
   result->mode = externDecl;
   storeLocation (result);
   checkSymbol (keyword_extern);
   if (isSymbol (string_literal))
   {
      result->language = readString ();
   }
   if (isSymbol (LBRACE))
   {
      checkSymbol (LBRACE);
      result->decl_list = parse_declaration_list ();
      checkSymbol (RBRACE);
   }
   else if (testSymbols (set_14))
   {
      result->inner_declaration = parse_declaration ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmTypedefDecl * Parser::parse_typedef_declaration ()
{
   CmmTypedefDecl * result = new CmmTypedefDecl;
   result->mode = typedefDecl;
   storeLocation (result);
   checkSymbol (keyword_typedef);
   result->type_spec = parse_type_specifiers ();
   result->declarator = parse_typedef_declarator ();
   on_typedef (result);
   checkSymbol (SEMICOLON);
   return result;
}

CmmFriendDecl * Parser::parse_friend_declaration ()
{
   CmmFriendDecl * result = new CmmFriendDecl;
   result->mode = friendDecl;
   storeLocation (result);
   checkSymbol (keyword_friend);
   result->inner_declaration = parse_declaration ();
   return result;
}

CmmEnumDecl * Parser::parse_enum_declaration ()
{
   CmmEnumDecl * result = new CmmEnumDecl;
   result->mode = enumDecl;
   storeLocation (result);
   on_start_enum (result);
   checkSymbol (keyword_enum);
   result->simp_name = parse_simple_name ();
   on_enum (result);
   if (isSymbol (LBRACE))
   {
      checkSymbol (LBRACE);
      on_open_enum (result);
      result->enum_items = parse_enum_list ();
      on_close_enum (result);
      checkSymbol (RBRACE);
   }
   checkSymbol (SEMICOLON);
   on_stop_enum (result);
   return result;
}

CmmEnumSect * Parser::parse_enum_list ()
{
   CmmEnumSect * result = new CmmEnumSect;
   storeLocation (result);
   if (isSymbol (identifier))
   {
      result->items.push_back (parse_enumerator ());
      while (isSymbol (COMMA))
      {
         checkSymbol (COMMA);
         result->items.push_back (parse_enumerator ());
      }
   }
   return result;
}

CmmEnumItem * Parser::parse_enumerator ()
{
   CmmEnumItem * result = new CmmEnumItem;
   storeLocation (result);
   result->simp_name = parse_simple_name ();
   on_enum_item (result);
   if (isSymbol (ASSIGN))
   {
      checkSymbol (ASSIGN);
      result->init_value = parse_const_expr ();
   }
   return result;
}

CmmClassDecl * Parser::parse_class_declaration ()
{
   CmmClassDecl * result = new CmmClassDecl;
   result->mode = classDecl;
   storeLocation (result);
   on_start_class (result);
   if (isSymbol (keyword_class))
   {
      checkSymbol (keyword_class);
      result->style = ClassStyle;
   }
   else if (isSymbol (keyword_struct))
   {
      checkSymbol (keyword_struct);
      result->style = StructStyle;
   }
   else if (isSymbol (keyword_union))
   {
      checkSymbol (keyword_union);
      result->style = UnionStyle;
   }
   else
   {
      stop ("Unexpected token");
   }
   result->simp_name = parse_simple_name ();
   result->attr = parse_attr_list ();
   on_class (result);
   if (isSymbol (COLON) || isSymbol (LBRACE))
   {
      if (isSymbol (COLON))
      {
         checkSymbol (COLON);
         result->base_list = parse_base_specifier_list ();
      }
      checkSymbol (LBRACE);
      on_open_class (result);
      result->members = parse_member_list ();
      on_close_class (result);
      checkSymbol (RBRACE);
   }
   checkSymbol (SEMICOLON);
   on_stop_class (result);
   return result;
}

CmmMemberVisibility * Parser::parse_member_visibility ()
{
   CmmMemberVisibility * result = new CmmMemberVisibility;
   result->mode = visibilityDecl;
   storeLocation (result);
   if (isSymbol (keyword_private))
   {
      checkSymbol (keyword_private);
      result->access = PrivateAccess;
   }
   else if (isSymbol (keyword_protected))
   {
      checkSymbol (keyword_protected);
      result->access = ProtectedAccess;
   }
   else if (isSymbol (keyword_public))
   {
      checkSymbol (keyword_public);
      result->access = PublicAccess;
   }
   else
   {
      stop ("Unexpected token");
   }
   checkSymbol (COLON);
   return result;
}

CmmStat * Parser::parse_member_item ()
{
   CmmStat * result = NULL;
   if (isSymbol (keyword_private) || isSymbol (keyword_protected) || isSymbol (keyword_public))
   {
      result = parse_member_visibility ();
   }
   else if (testSymbols (set_3))
   {
      result = parse_flexible_stat ();
   }
   else if (isSymbol (SEMICOLON))
   {
      result = parse_empty_stat ();
   }
   else if (isSymbol (LBRACE))
   {
      result = parse_compound_stat ();
   }
   else if (isSymbol (keyword_if))
   {
      result = parse_if_stat ();
   }
   else if (isSymbol (keyword_while))
   {
      result = parse_while_stat ();
   }
   else if (isSymbol (keyword_for))
   {
      result = parse_for_stat ();
   }
   else if (isSymbol (keyword_class) || isSymbol (keyword_struct) || isSymbol (keyword_union))
   {
      result = parse_class_declaration ();
   }
   else if (isSymbol (keyword_enum))
   {
      result = parse_enum_declaration ();
   }
   else if (isSymbol (keyword_typedef))
   {
      result = parse_typedef_declaration ();
   }
   else if (isSymbol (keyword_friend))
   {
      result = parse_friend_declaration ();
   }
   else if (isSymbol (keyword_using))
   {
      result = parse_using_declaration ();
   }
   else if (isSymbol (keyword_template))
   {
      result = parse_template_declaration ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmStatSect * Parser::parse_member_list ()
{
   CmmStatSect * result = new CmmStatSect;
   storeLocation (result);
   while (testSymbols (set_15))
   {
      result->items.push_back (parse_member_item ());
   }
   return result;
}

CmmBaseSect * Parser::parse_base_specifier_list ()
{
   CmmBaseSect * result = new CmmBaseSect;
   storeLocation (result);
   result->items.push_back (parse_base_specifier ());
   while (isSymbol (COMMA))
   {
      checkSymbol (COMMA);
      result->items.push_back (parse_base_specifier ());
   }
   return result;
}

CmmBaseItem * Parser::parse_base_specifier ()
{
   CmmBaseItem * result = new CmmBaseItem;
   storeLocation (result);
   if (isSymbol (keyword_virtual))
   {
      checkSymbol (keyword_virtual);
      result->a_virtual = true;
   }
   if (isSymbol (keyword_private))
   {
      checkSymbol (keyword_private);
      result->access = PrivateBase;
   }
   else if (isSymbol (keyword_protected))
   {
      checkSymbol (keyword_protected);
      result->access = ProtectedBase;
   }
   else if (isSymbol (keyword_public))
   {
      checkSymbol (keyword_public);
      result->access = PublicBase;
   }
   else
   {
      result->access = UnknownBase;
   }
   result->from_cls = parse_qualified_name ();
   return result;
}

CmmCtorInitializer * Parser::parse_ctor_initializer ()
{
   CmmCtorInitializer * result = new CmmCtorInitializer;
   storeLocation (result);
   result->items.push_back (parse_member_initializer ());
   while (isSymbol (COMMA))
   {
      checkSymbol (COMMA);
      result->items.push_back (parse_member_initializer ());
   }
   return result;
}

CmmMemberInitializer * Parser::parse_member_initializer ()
{
   CmmMemberInitializer * result = new CmmMemberInitializer;
   storeLocation (result);
   result->simp_name = parse_simple_name ();
   checkSymbol (LPAREN);
   result->params = parse_expr_list ();
   checkSymbol (RPAREN);
   return result;
}

CmmConvSpec * Parser::parse_conversion_specifiers ()
{
   CmmConvSpec * result = new CmmConvSpec;
   storeLocation (result);
   result->type_spec = parse_type_specifiers ();
   result->ptr_spec = parse_ptr_specifier_list ();
   return result;
}

CmmSpecialFuction * Parser::parse_special_function ()
{
   CmmSpecialFuction * result = new CmmSpecialFuction;
   result->kind = specialName;
   storeLocation (result);
   checkSymbol (keyword_operator);
   if (testSymbols (set_5))
   {
      result->conv = parse_conversion_specifiers ();
   }
   else if (isSymbol (keyword_new))
   {
      checkSymbol (keyword_new);
      result->spec_new = true;
      if (isSymbol (LBRACKET))
      {
         checkSymbol (LBRACKET);
         checkSymbol (RBRACKET);
         result->spec_new_array = true;
      }
   }
   else if (isSymbol (keyword_delete))
   {
      checkSymbol (keyword_delete);
      result->spec_delete = true;
      if (isSymbol (LBRACKET))
      {
         checkSymbol (LBRACKET);
         checkSymbol (RBRACKET);
         result->spec_delete_array = true;
      }
   }
   else if (isSymbol (PLUS))
   {
      checkSymbol (PLUS);
      result->spec_add = true;
   }
   else if (isSymbol (MINUS))
   {
      checkSymbol (MINUS);
      result->spec_sub = true;
   }
   else if (isSymbol (STAR))
   {
      checkSymbol (STAR);
      result->spec_mul = true;
   }
   else if (isSymbol (SLASH))
   {
      checkSymbol (SLASH);
      result->spec_div = true;
   }
   else if (isSymbol (MOD))
   {
      checkSymbol (MOD);
      result->spec_mod = true;
   }
   else if (isSymbol (BIT_XOR))
   {
      checkSymbol (BIT_XOR);
      result->spec_xor = true;
   }
   else if (isSymbol (BIT_AND))
   {
      checkSymbol (BIT_AND);
      result->spec_and = true;
   }
   else if (isSymbol (BIT_OR))
   {
      checkSymbol (BIT_OR);
      result->spec_or = true;
   }
   else if (isSymbol (BIT_NOT))
   {
      checkSymbol (BIT_NOT);
      result->spec_not = true;
   }
   else if (isSymbol (LOG_NOT))
   {
      checkSymbol (LOG_NOT);
      result->spec_log_not = true;
   }
   else if (isSymbol (ASSIGN))
   {
      checkSymbol (ASSIGN);
      result->spec_assign = true;
   }
   else if (isSymbol (LESS))
   {
      checkSymbol (LESS);
      result->spec_lt = true;
   }
   else if (isSymbol (GREATER))
   {
      checkSymbol (GREATER);
      result->spec_gt = true;
   }
   else if (isSymbol (PLUS_ASSIGN))
   {
      checkSymbol (PLUS_ASSIGN);
      result->spec_add_assign = true;
   }
   else if (isSymbol (MINUS_ASSIGN))
   {
      checkSymbol (MINUS_ASSIGN);
      result->spec_sub_assign = true;
   }
   else if (isSymbol (MUL_ASSIGN))
   {
      checkSymbol (MUL_ASSIGN);
      result->spec_mul_assign = true;
   }
   else if (isSymbol (DIV_ASSIGN))
   {
      checkSymbol (DIV_ASSIGN);
      result->spec_div_assign = true;
   }
   else if (isSymbol (MOD_ASSIGN))
   {
      checkSymbol (MOD_ASSIGN);
      result->spec_mod_assign = true;
   }
   else if (isSymbol (BIT_XOR_ASSIGN))
   {
      checkSymbol (BIT_XOR_ASSIGN);
      result->spec_xor_assign = true;
   }
   else if (isSymbol (BIT_AND_ASSIGN))
   {
      checkSymbol (BIT_AND_ASSIGN);
      result->spec_and_assign = true;
   }
   else if (isSymbol (BIT_OR_ASSIGN))
   {
      checkSymbol (BIT_OR_ASSIGN);
      result->spec_or_assign = true;
   }
   else if (isSymbol (SHL))
   {
      checkSymbol (SHL);
      result->spec_shl = true;
   }
   else if (isSymbol (SHR))
   {
      checkSymbol (SHR);
      result->spec_shr = true;
   }
   else if (isSymbol (SHR_ASSIGN))
   {
      checkSymbol (SHR_ASSIGN);
      result->spec_shl_assign = true;
   }
   else if (isSymbol (SHL_ASSIGN))
   {
      checkSymbol (SHL_ASSIGN);
      result->spec_shr_assign = true;
   }
   else if (isSymbol (EQUAL))
   {
      checkSymbol (EQUAL);
      result->spec_eq = true;
   }
   else if (isSymbol (UNEQUAL))
   {
      checkSymbol (UNEQUAL);
      result->spec_ne = true;
   }
   else if (isSymbol (LESS_OR_EQUAL))
   {
      checkSymbol (LESS_OR_EQUAL);
      result->spec_le = true;
   }
   else if (isSymbol (GREATER_OR_EQUAL))
   {
      checkSymbol (GREATER_OR_EQUAL);
      result->spec_ge = true;
   }
   else if (isSymbol (LOG_AND))
   {
      checkSymbol (LOG_AND);
      result->spec_log_and = true;
   }
   else if (isSymbol (LOG_OR))
   {
      checkSymbol (LOG_OR);
      result->spec_log_or = true;
   }
   else if (isSymbol (INC))
   {
      checkSymbol (INC);
      result->spec_inc = true;
   }
   else if (isSymbol (DEC))
   {
      checkSymbol (DEC);
      result->spec_dec = true;
   }
   else if (isSymbol (COMMA))
   {
      checkSymbol (COMMA);
      result->spec_comma = true;
   }
   else if (isSymbol (ARROW_STAR))
   {
      checkSymbol (ARROW_STAR);
      result->spec_member_deref = true;
   }
   else if (isSymbol (ARROW))
   {
      checkSymbol (ARROW);
      result->spec_deref = true;
   }
   else if (isSymbol (LPAREN))
   {
      checkSymbol (LPAREN);
      checkSymbol (RPAREN);
      result->spec_call = true;
   }
   else if (isSymbol (LBRACKET))
   {
      checkSymbol (LBRACKET);
      checkSymbol (RBRACKET);
      result->spec_index = true;
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmTemplateDecl * Parser::parse_template_declaration ()
{
   CmmTemplateDecl * result = new CmmTemplateDecl;
   result->mode = templateDecl;
   storeLocation (result);
   checkSymbol (keyword_template);
   if (isSymbol (LESS))
   {
      checkSymbol (LESS);
      result->params = parse_template_param_list ();
      checkSymbol (GREATER);
   }
   result->inner_declaration = parse_declaration ();
   on_template_declaration (result);
   return result;
}

CmmTemplateParamSect * Parser::parse_template_param_list ()
{
   CmmTemplateParamSect * result = new CmmTemplateParamSect;
   storeLocation (result);
   if (testSymbols (set_16))
   {
      result->items.push_back (parse_template_param ());
      while (isSymbol (COMMA))
      {
         checkSymbol (COMMA);
         result->items.push_back (parse_template_param ());
      }
   }
   return result;
}

CmmTemplateParam * Parser::parse_template_param ()
{
   CmmTemplateParam * result = NULL;
   if (isSymbol (keyword_class) || isSymbol (keyword_template) || isSymbol (keyword_typename))
   {
      result = parse_template_type_param ();
   }
   else if (testSymbols (set_5))
   {
      result = parse_template_normal_param ();
   }
   else
   {
      stop ("Unexpected token");
   }
   return result;
}

CmmTemplateTypeParam * Parser::parse_template_type_param ()
{
   CmmTemplateTypeParam * result = new CmmTemplateTypeParam;
   result->kind = templateTypeParam;
   storeLocation (result);
   if (isSymbol (keyword_template))
   {
      checkSymbol (keyword_template);
      checkSymbol (LESS);
      result->params = parse_template_param_list ();
      checkSymbol (GREATER);
   }
   if (isSymbol (keyword_class))
   {
      checkSymbol (keyword_class);
      result->a_class = true;
   }
   else if (isSymbol (keyword_typename))
   {
      checkSymbol (keyword_typename);
      result->a_typename = true;
   }
   else
   {
      stop ("Unexpected token");
   }
   if (isSymbol (identifier))
   {
      result->simp_name = parse_simple_name ();
   }
   return result;
}

CmmTemplateNormalParam * Parser::parse_template_normal_param ()
{
   CmmTemplateNormalParam * result = new CmmTemplateNormalParam;
   result->kind = templateCommonParam;
   storeLocation (result);
   result->type_spec = parse_type_specifiers ();
   result->declarator = parse_declarator ();
   if (isSymbol (ASSIGN))
   {
      checkSymbol (ASSIGN);
      result->init = parse_shift_expr ();
   }
   return result;
}

CmmTemplateArgSect * Parser::parse_template_arg_list ()
{
   CmmTemplateArgSect * result = new CmmTemplateArgSect;
   storeLocation (result);
   checkSymbol (LESS);
   if (testSymbols (set_9))
   {
      result->items.push_back (parse_template_arg ());
      while (isSymbol (COMMA))
      {
         checkSymbol (COMMA);
         result->items.push_back (parse_template_arg ());
      }
   }
   checkSymbol (GREATER);
   return result;
}

CmmTemplateArg * Parser::parse_template_arg ()
{
   CmmTemplateArg * result = new CmmTemplateArg;
   storeLocation (result);
   result->value = parse_parameter_declaration ();
   return result;
}

CmmTryStat * Parser::parse_try_stat ()
{
   CmmTryStat * result = new CmmTryStat;
   result->mode = tryStat;
   storeLocation (result);
   checkSymbol (keyword_try);
   result->body = parse_compound_stat ();
   result->handlers = parse_handler_list ();
   return result;
}

CmmHandlerSect * Parser::parse_handler_list ()
{
   CmmHandlerSect * result = new CmmHandlerSect;
   storeLocation (result);
   result->items.push_back (parse_handler ());
   while (isSymbol (keyword_catch))
   {
      result->items.push_back (parse_handler ());
   }
   return result;
}

CmmHandlerItem * Parser::parse_handler ()
{
   CmmHandlerItem * result = new CmmHandlerItem;
   storeLocation (result);
   checkSymbol (keyword_catch);
   checkSymbol (LPAREN);
   if (testSymbols (set_5))
   {
      result->type_spec = parse_type_specifiers ();
      result->declarator = parse_declarator ();
   }
   else if (isSymbol (DOTS))
   {
      checkSymbol (DOTS);
      result->dots = true;
   }
   else
   {
      stop ("Unexpected token");
   }
   checkSymbol (RPAREN);
   result->body = parse_compound_stat ();
   return result;
}

CmmThrowExpr * Parser::parse_throw_expr ()
{
   CmmThrowExpr * result = new CmmThrowExpr;
   result->kind = throwExp;
   storeLocation (result);
   on_other_expr ();
   checkSymbol (keyword_throw);
   if (testSymbols (set_3))
   {
      result->inner_expr = parse_assignment_expr ();
   }
   return result;
}

CmmExceptionSect * Parser::parse_exception_specification ()
{
   CmmExceptionSect * result = new CmmExceptionSect;
   storeLocation (result);
   checkSymbol (keyword_throw);
   checkSymbol (LPAREN);
   if (testSymbols (set_5))
   {
      result->items.push_back (parse_exception_specification_item ());
      while (isSymbol (COMMA))
      {
         checkSymbol (COMMA);
         result->items.push_back (parse_exception_specification_item ());
      }
   }
   checkSymbol (RPAREN);
   return result;
}

CmmExceptionItem * Parser::parse_exception_specification_item ()
{
   CmmExceptionItem * result = new CmmExceptionItem;
   storeLocation (result);
   result->type = parse_type_id ();
   return result;
}

void Parser::lookupKeyword ()
{
   string s = tokenText;
   int n = s.length ();
   if (n == 2)
   {
      if (s[0] == 'd')
      {
         if (s[1] == 'o')
         {
            token = keyword_do;
         }
      }
      else if (s[0] == 'i')
      {
         if (s[1] == 'f')
         {
            token = keyword_if;
         }
      }
   }
   else if (n == 3)
   {
      if (s[0] == 'f')
      {
         if (s.substr (1,2) == "or")
         {
            token = keyword_for;
         }
      }
      else if (s[0] == 'i')
      {
         if (s.substr (1,2) == "nt")
         {
            token = keyword_int;
         }
      }
      else if (s[0] == 'n')
      {
         if (s.substr (1,2) == "ew")
         {
            token = keyword_new;
         }
      }
      else if (s[0] == 't')
      {
         if (s.substr (1,2) == "ry")
         {
            token = keyword_try;
         }
      }
   }
   else if (n == 4)
   {
      if (s[0] == 'b')
      {
         if (s.substr (1,3) == "ool")
         {
            token = keyword_bool;
         }
      }
      else if (s[0] == 'c')
      {
         if (s[1] == 'a')
         {
            if (s.substr (2,2) == "se")
            {
               token = keyword_case;
            }
         }
         else if (s[1] == 'h')
         {
            if (s.substr (2,2) == "ar")
            {
               token = keyword_char;
            }
         }
      }
      else if (s[0] == 'e')
      {
         if (s[1] == 'l')
         {
            if (s.substr (2,2) == "se")
            {
               token = keyword_else;
            }
         }
         else if (s[1] == 'n')
         {
            if (s.substr (2,2) == "um")
            {
               token = keyword_enum;
            }
         }
      }
      else if (s[0] == 'g')
      {
         if (s.substr (1,3) == "oto")
         {
            token = keyword_goto;
         }
      }
      else if (s[0] == 'l')
      {
         if (s.substr (1,3) == "ong")
         {
            token = keyword_long;
         }
      }
      else if (s[0] == 't')
      {
         if (s.substr (1,3) == "his")
         {
            token = keyword_this;
         }
      }
      else if (s[0] == 'v')
      {
         if (s.substr (1,3) == "oid")
         {
            token = keyword_void;
         }
      }
   }
   else if (n == 5)
   {
      if (s[0] == 'b')
      {
         if (s.substr (1,4) == "reak")
         {
            token = keyword_break;
         }
      }
      else if (s[0] == 'c')
      {
         if (s[1] == 'a')
         {
            if (s.substr (2,3) == "tch")
            {
               token = keyword_catch;
            }
         }
         else if (s[1] == 'l')
         {
            if (s.substr (2,3) == "ass")
            {
               token = keyword_class;
            }
         }
         else if (s[1] == 'o')
         {
            if (s.substr (2,3) == "nst")
            {
               token = keyword_const;
            }
         }
      }
      else if (s[0] == 'f')
      {
         if (s.substr (1,4) == "loat")
         {
            token = keyword_float;
         }
      }
      else if (s[0] == 's')
      {
         if (s.substr (1,4) == "hort")
         {
            token = keyword_short;
         }
      }
      else if (s[0] == 't')
      {
         if (s.substr (1,4) == "hrow")
         {
            token = keyword_throw;
         }
      }
      else if (s[0] == 'u')
      {
         if (s[1] == 'n')
         {
            if (s.substr (2,3) == "ion")
            {
               token = keyword_union;
            }
         }
         else if (s[1] == 's')
         {
            if (s.substr (2,3) == "ing")
            {
               token = keyword_using;
            }
         }
      }
      else if (s[0] == 'w')
      {
         if (s.substr (1,4) == "hile")
         {
            token = keyword_while;
         }
      }
   }
   else if (n == 6)
   {
      if (s[0] == 'd')
      {
         if (s[1] == 'e')
         {
            if (s.substr (2,4) == "lete")
            {
               token = keyword_delete;
            }
         }
         else if (s[1] == 'o')
         {
            if (s.substr (2,4) == "uble")
            {
               token = keyword_double;
            }
         }
      }
      else if (s[0] == 'e')
      {
         if (s.substr (1,5) == "xtern")
         {
            token = keyword_extern;
         }
      }
      else if (s[0] == 'f')
      {
         if (s.substr (1,5) == "riend")
         {
            token = keyword_friend;
         }
      }
      else if (s[0] == 'i')
      {
         if (s.substr (1,5) == "nline")
         {
            token = keyword_inline;
         }
      }
      else if (s[0] == 'p')
      {
         if (s.substr (1,5) == "ublic")
         {
            token = keyword_public;
         }
      }
      else if (s[0] == 'r')
      {
         if (s.substr (1,5) == "eturn")
         {
            token = keyword_return;
         }
      }
      else if (s[0] == 's')
      {
         if (s[1] == 'i')
         {
            if (s[2] == 'g')
            {
               if (s.substr (3,3) == "ned")
               {
                  token = keyword_signed;
               }
            }
            else if (s[2] == 'z')
            {
               if (s.substr (3,3) == "eof")
               {
                  token = keyword_sizeof;
               }
            }
         }
         else if (s[1] == 't')
         {
            if (s[2] == 'a')
            {
               if (s.substr (3,3) == "tic")
               {
                  token = keyword_static;
               }
            }
            else if (s[2] == 'r')
            {
               if (s.substr (3,3) == "uct")
               {
                  token = keyword_struct;
               }
            }
         }
         else if (s[1] == 'w')
         {
            if (s.substr (2,4) == "itch")
            {
               token = keyword_switch;
            }
         }
      }
      else if (s[0] == 't')
      {
         if (s.substr (1,5) == "ypeid")
         {
            token = keyword_typeid;
         }
      }
   }
   else if (n == 7)
   {
      if (s[0] == 'd')
      {
         if (s.substr (1,6) == "efault")
         {
            token = keyword_default;
         }
      }
      else if (s[0] == 'm')
      {
         if (s.substr (1,6) == "utable")
         {
            token = keyword_mutable;
         }
      }
      else if (s[0] == 'p')
      {
         if (s.substr (1,6) == "rivate")
         {
            token = keyword_private;
         }
      }
      else if (s[0] == 't')
      {
         if (s.substr (1,6) == "ypedef")
         {
            token = keyword_typedef;
         }
      }
      else if (s[0] == 'v')
      {
         if (s.substr (1,6) == "irtual")
         {
            token = keyword_virtual;
         }
      }
      else if (s[0] == 'w')
      {
         if (s.substr (1,6) == "char_t")
         {
            token = keyword_wchar_t;
         }
      }
   }
   else if (n == 8)
   {
      if (s[0] == 'c')
      {
         if (s.substr (1,7) == "ontinue")
         {
            token = keyword_continue;
         }
      }
      else if (s[0] == 'e')
      {
         if (s.substr (1,7) == "xplicit")
         {
            token = keyword_explicit;
         }
      }
      else if (s[0] == 'o')
      {
         if (s.substr (1,7) == "perator")
         {
            token = keyword_operator;
         }
      }
      else if (s[0] == 'r')
      {
         if (s.substr (1,7) == "egister")
         {
            token = keyword_register;
         }
      }
      else if (s[0] == 't')
      {
         if (s[1] == 'e')
         {
            if (s.substr (2,6) == "mplate")
            {
               token = keyword_template;
            }
         }
         else if (s[1] == 'y')
         {
            if (s.substr (2,6) == "pename")
            {
               token = keyword_typename;
            }
         }
      }
      else if (s[0] == 'u')
      {
         if (s.substr (1,7) == "nsigned")
         {
            token = keyword_unsigned;
         }
      }
      else if (s[0] == 'v')
      {
         if (s.substr (1,7) == "olatile")
         {
            token = keyword_volatile;
         }
      }
   }
   else if (n == 9)
   {
      if (s[0] == 'n')
      {
         if (s.substr (1,8) == "amespace")
         {
            token = keyword_namespace;
         }
      }
      else if (s[0] == 'p')
      {
         if (s.substr (1,8) == "rotected")
         {
            token = keyword_protected;
         }
      }
      else if (s[0] == 't')
      {
         if (s.substr (1,8) == "ype_cast")
         {
            token = keyword_type_cast;
         }
      }
   }
   else if (n == 10)
   {
      if (s.substr (0,10) == "const_cast")
      {
         token = keyword_const_cast;
      }
   }
   else if (n == 11)
   {
      if (s.substr (0,11) == "static_cast")
      {
         token = keyword_static_cast;
      }
   }
   else if (n == 12)
   {
      if (s.substr (0,12) == "dynamic_cast")
      {
         token = keyword_dynamic_cast;
      }
   }
   else if (n == 16)
   {
      if (s.substr (0,16) == "reinterpret_cast")
      {
         token = keyword_reinterpret_cast;
      }
   }
}

void Parser::processSeparator ()
{
   if (tokenText == "!")
   {
      token = LOG_NOT;
      if (inpCh == '=')
      {
         token = UNEQUAL;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "%")
   {
      token = MOD;
      if (inpCh == '=')
      {
         token = MOD_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "&")
   {
      token = BIT_AND;
      if (inpCh == '&')
      {
         token = LOG_AND;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
      if (inpCh == '=')
      {
         token = BIT_AND_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "(")
   {
      token = LPAREN;
   }
   if (tokenText == ")")
   {
      token = RPAREN;
   }
   if (tokenText == "*")
   {
      token = STAR;
      if (inpCh == '=')
      {
         token = MUL_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "+")
   {
      token = PLUS;
      if (inpCh == '+')
      {
         token = INC;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
      if (inpCh == '=')
      {
         token = PLUS_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == ",")
   {
      token = COMMA;
   }
   if (tokenText == "-")
   {
      token = MINUS;
      if (inpCh == '-')
      {
         token = DEC;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
      if (inpCh == '=')
      {
         token = MINUS_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
      if (inpCh == '>')
      {
         token = ARROW;
         tokenText = tokenText + inpCh;
         nextCh ();
         if (inpCh == '*')
         {
            token = ARROW_STAR;
            tokenText = tokenText + inpCh;
            nextCh ();
         }
      }
   }
   if (tokenText == ".")
   {
      token = DOT;
      if (inpCh == '*')
      {
         token = DOT_STAR;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
      if (inpCh == '.')
      {
         tokenText = tokenText + inpCh;
         nextCh ();
         if (inpCh == '.')
         {
            token = DOTS;
            tokenText = tokenText + inpCh;
            nextCh ();
         }
      }
   }
   if (tokenText == "/")
   {
      token = SLASH;
      if (inpCh == '=')
      {
         token = DIV_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == ":")
   {
      token = COLON;
      if (inpCh == ':')
      {
         token = SCOPE;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == ";")
   {
      token = SEMICOLON;
   }
   if (tokenText == "<")
   {
      token = LESS;
      if (inpCh == '<')
      {
         token = SHL;
         tokenText = tokenText + inpCh;
         nextCh ();
         if (inpCh == '=')
         {
            token = SHL_ASSIGN;
            tokenText = tokenText + inpCh;
            nextCh ();
         }
      }
      if (inpCh == '=')
      {
         token = LESS_OR_EQUAL;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "=")
   {
      token = ASSIGN;
      if (inpCh == '=')
      {
         token = EQUAL;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == ">")
   {
      token = GREATER;
      if (inpCh == '=')
      {
         token = GREATER_OR_EQUAL;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
      if (inpCh == '>')
      {
         token = SHR;
         tokenText = tokenText + inpCh;
         nextCh ();
         if (inpCh == '=')
         {
            token = SHR_ASSIGN;
            tokenText = tokenText + inpCh;
            nextCh ();
         }
      }
   }
   if (tokenText == "?")
   {
      token = QUESTION;
   }
   if (tokenText == "[")
   {
      token = LBRACKET;
      if (inpCh == '[')
      {
         token = literal_1;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "]")
   {
      token = RBRACKET;
      if (inpCh == ']')
      {
         token = literal_2;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "^")
   {
      token = BIT_XOR;
      if (inpCh == '=')
      {
         token = BIT_XOR_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "{")
   {
      token = LBRACE;
   }
   if (tokenText == "|")
   {
      token = BIT_OR;
      if (inpCh == '=')
      {
         token = BIT_OR_ASSIGN;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
      if (inpCh == '|')
      {
         token = LOG_OR;
         tokenText = tokenText + inpCh;
         nextCh ();
      }
   }
   if (tokenText == "}")
   {
      token = RBRACE;
   }
   if (tokenText == "~")
   {
      token = BIT_NOT;
   }
   if (token == separator)
   {
      stop ("Unknown separator");
   }
}

string Parser::tokenToString (CmmSymbol value)
{
   if (value == 0) return "<end of source text>";
   if (value == 1) return "<identifier>";
   if (value == 2) return "<number>";
   if (value == 3) return "<real_number>";
   if (value == 4) return "<character_literal>";
   if (value == 5) return "<string_literal>";
   if (value == 6) return "<unknown separator>";
   if (value == 7) return "<end of line>";
   if (value == 8) return "!";
   if (value == 9) return "!=";
   if (value == 10) return "%";
   if (value == 11) return "%=";
   if (value == 12) return "&";
   if (value == 13) return "&&";
   if (value == 14) return "&=";
   if (value == 15) return "(";
   if (value == 16) return ")";
   if (value == 17) return "*";
   if (value == 18) return "*=";
   if (value == 19) return "+";
   if (value == 20) return "++";
   if (value == 21) return "+=";
   if (value == 22) return ",";
   if (value == 23) return "-";
   if (value == 24) return "--";
   if (value == 25) return "-=";
   if (value == 26) return "->";
   if (value == 27) return "->*";
   if (value == 28) return ".";
   if (value == 29) return ".*";
   if (value == 30) return "...";
   if (value == 31) return "/";
   if (value == 32) return "/=";
   if (value == 33) return ":";
   if (value == 34) return "::";
   if (value == 35) return ";";
   if (value == 36) return "<";
   if (value == 37) return "<<";
   if (value == 38) return "<<=";
   if (value == 39) return "<=";
   if (value == 40) return "=";
   if (value == 41) return "==";
   if (value == 42) return ">";
   if (value == 43) return ">=";
   if (value == 44) return ">>";
   if (value == 45) return ">>=";
   if (value == 46) return "?";
   if (value == 47) return "[";
   if (value == 48) return "[[";
   if (value == 49) return "]";
   if (value == 50) return "]]";
   if (value == 51) return "^";
   if (value == 52) return "^=";
   if (value == 53) return "{";
   if (value == 54) return "|";
   if (value == 55) return "|=";
   if (value == 56) return "||";
   if (value == 57) return "}";
   if (value == 58) return "~";
   if (value == 59) return "bool";
   if (value == 60) return "break";
   if (value == 61) return "case";
   if (value == 62) return "catch";
   if (value == 63) return "char";
   if (value == 64) return "class";
   if (value == 65) return "const";
   if (value == 66) return "const_cast";
   if (value == 67) return "continue";
   if (value == 68) return "default";
   if (value == 69) return "delete";
   if (value == 70) return "do";
   if (value == 71) return "double";
   if (value == 72) return "dynamic_cast";
   if (value == 73) return "else";
   if (value == 74) return "enum";
   if (value == 75) return "explicit";
   if (value == 76) return "extern";
   if (value == 77) return "float";
   if (value == 78) return "for";
   if (value == 79) return "friend";
   if (value == 80) return "goto";
   if (value == 81) return "if";
   if (value == 82) return "inline";
   if (value == 83) return "int";
   if (value == 84) return "long";
   if (value == 85) return "mutable";
   if (value == 86) return "namespace";
   if (value == 87) return "new";
   if (value == 88) return "operator";
   if (value == 89) return "private";
   if (value == 90) return "protected";
   if (value == 91) return "public";
   if (value == 92) return "register";
   if (value == 93) return "reinterpret_cast";
   if (value == 94) return "return";
   if (value == 95) return "short";
   if (value == 96) return "signed";
   if (value == 97) return "sizeof";
   if (value == 98) return "static";
   if (value == 99) return "static_cast";
   if (value == 100) return "struct";
   if (value == 101) return "switch";
   if (value == 102) return "template";
   if (value == 103) return "this";
   if (value == 104) return "throw";
   if (value == 105) return "try";
   if (value == 106) return "type_cast";
   if (value == 107) return "typedef";
   if (value == 108) return "typeid";
   if (value == 109) return "typename";
   if (value == 110) return "union";
   if (value == 111) return "unsigned";
   if (value == 112) return "using";
   if (value == 113) return "virtual";
   if (value == 114) return "void";
   if (value == 115) return "volatile";
   if (value == 116) return "wchar_t";
   if (value == 117) return "while";
   return "<unknown symbol>";
}

void Parser::translate ()
{
   if (tokenKind == identToken)
   {
      token = identifier;
      lookupKeyword ();
   }
   else if (tokenKind == numberToken)
   {
      token = number;
   }
   else if (tokenKind == numberToken)
   {
      token = number;
   }
   else if (tokenKind == charToken)
   {
      token = character_literal;
   }
   else if (tokenKind == stringToken)
   {
      token = string_literal;
   }
   else if (tokenKind == eofToken || tokenKind == startToken)
   {
      token = eos;
   }
   else
   {
      token = separator;
      processSeparator ();
   }
}

void Parser::storeLocation (CmmBasic * item)
{
   item->src_file = getTokenFileInx ();
   item->src_line = getTokenLine ();
   item->src_column = getTokenCol ();
}

Parser::Parser () :
   Lexer ()
{
}

