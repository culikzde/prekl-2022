#ifndef TYPES_H
#define TYPES_H

#include "cmm_parser.hpp"

class TypeParser : CmmParser
{
      void on_namespace (CmmNamespaceDecl * item) override;
      void on_open_namespace (CmmNamespaceDecl * item) override;
      void on_close_namespace (CmmNamespaceDecl * item) override;

      void on_class (CmmClassDecl * item) override;
      void on_open_class (CmmClassDecl * item) override;
      void on_close_class (CmmClassDecl * item) override;

      void on_typedef (CmmTypedefDecl * item) override;

      void on_enum (CmmEnumDecl * item) override;
      void on_open_enum (CmmEnumDecl * item) override;
      void on_close_enum (CmmEnumDecl * item) override;
      void on_enum_item (CmmEnumItem * item) override;
};

#endif // TYPES_H
