
#include "ckit_product.h"

void CmmProduct::send_simple_name (CmmSimpleName * param)
{
   send (param->id);
}

void CmmProduct::send_global_variant (CmmName * param)
{
   if (unknown)
   {
      send_simple_name (param);
   }
   else if (unknown)
   {
      send_special_function (param);
   }
}

void CmmProduct::send_global_name (CmmGlobalName * param)
{
   send ("::");
   send_global_variant (param->inner_name);
}

void CmmProduct::send_base_variant (CmmName * param)
{
   if (unknown)
   {
      send_global_name (param);
   }
   else if (unknown)
   {
      send_simple_name (param);
   }
   else if (unknown)
   {
      send_special_function (param);
   }
}

void CmmProduct::send_template_name (CmmTemplateName * param)
{
   send_template_arg_list (param->template_args);
}

void CmmProduct::send_base_name (CmmName * param)
{
   send_base_variant (param);
   if (unknown)
   {
      send_template_name (param);
   }
}
}

void CmmProduct::send_cont_name (CmmContName * param)
{
send (param->id);
}

void CmmProduct::send_destructor_name (CmmDestructorName * param)
{
send ("~");
send_simple_name (param->inner_name);
}

void CmmProduct::send_cont_variant (CmmName * param)
{
if (unknown)
{
   send_cont_name (param);
}
else if (unknown)
{
   send_special_function (param);
}
else if (unknown)
{
   send_destructor_name (param);
}
}

void CmmProduct::send_compound_variant (CmmName * param)
{
send_cont_variant (param);
if (unknown)
{
   send_template_name (param);
}
}
}

void CmmProduct::send_compound_name (CmmCompoundName * param)
{
style_no_space ();
send ("::");
style_no_space ();
send_compound_variant (param->right);
}

void CmmProduct::send_qualified_name (CmmName * param)
{
send_base_name (param);
if (unknown)
{
send_compound_name (param);
}
}
}

void CmmProduct::send_primary_expr (CmmExpr * param)
{
if (unknown)
{
send_ident_expr (param);
}
else if (unknown)
{
send_int_value (param);
}
else if (unknown)
{
send_real_value (param);
}
else if (unknown)
{
send_char_value (param);
}
else if (unknown)
{
send_string_value (param);
}
else if (unknown)
{
send_this_expr (param);
}
else if (unknown)
{
send_subexpr_expr (param);
}
}

void CmmProduct::send_ident_expr (CmmName * param)
{
send_qualified_name (param);
}

void CmmProduct::send_int_value (CmmIntValue * param)
{
send (param->value);
}

void CmmProduct::send_real_value (CmmRealValue * param)
{
send (param->value);
}

void CmmProduct::send_char_value (CmmCharValue * param)
{
putChar (param->value);
}

void CmmProduct::send_string_value (CmmStringValue * param)
{
int inx = 0;
int cnt = param->items.size();
putString (param->value);
while (inx < cnt)
{
send_string_value_cont (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_string_value_cont (CmmStringCont * param)
{
putString (param->value);
}

void CmmProduct::send_this_expr (CmmThisExpr * param)
{
send ("this");
}

void CmmProduct::send_subexpr_expr (CmmSubexprExpr * param)
{
send ("(");
send_expr (param->param);
send (")");
}

void CmmProduct::send_postfix_start (CmmExpr * param)
{
if (unknown)
{
send_primary_expr (param);
}
else if (unknown)
{
send_modern_cast_expr (param);
}
else if (unknown)
{
send_typeid_expr (param);
}
else if (unknown)
{
send_type_name (param);
}
else if (unknown)
{
send_type_specifier (param);
}
}

void CmmProduct::send_modern_cast_expr (CmmModernCastExpr * param)
{
if (param->cast_kind == DynamicCast)
{
send ("dynamic_cast");
}
else if (param->cast_kind == StaticCast)
{
send ("static_cast");
}
else if (param->cast_kind == ConstCast)
{
send ("const_cast");
}
else if (param->cast_kind == ReinterpreterCast)
{
send ("reinterpret_cast");
}
send ("<");
style_no_space ();
send_type_id (param->type);
style_no_space ();
send (">");
send ("(");
send_expr (param->param);
send (")");
}

void CmmProduct::send_typeid_expr (CmmTypeIdExpr * param)
{
send ("typeid");
send ("(");
send_expr (param->value);
send (")");
}

void CmmProduct::send_postfix_expr (CmmExpr * param)
{
send_postfix_start (param);
if (unknown || unknown || unknown || unknown || unknown || unknown)
{
if (unknown)
{
send_index_expr (param);
}
else if (unknown)
{
send_call_expr (param);
}
else if (unknown)
{
send_field_expr (param);
}
else if (unknown)
{
send_ptr_field_expr (param);
}
else if (unknown)
{
send_post_inc_expr (param);
}
else if (unknown)
{
send_post_dec_expr (param);
}
}
}

void CmmProduct::send_index_expr (CmmIndexExpr * param)
{
send ("[");
send_assignment_expr (param->param);
send ("]");
}

void CmmProduct::send_call_expr (CmmCallExpr * param)
{
send ("(");
send_expr_list (param->param_list);
send (")");
}

void CmmProduct::send_field_expr (CmmFieldExpr * param)
{
style_no_space ();
send (".");
style_no_space ();
send_simple_name (param->simp_name);
}

void CmmProduct::send_ptr_field_expr (CmmPtrFieldExpr * param)
{
style_no_space ();
send ("->");
style_no_space ();
send_simple_name (param->simp_name);
}

void CmmProduct::send_post_inc_expr (CmmPostIncExpr * param)
{
send ("++");
}

void CmmProduct::send_post_dec_expr (CmmPostDecExpr * param)
{
send ("--");
}

void CmmProduct::send_expr_list (CmmExprList * param)
{
int inx = 0;
int cnt = param->items.size();
if (inx < cnt)
{
send_assignment_expr (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_assignment_expr (param->items [inx]);
inx ++;
}
}
}

void CmmProduct::send_unary_expr (CmmExpr * param)
{
if (unknown)
{
send_postfix_expr (param);
}
else if (unknown)
{
send_inc_expr (param);
}
else if (unknown)
{
send_dec_expr (param);
}
else if (unknown)
{
send_deref_expr (param);
}
else if (unknown)
{
send_addr_expr (param);
}
else if (unknown)
{
send_plus_expr (param);
}
else if (unknown)
{
send_minus_expr (param);
}
else if (unknown)
{
send_bit_not_expr (param);
}
else if (unknown)
{
send_log_not_expr (param);
}
else if (unknown)
{
send_sizeof_expr (param);
}
else if (unknown)
{
send_allocation_expr (param);
}
else if (unknown)
{
send_deallocation_expr (param);
}
else if (unknown)
{
send_throw_expr (param);
}
}

void CmmProduct::send_inc_expr (CmmIncExpr * param)
{
send ("++");
send_cast_expr (param->param);
}

void CmmProduct::send_dec_expr (CmmDecExpr * param)
{
send ("--");
send_cast_expr (param->param);
}

void CmmProduct::send_deref_expr (CmmDerefExpr * param)
{
send ("*");
send_cast_expr (param->param);
}

void CmmProduct::send_addr_expr (CmmAddrExpr * param)
{
send ("&");
send_cast_expr (param->param);
}

void CmmProduct::send_plus_expr (CmmPlusExpr * param)
{
send ("+");
send_cast_expr (param->param);
}

void CmmProduct::send_minus_expr (CmmMinusExpr * param)
{
send ("-");
send_cast_expr (param->param);
}

void CmmProduct::send_bit_not_expr (CmmBitNotExpr * param)
{
send ("~");
send_cast_expr (param->param);
}

void CmmProduct::send_log_not_expr (CmmLogNotExpr * param)
{
send ("!");
send_cast_expr (param->param);
}

void CmmProduct::send_sizeof_expr (CmmSizeofExp * param)
{
send ("sizeof");
send_unary_expr (param->value);
}

void CmmProduct::send_allocation_expr (CmmNewExpr * param)
{
send ("new");
if (param->type1 != NULL)
{
send_new_type_id (param->type1);
}
else if (param->type2 != NULL)
{
send ("(");
send_type_id (param->type2);
send (")");
}
if (param->init_list != NULL)
{
send ("(");
send_expr_list (param->init_list);
send (")");
}
}

void CmmProduct::send_new_type_id (CmmNewTypeId * param)
{
int inx = 0;
int cnt = param->items.size();
send_type_specifiers (param->type_spec);
send_ptr_specifier_list (param->ptr);
while (inx < cnt)
{
send_allocation_array_limit (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_allocation_array_limit (CmmNewArrayLimit * param)
{
send ("[");
send_expr (param->value);
send ("]");
}

void CmmProduct::send_deallocation_expr (CmmDeleteExpr * param)
{
send ("delete");
if (param->a_array == true)
{
send ("[");
send ("]");
}
send_cast_expr (param->param);
}

void CmmProduct::send_cast_expr (CmmExpr * param)
{
if (unknown)
{
send_cast_formula (param);
}
else if (unknown)
{
send_unary_expr (param);
}
}

void CmmProduct::send_cast_formula (CmmTypeCastExpr * param)
{
send ("type_cast");
send ("<");
send_type_id (param->type);
send (">");
send ("(");
send_cast_expr (param->param);
send (")");
}

void CmmProduct::send_pm_expr (CmmExpr * param)
{
send_cast_expr (param);
if (param->right != NULL)
{
if (param->kind == dotMemberExp)
{
send (".*");
}
else if (param->kind == arrowMemberExp)
{
send ("->*");
}
send_cast_expr (param->right);
}
}
}

void CmmProduct::send_multiplicative_expr (CmmExpr * param)
{
send_pm_expr (param);
if (param->right != NULL)
{
if (param->kind == mulExp)
{
send ("*");
}
else if (param->kind == divExp)
{
send ("/");
}
else if (param->kind == modExp)
{
send ("%");
}
else if (param->kind == customMulExp)
{
send (param->custom_mul);
}
send_pm_expr (param->right);
}
}
}

void CmmProduct::send_additive_expr (CmmExpr * param)
{
send_multiplicative_expr (param);
if (param->right != NULL)
{
if (param->kind == addExp)
{
send ("+");
}
else if (param->kind == subExp)
{
send ("-");
}
else if (param->kind == customAddExp)
{
send (param->custom_add);
}
send_multiplicative_expr (param->right);
}
}
}

void CmmProduct::send_shift_expr (CmmExpr * param)
{
send_additive_expr (param);
if (param->right != NULL)
{
if (param->kind == shlExp)
{
send ("<<");
}
else if (param->kind == shrExp)
{
send (">>");
}
send_additive_expr (param->right);
}
}
}

void CmmProduct::send_relational_expr (CmmExpr * param)
{
send_shift_expr (param);
if (param->right != NULL)
{
if (param->kind == ltExp)
{
send ("<");
}
else if (param->kind == gtExp)
{
send (">");
}
else if (param->kind == leExp)
{
send ("<=");
}
else if (param->kind == geExp)
{
send (">=");
}
send_shift_expr (param->right);
}
}
}

void CmmProduct::send_equality_expr (CmmExpr * param)
{
send_relational_expr (param);
if (param->right != NULL)
{
if (param->kind == eqExp)
{
send ("==");
}
else if (param->kind == neExp)
{
send ("!=");
}
send_relational_expr (param->right);
}
}
}

void CmmProduct::send_and_expr (CmmExpr * param)
{
send_equality_expr (param);
if (param->kind == bitAndExp)
{
send ("&");
send_equality_expr (param->right);
}
}
}

void CmmProduct::send_exclusive_or_expr (CmmExpr * param)
{
send_and_expr (param);
if (param->kind == bitXorExp)
{
send ("^");
send_and_expr (param->right);
}
}
}

void CmmProduct::send_inclusive_or_expr (CmmExpr * param)
{
send_exclusive_or_expr (param);
if (param->right != NULL)
{
send ("|");
send_exclusive_or_expr (param->right);
}
}
}

void CmmProduct::send_logical_and_expr (CmmExpr * param)
{
send_inclusive_or_expr (param);
if (param->kind == logAndExp)
{
send ("&&");
send_inclusive_or_expr (param->right);
}
}
}

void CmmProduct::send_logical_or_expr (CmmExpr * param)
{
send_logical_and_expr (param);
if (param->kind == logOrExp)
{
send ("||");
send_logical_and_expr (param->right);
}
}
}

void CmmProduct::send_assignment_expr (CmmExpr * param)
{
send_logical_or_expr (param);
if (param->right != NULL)
{
if (param->kind == assignExp)
{
send ("=");
}
else if (param->kind == addAssignExp)
{
send ("+=");
}
else if (param->kind == subAssignExp)
{
send ("-=");
}
else if (param->kind == mulAssignExp)
{
send ("*=");
}
else if (param->kind == divAssignExp)
{
send ("/=");
}
else if (param->kind == modAssignExp)
{
send ("%=");
}
else if (param->kind == shlAssignExp)
{
send ("<<=");
}
else if (param->kind == shrAssignExp)
{
send (">>=");
}
else if (param->kind == andAssignExp)
{
send ("&=");
}
else if (param->kind == xorAssignExp)
{
send ("^=");
}
else if (param->kind == orAssignExp)
{
send ("|=");
}
else if (param->kind == condExp)
{
send ("?");
send_assignment_expr (param->middle);
send (":");
}
send_logical_or_expr (param->right);
}
}
}

void CmmProduct::send_colon_expr (CmmExpr * param)
{
send_assignment_expr (param);
if (param->kind == colonExp)
{
send (":");
send_assignment_expr (param->right);
}
}
}

void CmmProduct::send_comma_expr (CmmExpr * param)
{
send_colon_expr (param);
if (param->kind == commaExp)
{
send (",");
send_colon_expr (param->right);
}
}
}

void CmmProduct::send_expr (CmmExpr * param)
{
send_comma_expr (param);
}

void CmmProduct::send_const_expr (CmmExpr * param)
{
send_assignment_expr (param);
}

void CmmProduct::send_stat (CmmStat * param)
{
if (unknown)
{
send_flexible_stat (param);
}
else if (unknown)
{
send_empty_stat (param);
}
else if (unknown)
{
send_compound_stat (param);
}
else if (unknown)
{
send_if_stat (param);
}
else if (unknown)
{
send_while_stat (param);
}
else if (unknown)
{
send_do_stat (param);
}
else if (unknown)
{
send_for_stat (param);
}
else if (unknown)
{
send_switch_stat (param);
}
else if (unknown)
{
send_case_stat (param);
}
else if (unknown)
{
send_default_stat (param);
}
else if (unknown)
{
send_break_stat (param);
}
else if (unknown)
{
send_continue_stat (param);
}
else if (unknown)
{
send_return_stat (param);
}
else if (unknown)
{
send_goto_stat (param);
}
else if (unknown)
{
send_try_stat (param);
}
}

void CmmProduct::send_nested_stat (CmmStat * param)
{
style_indent ();
send_stat (param);
style_unindent ();
}

void CmmProduct::send_empty_stat (CmmEmptyStat * param)
{
send (";");
}

void CmmProduct::send_compound_stat (CmmCompoundStat * param)
{
int inx = 0;
int cnt = param->items.size();
send ("{");
style_indent ();
while (inx < cnt)
{
style_new_line ();
send_stat (param->items [inx]);
inx ++;
}
style_unindent ();
send ("}");
}

void CmmProduct::send_if_stat (CmmIfStat * param)
{
send ("if");
send ("(");
send_expr (param->cond);
send (")");
send_nested_stat (param->then_stat);
if (param->else_stat != NULL)
{
style_new_line ();
send ("else");
send_nested_stat (param->else_stat);
}
}

void CmmProduct::send_while_stat (CmmWhileStat * param)
{
send ("while");
send ("(");
send_expr (param->cond);
send (")");
send_nested_stat (param->body);
}

void CmmProduct::send_do_stat (CmmDoStat * param)
{
send ("do");
send_nested_stat (param->body);
send ("while");
send ("(");
send_expr (param->cond_expr);
send (")");
send (";");
}

void CmmProduct::send_for_stat (CmmForStat * param)
{
send ("for");
send ("(");
if (param->from_expr != NULL)
{
send_condition (param->from_expr);
}
if (param->iter_expr != NULL)
{
send (":");
send_expr (param->iter_expr);
}
else if (unknown)
{
send (";");
if (param->cond_expr != NULL)
{
send_expr (param->cond_expr);
}
send (";");
if (param->step_expr != NULL)
{
send_expr (param->step_expr);
}
}
send (")");
send_nested_stat (param->body);
}

void CmmProduct::send_switch_stat (CmmSwitchStat * param)
{
send ("switch");
send ("(");
send_expr (param->cond);
send (")");
send_nested_stat (param->body);
}

void CmmProduct::send_case_stat (CmmCaseStat * param)
{
send ("case");
send_expr (param->case_expr);
send (":");
style_new_line ();
send_nested_stat (param->body);
}

void CmmProduct::send_default_stat (CmmDefaultStat * param)
{
send ("default");
send (":");
style_new_line ();
send_nested_stat (param->body);
}

void CmmProduct::send_break_stat (CmmBreakStat * param)
{
send ("break");
send (";");
}

void CmmProduct::send_continue_stat (CmmContinueStat * param)
{
send ("continue");
send (";");
}

void CmmProduct::send_return_stat (CmmReturnStat * param)
{
send ("return");
if (param->return_expr != NULL)
{
send_expr (param->return_expr);
}
send (";");
}

void CmmProduct::send_goto_stat (CmmGotoStat * param)
{
send ("goto");
send (param->goto_lab);
send (";");
}

void CmmProduct::send_flexible_stat (CmmStat * param)
{
send_expr (param);
if (unknown)
{
send_simple_stat (param);
}
else if (unknown)
{
send_block_stat (param);
}
else if (unknown)
{
send_continue_simple_declaration (param);
}
}

void CmmProduct::send_simple_stat (CmmSimpleStat * param)
{
send (";");
}

void CmmProduct::send_block_stat (CmmBlockStat * param)
{
style_new_line ();
send_compound_stat (param->body);
}

void CmmProduct::send_decl_specifiers (CmmDeclSpec * param)
{
if (param->a_inline == true || param->a_virtual == true || param->a_explicit == true || param->a_mutable == true || param->a_static == true || param->a_register == true)
{
if (param->a_inline == true)
{
send ("inline");
}
else if (param->a_virtual == true)
{
send ("virtual");
}
else if (param->a_explicit == true)
{
send ("explicit");
}
else if (param->a_mutable == true)
{
send ("mutable");
}
else if (param->a_static == true)
{
send ("static");
}
else if (param->a_register == true)
{
send ("register");
}
}
}

void CmmProduct::send_const_specifier (CmmConstSpecifier * param)
{
send ("const");
send_type_specifiers (param->param);
}

void CmmProduct::send_volatile_specifier (CmmVolatileSpecifier * param)
{
send ("volatile");
send_type_specifiers (param->param);
}

void CmmProduct::send_type_specifier (CmmTypeSpec * param)
{
if (param->a_signed == true || param->a_unsigned == true)
{
if (param->a_signed == true)
{
send ("signed");
}
else if (param->a_unsigned == true)
{
send ("unsigned");
}
}
if (param->a_short == true)
{
send ("short");
}
else if (param->a_long == true)
{
send ("long");
if (param->a_long_long == true || param->a_long_double == true)
{
if (param->a_long_long == true)
{
send ("long");
}
else if (param->a_long_double == true)
{
send ("double");
}
}
}
else if (param->a_bool == true)
{
send ("bool");
}
else if (param->a_char == true)
{
send ("char");
}
else if (param->a_wchar == true)
{
send ("wchar_t");
}
else if (param->a_int == true)
{
send ("int");
}
else if (param->a_float == true)
{
send ("float");
}
else if (param->a_double == true)
{
send ("double");
}
else if (param->a_void == true)
{
send ("void");
}
}

void CmmProduct::send_type_name (CmmTypeName * param)
{
send ("typename");
send_qualified_name (param->qual_name);
}

void CmmProduct::send_type_specifiers (CmmExpr * param)
{
if (unknown)
{
send_ident_expr (param);
}
else if (unknown)
{
send_type_name (param);
}
else if (unknown)
{
send_type_specifier (param);
}
else if (unknown)
{
send_const_specifier (param);
}
else if (unknown)
{
send_volatile_specifier (param);
}
}

void CmmProduct::send_ptr_specifier_list (CmmPtrSpecifierSect * param)
{
int inx = 0;
int cnt = param->items.size();
while (inx < cnt)
{
send_ptr_specifier (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_ptr_specifier (CmmPtrSpecifier * param)
{
if (unknown)
{
send_pointer_specifier (param);
}
else if (unknown)
{
send_reference_specifier (param);
}
}

void CmmProduct::send_pointer_specifier (CmmPointerSpecifier * param)
{
send ("*");
send_cv_specifier_list (param->cv_spec);
}

void CmmProduct::send_reference_specifier (CmmReferenceSpecifier * param)
{
send ("&");
send_cv_specifier_list (param->cv_spec);
}

void CmmProduct::send_cv_specifier_list (CmmCvSpecifier * param)
{
if (param->cv_const == true || param->cv_volatile == true)
{
if (param->cv_const == true)
{
send ("const");
}
else if (param->cv_volatile == true)
{
send ("volatile");
}
}
}

void CmmProduct::send_cont_specifier_list (CmmContSpecifierSect * param)
{
int inx = 0;
int cnt = param->items.size();
while (inx < cnt)
{
send_cont_specifier (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_cont_specifier (CmmContSpecifier * param)
{
if (unknown)
{
send_array_specifier (param);
}
else if (unknown)
{
send_function_specifier (param);
}
}

void CmmProduct::send_array_specifier (CmmArraySpecifier * param)
{
send ("[");
if (param->lim != NULL)
{
send_expr (param->lim);
}
send ("]");
}

void CmmProduct::send_function_specifier (CmmFunctionSpecifier * param)
{
send ("(");
send_parameter_declaration_list (param->parameters);
send (")");
if (param->a_const == true)
{
send ("const");
}
if (param->exception_spec != NULL)
{
send_exception_specification (param->exception_spec);
}
}

void CmmProduct::send_parameter_declaration_list (CmmStatSect * param)
{
int inx = 0;
int cnt = param->items.size();
if (inx < cnt)
{
send_parameter_declaration (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_parameter_declaration (param->items [inx]);
inx ++;
}
}
}

void CmmProduct::send_parameter_declaration (CmmStat * param)
{
if (unknown)
{
send_assignment_expr (param);
if (unknown)
{
send_value_parameter (param);
}
else if (unknown)
{
send_plain_parameter (param);
}
}
else if (unknown)
{
send_dots_parameter (param);
}
}

void CmmProduct::send_value_parameter (CmmSimpleStat * param)
{
}

void CmmProduct::send_plain_parameter (CmmSimpleDecl * param)
{
int inx = 0;
int cnt = param->items.size();
send_parameter_item (param->items [inx]);
inx ++;
}

void CmmProduct::send_parameter_item (CmmSimpleItem * param)
{
send_declarator (param->declarator);
if (param->init != NULL)
{
send_initializer (param->init);
}
send_attr_list (param->attr);
}

void CmmProduct::send_dots_parameter (CmmDotsParam * param)
{
send ("...");
}

void CmmProduct::send_declarator (CmmDeclarator * param)
{
send_ptr_specifier_list (param);
if (unknown)
{
send_basic_declarator (param);
}
else if (unknown)
{
send_empty_declarator (param);
}
}

void CmmProduct::send_basic_declarator (CmmBasicDeclarator * param)
{
send_qualified_name (param->qual_name);
send_cont_specifier_list (param->cont_spec);
}

void CmmProduct::send_empty_declarator (CmmEmptyDeclarator * param)
{
send_cont_specifier_list (param->cont_spec);
}

void CmmProduct::send_nested_declarator (CmmNestedDeclarator * param)
{
send ("(");
send_declarator (param->inner_declarator);
send (")");
send_cont_specifier_list (param->cont_spec);
}

void CmmProduct::send_typedef_declarator (CmmDeclarator * param)
{
send_ptr_specifier_list (param);
if (unknown)
{
send_basic_declarator (param);
}
else if (unknown)
{
send_nested_declarator (param);
}
}

void CmmProduct::send_type_id (CmmTypeId * param)
{
send_type_specifiers (param->type_spec);
send_declarator (param->declarator);
}

void CmmProduct::send_initializer (CmmInitializer * param)
{
send ("=");
send_initializer_item (param->value);
}

void CmmProduct::send_initializer_item (CmmInitItem * param)
{
if (unknown)
{
send_simple_initializer (param);
}
else if (unknown)
{
send_initializer_list (param);
}
}

void CmmProduct::send_simple_initializer (CmmInitSimple * param)
{
send_assignment_expr (param->inner_expr);
}

void CmmProduct::send_initializer_list (CmmInitList * param)
{
int inx = 0;
int cnt = param->items.size();
send ("{");
style_indent ();
if (inx < cnt)
{
send_initializer_item (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
style_new_line ();
send_initializer_item (param->items [inx]);
inx ++;
}
}
style_unindent ();
send ("}");
}

void CmmProduct::send_attr_item (CmmAttr * param)
{
send_assignment_expr (param->attr_expr);
}

void CmmProduct::send_attr_group (CmmAttrGroup * param)
{
int inx = 0;
int cnt = param->items.size();
send ("[[");
send_attr_item (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_attr_item (param->items [inx]);
inx ++;
}
send ("]]");
}

void CmmProduct::send_attr_list (CmmAttrSect * param)
{
int inx = 0;
int cnt = param->items.size();
while (inx < cnt)
{
send_attr_group (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_simple_declaration (CmmSimpleDecl * param)
{
if (param->decl_spec != NULL)
{
send_decl_specifiers (param->decl_spec);
}
if (param->a_destructor == true)
{
send ("~");
}
send_type_specifiers (param->type_spec);
send_modify_simple_declaration (param);
}

void CmmProduct::send_continue_simple_declaration (CmmSimpleDecl * param)
{
send_modify_simple_declaration (param);
}

void CmmProduct::send_modify_simple_declaration (CmmSimpleDecl * param)
{
int inx = 0;
int cnt = param->items.size();
send_simple_item (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_simple_item (param->items [inx]);
inx ++;
}
if (param->body != NULL)
{
if (param->ctor_init != NULL)
{
send (":");
style_new_line ();
send_ctor_initializer (param->ctor_init);
}
style_new_line ();
send_compound_stat (param->body);
}
else if (unknown)
{
send (";");
}
}

void CmmProduct::send_simple_item (CmmSimpleItem * param)
{
send_declarator (param->declarator);
if (param->width != NULL)
{
send (":");
send_const_expr (param->width);
}
if (param->init != NULL)
{
send_initializer (param->init);
}
send_attr_list (param->attr);
}

void CmmProduct::send_condition (CmmStat * param)
{
send_expr (param);
if (unknown)
{
send_condition_value (param);
}
else if (unknown)
{
send_condition_declaration (param);
}
}

void CmmProduct::send_condition_declaration (CmmSimpleDecl * param)
{
int inx = 0;
int cnt = param->items.size();
send_simple_item (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_simple_item (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_condition_value (CmmSimpleStat * param)
{
}

void CmmProduct::send_declaration_list (CmmStatSect * param)
{
int inx = 0;
int cnt = param->items.size();
while (inx < cnt)
{
send_declaration (param->items [inx]);
inx ++;
style_new_line ();
}
}

void CmmProduct::send_declaration (CmmStat * param)
{
if (unknown)
{
send_flexible_stat (param);
}
else if (unknown)
{
send_empty_stat (param);
}
else if (unknown)
{
send_compound_stat (param);
}
else if (unknown)
{
send_if_stat (param);
}
else if (unknown)
{
send_while_stat (param);
}
else if (unknown)
{
send_for_stat (param);
}
else if (unknown)
{
send_class_declaration (param);
}
else if (unknown)
{
send_enum_declaration (param);
}
else if (unknown)
{
send_typedef_declaration (param);
}
else if (unknown)
{
send_friend_declaration (param);
}
else if (unknown)
{
send_namespace_declaration (param);
}
else if (unknown)
{
send_extern_declaration (param);
}
else if (unknown)
{
send_using_declaration (param);
}
else if (unknown)
{
send_template_declaration (param);
}
}

void CmmProduct::send_namespace_declaration (CmmNamespaceDecl * param)
{
send ("namespace");
send_simple_name (param->simp_name);
send_attr_list (param->attr);
style_new_line ();
send ("{");
style_new_line ();
style_indent ();
send_declaration_list (param->body);
style_unindent ();
send ("}");
}

void CmmProduct::send_using_declaration (CmmUsingDecl * param)
{
send ("using");
if (param->a_namespace == true)
{
send ("namespace");
}
send_qualified_name (param->qual_name);
send (";");
}

void CmmProduct::send_extern_declaration (CmmExternDecl * param)
{
send ("extern");
if (param->language != "")
{
putString (param->language);
}
if (param->decl_list != NULL)
{
send ("{");
send_declaration_list (param->decl_list);
send ("}");
}
else if (param->inner_declaration != NULL)
{
send_declaration (param->inner_declaration);
}
}

void CmmProduct::send_typedef_declaration (CmmTypedefDecl * param)
{
send ("typedef");
send_type_specifiers (param->type_spec);
send_typedef_declarator (param->declarator);
send (";");
}

void CmmProduct::send_friend_declaration (CmmFriendDecl * param)
{
send ("friend");
send_declaration (param->inner_declaration);
}

void CmmProduct::send_enum_declaration (CmmEnumDecl * param)
{
send ("enum");
send_simple_name (param->simp_name);
if (param->enum_items != NULL)
{
style_new_line ();
send ("{");
style_indent ();
send_enum_list (param->enum_items);
style_unindent ();
style_new_line ();
send ("}");
}
send (";");
}

void CmmProduct::send_enum_list (CmmEnumSect * param)
{
int inx = 0;
int cnt = param->items.size();
if (inx < cnt)
{
send_enumerator (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_enumerator (param->items [inx]);
inx ++;
}
}
}

void CmmProduct::send_enumerator (CmmEnumItem * param)
{
style_new_line ();
send_simple_name (param->simp_name);
if (param->init_value != NULL)
{
send ("=");
send_const_expr (param->init_value);
}
}

void CmmProduct::send_class_declaration (CmmClassDecl * param)
{
if (param->style == ClassStyle)
{
send ("class");
}
else if (param->style == StructStyle)
{
send ("struct");
}
else if (param->style == UnionStyle)
{
send ("union");
}
send_simple_name (param->simp_name);
send_attr_list (param->attr);
if (param->members != NULL)
{
if (param->base_list != NULL)
{
send (":");
send_base_specifier_list (param->base_list);
}
style_new_line ();
send ("{");
style_indent ();
send_member_list (param->members);
style_unindent ();
send ("}");
}
send (";");
}

void CmmProduct::send_member_visibility (CmmMemberVisibility * param)
{
if (param->access == PrivateAccess)
{
send ("private");
}
else if (param->access == ProtectedAccess)
{
send ("protected");
}
else if (param->access == PublicAccess)
{
send ("public");
}
style_no_space ();
send (":");
}

void CmmProduct::send_member_item (CmmStat * param)
{
if (unknown)
{
send_member_visibility (param);
}
else if (unknown)
{
send_flexible_stat (param);
}
else if (unknown)
{
send_empty_stat (param);
}
else if (unknown)
{
send_compound_stat (param);
}
else if (unknown)
{
send_if_stat (param);
}
else if (unknown)
{
send_while_stat (param);
}
else if (unknown)
{
send_for_stat (param);
}
else if (unknown)
{
send_class_declaration (param);
}
else if (unknown)
{
send_enum_declaration (param);
}
else if (unknown)
{
send_typedef_declaration (param);
}
else if (unknown)
{
send_friend_declaration (param);
}
else if (unknown)
{
send_using_declaration (param);
}
else if (unknown)
{
send_template_declaration (param);
}
}

void CmmProduct::send_member_list (CmmStatSect * param)
{
int inx = 0;
int cnt = param->items.size();
while (inx < cnt)
{
send_member_item (param->items [inx]);
inx ++;
style_new_line ();
}
}

void CmmProduct::send_base_specifier_list (CmmBaseSect * param)
{
int inx = 0;
int cnt = param->items.size();
send_base_specifier (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_base_specifier (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_base_specifier (CmmBaseItem * param)
{
if (param->a_virtual == true)
{
send ("virtual");
}
if (param->access == PrivateBase)
{
send ("private");
}
else if (param->access == ProtectedBase)
{
send ("protected");
}
else if (param->access == PublicBase)
{
send ("public");
}
else if (param->access == UnknownBase)
{
}
send_qualified_name (param->from_cls);
}

void CmmProduct::send_ctor_initializer (CmmCtorInitializer * param)
{
int inx = 0;
int cnt = param->items.size();
style_indent ();
send_member_initializer (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
style_new_line ();
send_member_initializer (param->items [inx]);
inx ++;
}
style_unindent ();
}

void CmmProduct::send_member_initializer (CmmMemberInitializer * param)
{
send_simple_name (param->simp_name);
send ("(");
send_expr_list (param->params);
send (")");
}

void CmmProduct::send_conversion_specifiers (CmmConvSpec * param)
{
send_type_specifiers (param->type_spec);
send_ptr_specifier_list (param->ptr_spec);
}

void CmmProduct::send_special_function (CmmSpecialFuction * param)
{
send ("operator");
if (param->conv != NULL)
{
send_conversion_specifiers (param->conv);
}
else if (param->spec_new == true)
{
send ("new");
if (param->spec_new_array == true)
{
send ("[");
send ("]");
}
}
else if (param->spec_delete == true)
{
send ("delete");
if (param->spec_delete_array == true)
{
send ("[");
send ("]");
}
}
else if (param->spec_add == true)
{
send ("+");
}
else if (param->spec_sub == true)
{
send ("-");
}
else if (param->spec_mul == true)
{
send ("*");
}
else if (param->spec_div == true)
{
send ("/");
}
else if (param->spec_mod == true)
{
send ("%");
}
else if (param->spec_xor == true)
{
send ("^");
}
else if (param->spec_and == true)
{
send ("&");
}
else if (param->spec_or == true)
{
send ("|");
}
else if (param->spec_not == true)
{
send ("~");
}
else if (param->spec_log_not == true)
{
send ("!");
}
else if (param->spec_assign == true)
{
send ("=");
}
else if (param->spec_lt == true)
{
send ("<");
}
else if (param->spec_gt == true)
{
send (">");
}
else if (param->spec_add_assign == true)
{
send ("+=");
}
else if (param->spec_sub_assign == true)
{
send ("-=");
}
else if (param->spec_mul_assign == true)
{
send ("*=");
}
else if (param->spec_div_assign == true)
{
send ("/=");
}
else if (param->spec_mod_assign == true)
{
send ("%=");
}
else if (param->spec_xor_assign == true)
{
send ("^=");
}
else if (param->spec_and_assign == true)
{
send ("&=");
}
else if (param->spec_or_assign == true)
{
send ("|=");
}
else if (param->spec_shl == true)
{
send ("<<");
}
else if (param->spec_shr == true)
{
send (">>");
}
else if (param->spec_shl_assign == true)
{
send (">>=");
}
else if (param->spec_shr_assign == true)
{
send ("<<=");
}
else if (param->spec_eq == true)
{
send ("==");
}
else if (param->spec_ne == true)
{
send ("!=");
}
else if (param->spec_le == true)
{
send ("<=");
}
else if (param->spec_ge == true)
{
send (">=");
}
else if (param->spec_log_and == true)
{
send ("&&");
}
else if (param->spec_log_or == true)
{
send ("||");
}
else if (param->spec_inc == true)
{
send ("++");
}
else if (param->spec_dec == true)
{
send ("--");
}
else if (param->spec_comma == true)
{
send (",");
}
else if (param->spec_member_deref == true)
{
send ("->*");
}
else if (param->spec_deref == true)
{
send ("->");
}
else if (param->spec_call == true)
{
send ("(");
send (")");
}
else if (param->spec_index == true)
{
send ("[");
send ("]");
}
}

void CmmProduct::send_template_declaration (CmmTemplateDecl * param)
{
send ("template");
if (param->params != NULL)
{
send ("<");
send_template_param_list (param->params);
send (">");
}
send_declaration (param->inner_declaration);
}

void CmmProduct::send_template_param_list (CmmTemplateParamSect * param)
{
int inx = 0;
int cnt = param->items.size();
if (inx < cnt)
{
send_template_param (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_template_param (param->items [inx]);
inx ++;
}
}
}

void CmmProduct::send_template_param (CmmTemplateParam * param)
{
if (unknown)
{
send_template_type_param (param);
}
else if (unknown)
{
// !? send_template_normal_param (param);
}
}

void CmmProduct::send_template_type_param (CmmTemplateTypeParam * param)
{
if (param->params != NULL)
{
send ("template");
send ("<");
send_template_param_list (param->params);
send (">");
}
if (param->a_class == true)
{
send ("class");
}
else if (param->a_typename == true)
{
send ("typename");
}
if (param->simp_name != NULL)
{
send_simple_name (param->simp_name);
}
}

void CmmProduct::send_template_normal_param (CmmTemplateNormalParam * param)
{
send_type_specifiers (param->type_spec);
send_declarator (param->declarator);
if (param->init != NULL)
{
send ("=");
send_shift_expr (param->init);
}
}

void CmmProduct::send_template_arg_list (CmmTemplateArgSect * param)
{
int inx = 0;
int cnt = param->items.size();
send ("<");
if (inx < cnt)
{
send_template_arg (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_template_arg (param->items [inx]);
inx ++;
}
}
send (">");
}

void CmmProduct::send_template_arg (CmmTemplateArg * param)
{
send_parameter_declaration (param->value);
}

void CmmProduct::send_try_stat (CmmTryStat * param)
{
send ("try");
style_new_line ();
send_compound_stat (param->body);
send_handler_list (param->handlers);
}

void CmmProduct::send_handler_list (CmmHandlerSect * param)
{
int inx = 0;
int cnt = param->items.size();
send_handler (param->items [inx]);
inx ++;
while (inx < cnt)
{
send_handler (param->items [inx]);
inx ++;
}
}

void CmmProduct::send_handler (CmmHandlerItem * param)
{
style_new_line ();
send ("catch");
send ("(");
if (param->type_spec != NULL)
{
send_type_specifiers (param->type_spec);
send_declarator (param->declarator);
}
else if (param->dots == true)
{
send ("...");
}
send (")");
style_new_line ();
send_compound_stat (param->body);
}

void CmmProduct::send_throw_expr (CmmThrowExpr * param)
{
send ("throw");
if (param->inner_expr != NULL)
{
send_assignment_expr (param->inner_expr);
}
}

void CmmProduct::send_exception_specification (CmmExceptionSect * param)
{
int inx = 0;
int cnt = param->items.size();
send ("throw");
send ("(");
if (inx < cnt)
{
send_exception_specification_item (param->items [inx]);
inx ++;
while (inx < cnt)
{
send (",");
send_exception_specification_item (param->items [inx]);
inx ++;
}
}
send (")");
}

void CmmProduct::send_exception_specification_item (CmmExceptionItem * param)
{
send_type_id (param->type);
}
