
/* trace.h */

#ifndef TRACE_H
#define TRACE_H

/* ---------------------------------------------------------------------- */

void set_error_handlers ();
// throw exception when some signal happens

void trace ();

/* ---------------------------------------------------------------------- */

#endif /* TRACE_H */
