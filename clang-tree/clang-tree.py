#!/usr/bin/env python

from __future__ import print_function

import os, sys

try :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
   use_new_api = True
except :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *
   use_new_api = False

# sys.path.append (os.path.expanduser ("/abc/llvm-python"))
# directory with clang/cindex.py

# import clang.cindex
from clang.cindex import Index, Config, Cursor, CursorKind

# clang.cindex.Config.set_library_path ("/usr/lib/llvm")
# clang.cindex.Config.set_library_file ("/usr/lib/llvm/libclang.so")

def columnNumber (cursor) :
    return cursor.columnNumber ()
    # return cursor.positionInBlock ()

# --------------------------------------------------------------------------

def findIcon (icon_name):
    icon = QIcon.fromTheme (icon_name)
    return icon

class ItemData :
   def __init__ (self, above) :
       super (ItemData, self).__init__ ()

       self.text = ""
       self.icon = None
       self.ink = None

       self.display = False # True => self.cursor is valid
       self.cursor = None

       self.display2 = False # True => self.cursor is valid
       self.cursor2 = None

       self.location = None
       self.start = None
       self.stop = None

       self.subitems = [ ]

       self.up = above

       if above != None :
          above.subitems.append (self)

   def addIcon (self, icon_name) :
       self.icon = findIcon (icon_name)

   def setup (self, cursor) :
       self.cursor = cursor
       self.display = True
       self.cursor2 = cursor
       self.display2 = True

   def clean (self) :
       self.display = False
       self.cursor = None

   def get_subitems (self) :
       if self.display :
          add_subitems (self)
          self.clean ()

# --------------------------------------------------------------------------

def add_subitems (item : ItemData) :
    for t in item.cursor.get_children () :
        new_item (item, t)

def new_item (above : ItemData, cursor : Cursor) :
    txt = str (cursor.spelling)
    txt = str (cursor.kind).replace ("CursorKind.", "", 1) + ": " + txt

    item = ItemData (above)
    item.text = txt
    item.setup (cursor)

    if (cursor.kind == CursorKind.CLASS_DECL or
        cursor.kind == CursorKind.STRUCT_DECL or
        cursor.kind == CursorKind.UNION_DECL) :
            item.addIcon ("code-class")
    elif (cursor.kind == CursorKind.FUNCTION_DECL or
          cursor.kind == CursorKind.CXX_METHOD or
          cursor.kind == CursorKind.CONSTRUCTOR or
          cursor.kind == CursorKind.DESTRUCTOR) :
             item.addIcon ("code-function")
    elif (cursor.kind == CursorKind.VAR_DECL or
          cursor.kind == CursorKind.PARM_DECL or
          cursor.kind == CursorKind.FIELD_DECL) :
             item.addIcon ("code-variable")

    item.location = cursor.location
    item.start = cursor.extent.start
    item.stop = cursor.extent.end

    start = item.start
    stop = item.stop

    if hasattr (start, "file") and hasattr (start.file, "name"):
       pass
    else :
       start = item.location

    if hasattr (start, "file") and hasattr (start.file, "name"):
       item.fileName = start.file.name
       item.line = start.line
       item.column = start.column
       if item.stop.file != None and item.fileName == stop.file.name :
          item.stop_line = stop.line
          item.stop_column = stop.column
       else :
          item.end_line = -1
          item.end_column = -1

# --------------------------------------------------------------------------

class TreeModel (QAbstractItemModel) :

   def __init__ (self, parent, root) :
       super (TreeModel, self).__init__ (parent)
       self.root = root

   def columnCount (self, parent) :
       return 1

   def data (self, index, role) :
       result = QVariant ()
       if index.isValid () :
          if role == Qt.DisplayRole :
             item = index.internalPointer ()
             result = item.text
          if role == Qt.DecorationRole :
             item = index.internalPointer ()
             if item.icon != None :
                result = item.icon
          if role == Qt.ForegroundRole :
             item = index.internalPointer ()
             if item.ink != None :
                result = item.ink
       return result

   def index (self, line, column, parent) :
       result = QModelIndex ()
       if self.hasIndex (line, column, parent) :
          parentItem = None
          if not parent.isValid () :
             parentItem = self.root
          else :
             parentItem = parent.internalPointer ()
          childItem = None
          if parentItem != None :
             parentItem.get_subitems ()
             if line < len (parentItem.subitems) :
                childItem = parentItem.subitems [line]
          if childItem != None :
             result = self.createIndex (line, column, childItem)
       return result

   def parent (self, index) :
       result = QModelIndex ()
       if index.isValid () :
          childItem = index.internalPointer ()
          parentItem = childItem.up
          if parentItem != None and parentItem != self.root :
             parentItem.get_subitems ()
             pos = parentItem.subitems.index (childItem)
             result = self.createIndex (pos, 0, parentItem)
       return result

   def rowCount (self, parent) :
       parentItem = None
       if not parent.isValid () :
          parentItem = self.root
       else :
          parentItem = parent.internalPointer ()
       cnt = 0
       if parentItem != None :
          parentItem.get_subitems ()
          cnt = len (parentItem.subitems)
       return cnt

# --------------------------------------------------------------------------

def add_property (prop, name, value) :
    node = QTreeWidgetItem (prop)
    node.setText (0, name)
    node.setText (1, str (value))
    prop.addTopLevelItem (node)

def show_properties (prop, item) :
    prop.clear ()
    if item.display2 :
       obj = item.cursor2
       for name in dir (obj) :
           try :
              add_property (prop, name, getattr (obj, name))
           except :
              pass

# --------------------------------------------------------------------------

class CompletionTextEdit (QPlainTextEdit) :

   def __init__ (self, parent = None) :
       super (CompletionTextEdit, self).__init__ (parent)

       self.win = parent
       self.completer = None

       self.minLength = 1
       self.automatic_completion = False
       # self.automatic_completion == False ... popup is displayed only when Ctrl+Space is pressed

       localList = [ ]
       localCompleter = QCompleter (localList, self)
       self.setCompleter (localCompleter, filtered = True)

   def setCompleter (self, completer, filtered = False) :
       if self.completer :
           self.completer.activated.disconnect (self.insertCompletion)
       if completer == None :
           return

       completer.setWidget (self)

       if filtered :
          completer.setCompletionMode (QCompleter.PopupCompletion)
       else :
          completer.setCompletionMode (QCompleter.UnfilteredPopupCompletion)

       completer.setCaseSensitivity (Qt.CaseInsensitive)

       self.completer = completer
       # self.connect (self.completer, SIGNAL ("activated(const QString&)"), self.insertCompletion)
       self.completer.activated.connect (self.insertCompletion)

   def insertCompletion (self, completion) :
       if self.completer.widget() == self :
          tc = self.textCursor ()
          if use_new_api :
             extra = len (completion) - len (self.completer.completionPrefix())
          else :
             extra = completion.length() - self.completer.completionPrefix().length()
          if extra != 0 :
             tc.movePosition (QTextCursor.Left)
             tc.movePosition (QTextCursor.EndOfWord)
          if use_new_api :
             tc.insertText (completion [-extra : ])
          else :
             tc.insertText (completion.right (extra))
          self.setTextCursor (tc)

   def textUnderCursor (self) :
       tc = self.textCursor ()
       tc.select (QTextCursor.WordUnderCursor)
       return tc.selectedText ()

   def focusInEvent (self, event) :
       if self.completer :
           self.completer.setWidget (self);
       QPlainTextEdit.focusInEvent (self, event)

   def focusOutEvent (self, event) :
       QPlainTextEdit.focusOutEvent (self, event)

   def keyPressEvent (self, event) :
       if self.completer != None and self.completer.popup().isVisible() :
           if event.key () in (
              Qt.Key_Enter,
              Qt.Key_Return,
              Qt.Key_Escape,
              Qt.Key_Tab,
              Qt.Key_Backtab) :
                 event.ignore ()
                 return

       mask = Qt.ShiftModifier | Qt.ControlModifier | Qt.AltModifier | Qt.MetaModifier
       mod = event.modifiers () & mask

       isShortcut = mod == Qt.ControlModifier and event.key () == Qt.Key_Space # Ctrl + Space

       if (self.completer == None or not isShortcut) :
           QPlainTextEdit.keyPressEvent (self, event) # do not process the shortcut when we have a completer
           if not self.automatic_completion :
              return

       ctrlOrShift = mod in (Qt.ControlModifier, Qt.ShiftModifier)
       if self.completer == None or (ctrlOrShift and event.text () == "") : # unicode has not isEmpty method
           # ctrl or shift key on it's own
           return

       eow = "~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-=" # end of word
       if not use_new_api :
          eow = QString (eow)

       hasModifier = ((event.modifiers () != Qt.NoModifier) and not ctrlOrShift)

       completionList = self.getCompletionList ()
       model = QStandardItemModel (self.completer)
       for obj in completionList :
          if isinstance (obj, str) :
             item = QStandardItem (obj)
          else :
             item = obj
          model.appendRow (item)
       self.completer.setModel (model)

       completionPrefix = self.textUnderCursor ()
       # print ("completion", completionPrefix, "," , completionList)

       if use_new_api :
          if (not isShortcut
              and (hasModifier or
                   event.text () == "" or
                   len (completionPrefix) < self.minLength or
                   event.text()[-1] in eow )) :
              self.completer.popup ().hide ()
              # print ("hide completer")
              return
       else :
          if (not isShortcut
              and (hasModifier or
                   event.text ().isEmpty () or
                   completionPrefix.length () < self.minLength or
                   eow.contains (event.text().right(1)) )) :
              self.completer.popup ().hide ()
              # print ("hide completer")
              return

       if (completionPrefix != self.completer.completionPrefix ()) :
           self.completer.setCompletionPrefix (completionPrefix)
           popup = self.completer.popup ()
           popup.setCurrentIndex (self.completer.completionModel ().index (0,0))

       cr = self.cursorRect ()
       cr.setWidth (self.completer.popup ().sizeHintForColumn (0) +
                    self.completer.popup ().verticalScrollBar ().sizeHint ().width ())
       self.completer.complete (cr) # popup it

   def createModelItem (self, obj) :
       # see setupTreeItem in tree.py

       item = QStandardItem (obj.item_name)
       item.setToolTip (obj.item_qual)

       if hasattr (obj, "item_icon") :
          if isinstance (obj.item_icon, str) :
             item.setIcon (findIcon (obj.item_icon))
          elif isinstance (obj.item_icon, QIcon) :
             item.setIcon (obj.item_icon)

       if getattr (obj, "item_tooltip", None) != None :
          item.setToolTip (obj.item_tooltip)

       if hasattr (obj, "item_ink") :
          if isinstance (obj.item_ink, str) :
             item.setForeground (findColor (obj.item_ink))
          elif obj.item_ink != None :
             item.setForeground (obj.item_ink)

       if hasattr (obj, "item_paper") :
          if isinstance (obj.item_paper, str) :
             item.setBackground (findColor (obj.item_paper))
          elif obj.item_paper != None :
             item.setBackground (obj.item_paper)

       # column = QStandardItem ("type")
       # item.appendColumn ([column])

       return item

   # def getCompletionList (self) :
   #     return [ "alpha", "beta", "gamma" ]

   def getCompletionList (self) :
       result = [ ]

       fileName = ""
       for edit_name in self.win.editors :
           if self.win.editors [edit_name] == self :
              fileName = edit_name

       tu = self.win.translation_unit
       if tu != None :
          # print ("using CLang completion")
          tu.reparse ()
          cursor = self.textCursor ()
          # fileName = self.getFileName ()
          line = cursor.blockNumber () + 1
          column = columnNumber (cursor) + 1
          source = self.toPlainText ()
          answer = tu.codeComplete (fileName, line, column,
                      unsaved_files = [ (fileName, source) ],
                      include_macros = True,
                      include_code_patterns = True,
                      include_brief_comments = True)

          for t in answer.results :
             print (t)
             item = QStandardItem (str (t))
             result.append (item)

       return result

# http://doc.qt.io/qt-5/qtwidgets-tools-customcompleter-example.html
# http://rowinggolfer.blogspot.cz/2010/08/qtextedit-with-autocompletion-using.html

# --------------------------------------------------------------------------

class MainWin (QMainWindow):

    def __init__ (self, parent=None):
        QMainWindow.__init__ (self, parent)
        self.translation_unit = None
        self.translation_index = None
        self.source_model = None
        self.filter_model = None
        self.setupUI ()
        self.setupMenus ()
        self.setupConnections ()

    def setupUI (self) :
        self.setStatusBar (QStatusBar ())
        self.resize (640, 480);

        self.vsplit = QSplitter ()
        self.vsplit.setOrientation (Qt.Vertical)
        self.setCentralWidget (self.vsplit)

        self.hsplit = QSplitter ()
        self.vsplit.addWidget (self.hsplit)

        self.filterLine = QLineEdit ()
        self.treeView = QTreeView ()

        self.leftLayout = QVBoxLayout ()
        self.leftLayout.addWidget (self.filterLine)
        self.leftLayout.addWidget (self.treeView)

        self.leftBox = QWidget ()
        self.leftBox.setLayout (self.leftLayout)
        self.hsplit.addWidget (self.leftBox)

        self.editTabs = QTabWidget ()
        self.editTabs.tabBar().setFocusPolicy (Qt.NoFocus)
        self.editTabs.setTabPosition (QTabWidget.South)
        self.hsplit.addWidget (self.editTabs)

        self.prop = QTreeWidget ()
        self.prop.setHeaderLabels (["name", "value"])
        self.hsplit.addWidget (self.prop)

        self.info = QTextEdit ()
        self.vsplit.addWidget (self.info)

        self.vsplit.setStretchFactor (0, 3)
        self.vsplit.setStretchFactor (1, 1)

        self.emptyEdit = QTextEdit ()
        self.editTabs.addTab (self.emptyEdit, "empty")
        self.hsplit.setStretchFactor (0, 2)
        self.hsplit.setStretchFactor (1, 3)
        self.hsplit.setStretchFactor (2, 1)

        self.editors = { }


    def action (self, name, icon_image = None, shortcut = None ) :
        title = str (name).title () # upper case first character
        act = QAction ("&" + title, self)
        if icon_image != None:
           act.setIcon (icon_image)
        if shortcut != None :
           act.setShortcut (shortcut)
        return act

    def setupMenus (self):

        fileMenu = self.menuBar().addMenu ("&File")

        # act = self.action ("Open...")
        # self.connect (act, click, self.openFile)
        # fileMenu.addAction (act)

        act = self.action ("Quit", shortcut = "Ctrl+Q")
        act.triggered.connect (self.close)
        fileMenu.addAction (act)

        editMenu = self.menuBar().addMenu ("&Edit")

        # act = self.action ("Complete", shortcut = "Ctrl+Space")
        act = self.action ("Complete", shortcut = "F9")
        act.triggered.connect (self.complete)
        editMenu.addAction (act)

    def setupConnections (self):

        self.treeView.clicked.connect (self.treeView_clicked)
        self.filterLine.textChanged.connect (self.filterLine_textChanged)

    def complete (self):
        if self.translation_unit != None :

           fileName = ""
           edit = self.editTabs.currentWidget ()
           for name in self.editors :
               if self.editors [name] == edit :
                  fileName = name

           cursor = edit.textCursor ()
           line = cursor.blockNumber () + 1
           column = cursor.positionInBlock () + 1

           print (fileName, str (line), str (column))

           answer = self.translation_unit.codeComplete (fileName, line, column,
                                                        unsaved_files=None,
                                                        include_macros=False,
                                                        include_code_patterns=False,
                                                        include_brief_comments=False)
           if answer != None :
              menu = QMenu (self);
              for t in answer.results :
                 print (t)
                 for u in t.string :
                   if u.isKindTypedText () :
                      action = QAction (u.spelling, menu)
                      menu.addAction (action)
              # menu.exec_ (QCursor.pos())

    def treeView_clicked (self, index) :
        item = self.filter_model.mapToSource (index).internalPointer ()
        txt = ""
        if hasattr (item, "fileName") :
           txt = item.text + ": " + item.fileName + ", line: " + str (item.line) + ", column: " + str (item.column)
           self.openSource (item.fileName, item.line, item.column, item.stop_line, item.stop_column)
        show_properties (self.prop, item)
        self.statusBar().showMessage (txt)

    def filterLine_textChanged (self, text) :
        if self.filter_model != None :
           self.filter_model.setFilterFixedString (text)

    def openSource (self, fileName, line = -1, col = -1, stop_line = -1, stop_col = -1):
        if fileName not in self.editors :

           # view = QTextEdit ()
           # view.setLineWrapMode (QTextEdit.NoWrap)
           view = CompletionTextEdit (self)
           view.setLineWrapMode (QPlainTextEdit.NoWrap)

           name = os.path.basename (fileName)
           self.editTabs.addTab (view, name)
           self.editors[fileName] = view

           if self.emptyEdit != None :
              self.editTabs.removeTab (self.editTabs.indexOf(self.emptyEdit))
              self.emptyEdit = None

           try :
              text = open(fileName).read()
              # view.setText (text)
              view.setPlainText (text)
           except :
              pass

        edit = self.editors [fileName]
        self.editTabs.setCurrentWidget (edit)

        if line >= 0 and col >= 0 :
           cursor = edit.textCursor ()

           cursor.movePosition (QTextCursor.Start)
           cursor.movePosition (QTextCursor.Down, QTextCursor.MoveAnchor, line-1)
           cursor.movePosition (QTextCursor.Right, QTextCursor.MoveAnchor, col-1)

           if stop_line >= 0 and stop_col >= 0 :
               if stop_line == line :
                  cursor.movePosition (QTextCursor.Right, QTextCursor.KeepAnchor, stop_col-col)
               else :
                  cursor.movePosition (QTextCursor.Down, QTextCursor.KeepAnchor, stop_line-line)
                  cursor.movePosition (QTextCursor.StartOfLine, QTextCursor.KeepAnchor)
                  cursor.movePosition (QTextCursor.Right, QTextCursor.KeepAnchor, stop_col)

           edit.setTextCursor (cursor)

    # def openFile (self) :
    #     fileName = QFileDialog.getOpenFileName (self, "Open File")
    #     if fileName :
    #        self.translateFile (str (fileName))

    def translate (self, args) :
        for arg in args :
            if os.path.isfile (arg) :
               self.openSource (arg)

        index = Index.create ()
        tu = index.parse (None, args)
        if tu :
           # tu.reparse ()
           self.show_translation_unit (index, tu)

    def show_translation_unit (self, index, tu) :
        top = ItemData (None)
        top.text = "translation unit"
        top.addIcon  ("text-plain");

        # tst = ItemData (top)
        # tst.text = "test item"
        # tst.addIcon ("text-plain");
        # tst.ink = QColor ("blue")

        self.translation_index = index # important, keep index
        self.translation_unit = tu

        top.setup (tu.cursor)
        add_subitems (top)
        top.clean ()

        # self.model = TreeModel (self, top)
        # self.treeView.setModel (self.model)

        self.source_model = TreeModel (self, top)
        self.filter_model = QSortFilterProxyModel (self)
        self.filter_model.setSourceModel (self.source_model)
        self.filter_model.setFilterFixedString (self.filterLine.text ())
        self.treeView.setModel (self.filter_model)

        item = self.treeView.model().index (0, 0, QModelIndex ())
        self.treeView.expand (item)

# --------------------------------------------------------------------------

QIcon.setThemeName ("oxygen")

app = QApplication ([""])

win = MainWin ()
win.show ()
win.translate (sys.argv [ 1 : ])

app.exec_ ()

# --------------------------------------------------------------------------

# Debian 9: apt-get install clang-7 llvm-7 python-clang-7

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
